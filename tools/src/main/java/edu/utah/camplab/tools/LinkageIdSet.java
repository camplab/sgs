package edu.utah.camplab.tools;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import jpsgcs.linkage.LinkageId;
import jpsgcs.linkage.LinkageIndividual;
import jpsgcs.linkage.LinkageInterface;
import jpsgcs.linkage.LinkagePedigreeData;

// A wrapper on the a list of ids
public class LinkageIdSet implements Serializable, Comparable<LinkageIdSet> {

    private static final long serialVersionUID = -6716115767062578643L;
    
    private List<LinkageId<?>> idList = new ArrayList<>();
    private String asString = null;

    public LinkageIdSet(Collection<?> ids) {
        List<String> holder = new ArrayList<String>();
        for (Object oid : ids) {
            if ( oid instanceof String ) {
                holder.add((String)oid);
            }
            else if (oid instanceof Integer ) {
                holder.add(""+oid);
                //idList.add(new LinkageId(oid));
            }
            else if (oid instanceof LinkageId) {
                String tid = ((LinkageId<?>)(oid)).stringGetId();
                holder.add(tid);
                //idList.add((LinkageId)oid);
            }
            else {
                String msg = "Invalid id data type: " + oid.getClass().getName();
                throw new RuntimeException(msg);
            }
        }
        Collections.sort(holder);
        for (String s : holder) {
            idList.add(new LinkageId<String>(s));
        }
        asString = makeCSV();
    }

    public LinkageIdSet(String csv) {
        this(Arrays.asList(csv.split(",")));
    }

    public LinkageIdSet(LinkagePedigreeData lpd) {
        List<String> holder = new ArrayList<String>();
        for (LinkageIndividual indo : lpd.getIndividuals()) {
            if (indo.proband == 1) {
                holder.add(""+indo.id);
            }
        }
        Collections.sort(holder);
        for (String s : holder) {
            idList.add(new LinkageId<>(s));
        }
        asString = makeCSV();
    }
    
    // TODO: push to LinkageInterface or thereabouts.
    public static void resetProbands(LinkageInterface explicitLinkageInterface, LinkageIdSet probandSet) {
        List<LinkageId<?>> pbList = probandSet.asList();
        explicitLinkageInterface.raw().getPedigreeData().resetProbands(pbList);
        explicitLinkageInterface.setProbandsCounted(pbList.size());
    }

    public List<LinkageId<?>> asList() {
        return idList;
    }

    public List<String> asStringList() {
        List<String> slist = new ArrayList<>();
        for (LinkageId<?> lid : idList) {
            slist.add(lid.toString());
        }
	return slist;
    }

    public int size(){
        return idList.size();
    }
        
    public String asFileName() 
    {
	return asString.replace(",", "-");
    }
    
    public String asCSV() 
    {
	return asString;
    }

    public Set<LinkageId<?>> asSet() 
    {
        TreeSet<LinkageId<?>> idset = new TreeSet<>();
        for (LinkageId<?> id : idList) {
            idset.add(id);
        }
	return idset;
    }

    public boolean equals(LinkageIdSet other) {
        if (idList.size() != other.asList().size()) {
            return false;
        }
        return subsumes(other);
    }

    public boolean subsumes(LinkageIdSet other) {
        return idList.containsAll(other.asList());
    }

    @Override
	public int compareTo(LinkageIdSet other) {
        if (other == null) {
            return 1;
        }
        else {
            return asCSV().compareTo(other.asCSV());
        }
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((asString == null) ? 0 : asString.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        LinkageIdSet other = (LinkageIdSet) obj;
        if (asString == null) {
            if (other.asString != null)
                return false;
        } else if (!asString.equals(other.asString))
            return false;
        return true;
    }

    private String makeCSV() {
        StringBuffer idbuff = new StringBuffer();
        String comma = "";
        for (LinkageId<?> lid : idList) {
            idbuff.append(comma + lid.stringGetId());
            comma = ",";
        }
        return idbuff.toString();
    }
}
