package edu.utah.camplab.tools;

import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.GetObjectRequest;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

public class S3FileInputStream {
  private static final AmazonS3 s3Client = AmazonS3ClientBuilder
    .standard()
    .withRegion(Regions.DEFAULT_REGION)
    .withCredentials(new ProfileCredentialsProvider())
    .build();

  private S3FileInputStream() {
    ;
  }

  public static BufferedReader getS3Reader(String url) {
    String[] parts = S3FileInputStream.parseS3URI(url);
    InputStream istr = S3FileInputStream.s3Client.getObject(new GetObjectRequest(parts[0], parts[1])).getObjectContent();
    BufferedReader reader = new BufferedReader(new InputStreamReader(istr));
    return reader;
  }

  private static String[] parseS3URI(String sarg) {
    String retval[] = new String[2];
    int idx = sarg.indexOf('/', 5);
    //int idx = sarg.lastIndexOf('/');
    String bucket = sarg.substring(0, idx);
    String path = sarg.substring(idx + 1);
    retval[0] = bucket;
    retval[1] = path;
    return retval;
  }
}
