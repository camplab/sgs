package edu.utah.camplab.tools;
import java.io.File;

public class FileUtils {
    public static boolean deleteRecursive(File path) {
        if (!path.exists()) {
	    return true;
	}
        boolean ret = true;
        if (path.isDirectory()){
            for (File f : path.listFiles()){
                ret = ret && FileUtils.deleteRecursive(f);
            }
        }
        return ret && path.delete();
    }

    public static File openExistingFile(File filespec, String suffix) {
        File openedFile = filespec;
        if (!openedFile.exists()) {
            String filename = filespec.getAbsolutePath();
            if (!filename.endsWith(suffix)) {
                openedFile = new File(filename + "." + suffix);
                if (!openedFile.exists()) {
                    throw new RuntimeException(String.format("Could not find file for\n\t%s\n\twith or without suffixt = %s", filename, suffix));
                }
            }
        }
        return openedFile;
    }

}
