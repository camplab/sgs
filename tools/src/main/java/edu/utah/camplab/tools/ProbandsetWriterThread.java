package edu.utah.camplab.tools;

import edu.utah.camplab.db.crafted.UUIDComparator;
import jpsgcs.linkage.LinkagePedigreeData;
import org.postgresql.copy.CopyIn;
import org.postgresql.copy.CopyManager;
import org.postgresql.copy.PGCopyOutputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.*;

public class ProbandsetWriterThread extends Thread implements Thread.UncaughtExceptionHandler {
    public static Logger logger = LoggerFactory.getLogger(ProbandsetWriterThread.class);

    private static final String BASE_TABLENAME = "bulk.pbs_";

    private  Connection           connection         =  null;
    private  Set<Set<String>>     powerSubset;
    private  UUID                 peopleId;
    private  Map<String,UUID>     nameUUIDMap        =  null;
    private  String               bulktablename      =  null;
    private  CopyIn               copyIn             =  null;
    private  int                  ordinal            =  0;
    private  boolean              isMeiosesCounting  =  true;
    private  LinkagePedigreeData  pedigreeData;

    public ProbandsetWriterThread(Map<String, UUID> nameIdMap, 
                                  UUID peeper ) {
        nameUUIDMap = nameIdMap;
        peopleId = peeper;
    }

    public Map<String, UUID> getNameUUIDMap() {
        return nameUUIDMap;
    }

    public void setNameUUIDMap(Map<String,UUID> npmap) {
        nameUUIDMap = npmap;
    }

    public UUID getPeopleId() {
        return peopleId;
    }

    public void setPeopleId(UUID id) {
        peopleId = id;
    }
    
    public int getOrdinal() {
        return ordinal;
    }

    public void setOrdinal(int id) {
        ordinal = id;
    }

    public void setIsMeiosesCounting( boolean isCounting) {
        isMeiosesCounting = isCounting;
    }
    
    public boolean getIsMeiosesCounting() {
        return isMeiosesCounting;
    }
    
    public Set<Set<String>> getPowerset() {
        return powerSubset;
    }

    public void setPowerset(Set<Set<String>> pbSets) {
        powerSubset = pbSets;
    }
    
    public Connection getConnection() {
        return connection;
    }

    public void setConnection(Connection c) {
        connection = c;
    }
    
    public CopyIn getCopyIn() {
        return copyIn;
    }

    public void setCopyIn(CopyManager manager) throws SQLException {
        copyIn = manager.copyIn(getCopyStatement());
    }

    private String getCopyStatement() {
        String sql = String.format("COPY %s FROM STDIN WITH CSV NULL '\\N'", getBulkTablename());
        return sql;
    }

    //TODO.  probably should use pbs group name...        
    public String getBulkTablename() {
        return bulktablename;
    }

    public void setBulkTablename(String tname) {
        if (! tname.toLowerCase().startsWith(ProbandsetWriterThread.BASE_TABLENAME)) {
            bulktablename = ProbandsetWriterThread.BASE_TABLENAME + tname.replace("-", "_");
        }
        else {
            bulktablename = tname.replace("-", "_");
        }
    }

    public void prepPeopleInfo(String peopName, UUID peopId) throws SQLException {
        setName("pbs."+peopName+ordinal); //thread name
        setPeopleId(peopId);
    }

    public void run() {
        long timer = System.currentTimeMillis();
        logger.error("running part {}, {}", ordinal, getBulkTablename());
        setUncaughtExceptionHandler(this);
        Set<String> fullRecordSet = new HashSet<>();
        int counter = 0;
        try(PGCopyOutputStream writer = new PGCopyOutputStream(copyIn)) {
            for(Set<String> subset : powerSubset) {
                if (subset.size() < 2) {
                    continue;
                }
                fullRecordSet.add(buildCSV(subset));
                counter++;
            }
            logger.error("Generated {} csv records for part {}, people {}", fullRecordSet.size(),  ordinal, peopleId);
            for (String csv : fullRecordSet) {
                writer.write(csv.getBytes());  // ship pbs to server, finally
            }
            logger.error("Loaded from {}, duration {}s", getBulkTablename(), (System.currentTimeMillis() - timer)/1000);
        }
        catch (IOException ioe) {
            ioe.printStackTrace();
            logger.error("table {} had i/o trouble", getBulkTablename(), ioe);
            throw new RuntimeException("Bulk copy failed on i/o", ioe);
        }
    }

    private String buildCSV (Set<String> namesubset) {
        UUID[] ids = new UUID[namesubset.size()];
        String[] names = namesubset.toArray(new String[namesubset.size()]);
        StringBuffer nameBuf = new StringBuffer();
        Arrays.sort(names);
        int index = 0;
        for (String name : names) {
            nameBuf.append((index == 0 ? "" : ",") + name);
            UUID pbId = getNameUUIDMap().get(name);
            if (pbId == null) {
                logger.error("Could not find {} in proband/id map", name);
                //TODO: Full Stop??
                continue;
            }
            ids[index++] = pbId;
        }
        Arrays.sort(ids, new UUIDComparator());
        int meioses = 0;
        double[] kinCoefs = {0.0, 0.0};
        if (getIsMeiosesCounting()) {
            LinkageIdSet lis = new LinkageIdSet(namesubset);
            pedigreeData.resetProbands(lis.asList());
            MeiosesCounter mc = new MeiosesCounter(pedigreeData);
            meioses = mc.getCount(0);
            kinCoefs = mc.getKinshipCoeffRange(lis.asSet());
        }
        UUID pbsId = UUID.randomUUID();
        StringBuilder pbsCSV = new StringBuilder(pbsId.toString());              // id
        pbsCSV.append(",\"" + nameBuf.toString() +"\"");                         // name
        pbsCSV.append(",\"" + buildIdCSV(ids) + "\"");                           // probands
        pbsCSV.append("," + meioses) ;                                          // meioses
        pbsCSV.append(String.format(",%5.3E,%5.3E", kinCoefs[0], kinCoefs[1]));  // min_kincoef, max_kincoef
        pbsCSV.append("," + peopleId.toString());                                // people_id
        pbsCSV.append("\n");
        return pbsCSV.toString();
    }

    private String buildIdCSV(UUID[] ids) {
        StringBuffer idBuf = new StringBuffer("{");
        String comma = "";
        for ( UUID id : ids ) {
            idBuf.append( comma + id.toString() );
            comma = ",";
        }
        idBuf.append("}");
        return idBuf.toString();
    }

    // Done in prep work i.e. by main thread)
    public void localizePedigree(LinkagePedigreeData incoming) {
        pedigreeData = new LinkagePedigreeData(incoming);
        
    }

    @Override
    public void uncaughtException(Thread accThread, Throwable t) {
        interrupt();
        logger.error("Thread {} surprised by {}", accThread.getName(), t.getMessage(), t);
        RuntimeException rte = new RuntimeException(t);
        rte.printStackTrace();
        throw rte;
    }
}
