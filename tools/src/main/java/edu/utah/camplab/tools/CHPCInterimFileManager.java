package edu.utah.camplab.tools;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class CHPCInterimFileManager implements InterimFileManagerInterface {
  /* notice we add trailing slash */
  public static final String CHPC_PE_ROOT_TEMPLATE = "/uufs/chpc.utah.edu/common/HIPAA/%s/sgsWork/%s";

  private Path irbDir;

  /*
   * we expect that clients have access to the IRB partition, but certain callers need their own playpen
   */
  public CHPCInterimFileManager(String IRB, String operation) {
    irbDir = Path.of(CHPCInterimFileManager.CHPC_PE_ROOT_TEMPLATE.formatted(IRB, operation));
    try {
      if ( ! Files.exists(irbDir)) {
	irbDir = Files.createDirectories(irbDir); //blows up, catch
      }
      if (! Files.isWritable(irbDir)) {
	irbDir = makeAlternate();
      }
    }
    catch (IOException ioe) {
      irbDir = makeAlternate();
    }
  }
  public void write(File jsonFile) {
    try {
      Files.copy(jsonFile.toPath(), irbDir.resolve(jsonFile.toPath().getFileName()));
    }
    catch (IOException ioe) {
      throw new RuntimeException("did not write " + jsonFile.getAbsolutePath());
    }
  }
  private Path makeAlternate() {
    String altpath = System.getProperties().getProperty("SGS_ALT_INTERIM_DIR");
    if (altpath == null) {
      throw new RuntimeException("IRB un-writeable, alt not specified");
    }
    return Path.of(altpath);
  }
}
