package edu.utah.camplab.app;

import static edu.utah.camplab.db.generated.base.Tables.ALIAS;
import static edu.utah.camplab.db.generated.base.Tables.DEFINER;
import static edu.utah.camplab.db.generated.base.Tables.DEFINERVALUE;
import static edu.utah.camplab.db.generated.base.Tables.PEOPLE;
import static edu.utah.camplab.db.generated.base.Tables.PEOPLE_MEMBER;
import static edu.utah.camplab.db.generated.base.Tables.PERSON;

import edu.utah.camplab.db.generated.base.tables.records.AliasRecord;
import edu.utah.camplab.db.generated.base.tables.records.DefinerRecord;
import edu.utah.camplab.db.generated.base.tables.records.DefinervalueRecord;
import edu.utah.camplab.db.generated.base.tables.records.PeopleMemberRecord;
import edu.utah.camplab.db.generated.base.tables.records.PeopleRecord;
import edu.utah.camplab.db.generated.base.tables.records.PersonRecord;
import edu.utah.camplab.tools.NamePassPrompter;
import edu.utah.camplab.jx.SGSFullContext;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import joptsimple.BuiltinHelpFormatter;
import joptsimple.OptionParser;
import joptsimple.OptionSet;
import joptsimple.OptionSpec;

public class AliasLoader {
    final static Logger logger = LoggerFactory.getLogger(AliasLoader.class);

    private DefinervalueRecord sourceType;
    private DefinervalueRecord aliasType;
    private File pairingFile;
    private Map<String, DefinervalueRecord> nameDefinervalueMap;
    private String piName;
    private String dbHost;
    private Connection dbconn = null;
    private DSLContext ctx = null;
    
    public static void main(String[] args) {
        AliasLoader loader = new AliasLoader();
        try {
            loader.handleArgs(args);
            loader.load();
        }
        catch(Exception e) {
            logger.error("All is lost", e);
        }
    }
    
    public void handleArgs(String[] cli) throws IOException, SQLException {
        logger.error("in handleargs");
        OptionParser op = new OptionParser();
        op.formatHelpWith(new BuiltinHelpFormatter(120,4));
        
        OptionSpec<File> fileSpec = op.accepts("file", "file of name/alias pairs").withRequiredArg().ofType(File.class).required();
        OptionSpec<String> sourceSpec = op.accepts("source", "existing name/alias category (e.g. \"gid\"); defaults to current name").withRequiredArg().ofType(String.class).defaultsTo("name");
        OptionSpec<String> targetSpec = op.accepts("target", "incoming alias category (e.g. \"gid\")").withRequiredArg().ofType(String.class).required();
        OptionSpec<String> piSpec = op.accepts("namedb",   "typically the PI (camp, coon, clark, feldkamp)").withRequiredArg().ofType(String.class).required();
        OptionSpec<String> hostSpec = op.accepts("hostdb",  "db connect info as 'host:port'").withRequiredArg().ofType(String.class).required();
        OptionSpec<Void> helpSpec = op.accepts("help", "eats, shoots and leaves. Prompts for name, password");

        OptionSet opSet = null;
        try {
            opSet = op.parse(cli);
            if (opSet.has(helpSpec)) {
                showHelpAndExit(op);
            }
            dbHost = opSet.valueOf(hostSpec);
            piName = opSet.valueOf(piSpec);
            setDbConn();
            aliasType = findDefinervalue(opSet.valueOf(targetSpec));
            if (opSet.has(sourceSpec)) {
                sourceType = findDefinervalue(opSet.valueOf(sourceSpec));
            }
            pairingFile = opSet.valueOf(fileSpec);
            dbHost = opSet.valueOf(hostSpec);
        }
        catch(Exception ex) {
            logger.error("Failed to parse args: {}", ex.getMessage(), ex);
            showHelpAndExit(op);
        }
    }

    public void load() throws IOException, FileNotFoundException, SQLException{
        Map<String,String> nameAliasMap = readPairings();
        Map<String,PersonRecord> namePersonMap = readNamePersonMap(nameAliasMap.keySet());
        if (nameAliasMap.size() > namePersonMap.size()) {
            logger.error("AliasLoader Warning: suspect duplicate instances of a known alias. Only the last new alias will be recorded");
        }
        else if (nameAliasMap.size() < namePersonMap.size()) {
            logger.error("AliasLoader Error: suspect multiple persons have the known alias. Since we can't be sure this is expected, we're quitting");
            throw new RuntimeException("Suspect multiple persons share a source(known) alias.  Not sure what to do, so we quit");
        }
        ctx.transaction(t1 -> {
                int aliasesSaved =saveNewAliases(nameAliasMap, namePersonMap, aliasType);
                logger.info("Recored {} new aliases", aliasesSaved);
            });
    }

    private Map<String, PersonRecord> readNamePersonMap(Set<String> names) {
        HashMap<String, PersonRecord> pMap = new HashMap<>();
        if (sourceType != null) {
            pMap.putAll(ctx.select().from(PERSON)
                        .join(ALIAS).on(PERSON.ID.equal(ALIAS.PERSON_ID))
                        .where(ALIAS.AKADEF_ID.equal(sourceType.getId()))
                        .fetchMap(ALIAS.AKA, r -> r.into(PERSON)));
        }
        else {
            pMap.putAll(ctx.selectFrom(PERSON).where(PERSON.NAME.in(names)).fetchMap(PERSON.NAME));
        }
        return pMap;
    }
            
    private Map<String,String> readPairings() throws FileNotFoundException, IOException{
        BufferedReader reader = new BufferedReader(new FileReader(pairingFile));
        String namepair = null;
        HashMap<String, String> nameMap = new HashMap<>();
        while ((namepair = reader.readLine()) != null) {
            if (namepair.startsWith("#")) continue;
            String[] name_alias = namepair.split(",");
            nameMap.put(name_alias[0].trim(),name_alias[1].trim());
        }
        logger.info("read in {} alias pairing", nameMap.size());
        return nameMap;
    }

    private int saveNewAliases(Map<String, String> naMap, Map<String,PersonRecord> personMap, DefinervalueRecord defRec) {
        int totalSaved = 0;
        int aliasCount = 0;
        ArrayList<AliasRecord> newAliases = new ArrayList<>();
        for (Map.Entry<String,String> meSS : naMap.entrySet()) {
            aliasCount++;
            if (aliasCount == 1000) {
                aliasCount = 0;
                ctx.batchStore(newAliases).execute();
                newAliases = new ArrayList<>();
                totalSaved += 1000;
            }
            AliasRecord aRec = ctx.newRecord(ALIAS);
            aRec.setId(UUID.randomUUID());
            aRec.setPersonId(personMap.get(meSS.getKey()).getId());
            aRec.setAka(meSS.getValue());
            aRec.setAkadefId(defRec.getId());
            newAliases.add(aRec);
        }
        ctx.batchStore(newAliases).execute();
        totalSaved += newAliases.size();
        return totalSaved;
    }

    private DefinervalueRecord findDefinervalue(String defname) {
        DefinervalueRecord defRec = ctx.selectFrom(DEFINERVALUE).where(DEFINERVALUE.VALUESTRING.equal(defname)).fetchOne();
        return defRec;
    }

    private void setDbConn() throws SQLException{
        String prop = null;
        String upass = null;
        String uname = null;
        prop = System.getProperty("upass");
        if ( prop == null ) {
            Map<String, String> npMap = NamePassPrompter.prompt();
            upass = npMap.get("password");
            uname = npMap.get("name");
            System.setProperty("upass", String.format("%s:%s", upass, uname));
        }
        else {
            String[] parts = prop.split(":");
            uname = parts[0];
            upass = parts[1];
        }

        String dburl = String.format("jdbc:postgresql://%s/%s", dbHost, piName);
        logger.error("connector: {}", dburl);
        dbconn = DriverManager.getConnection(dburl, uname, upass.length() < 1 ? null : upass);
        ctx = SGSFullContext.DSLContextFactory(dbconn);
    }

    private void showHelpAndExit(OptionParser op) throws IOException {
        //System.out.print(justifyHelp(getHelpDescription(), 100));
        op.printHelpOn(System.out);
        System.exit(0);
    }
}
