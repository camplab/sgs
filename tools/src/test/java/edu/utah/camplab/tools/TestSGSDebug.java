package edu.utah.camplab.tools;

import com.jonoler.longpowerset.*;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class TestSGSDebug {
  public static void main(String[] args) {
    HashSet<String> allFlagNames = new HashSet<>(Arrays.asList("eval","linkage","drop","peel"));
    LongSet<Set<String>> lps = LongPowerSet.create(allFlagNames);

    for(Set<String> flagSet : lps) {
      System.out.printf("====Input set: %s%n", flagSet.toString());
      runTest(flagSet);
    }
  }
  public static void runTest(Set<String> arg) {
    String csv = arg.toString().replaceAll("[\\[\\]]","");
    if (csv.length() == 0) return;

    SGSDebug.setRuntimeDebug(0L);
    SGSDebug.addFlags(csv);
    System.out.printf("runtime mask is %x\n", SGSDebug.getRuntimeDebug());
    for(SGSDebugEnum dn : SGSDebugEnum.values()) {
      boolean tested = (dn.getMaskValue() & SGSDebug.getRuntimeDebug()) == dn.getMaskValue();
      System.out.printf("%s (%x) is %s (%b)%n", dn.getUserHandle(), dn.getMaskValue(), tested ? "on" : "off", tested);
    }
  }
}
