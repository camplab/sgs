package edu.utah.camplab.jx;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;
import org.postgresql.copy.CopyIn;
import org.postgresql.copy.CopyManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Map;
import java.util.UUID;

import edu.utah.camplab.jx.SGSFullContext;

public class PayloadFromMux extends AbstractPayload {
  private final static Logger logger = LoggerFactory.getLogger(PayloadFromMux.class);
  private final String zeros = "0000000-0000-0000-0000-000000000000"; // One leading digit shy of full UUID

  @JsonIgnore
  private String stagingTableName = null;

  public PayloadFromMux() { }

  @Override
  public void writedb(Connection conn) {
    // The incoming connection is within a try/with
    DSLContext ctx = SGSFullContext.DSLContextFactory(conn);
    ctx.transaction(ftx -> {
      startProcess(ctx);
    });
    ctx.transaction(ltx -> {
        writeSegments(ctx);
      });
    finishProcess();  // This doesn't record segment super structure as it hasn't been "built"
    logger.debug("{}: end dbwrite at {}", getRunTag(), System.currentTimeMillis());
    completeSegmentSave(conn);
  }

  @Override
  public void writeSegments(DSLContext ctx) {
    // Transform the "Block" from pojos to records
    setPeopleExtended();
    getStagingTableName();  //Also a setter, do it early
    long startAt = System.currentTimeMillis();
    UUID markersId = getProcessExtended().getMarkersetRecord().getId();
    addNakedSegmentset(ctx);
    Map<String, UUID>csvToUUID = getPeopleExtended().getProbandsetIdMap(ctx);
    writeSegmentWithCOPY(ctx,  markersId, csvToUUID);
    logger.info("{}: Total segment bulk copy took {} ms", getRunTag(), System.currentTimeMillis() - startAt);
  }

  @Override
  public String toString() {
    StringBuilder b = new StringBuilder("MA Report\n");
    b.append(String.format("Project name: %s\n", getProjectName()));
    b.append(String.format("Database name: %s\n", getDbName()));
    b.append(String.format("User name: %s\n", getClientUsername()));
    b.append(String.format("Run tag: %s\n", getRunTag().toString()));

    if (getProcessBlock() == null) {
      b.append("No process block.\n");
    }
    else {
      b.append(getProcessBlock().toString());
    }
    if (getSegmentBlock() == null) {
      b.append("No segment block.\n");
    }
    else {
      b.append(getSegmentBlock().toString());
    }
    if (getFilesetBlock() == null) {
      b.append("No fileset block.\n");
    }
    else {
      b.append(getFilesetBlock().toString());
    }
    return b.toString();
  }

  @Override
  public void writeFile(){
    String reportFileName = getProcessBlock().getArgValue("reportFile");
    try (Writer writer = new BufferedWriter(new FileWriter(reportFileName))) {
      asJson(writer);
    }
    catch (IOException ioe) {
      throw new RuntimeException("trouble writing json", ioe);
    }
  }

  public String getStagingTableName() {
    if (stagingTableName == null) {
      setPeopleExtended();
      stagingTableName = String.format("%s_%s_%02d_%s", 
				       getProjectName(), 
				       getPeopleExtended().getName(),
				       getSegmentBlock().getChromosome(), 
				       getRunTag().toString().replace("-", "_"));
    }
    if (stagingTableName.length() > 63) {
      logger.error ("{}: Staging table name is too long: {}. Truncating to 63 chars.", getRunTag(), stagingTableName);
      System.err.println(String.format("%s: Staging table name is too long: %s. Truncating to 63 chars: %s.",
				       getRunTag().toString(), stagingTableName, stagingTableName.substring(0,62)));
      stagingTableName = stagingTableName.substring(0,62);
    }
        
    return ("bulk." + "\"" + stagingTableName) + "\"".replaceAll("\\s+", "_");
  }

  @JsonIgnore
  public String getPayloadType() {
    return AbstractPayload.PAYLOADTYPE_MULTIASSESS;
  }

  private void writeSegmentWithCOPY(DSLContext topctx, UUID markers, Map<String,UUID>csvIds){
    logger.debug("Preparing COPY work {}", getRunTag());
    topctx.transaction(topconf -> {
	DSLContext ctx = DSL.using(topconf);
	Connection driverConn = DriverManager.getConnection(getDbUrl(), getProjectName(), getProjectName()+ System.getProperty("SGSSRVR_roleExtension"));
	//CopyManager copyManager = new CopyManager((BaseConnection) driverConn);
	CopyManager copyManager = getConnection().unwrap(org.postgresql.PGConnection.class).getCopyAPI();
	String copyToStatement = String.format("COPY %s FROM STDIN CSV", getStagingTableName());
	createStagingTable(ctx, getStagingTableName());
	CopyIn copyIn = copyManager.copyIn(copyToStatement);

	PayloadWriterThread writerThread = prepWriterThread(topctx.parsingConnection(), getStagingTableName(), copyIn);
	writerThread.setProbandsetIdMap(csvIds);
	writerThread.setSegmentListMap(getSegmentBlock().getSegmentMap());
	writerThread.setMarkersetId(markers);
	logger.info("{}: started COPY work at {}", getRunTag(), System.currentTimeMillis());
	writerThread.start();
      });
  }

  private PayloadWriterThread prepWriterThread(Connection db, String table, CopyIn ci) {
    PayloadWriterThread pwt = new PayloadWriterThread(db, table, ci);
    pwt.setName(this.getRunTag().toString());
    return pwt;
  }
    
  private void createStagingTable(DSLContext ctx, String tname) {
    String sql = String.format("create table %s as select * from segment where 1 = 2;", tname);
    ctx.query(sql).execute();
    logger.debug("STAGING TABLE CREATED: {}", tname);
  }
    
  private void completeSegmentSave(Connection conn) {
    DSLContext ctx = SGSFullContext.DSLContextFactory(conn);
    ctx.transaction(t1 -> {
	if (isMerger()) {
	  updateAllSegments(ctx);
	}
	else {
	  insertAllSegments(ctx);
	}
      });
  }

  private void insertAllSegments(DSLContext ctx) {
    logger.info("{}: Begin transfer from bulk to segment", getStagingTableName());
    String sqlInsert = String.format("insert into segment select * from %s as s where ", getStagingTableName());
    StringBuilder sqlPart = null;
    for(int i = 0; i < 15; i++) {
      sqlPart = new StringBuilder(sqlInsert);
      sqlPart.append(getPartitionClause(i));
      ctx.query(sqlPart.toString()).execute();
    }
    sqlPart = new StringBuilder(sqlInsert);
    sqlPart.append(getFinalPartition());
    ctx.query(sqlPart.toString()).execute();
    markTableDone(ctx, getStagingTableName());
    logger.debug("finished segment insert");
  }
    
  private void markTableDone(DSLContext ctx, String bulkTable) {
    StringBuilder tableOnly = new StringBuilder(bulkTable.replace("bulk.", ""));
    tableOnly = tableOnly.insert(tableOnly.length() - 1, "_done");
    if (tableOnly.length() > 63) tableOnly = new StringBuilder(tableOnly.substring(tableOnly.length()-63)) ;
    String sql = String.format("alter table %s rename to %s", bulkTable, tableOnly);
    ctx.query(sql).execute();
  }

  private void updateAllSegments(DSLContext ctx) {
    logger.info("{}: Begin update from bulk to segment", getStagingTableName());
        
    UUID markerSetId = getProcessExtended().getMarkersetRecord().getId();
    StringBuilder sqlRoot = new StringBuilder();
    sqlRoot.append(String.format("update segment as s set "
				 + "events_less = s.events_less + b.events_less, "
				 + "events_equal = s.events_equal + b.events_equal, " 
				 + "events_greater = s.events_greater + b.events_greater, "
				 + "threshold_events = s.threshold_events + b.threshold_events "
				 + "from %s as b where s.markerset_id = '%s'",
				 getStagingTableName(), markerSetId.toString()));
    sqlRoot.append(" and s.probandset_id = b.probandset_id and s.markerset_id = b.markerset_id "
		   + "and s.startbase = b.startbase and s.endbase = b.endbase");

    StringBuilder sqlPart = null;
    long startAt = 0;
    for (int i = 0; i < 15; i++) {
      logger.info("updating part {}", (i+1));
      sqlPart = new StringBuilder();
      sqlPart.append(sqlRoot);
      sqlPart.append(" and ");
      sqlPart.append(getPartitionClause(i));
      startAt = System.currentTimeMillis();
      ctx.query(sqlPart.toString()).execute();
      logger.info("mux update sql [{}] part {} took {}s ", sqlPart, i+1, (System.currentTimeMillis() - startAt)/1000);
    }
    sqlPart = new StringBuilder();
    sqlPart.append(sqlRoot);
    sqlPart.append(" and ");
    sqlPart.append(getFinalPartition());
    startAt = System.currentTimeMillis();
    logger.info("updating part {}", 16);
    ctx.query(sqlPart.toString()).execute();
    markTableDone(ctx, getStagingTableName());
    logger.info("mux update sql [{}] part {} took {}s ", sqlPart, 16, (System.currentTimeMillis() - startAt)/1000);
  }

  private String getPartitionClause(int part) {
    return String.format(" s.probandset_id >= '%x%s' and s.probandset_id < '%x%s' ", part, zeros, part+1, zeros);
  }

  private String getFinalPartition() {
    return String.format(" s.probandset_id >= '%x%s' ", 15, zeros);
  }

  private void addNakedSegmentset(DSLContext ctx) {
    getProcessExtended().addSegmentset();
  }

  private boolean isMerger() { 
    return getProcessExtended().checkSegmentExists(getSegmentBlock());
  }
}
