package edu.utah.camplab.jx;

import java.sql.Connection;

import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;
import org.jooq.conf.Settings;
import org.jooq.conf.StatementType;

public class SGSFullContext {

  public static DSLContext DSLContextFactory(Connection conn) {
    Settings settings = new Settings();
    settings.setStatementType(StatementType.STATIC_STATEMENT);
    DSLContext ctx = DSL.using(conn, SQLDialect.POSTGRES, settings);
    return ctx;
  }    
    
  public static DSLContext DSLContextFactory(Connection conn, Settings settings) {
    settings.setStatementType(StatementType.STATIC_STATEMENT);
    DSLContext ctx = DSL.using(conn, SQLDialect.POSTGRES, settings);
    return ctx;
  }    
}
