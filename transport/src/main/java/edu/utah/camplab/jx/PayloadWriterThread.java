package edu.utah.camplab.jx;

import edu.utah.camplab.db.generated.default_schema.tables.pojos.Segment;
import org.postgresql.copy.CopyIn;
import org.postgresql.copy.PGCopyOutputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class PayloadWriterThread extends Thread implements Thread.UncaughtExceptionHandler {

    private final static Logger logger = LoggerFactory.getLogger(PayloadWriterThread.class);

    private Map<String, List<Segment>> segmentListMap = null;
    private Map<String, UUID> probandsetIdMap = null;
    private Connection db = null;
    private String tableName;
    private UUID markersetId = null;
    private CopyIn copyIn = null;

    public PayloadWriterThread() { };
    
    public PayloadWriterThread(Connection conn, String tablename, CopyIn ci) {
        setConnection(conn);
        setTableName(tablename);
        setCopyIn(ci);
    }


    public void setCopyIn(CopyIn ci) {
        copyIn = ci;
    }

    public void setConnection(Connection c) {
        db = c;
    }

    public void setTableName(String t) {
        tableName = t;
    }
    
    public void setSegmentListMap(Map<String, List<Segment>>seglistMap) {
        segmentListMap = seglistMap;
    }

    public void setMarkersetId(UUID id) {
        markersetId = id;
    }

    public void setProbandsetIdMap(Map<String, UUID> map) {
        probandsetIdMap = map;
    }

    @Override
    public void run() {
        try (PGCopyOutputStream writer = new PGCopyOutputStream(copyIn)) {
            long startAt = System.currentTimeMillis();
            deliverSegments(writer);
            long postDeliver = System.currentTimeMillis();
            logger.debug("staged in {} ms", postDeliver - startAt);
        }
        catch (SQLException sqle) {
            sqle.printStackTrace();
            logger.error("{}: sql trouble", tableName, sqle);
            throw new RuntimeException("Bulk copy failed on sql", sqle);
        }
        catch (IOException ioe) {
            ioe.printStackTrace();
            logger.error("{}: i/o trouble", tableName, ioe);
            throw new RuntimeException("Bulk copy failed on i/o", ioe);
        }
    }

    private void deliverSegments(PGCopyOutputStream writer) throws IOException, SQLException {
        logger.info("{}: Begin bulk copy segment", tableName);
        long intended = 0;
        //These had better end up the same number
        long startAt = System.currentTimeMillis();
        for (Map.Entry<String, List<Segment>> seglistME : segmentListMap.entrySet()) {
            String pbsCSV = seglistME.getKey();
            UUID currentPbsId = probandsetIdMap.get(pbsCSV);
            intended += seglistME.getValue().size();
            if (currentPbsId == null) { 
            	throw new RuntimeException("Could not find pbsId by " + pbsCSV);
            }
            for (Segment segment : seglistME.getValue()) {
                String segmentCSV = makeCSV(segment, currentPbsId);
                writer.write(segmentCSV.getBytes());
            }
        }
        logger.info ("bulk transfer of {} took {}s", intended, (System.currentTimeMillis() - startAt)/1000.0);
    }

    private String makeCSV(Segment seg, UUID pbsId) {
        //TODO: move this to SegmentBlock/SementsetExtended
        StringBuilder sb = new StringBuilder();
        seg.setMarkersetId(markersetId);
        seg.setProbandsetId(pbsId);
        sb.append(UUID.randomUUID());
        sb.append(",").append(seg.getChrom());
        sb.append(",").append(seg.getMarkersetId());
        sb.append(",").append(seg.getProbandsetId());
        sb.append(",").append(seg.getStartbase());
        sb.append(",").append(seg.getEndbase());
        sb.append(",").append(seg.getFirstmarker());
        sb.append(",").append(seg.getLastmarker());
        sb.append(",").append(seg.getEventsLess());
        sb.append(",").append(seg.getEventsEqual());
        sb.append(",").append(seg.getEventsGreater());
        sb.append(",").append(seg.getThresholdEvents());
        return sb.append("\n").toString();
    }

    @Override
    public void uncaughtException(Thread accThread, Throwable t) {
        logger.error("Thread {} surprised by {}", accThread.getName(), t.getMessage(), t);
        RuntimeException rte = new RuntimeException(t);
        rte.printStackTrace();
        throw rte;
    }
}
