package edu.utah.camplab.jx;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import edu.utah.camplab.db.crafted.PeopleExtended;
import edu.utah.camplab.db.crafted.ProcessExtended;
import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.conf.Settings;
import org.jooq.conf.StatementType;
import org.jooq.impl.DSL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Writer;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.UUID;
//TODO: rid this package of pojos

@JsonTypeInfo( use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "class" )
@JsonSubTypes( {
    @Type(value = PayloadFromMux.class, name = "multiassess"),
      @Type(value = PayloadFromChase.class, name = "chase")
      }
)
public abstract class AbstractPayload implements SGSPayloadInterface {
  final static Logger logger = LoggerFactory.getLogger(AbstractPayload.class);

  public static String		PAYLOADTYPE_CHASE       = "chase";
  public static String		PAYLOADTYPE_MULTIASSESS = "multiassess";

  private String		projectName		= null;
  private String		clientUsername		= null;
  private String		clientHostname		= null;
  private UUID			runTag			= null;
  private UUID			markersetId		= null;
  private UUID			peopleId 		= null;
  private String		dbName			= null;
  private String		dbUrl			= null;
  private File			report			= null;
    
  private ProcessBlock		processBlock;
  private SegmentBlock		segmentBlock;
  private FilesetBlock		filesetBlock;
  private ProcessExtended	processEx		= null;
  private PeopleExtended	peopleEx		= null;

  private ObjectMapper		objectMapper		= null;
  private Connection		connection              = null;

  // Abstractions
  public abstract void writeFile() throws IOException;
  public abstract void writedb(Connection c) throws SQLException;
  // Chase, MA specific
  public abstract void writeSegments(DSLContext dsl) throws Exception;

  public void write() throws SQLException {
    // the Connection from the payload.doPost is in try-with
    writedb(getConnection());
  }

  public void setProjectName(String n) {
    projectName = n;
  }

  public String getProjectName() {
    return projectName;
  }

  public void setClientUsername(String user) {
    clientUsername = user;
  }

  public String getClientUsername() {
    return clientUsername;
  }
    
  public void setClientHostname(String host) {
    clientHostname = host;
  }

  public String getClientHostname() {
    return clientHostname;
  }
    
  public void setRunTag(UUID u) {
    runTag = u;
  }

  public UUID getRunTag() {
    return runTag;
  }

  public void setMarkersetId(UUID u) {
    markersetId = u;
  }

  public UUID getMarkersetId() {
    return markersetId;
  }

  public void setPeopleId(UUID u) {
    peopleId = u;
  }

  public UUID getPeopleId() {
    return peopleId;
  }

  public void setProcessBlock(ProcessBlock block) {
    processBlock = block;
  }
    
  public ProcessBlock getProcessBlock() {
    return processBlock;
  }
    
  public FilesetBlock getFilesetBlock() {
    return filesetBlock;
  }
    
  public SegmentBlock getSegmentBlock() {
    return segmentBlock;
  }
    
  public void setSegmentBlock(SegmentBlock block){
    segmentBlock = block;
  }
    
  public void setFilesetBlock(FilesetBlock block) {
    filesetBlock = block;
  }

  // We don't need to hold on to this data while iterating through the bulk/staging table.
  public void emptyDataStructures() {
    segmentBlock = null;
    filesetBlock = null;
    processBlock = null;
    peopleEx     = null;
    processEx    = null;
  }

  @Override
  public void setDbName(String name) {
    dbName = name;
  }

  @Override
  public String getDbName() {
    return dbName;
  }
    
  public void setDbUrl(String url) {
    dbUrl = url;
  }

  public String getDbUrl() {
    return dbUrl;
  }

  public File getReport() {
    return report;
  }

  public void setReport(File repfile) {
    report = repfile; //may be a dir in mux-land
  }

  public PeopleExtended getPeopleExtended() {
    return peopleEx;
  }
    
  public void setPeopleExtended() {
    if (peopleEx == null)
      peopleEx = new PeopleExtended(processEx.getPeopleRecord());
  }

  public ProcessExtended getProcessExtended() {
    return processEx;
  }

  protected void startProcess(DSLContext ctx) {
    //TODO: POSSIBLE RMSTAR CASUALTY
    //incoming dsl is an open transaction
    processEx = new ProcessExtended(ctx, getProcessBlock(), getRunTag());  //, getPeopleId(), getMarkersetId()
    processEx.startSave();
  }

  public void asJson(OutputStream stream) {
    try {
      getObjectMapper().writeValue(stream, this);
    }
    catch (Exception e) {
      e.printStackTrace();
      logger.error("{}: could not serialize to stream", getRunTag());
    }
  }

  public void asJson(Writer writer) {
    try {
      getObjectMapper().writeValue(writer, this);
    }
    catch (Exception e) {
      e.printStackTrace();
      logger.error("{}: could not serialize to file", getRunTag());
    }
  }

  public Connection getConnection() {
    return connection;
  }

  public void setConnection(Connection conn) {
    connection = conn;
  }
    
  private ObjectMapper getObjectMapper() {
    if (objectMapper == null) {
      objectMapper = JsonMapper.builder().addModule(new JavaTimeModule()).build();
      objectMapper.configure(SerializationFeature.INDENT_OUTPUT, false);
      objectMapper.configure(JsonGenerator.Feature.AUTO_CLOSE_TARGET, false);
      objectMapper.setDefaultPropertyInclusion(JsonInclude.Value.construct(Include.NON_EMPTY, Include.ALWAYS));
    }
    return objectMapper;
  }

  protected void finishProcess() {
    logger.info("{}: closing process {}", getRunTag(),  processEx.getId());
    processEx.finishSave();
  }
}
