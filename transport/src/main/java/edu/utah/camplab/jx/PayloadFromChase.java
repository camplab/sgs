package edu.utah.camplab.jx;

import com.fasterxml.jackson.annotation.JsonIgnore;
import edu.utah.camplab.db.crafted.SegmentsetExtended;
import edu.utah.camplab.db.generated.default_schema.tables.pojos.Segmentset;
import edu.utah.camplab.jx.SGSFullContext;
import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.util.Map;

public class PayloadFromChase extends AbstractPayload {

  public PayloadFromChase() { }

  @Override
  public void writeFile() throws IOException {
    String reportFileName = getProcessBlock().getArgValue("reportFile");
    try (PrintWriter printWriter = new PrintWriter(new File(reportFileName))){
      asJson(printWriter);
    }
    catch (FileNotFoundException fnf) {
      logger.error("payload write failed on filename {}", reportFileName, fnf);
      throw new IOException(fnf);
    }
  }

  @Override
  public void writedb(Connection cnx) {
    logger.debug("{}: start transaction at {}", getRunTag(), System.currentTimeMillis());
    DSLContext ctx = SGSFullContext.DSLContextFactory(cnx);
    ctx.transaction(ltx -> {
	startProcess(ctx);
	writeSegments(ctx);
	finishProcess();
      });
    logger.debug("{}: end transaction at {}", getRunTag(), System.currentTimeMillis());
  }

  @Override
  public void writeSegments(DSLContext ctx) {
    SegmentsetExtended segmentsetEx = buildSingleSegmentsetExtended(ctx, getSegmentBlock());
    if (segmentsetEx == null) {
      logger.error("{}: No proto-segments in the payload", getRunTag());
      throw new RuntimeException("No proto-segments in the payload");
    }
    segmentsetEx.updateSegmentsById();
  }	

  @JsonIgnore
  public String getPayloadType() {
    return AbstractPayload.PAYLOADTYPE_CHASE;
  }

  private SegmentsetExtended buildSingleSegmentsetExtended(DSLContext transaction, SegmentBlock segblock) {
    // We're mainly avoiding reading ALL the proband sets for the given people
    logger.info("chase block has {} segmentSets", segblock.getSegmentsetMap().size());
    setPeopleExtended();
    if (getPeopleExtended() == null) {
      throw new RuntimeException("Missing the people, can't build segment set");
    }
    for (Map.Entry<String, Segmentset> segsetME : segblock.getSegmentsetMap().entrySet()) {
      String idsCSV = segsetME.getKey();
      logger.debug("chasing pbs by {} for people {}", idsCSV, getPeopleExtended().getName());
      Segmentset segSet = segsetME.getValue();
      SegmentsetExtended segmentsetEx = new SegmentsetExtended(transaction,
							       segSet.getName(),
							       getSegmentBlock().getSegmentList(idsCSV),
							       getProjectName(),
							       getPeopleExtended().getSingleProbandset(transaction, idsCSV),
							       getProcessExtended());
      return segmentsetEx; //This is the only one here.
    }
    return null;
  }
}
