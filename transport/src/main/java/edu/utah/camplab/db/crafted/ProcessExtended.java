package edu.utah.camplab.db.crafted;

import edu.utah.camplab.db.generated.base.tables.records.MarkersetRecord;
import edu.utah.camplab.db.generated.base.tables.records.PeopleRecord;
import edu.utah.camplab.db.generated.default_schema.tables.pojos.ProcessArg;
import edu.utah.camplab.db.generated.default_schema.tables.pojos.ProcessInput;
import edu.utah.camplab.db.generated.default_schema.tables.pojos.ProcessOutput;
import edu.utah.camplab.db.generated.default_schema.tables.pojos.Segment;
import edu.utah.camplab.db.generated.default_schema.tables.records.*;
import edu.utah.camplab.jx.ProcessBlock;
import edu.utah.camplab.jx.SegmentBlock;
import org.jooq.DSLContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetAddress;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static edu.utah.camplab.db.generated.base.Tables.MARKERSET;
import static edu.utah.camplab.db.generated.base.Tables.PEOPLE;
import static edu.utah.camplab.db.generated.default_schema.Tables.*;

public class ProcessExtended extends ProcessRecord {
  private static final long serialVersionUID = -2602345246448146767L;
  private final static Logger logger = LoggerFactory.getLogger(ProcessExtended.class);
    
  private List<ProcessInputRecord>  inputList    = null;
  private List<ProcessOutputRecord> outputList   = null;
  private List<ProcessArgRecord>    argList      = null;
  private DSLContext dslContext = null;
  private ProjectfileRecord         pedfileRec   = null;
  private ProjectfileRecord         parfileRec   = null;
  private PeopleRecord              peopleRec    = null;
  private MarkersetRecord           markersetRec = null;
    

  public ProcessExtended(DSLContext ctx, ProcessBlock processBlock, UUID procId) {
    super(procId,
	  processBlock.getProcess().getWho(),
	  processBlock.getProcess().getWhat(),
	  processBlock.getProcess().getPit(),
	  processBlock.getProcess().getRunhost());
    dslContext = ctx;
    dslContext.attach((ProcessRecord)this);
    fleshOut(processBlock);
  }

  public ProcessExtended(DSLContext ctx, String activityName, InetAddress localhost, UUID procId) {
    super(procId,
	  System.getProperty("user.name", "unknown"),
	  activityName,
	  LocalDateTime.now(),
	  localhost.getHostName());
    dslContext = ctx;
    dslContext.attach((ProcessRecord)this);
    logger.debug("New PROCESS {} for {}", getId(), activityName);
  }
    
  public List<ProcessInputRecord> getInputRecordList() {
    if ( inputList == null ) {
      inputList = new ArrayList<ProcessInputRecord>();
    }
    return inputList;
  }

  public List<ProcessOutputRecord> getOutputRecordList() {
    if ( outputList == null ) {
      outputList = new ArrayList<ProcessOutputRecord>();
    }
    return outputList;
  }

  // public ProjectfileRecord getParfileRecord() {
  //     return parfileRec;
  // }

  // public ProjectfileRecord getPedfileRecord() {
  //     return pedfileRec;
  // }

  public List<ProcessArgRecord> getArgRecordList() {
    if ( argList == null ) {
      argList = new ArrayList<ProcessArgRecord>();
    }
    return argList;
  }

  public void addArg(String attribute, double value) {
    ProcessArgRecord par = addArgBasics(attribute);
    par.setArgvalueFloat(value);
    getArgRecordList().add(par);
  }

  public void addArg(String attribute, long value) {
    ProcessArgRecord par = addArgBasics(attribute);
    par.setArgvalueInt(value);
    getArgRecordList().add(par);
  }

  public void addArg(String attribute, String value) {
    ProcessArgRecord par = addArgBasics(attribute);
    par.setArgvalueText(value);
    getArgRecordList().add(par);
  }

  public PeopleRecord getPeopleRecord() {
    return peopleRec;
  }
    
  public MarkersetRecord getMarkersetRecord() {
    if (markersetRec == null) {
      //markersetRec = transaction.selectFrom(MARKERSET).where(MARKERSET.NAME.equal(parfileRec.getName())).fetchOne();
    }
    return markersetRec;
  }
    
  private ProcessArgRecord addArgBasics(String attr) {
    ProcessArgRecord par = dslContext.newRecord(PROCESS_ARG);
    par.setId(UUID.randomUUID());
    par.setProcessId(getId());
    par.setArgname(attr);
    return par;
  }
        

  public void addOutput(String kind, UUID ref) {
    ProcessOutputRecord por = dslContext.newRecord(PROCESS_OUTPUT);
    por.setId(UUID.randomUUID());
    por.setProcessId(getId());
    por.setOutputType(kind);
    por.setOutputRef(ref);
    getOutputRecordList().add(por);
  }
    
  public void startSave() {
    insert();

  }

  public void finishSave() {
    dslContext.batchStore(getInputRecordList()).execute();
    dslContext.batchStore(getOutputRecordList()).execute();
    dslContext.batchStore(getArgRecordList()).execute();
  }

  private void fleshOut(ProcessBlock pblock) {
    //Anything else here?
    spreadProcessId(pblock);
    addExtraArgs(pblock);
    findPeopleMarkerset();
  }

  public void addInput(String typeName, String refid) {
    ProcessInputRecord pir = dslContext.newRecord(PROCESS_INPUT);
    pir.setId(UUID.randomUUID());
    pir.setProcessId(getId());
    pir.setInputType(typeName);
    pir.setInputRef(refid);	
    getInputRecordList().add(pir);
  }

  private void spreadProcessId(ProcessBlock pb) {
    for (ProcessInput pi : pb.getInputList()) {
      addInput(pi.getInputType(), pi.getInputRef());
    }
    for (ProcessOutput po : pb.getOutputList()) {
      addOutput(po.getOutputType(), po.getOutputRef());
    }
    for (ProcessArg pi : pb.getArgList()) {
      ProcessArgRecord par = dslContext.newRecord(PROCESS_ARG);
      par.setId(UUID.randomUUID());
      par.setProcessId(getId());
      par.setArgname(pi.getArgname());
      par.setArgvalueInt(pi.getArgvalueInt());
      par.setArgvalueFloat(pi.getArgvalueFloat());
      par.setArgvalueText(pi.getArgvalueText());
      getArgRecordList().add(par);
    }
  }

  private void addExtraArgs(ProcessBlock pb) {
    // addJarsUsed();
  }

  private void findPeopleMarkerset() {
    // TODO: more save-time lookups but these are one-and-done.
    for (ProcessArgRecord pa : getArgRecordList()) {
      // Now have to destinguish between chase and mux
      if (pa.getArgname().equals("segment")) {
	UUID segId = UUID.fromString(pa.getArgvalueText());
	peopleRec = dslContext.select()
	  .from(PEOPLE).join(PROBANDSET).on(PEOPLE.ID.eq(PROBANDSET.PEOPLE_ID))
	  .join(SEGMENT).on(PROBANDSET.ID.eq(SEGMENT.PROBANDSET_ID))
	  .where(SEGMENT.ID.equal(segId))
	  .fetchOne(r->r.into(PEOPLE));
	markersetRec = dslContext.select()
	  .from(MARKERSET).join(SEGMENT).on(MARKERSET.ID.eq(SEGMENT.MARKERSET_ID))
	  .where(SEGMENT.ID.equal(segId))
	  .fetchOne(r->r.into(MARKERSET));
	break;
      }
      if (pa.getArgname().equals("pedigree")) {
        UUID pid = UUID.fromString(pa.getArgvalueText());
        peopleRec = dslContext.selectFrom(PEOPLE).where(PEOPLE.ID.equal(pid)).fetchOne();
        if (peopleRec == null) {
          logger.error("No people found for ped file {}/{}", pedfileRec.getId(), pedfileRec.getUri());
        }
        else {
          addInput("People", pid.toString()/*sigh*/);
        }
      }
      if (pa.getArgname().equals("markerset")) {
        UUID mid = UUID.fromString(pa.getArgvalueText());
        markersetRec = dslContext.selectFrom(MARKERSET).where(MARKERSET.ID.equal(mid)).fetchOne();
        addInput("Markerset", mid.toString());
      }
    }
   }

  public void addJarsUsed(List<String> jars) {
    for (String j : jars) {
      ProcessArgRecord par = dslContext.newRecord(PROCESS_ARG);
      par.setId(UUID.randomUUID());
      par.setProcessId(getId());
      par.setArgname("classpath entry");
      par.setArgvalueText(j);
      getArgRecordList().add(par);
    }
  }
  public void addSegmentset() {
    String segsetName = makeSegmentsetName(getMarkersetRecord().getChrom());
        
    SegmentsetRecord segsetRec = dslContext.selectFrom(SEGMENTSET)
      .where(SEGMENTSET.NAME.equal(segsetName))
      .fetchOne();
    if (segsetRec == null) {
      segsetRec = dslContext.newRecord(SEGMENTSET);
      segsetRec.setId(UUID.randomUUID());
      segsetRec.setName(segsetName);
      segsetRec.setMarkersetId(getMarkersetRecord().getId());
      segsetRec.setPeopleId(getPeopleRecord().getId());
      // segsetRec.setParfileId(getParfileRecord().getId());
      // segsetRec.setPedfileId(getPedfileRecord().getId());
      segsetRec.insert();
        
      addOutput("segmentset", segsetRec.getId()); 
    }
  }
  //TODO: add ENDBASE constraint
  public boolean checkSegmentExists(SegmentBlock segblock) {
    String segmentKey = segblock.getSegmentMap().keySet().iterator().next();
    Segment testSeg = segblock.getSegmentMap().get(segmentKey).get(0);
    logger.error("checking existance of segment - {}", testSeg.toString());
    SegmentRecord sRec = dslContext.selectFrom(SEGMENT)
      .where(SEGMENT.PROBANDSET_ID.equal(testSeg.getProbandsetId())
	     .and(SEGMENT.MARKERSET_ID.equal(testSeg.getMarkersetId()))
	     .and(SEGMENT.STARTBASE.equal(testSeg.getStartbase()))
	     .and(SEGMENT.ENDBASE.equal(testSeg.getEndbase()))
	     )
      .fetchOne();
    return sRec != null;
  }

  private String makeSegmentsetName(int chrm) {
    String trimmer = getId().toString()  + ":" + chrm; // a default value of process id
    for (ProcessArgRecord parg : getArgRecordList()) {
      if (parg.getArgname().contains("trim")) {
	if ( ! "true".equals(parg.getArgvalueText()) )
	  trimmer = parg.getArgvalueText() + ":" + chrm;
	break;
      }
    }
    return trimmer;
  }
}
