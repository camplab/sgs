--
-- PostgreSQL database dump
--

-- Dumped from database version 12.9 (Ubuntu 12.9-0ubuntu0.20.04.1)
-- Dumped by pg_dump version 12.9 (Ubuntu 12.9-0ubuntu0.20.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: base; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA base;


ALTER SCHEMA base OWNER TO postgres;

--
-- Name: bulk; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA bulk;


ALTER SCHEMA bulk OWNER TO postgres;

--
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO postgres;

--
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA public IS 'standard public schema';


--
-- Name: worklist_fill(text, double precision); Type: FUNCTION; Schema: base; Owner: postgres
--

CREATE FUNCTION base.worklist_fill(groupname text, maxpv double precision) RETURNS void
    LANGUAGE plpgsql
    AS $$
declare
  workitems int;
begin
  insert into chaseable
  select uuid_generate_v4() as id, s.id as segment_id , t.id as threshold_id 
  from segment s
      join probandset p on s.probandset_id = p.id 
      join probandset_group_member m on p.id = m.member_id 
      join probandset_group g on m.group_id = g.id 
      join threshold t on array[g.id] = t.probandset_groups
  where 
      g.name = groupname
      and (
        pv(s.events_less, s.events_equal, s.events_greater, 0) = 0
        or (
            pv(s.events_less, s.events_equal, s.events_greater, 0) <= maxpv
            and (
                ( pv_upper95(s.events_less, s.events_equal, s.events_greater, 0) > t.significant 
                  and pv_lower95(s.events_less, s.events_equal, s.events_greater, 0) < t.significant
                ) 
                or 
                ( pv_upper95(s.events_less, s.events_equal, s.events_greater, 0) > t.suggestive 
                  and pv_lower95(s.events_less, s.events_equal, s.events_greater, 0) < t.suggestive 
                )
            )
          )
        );
  get diagnostics workitems = ROW_COUNT;
  raise notice 'added % worklist items for %', workitems, groupname;
end;
$$;


ALTER FUNCTION base.worklist_fill(groupname text, maxpv double precision) OWNER TO postgres;

--
-- Name: addprobandset(text[], uuid, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.addprobandset(names text[], people uuid, groupname text) RETURNS void
    LANGUAGE plpgsql
    AS $$
declare
  pbsid uuid;
  pbsset uuid[];
  groupid uuid;
begin
  select id into groupid from probandset_group where name = groupname;
  if groupid is null then
    select uuid_generate_v4() into groupid;
    insert into probandset_group values(groupid, groupname, null, 'water boarding');
  end if;
  select uuid_generate_v4() into pbsid;
  select array_agg(id order by id) into pbsset from person
  where name = any (names);
--
  insert into probandset(id, name, probands, people_id)
  values(pbsid, array_to_string(names, ','), pbsset, people);
--
  insert into probandset_group_member values (groupid, pbsid);
end;
$$;


ALTER FUNCTION public.addprobandset(names text[], people uuid, groupname text) OWNER TO postgres;

--
-- Name: alias_definers(text, text[], text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.alias_definers(category text, defs text[], verbage text) RETURNS void
    LANGUAGE plpgsql
    AS $$
declare
  defid uuid;
  defval text;
  defparts text[];
begin
  select id into defid from base.definer where name = category;
  if defid is null
  then
    select uuid_generate_v4() into defid;
    insert into base.definer (id, name, description) values (defid, category, verbage);
  end if;
  foreach defval in array defs loop
    select regexp_split_to_array(defval, E';;') into defparts;
    insert into base.definervalue (id, valuestring, description, definer_id) values (uuid_generate_v4(), defparts[1], defparts[2], defid);
  end loop;
end;
$$;


ALTER FUNCTION public.alias_definers(category text, defs text[], verbage text) OWNER TO postgres;

--
-- Name: ascend(uuid); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.ascend(focus uuid) RETURNS TABLE(id uuid, name text, pa uuid, ma uuid, gender character, gennum integer)
    LANGUAGE sql
    AS $$
with recursive generation(id, name, pa, ma, gender, gen) as (
     select  id, name, pa, ma, gender, 0::int as gennum from person
     where id = focus
     union
     select b.id, b.name, b.pa, b.ma, b.gender, g.gen-1
     from person b, generation g
     where (b.gender = 'f' and g.ma = b.id) or (b.gender = 'm' and g.pa = b.id)
)
select * from generation;
$$;


ALTER FUNCTION public.ascend(focus uuid) OWNER TO postgres;

--
-- Name: ascend_all(uuid[]); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.ascend_all(focus uuid[]) RETURNS TABLE(id uuid, name text, pa uuid, ma uuid, gender character, is_proband boolean)
    LANGUAGE sql
    AS $$
with recursive generation(id, name, pa, ma, gender, gennum) as (
     select  id, name, pa, ma, gender, 0::int as gennum from person
     where id = any(focus)
     union
     select b.id, b.name, b.pa, b.ma, b.gender, g.gennum-1
     from person b, generation g
     where (b.gender = 'f' and g.ma = b.id) or (b.gender = 'm' and g.pa = b.id)
)
select id, name, pa, ma, gender, id = any(focus) as is_proband from generation group by id, name, pa, ma, gender
$$;


ALTER FUNCTION public.ascend_all(focus uuid[]) OWNER TO postgres;

--
-- Name: ascend_trim(uuid[]); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.ascend_trim(focus uuid[]) RETURNS TABLE(id uuid, pa uuid, ma uuid, gender character, is_proband boolean)
    LANGUAGE plpgsql
    AS $$
declare
  delcount int := 1;
begin
  drop table if exists gevt;
  create temp table gevt as
  select * from ascend_all(focus);
  while delcount > 0 loop
    drop table if exists singleton;
    create temp table singleton as
    with ped as (select * from gevt),
    founder as (select p.id, p.gender from ped p where p.ma is null and p.pa is null),
    fmother as (select f.id, count(*) as kids from founder f join ped p on f.id = p.ma where f.gender = 'f' group by f.id having count(*) = 1),
    ffather as (select f.id, count(*) as kids from founder f join ped p on f.id = p.pa where f.gender = 'm' group by f.id having count(*) = 1),
    singleton as (select * from ped p where p.ma in (select fm.id from fmother fm where fm.kids = 1) and p.pa in (select ff.id from ffather ff where ff.kids = 1))
    select * from singleton;
    update gevt g set ma = null, pa = null where g.id in (select s.id from singleton s);
    delete from gevt g where g.id in (select t.ma from singleton t union select s.pa from singleton s);
    get diagnostics delcount = ROW_COUNT;
    raise notice 'dropped % singleton parents', delcount;
  end loop;
  -- return query select g.id, g.pa, g.ma, g.gender, case when d.id is not null then true else false end as proband 
  -- from gevt g left join dna d on g.id = d.id;
  return query select g.id, g.pa, g.ma, g.gender, case when g.id = any(focus) then true else false end as proband 
  from gevt g;
end;
$$;


ALTER FUNCTION public.ascend_trim(focus uuid[]) OWNER TO postgres;

--
-- Name: ascend_trimlite(uuid[]); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.ascend_trimlite(focus uuid[]) RETURNS TABLE(id uuid, name text, pa uuid, ma uuid, gender character, is_proband boolean)
    LANGUAGE plpgsql
    AS $$
declare
  delcount int := 1;
begin
  drop table if exists gevt;
  create temp table gevt as
  select * from ascend_all(focus);
  while delcount > 0 loop
    drop table if exists singleton;
    create temp table singleton as
    with ped as (select * from gevt),
    founder as (select p.id, p.gender from ped p where p.ma is null and p.pa is null),
    fmother as (select f.id, count(*) as kids from founder f join ped p on f.id = p.ma where f.gender = 'f' group by f.id having count(*) = 1),
    ffather as (select f.id, count(*) as kids from founder f join ped p on f.id = p.pa where f.gender = 'm' group by f.id having count(*) = 1),
    singleton as (select * from ped p where p.ma in (select fm.id from fmother fm where fm.kids = 1) and p.pa in (select ff.id from ffather ff where ff.kids = 1))
    select * from singleton;
    update gevt g set ma = null, pa = null where g.id in (select s.id from singleton s where s.id != any(focus));
    delete from gevt g where g.id in (select t.ma from singleton t union select s.pa from singleton s) and g.id != any(focus);
    get diagnostics delcount = ROW_COUNT;
    raise notice 'dropped % singleton parents', delcount;
  end loop;
  delete from gevt g where g.ma is null and g.pa is null and not exists(select 1 from gevt h where (g.id = h.ma or g.id = h.pa));
  get diagnostics delcount = ROW_COUNT;
  raise notice 'removed % duds', delcount;
  -- return query select g.id, g.pa, g.ma, g.gender, case when d.id is not null then true else false end as proband 
  -- from gevt g left join dna d on g.id = d.id;
  return query select g.id, g.name, g.pa, g.ma, g.gender, case when g.id = any(focus) then true else false end as proband 
  from gevt g;
end;
$$;


ALTER FUNCTION public.ascend_trimlite(focus uuid[]) OWNER TO postgres;

--
-- Name: byteasub(bytea, integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.byteasub(bits bytea, fbyte integer, lbyte integer) RETURNS bytea
    LANGUAGE sql
    AS $$
select substring(bits,fbyte+1,lbyte);
$$;


ALTER FUNCTION public.byteasub(bits bytea, fbyte integer, lbyte integer) OWNER TO postgres;

--
-- Name: chase_segment_finder(text, integer, double precision); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.chase_segment_finder(pbsg text, chrm integer, cutoff double precision) RETURNS TABLE(chaser text)
    LANGUAGE plpgsql
    AS $$
begin
  return query        
  with chaseable as(
       select s.* 
       from segment s
            join probandset_group_member gm on s.probandset_id = gm.member_id
            join probandset_group g on gm.group_id = g.id
            where s.chrom = chrm 
                  and pv(s.events_less,s.events_equal,s.events_greater,1) < cutoff
                  and g.name = pbsg)
  select 
         s.id || ' ' ||
         s.chrom || ' ' ||
         m1.ordinal || ' ' ||
         m2.ordinal || ' ' ||
         s.lastmarker - s.firstmarker + 1 || ' '  ||
         s.startbase || ' ' ||
         s.endbase || ' ' ||
         s.events_less || ' ' ||
         s.events_equal || ' ' ||
         s.events_greater as chaser
  from chaseable s
       join markerset_member m1 on s.markerset_id = m1.markerset_id join marker k1 on m1.member_id = k1.id
       join markerset_member m2 on s.markerset_id = m2.markerset_id join marker k2 on m2.member_id = k2.id;
end;
$$;


ALTER FUNCTION public.chase_segment_finder(pbsg text, chrm integer, cutoff double precision) OWNER TO postgres;

--
-- Name: chase_segment_reduced(uuid, boolean); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.chase_segment_reduced(segid uuid, extend boolean) RETURNS text
    LANGUAGE plpgsql
    AS $$
declare
  fmark int;
  lmark int;
  chaser text;
begin
  select m.ordinal into fmark 
  from markerset_member m join segment s on m.markerset_id = s.markerset_id 
       join marker k on m.member_id = k.id
  where s.startbase = k.basepos and s.id = segid;
  --
  select m.ordinal into lmark 
  from markerset_member m join segment s on m.markerset_id = s.markerset_id 
       join marker k on m.member_id = k.id
  where s.endbase = k.basepos and s.id = segid;
  --
  if extend then
    select s.id || ' ' ||
           fmark || ' ' ||
           lmark || ' ' ||
           s.lastmarker - s.firstmarker + 1 || ' '  ||
           s.startbase || ' ' ||
           s.endbase || ' ' ||
           s.events_less || ' ' || 
           s.events_equal || ' ' ||
           s.events_greater || ' '
    into chaser
    from segment s
    where s.id = segid;
  else  
    select s.id || ' ' ||
           fmark || ' ' ||
           lmark || ' ' ||
           s.lastmarker - s.firstmarker + 1 || ' '  ||
           s.startbase || ' ' ||
           s.endbase || ' 0 0 0'
    into chaser
    from segment s
    where s.id = segid;
  end if;
--
  return chaser;        
end;
$$;


ALTER FUNCTION public.chase_segment_reduced(segid uuid, extend boolean) OWNER TO postgres;

--
-- Name: cytofinder(integer, integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.cytofinder(p_chrom integer, p_start integer, p_end integer) RETURNS text
    LANGUAGE plpgsql
    AS $$
declare 
cyto1 text;
cyto2 text;
begin
-- Look for cytoband that completely encloses segment
  select c.name
  from cytoband c
  into cyto1
  where c.chrom = 'chr'||p_chrom::text
  and c.chrom_start = (select max(chrom_start) from cytoband where chrom_start <= p_start and chrom = 'chr'||p_chrom::text);
  
  select c.name
  from cytoband c
  into cyto2
  where c.chrom = 'chr'||p_chrom::text
   and c.chrom_end = (select min(chrom_end) from cytoband where chrom_end >= p_end and chrom = 'chr'||p_chrom::text);
--
  if(cyto1=cyto2) then
     return p_chrom||cyto1;
   else
     return p_chrom||cyto1||'-'||cyto2;
   end if;
end;
$$;


ALTER FUNCTION public.cytofinder(p_chrom integer, p_start integer, p_end integer) OWNER TO postgres;

--
-- Name: descend(uuid); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.descend(founder uuid) RETURNS TABLE(id uuid, name text, pa uuid, ma uuid, gender character, genr integer)
    LANGUAGE sql
    AS $$
with recursive generation(id, name, pa, ma, gender, gen) as (
     select p.id, p.name, p.pa, p.ma, p.gender, 0 as gen from person p where p.id = founder
     union
     select b.id, b.name, b.pa, b.ma, b.gender, g.gen+1
     from person b, generation g where (g.gender = 'f' and g.id = b.ma) or (g.gender = 'm' and g.id = b.pa)
)
select * from generation order by gen;
$$;


ALTER FUNCTION public.descend(founder uuid) OWNER TO postgres;

--
-- Name: descend_wide(uuid); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.descend_wide(founder uuid) RETURNS TABLE(id uuid, name text, pa uuid, ma uuid, gender character, genr integer)
    LANGUAGE plpgsql
    AS $$
begin
drop table if exists genbuilder;
create temp table genbuilder as
with recursive generation(id, name, pa, ma, gender, gen) as (
     select p.id, p.name, p.pa, p.ma, p.gender, 0 as gen from person p where p.id = founder
     union
     select b.id, b.name, b.pa, b.ma, b.gender, g.gen+1
     from person b, generation g where (g.gender = 'f' and g.id = b.ma) or (g.gender = 'm' and g.id = b.pa)
)
select * from generation order by gen;
return query
select * from genbuilder 
       union distinct 
       select e.id, e.name, e.pa, e.ma, e.gender, g1.gen - 1 
       from  person e join genbuilder g1 on e.id = g1.pa or e.id = g1.ma 
             and not exists (select 1 from genbuilder g2 where g2.id = e.id)
       order by gen;
end;       
$$;


ALTER FUNCTION public.descend_wide(founder uuid) OWNER TO postgres;

--
-- Name: duopv(double precision, double precision); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.duopv(p1 double precision, p2 double precision) RETURNS double precision
    LANGUAGE sql
    AS $$
select p1*p2*(1-ln(p1*p2))::float;
$$;


ALTER FUNCTION public.duopv(p1 double precision, p2 double precision) OWNER TO postgres;

--
-- Name: extendpeople(text[], text[], text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.extendpeople(pnames text[], paliases text[], peep text) RETURNS void
    LANGUAGE plpgsql
    AS $$
declare
  rcount int;
  memberord int;
  idx int;
  aname text;
  nom text;
  personid uuid;
  peepid uuid;
begin
  select id into peepid from people where name = peep;
  select max(ordinal) into memberord from people_member where people_id = peepid;
  if peepid is null then
     raise exception 'Who are the %?', peep;
     return;
  end if;
  if paliases is not null and array_length(pnames, 1) != array_length(paliases, 1) then
     raise exception 'number of names(%) does not match aliases(%)', array_length(pnames), array_length(paliases);
     return;
  end if;
  idx = 0;
  foreach nom in array pnames loop
    aname = null;
    idx = idx + 1;
    select id into personid from person p where p.name = nom;
    if personid is null then
      raise exception '% is unknown', pname;
      -- select addNewPerson(name, paliases[idx], null, null, peepid);
    else
      if memberord is not null then
        memberord = memberord + 1;
      end if;
      insert into people_member values(uuid_generate_v4(), peepid, personid, memberord);
      if paliases is not null then
        aname = paliases[idx];
      end if;
      if aname is null then
        aname = nom;
      end if;
      raise notice 'Aliasing %', aname;
      insert into alias values(uuid_generate_v4(), personid, aname, peepid);
    end if;
  end loop;
end;
$$;


ALTER FUNCTION public.extendpeople(pnames text[], paliases text[], peep text) OWNER TO postgres;

--
-- Name: genome_fisher_fixed(uuid, uuid[]); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.genome_fisher_fixed(maint uuid, tids uuid[]) RETURNS TABLE(tseg1 uuid, tseg2 uuid, fisher_combined double precision)
    LANGUAGE plpgsql
    AS $$
declare
  rc           int;
  markersdone  int = 0;
  segn         int = 0;
  tid          uuid;
  mkset        uuid;
  fisher       float;
  t1           RECORD;
  t2           RECORD;
--  
begin
    raise notice '%: Off we go', clock_timestamp();
    create temp table fishers(tseg1 uuid, tseg2 uuid, fisher_combined float) on commit drop;
    -- we believe all input optimalset use the same markers
    for chrm in 1..22
    loop
        select distinct s.markerset_id into mkset from segment s 
               where exists(select * from optimalset_segment t where t.segment_id = s.id and t.optimalset_id = maint) and s.chrom = chrm;
--
--        
        create temp table tlist on commit drop as
        select t.id as threshid, v.id as tsid
               , v.smooth_pvalue
               , s.id as segid
               , s.firstmarker
               , s.lastmarker
               , s.lastmarker-s.firstmarker+1 as seglength
               , p.meioses
               , array_length(p.probands,1) as pbs
               , p.name as name
        from optimalset t
             join optimalset_segment v on t.id = v.optimalset_id
             join segment s on v.segment_id = s.id
             join probandset p on p.id = s.probandset_id
        where t.id = any (tids) and s.markerset_id = mkset;
        get diagnostics rc = row_count;
        raise notice '%: Starting % with % optimalset segments across %', clock_timestamp(), chrm, rc, array_to_string(tids,'/');
        create index on tlist(firstmarker, threshid);
        create index on tlist(lastmarker, threshid);
        create index on tlist(tsid);
        raise notice '%: indexing done', clock_timestamp();
--
        drop table if exists mkridx;
        create temp table mkridx on commit drop as
            select m.member_id, m.ordinal from markerset_member m where m.markerset_id = mkset;
        get diagnostics rc = row_count;
        raise notice '%: chromosome % has % markers', clock_timestamp(), chrm, rc;
        create index on mkridx(ordinal);
        --
        for t1 in
            select t.id as threshid
                   , v.id as tsid
                   , v.smooth_pvalue
                   , s.id as segid
                   , s.firstmarker
                   , s.lastmarker
                   , s.lastmarker-s.firstmarker+1 as seglength
                   , p.name
                   , p.meioses
                   , array_length(p.probands,1) as pbs
            from optimalset t
            join optimalset_segment v on t.id = v.optimalset_id
            join segment s on v.segment_id = s.id
            join probandset p on p.id = s.probandset_id
            where t.id = maint
            and s.markerset_id = mkset
            

            order by smooth_pvalue asc, coalesce(p.meioses,0) desc, pbs desc, seglength desc, p.name asc
        loop
        segn = 0;
--
        for t2 in
             -- collect all optimalset segment for the unfixed list of optimalset which overlap this fixed segment
             select * from tlist l
             where l.lastmarker >= t1.firstmarker and l.firstmarker <= t1.lastmarker
             order by l.smooth_pvalue asc, coalesce(l.meioses, 0) desc, l.pbs desc, l.seglength desc, l.name asc
            loop
                segn = segn + 1;
                -- raise notice '%: working with t2=%', clock_timestamp(), t2.tsid;
                -- still some markers for fixed segment
                select count(*) into rc from mkridx x where x.ordinal >= t1.firstmarker and x.ordinal <= t1.lastmarker;  
                if rc > 0 then
                -- if FOUND then
                   -- raise notice '%: Still % markers for boss %', clock_timestamp(), rc, t1.tsid;
                   delete from mkridx x 
                          where (x.ordinal >= t1.firstmarker and x.ordinal >= t2.firstmarker)
                          and (x.ordinal <= t1.lastmarker and x.ordinal <= t2.lastmarker);
                   get diagnostics rc = row_count;
                   if rc > 0 then
                       -- raise notice '%: accounted % for fixed segid = %, other segid = %', clock_timestamp(), rc, t1.segid, t2.segid;
                       markersdone = markersdone + rc;                       
                       fisher = duopv(t1.smooth_pvalue, t2.smooth_pvalue);
                       -- raise notice '%: pairing %,% accounts for %, fish = %, markersdone = %',
                       -- clock_timestamp(), t1.segid, t2.segid, rc, fisher, markersdone;
                       insert into fishers values(t1.tsid, t2.tsid, fisher);
                   -- else
                       -- raise notice '%: zero delete %/%', clock_timestamp(), t1.tsid, t2.tsid;
                   end if;
                else
                   -- raise notice 'early exit: zero markers left for %', t1.tsid;
                   exit;
                end if;
            end loop; --t2
            select count(*) into rc from mkridx;
            if rc = 0 then
               raise notice '%: zero markers left after % primary segments', clock_timestamp(), segn;
               exit;
            end if;
        end loop; -- t1
        -- select count(*) into rc from mkridx;
        raise notice '%: finished chromosome %, covered % markers', clock_timestamp(), chrm, markersdone;
        drop table if exists tlist;
    end loop; --chrom
    return query select * from fishers;
end;
$$;


ALTER FUNCTION public.genome_fisher_fixed(maint uuid, tids uuid[]) OWNER TO postgres;

--
-- Name: genome_pvalue_duo(text[], text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.genome_pvalue_duo(pbs_groups text[], markerset_pattern text) RETURNS TABLE(segment1_id uuid, ped1_id uuid, optval1 double precision, segment2_id uuid, ped2_id uuid, optval2 double precision, duo_optval double precision)
    LANGUAGE plpgsql
    AS $$
declare 
  rcount int := 0;
  markersetnames text[];
  setname text;
begin
  create temp table if not exists gwo(seg1 uuid, ped1 uuid, optval1 float, seg2 uuid, ped2 uuid, optval2 float, fisher_exact float) on commit drop;

  create temp table tlist on commit drop as
  select t.id as threshid
  	 , v.id as tsid
	 , v.smooth_pvalue
	 , s.id as segid
	 , s.firstmarker
	 , s.lastmarker
	 , s.lastmarker-s.firstmarker+1 as seglength
	 , p.name
 	 , p.meioses
	 , array_length(p.probands,1) as pbs
  from optimalset t
  join optimalset_segment v on t.id = v.optimalset_id
  join segment s on v.segment_id = s.id
  join probandset p on p.id = s.probandset_id
  where t.id = any (tids) and s.markerset_id = mkset;

  truncate gwo;
  raise notice 'peoples named:  %\nmarker pattern: %', array_to_string(pbs_groups, ', '), markerset_pattern;
  select array_agg(name) into markersetnames from markerset where name ~ markerset_pattern;
  foreach setname in array markersetnames loop
    insert into gwo 
    select distinct f.segment1_id, f.ped1_id, f.optval1, f.segment2_id, f.ped2_id , f.optval2,  duopv(f.optval1, f.optval2) as duo_optval
    from optimal_pvalue_mped(pbs_groups, setname)as f;
  end loop;
  return query select * from gwo;
end;
$$;


ALTER FUNCTION public.genome_pvalue_duo(pbs_groups text[], markerset_pattern text) OWNER TO postgres;

--
-- Name: genome_pvalue_mono(text, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.genome_pvalue_mono(people_name text, markers_rx text) RETURNS TABLE(segment_id uuid, base1 integer, pval double precision)
    LANGUAGE plpgsql
    AS $$
-- ------
-- single "people" genome wide best segments
-- ------
begin
  raise notice 'Defaulting confidence level to % and maximum interesting event count to %', 1.96, 10;
  return query select * from genome_pvalue_mono(people_name, markers_rx, 1.96, 10);
end;
$$;


ALTER FUNCTION public.genome_pvalue_mono(people_name text, markers_rx text) OWNER TO postgres;

--
-- Name: genome_pvalue_mono(text, text, double precision, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.genome_pvalue_mono(people_name text, markers_rx text, conf double precision, maxi integer) RETURNS TABLE(segment_id uuid, base1 integer, pval double precision)
    LANGUAGE plpgsql
    AS $$
-- ------
-- single "people" genome wide best segments
-- ------
declare
  loops int := 0;
  mvec RECORD;                
  markerset_id uuid;
  markerset_name text;
begin
  create temp table if not exists goptset(segment_id uuid, startbase int, pvalue float) on commit drop;
  truncate table goptset;
  for mvec in      
    select chrom, name
    from markerset where name ~ markers_rx
    order by chrom
  loop
    loops = loops+1;
    raise notice 'doing chrom %', mvec.chrom;
    insert into goptset
    select * from optimal_pvalue_mono(people_name, mvec.name, mvec.chrom, conf, maxi);
  end loop;
  if loops = 0 then
    raise exception 'No markersets found matching "%"', markers_rx;
  end if;
  return query select * from goptset;
end;
$$;


ALTER FUNCTION public.genome_pvalue_mono(people_name text, markers_rx text, conf double precision, maxi integer) OWNER TO postgres;

--
-- Name: genome_threshold_mono(text, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.genome_threshold_mono(pbs_name text, markers_rx text) RETURNS uuid
    LANGUAGE plpgsql
    AS $$
declare
  tid uuid;
begin
  select genome_threshold_mono(pbs_name, markers_rx, 1.96, 10) into tid;
  return tid;
end;
$$;


ALTER FUNCTION public.genome_threshold_mono(pbs_name text, markers_rx text) OWNER TO postgres;

--
-- Name: genome_threshold_mono(text, text, double precision, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.genome_threshold_mono(pbs_name text, markers_rx text, conf double precision, maxi integer) RETURNS uuid
    LANGUAGE plpgsql
    AS $$
declare
  pid uuid;        
  tid uuid;
  gids uuid[];  -- because threshold record uses an array of probandset group ids
  rows int;
  startAt timestamp;
begin
-- create a optimalset entry and load (mono) optimal set
  select clock_timestamp() into startAt;
  tid = uuid_generate_v4();
  select array_agg(id) into gids from probandset_group where name = pbs_name; 
  get diagnostics rows = ROW_COUNT;
  if rows = 0 then
     raise exception 'NO PROBANDSET_GROUP for %s', pbs_name;
  else
     raise notice 'group id is %', gids[1];
  end if;
-- process to ties ends together
  select threshold_process(tid, gids[1], markers_rx) into pid;
--
  insert into optimalset(id, suggestive, significant, name, probandset_groups)
         values(tid, 0.0, 0.0, pbs_name, gids);
  get diagnostics rows = ROW_COUNT;
  if rows = 1 then
    raise notice 'New optimalset: %', tid;
  else
    raise exception 'Failed to insert optimalset record';
  end if;
  insert into duo_segment(optimalset_id, segment1_id, duo_optval, id)
         select tid, f.segment_id, f.pval, uuid_generate_v4() 
         from genome_pvalue_mono(pbs_name, markers_rx, conf, maxi) as f;
  get diagnostics rows = ROW_COUNT;
  raise notice 'process % generated optval set of % segments at % (duration: %)', pid, rows, clock_timestamp(), clock_timestamp() - startAt;
  return tid;
end;
$$;


ALTER FUNCTION public.genome_threshold_mono(pbs_name text, markers_rx text, conf double precision, maxi integer) OWNER TO postgres;

--
-- Name: genome_threshold_multi(text[], text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.genome_threshold_multi(namepair text[], mrkr_rgx text) RETURNS void
    LANGUAGE plpgsql
    AS $$
declare
  groupids	uuid[];
  threshid	uuid;
  procid	uuid;
  rc		int;
begin
  -- create optimalset entry and load duo optimal set
  raise notice '%: === STARTING PAIR "%"', clock_timestamp(), array_to_string(namepair,'/');
  threshid = uuid_generate_v4();
  select array[a.id, b.id] into groupids
  from probandset_group a, probandset_group b 
  where a.name = namepair[1] 
        and b.name = namepair[2];
  insert into optimalset (id, suggestive, significant, name, probandset_groups)
        values (threshid, 0.0, 0.0, array_to_string(namepair,'/'), groupids);
  insert into duo_segment(id, optimalset_id, segment1_id, segment2_id, duo_optval)
    select uuid_generate_v4(), threshid, o.segment1_id, o.segment2_id, duopv( o.optval1, o.optval2) 
    from genome_pvalue_duo(namepair, mrkr_rgx) as o
    where o.segment1_id is not null;
  get diagnostics rc = row_count;
  procid = uuid_generate_v4();
    insert into process values(procid, current_user, 'optimalset_multi', now(), inet_server_addr()::text);
  insert into process_output values(uuid_generate_v4(), procid, 'optimalset', threshid);
  raise notice '%: added % duo optimals for %', clock_timestamp(), rc, array_to_string(namepair,'/');
end;
$$;


ALTER FUNCTION public.genome_threshold_multi(namepair text[], mrkr_rgx text) OWNER TO postgres;

--
-- Name: getgt_markers(integer, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.getgt_markers(first integer, last integer) RETURNS TABLE(id uuid, person_id uuid, markerset_id uuid, alleles text)
    LANGUAGE plpgsql
    AS $$
declare
begin
  return query 
  select g.id
         , g.person_id
         , g.markerset_id
         , substring(encode(g.calls, 'hex'), 2*first+1, (2*(last-first+1)))
  from genotype g;
end;
$$;


ALTER FUNCTION public.getgt_markers(first integer, last integer) OWNER TO postgres;

--
-- Name: getgt_segment(uuid); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.getgt_segment(segid uuid) RETURNS TABLE(person_id uuid, markerset_id uuid, alleles text)
    LANGUAGE plpgsql
    AS $$
declare
  startindex int;
  endindex int;
  mksid uuid;
  pbids uuid[];
begin
  select s.markerset_id, s.firstmarker, s.lastmarker 
  into mksid,startindex,endindex
  from segment s where id = segid;
  select probands into pbids from segment s join probandset b on s.probandset_id = b.id where s.id = segid;
  return query
  -- markerset is indexed from zero; postgres strings are indexed from 1, substring uses offset and length
  select g.person_id
         , g.markerset_id
         , substring(encode(g.calls,'hex'), 2*(startindex+1), (2*(endindex-startindex+1)))
  from genotype g
  where g.markerset_id = mksid
        and g.person_id = any(pbids);
end;
$$;


ALTER FUNCTION public.getgt_segment(segid uuid) OWNER TO postgres;

--
-- Name: idset_lookup(text[]); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.idset_lookup(aids text[]) RETURNS uuid
    LANGUAGE plpgsql IMMUTABLE
    AS $$
declare
        setid uuid;
        tids uuid[];
begin
        select array_agg(p.id) into tids
        from seg.person as p
             join seg.alias as a on p.id = a.person_id
        where a.aka = any(aids);
--
        if array_length(tids,1) != array_length(aids,1)
        then
                raise exception 'failed to find all person ids';
        end if;
--
        select s.id into setid
        from seg.probandset s
        where s.probands = sortuuidvector(tids);
--
        return setid;
end;
$$;


ALTER FUNCTION public.idset_lookup(aids text[]) OWNER TO postgres;

--
-- Name: interesting_segments(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.interesting_segments() RETURNS TABLE(name text, interest text, band text, n bigint)
    LANGUAGE plpgsql
    AS $$
begin	 
  with interest as (
  select 
       t.id
       ,t.name
       , case when d.fisher_pvalue <= t.significant then 'SIG'
	     when d.fisher_pvalue between t.significant and t.suggestive then 'SUG'
	     end as interest
       , d.optimalset_segment1_id
       from optimalset t 
	    join optimalset_duo_segment d on t.id = d.optimalset_id
       where d.fisher_pvalue <= t.suggestive)
  select i.name
	 , i.interest
	 , cytofinder(s.chrom, s.startbase, s.endbase) as band
	 ,count(*)
  from interest i 
       join optimalset_segment ts on i.optimalset_segment1_id = ts.id
       join segment s on ts.segment_id = s.id
  group by name, band, interest;
end;
$$;


ALTER FUNCTION public.interesting_segments() OWNER TO postgres;

--
-- Name: issorteduuids(uuid[]); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.issorteduuids(uset uuid[]) RETURNS boolean
    LANGUAGE plpgsql IMMUTABLE
    AS $$
declare
   sset uuid[];
begin
   select array_agg( u order by u) from unnest(uset) as t(u) into sset;
   return sset = uset;
end;
$$;


ALTER FUNCTION public.issorteduuids(uset uuid[]) OWNER TO postgres;

--
-- Name: nameset_lookup(uuid[]); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.nameset_lookup(aids uuid[]) RETURNS text[]
    LANGUAGE plpgsql IMMUTABLE
    AS $$
declare
        tids text[];
begin
        select array_agg(p.name) into tids
        from seg.person as p
        where p.id = any(aids);
--
        if array_length(tids,1) != array_length(aids,1)
        then
                raise exception 'failed to find name for all person ids';
        end if;
--
        return tids;
end;
$$;


ALTER FUNCTION public.nameset_lookup(aids uuid[]) OWNER TO postgres;

--
-- Name: optimal_pvalue_mono(text, text, integer, double precision, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.optimal_pvalue_mono(pbsg_name text, markers_name text, chr integer, conf double precision, maxi integer) RETURNS TABLE(segment_id uuid, base1 integer, pval double precision)
    LANGUAGE plpgsql
    AS $$
-- ------
-- single "people" best segments for given chrom
-- ------
declare
  segp          RECORD;
  mkset         uuid;
  pbsgid        uuid;
  rcount        int;
  mkrcnt        int;
  segsdone      int  :=  0;
  totalinserts  int  :=  0;
-- ------
begin
  select id into mkset from markerset where name = markers_name and chrom = chr; 
  select id into pbsgid from probandset_group where name = pbsg_name;
  if pbsgid is null then
     raise exception 'NO SUCH probandset_group name: %', pbsg_name;
  end if;
  raise notice '%: markerset id is %(%), people (%) id is %', clock_timestamp(), mkset, markers_name, pbsgid, pbsg_name;
--
  drop table if exists collected;	
  drop table if exists mrkidx;
--
  create temp table if not exists imputed_pvalue_t(segment_id uuid, probandset_group_id uuid, ipv float) on commit drop;
  create temp table collected(segid uuid, cpv float) on commit drop;
  create temp table mrkidx on commit drop as
    select member_id as marker_id, ordinal 
    from markerset_member where markerset_id = mkset;
  get diagnostics mkrcnt = ROW_COUNT;
  create unique index on mrkidx(ordinal);
  raise notice '%: working with % markers', clock_timestamp(), mkrcnt;
--
  insert into imputed_pvalue_t (segment_id, probandset_group_id, ipv)
  select s.id,
         pbsgid,
         case when s.events_equal + s.events_greater <= maxi 
              then rand_pv( wilson_ci(conf,  s.events_equal+s.events_greater, s.events_less+s.events_equal+s.events_greater))
         else 
              pv(s.events_less, s.events_equal, s.events_greater, 0)
         end
  from segment s join probandset b on s.probandset_id = b.id
       join probandset_group_member m on b.id = m.member_id
  where s.markerset_id = mkset 
        and m.group_id = pbsgid;
--
  get diagnostics rcount = ROW_COUNT;
  raise notice '%: added % segments to imputed_pvalue_t', clock_timestamp(), rcount;
--  
  for segp in
    select s.id, s.firstmarker, s.lastmarker, 
           v.ipv,
           array_length(p.probands,1) as pbs,
           s.lastmarker - s.firstmarker + 1 as mks
    from segment s 
         join imputed_pvalue_t v on s.id = v.segment_id
         join probandset p on s.probandset_id = p.id
         join probandset_group_member m on p.id = m.member_id
    where s.markerset_id = mkset
          and m.group_id = pbsgid
    order by ipv asc, p.meioses desc, pbs desc, mks desc, p.name asc
  LOOP
    delete from mrkidx where ordinal between segp.firstmarker and segp.lastmarker;
    get diagnostics rcount = ROW_COUNT;
    segsdone = segsdone + 1;
    if rcount > 0 then
       insert into collected values(segp.id, segp.ipv);
       totalinserts = totalinserts + rcount;
       if totalinserts = mkrcnt then  -- really totalDELETES
          raise notice '%: no markers left on %th segment %', clock_timestamp(), segsdone, segp.id;
          exit;
       end if;
    end if;
  end loop;
  select count(*) into rcount from mrkidx;
  raise notice '%: finished assessing markers; % leftover', clock_timestamp(), rcount;
  truncate imputed_pvalue_t;
--
  return query
    select s.id as segment_id, s.startbase as base1, c.cpv
    from segment as s join collected as c on s.id = c.segid
    order by s.startbase;
end;
$$;


ALTER FUNCTION public.optimal_pvalue_mono(pbsg_name text, markers_name text, chr integer, conf double precision, maxi integer) OWNER TO postgres;

--
-- Name: optimal_pvalue_mped(text[], text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.optimal_pvalue_mped(group_names text[], markersetname text) RETURNS TABLE(marker_id uuid, marker_name text, basepos integer, segment1_id uuid, ped1_id uuid, optval1 double precision, segment2_id uuid, ped2_id uuid, optval2 double precision)
    LANGUAGE plpgsql
    AS $$
-- ------
-- For one chromosome get two best segments per marker
-- -----
declare
  msetid uuid;
  groups uuid[];
  rowscount int;
  markercount int;
  updatecount int := 0;
  seg1count int := 0;
  seg2count int := 0;
  seg record;
begin
  select id into msetid from markerset where name = markersetname;
  select array_agg(id) into groups from probandset_group where name = any(group_names);
-- check length
  raise notice '%: markerset id is %, group ids are %', clock_timestamp(), msetid, array_to_string(groups,'/','nil');
--
  create temp table if not exists optmarkers (
         mkrid uuid,
         marker_name text,
         marker_base integer,
         segment1_id uuid,
         group1_id uuid,  --should be 'group'
         optval1 float,
         segment2_id uuid,
         group2_id uuid,
         optval2 float)on commit drop;
  truncate optmarkers;
  insert into optmarkers
  select m.id as mkrid
    ,m.name as marker_name
    ,m.basepos as marker_base
    ,null::uuid as segment1_id
    ,null::uuid as group1_id
    ,null::float as optval1
    ,null::uuid as segment2_id
    ,null::uuid as group2_id
    ,null::float as optval2
  from marker m join markerset_member mm on m.id = member_id
  where mm.markerset_id = msetid;
  get diagnostics markercount = ROW_COUNT;
  create unique index on optmarkers(marker_base);
  raise notice '%: markerset preped, % markers total', clock_timestamp(), markercount;
--
  for seg in
      select s.id, s.startbase, s.endbase, m.group_id
             , pv(s.events_less, s.events_equal, s.events_greater, 1) as spv
             , array_length(b.probands, 1) as pbs
             , s.endbase - s.startbase as bases

      from segment s join probandset b on s.probandset_id = b.id 
                     join probandset_group_member m on b.id = m.member_id and m.group_id = any(groups)
      where s.markerset_id = msetid
      order by spv, pbs, bases
  loop
    seg1count = seg1count +1;
    update optmarkers as o 
           set segment1_id = seg.id, 
               group1_id = seg.group_id, 
               optval1 = seg.spv
    where o.marker_base >= seg.startbase 
          and o.marker_base <= seg.endbase 
          and o.segment1_id is null;
    get diagnostics rowscount = ROW_COUNT;
    updatecount = updatecount + rowscount;
    if updatecount >= markercount then
      raise notice '%: all markers accounted for once at % segments', clock_timestamp(), seg1count;
      exit;
    end if;
  end loop;
--
  raise notice '%: first pass completed, % markers assigned a pval', clock_timestamp(), updatecount;
  updatecount = 0;
  for seg in
      select s.id, s.startbase, s.endbase, m.group_id
             , pv(s.events_less, s.events_equal, s.events_greater, 1) as spv
             , array_length(b.probands,1) as pbs
             , s.endbase - s.startbase as bases
      from segment s join probandset b on s.probandset_id = b.id join probandset_group_member m on b.id = m.member_id and m.group_id = any(groups)
      where s.markerset_id = msetid
      order by spv, pbs, bases
  loop
    seg2count = seg2count +1;
    update optmarkers as o 
    set segment2_id = seg.id, 
        group2_id = seg.group_id, 
        optval2 = seg.spv
    where o.marker_base between seg.startbase and seg.endbase
       and o.segment2_id is null
       and seg.group_id != o.group1_id;
    get diagnostics rowscount = ROW_COUNT;
    updatecount = updatecount + rowscount;
    if updatecount >= markercount then
      raise notice '%: all markers in accounted for twice after % segments', clock_timestamp(), seg2count;
      exit;
    end if;
  end loop;
  raise notice '%: second pass completed, % markers assigned a pval', clock_timestamp(), updatecount;
--
  return query select * from optmarkers;
end;
$$;


ALTER FUNCTION public.optimal_pvalue_mped(group_names text[], markersetname text) OWNER TO postgres;

--
-- Name: powerset(anyarray); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.powerset(a anyarray) RETURNS SETOF anyarray
    LANGUAGE plpgsql IMMUTABLE STRICT
    AS $$
declare
retval a%type;
alower	integer := array_lower(a,1);
aupper	integer := array_upper(a,1);
j integer;
k integer;
begin
for i in 0 .. (1 << (aupper - alower + 1)) - 1 LOOP
	retval := '{}';
	j:= alower;
	k :=i;
--	
	while k > 0 loop
		if k & 1 = 1 then
			retval := array_append(retval, a[j]);
		end if;
		
		j := j + 1;
		k := k >> 1;
	end loop;
--
	return next retval;
end loop;
return;
end;
$$;


ALTER FUNCTION public.powerset(a anyarray) OWNER TO postgres;

--
-- Name: pv(bigint, bigint, bigint, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.pv(l bigint, e bigint, g bigint, o integer) RETURNS double precision
    LANGUAGE sql
    AS $$
select 1.0*(g+e+o)/(l+e+g+o)::float;
$$;


ALTER FUNCTION public.pv(l bigint, e bigint, g bigint, o integer) OWNER TO postgres;

--
-- Name: pv_lower95(bigint, bigint, bigint, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.pv_lower95(l bigint, e bigint, g bigint, o integer) RETURNS double precision
    LANGUAGE plpgsql
    AS $$
declare
  pv double precision;
  ll double precision;
begin
       pv:=1.0*(g+e+o)/(l+e+g+o);
       ll:=pv-1.96*sqrt((pv/(l+e+g))*(1-pv));
       return ll;
end;
$$;


ALTER FUNCTION public.pv_lower95(l bigint, e bigint, g bigint, o integer) OWNER TO postgres;

--
-- Name: pv_upper95(bigint, bigint, bigint, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.pv_upper95(l bigint, e bigint, g bigint, o integer) RETURNS double precision
    LANGUAGE plpgsql
    AS $$
declare
  pv double precision;
  ul double precision;
begin
       pv:=1.0*(g+e+o)/(l+e+g+o);
       ul:=pv+1.96*sqrt((pv/(l+e+g))*(1-pv));
       return ul;
end;
$$;


ALTER FUNCTION public.pv_upper95(l bigint, e bigint, g bigint, o integer) OWNER TO postgres;

--
-- Name: rand_pv(double precision[]); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.rand_pv(bounds double precision[]) RETURNS double precision
    LANGUAGE sql
    AS $$
select random()*(bounds[2]-bounds[1]) + bounds[1];
$$;


ALTER FUNCTION public.rand_pv(bounds double precision[]) OWNER TO postgres;

--
-- Name: remove_people(text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.remove_people(pname text) RETURNS void
    LANGUAGE plpgsql
    AS $$
declare 
  segmentsetid uuid;        
  peopleid uuid;
  pbsgroupid uuid;
  rowc int;
begin
  select id into peopleid from people  where name = pname;
  if peopleid is null then
    raise exception 'No such people: %', pname;
  else
    raise notice '% id is %', pname, peopleid;
  end if;
  -- select id into segmentsetid from segmentset s where exists (
  --        select 1 from people p join probandset b on p.id = b.people_id 
  --                 join segment t on b.id = t.probandset_id
  --                 join segmentset_member m on t.id = m.segment_id 
  --                 where m.segmentset_id = s.id)
--
  delete from segmentset_member m
         using segmentset ss, projectfile f
         where m.segmentset_id = ss.id and ss.pedfile_id = f.id and f.people_id = peopleid;
  get diagnostics rowc = ROW_COUNT;
  raise notice 'Culling % segmentset members', rowc;
  delete from segmentset ss
         using projectfile f
         where ss.pedfile_id = f.id and f.people_id = peopleid;
--
  delete from projectfile where people_id = peopleid;      
  get diagnostics rowc = ROW_COUNT;
  raise notice 'delete % files for %', rowc, pname;
--  
  select g.id into pbsgroupid from probandset_group g join probandset s on g.proband_superset_id = s.id and s.people_id = peopleid;
  get diagnostics rowc = ROW_COUNT;
  if pbsgroupid is null then
    raise notice 'Did not find a proband superset';
  end if;
--  
  create temp table peeps on commit drop as 
         select person_id as killid from people_member where people_id = peopleid;
  get diagnostics rowc = ROW_COUNT;
  raise notice 'People % has % members', pname, rowc;
--
  delete from peeps s where exists 
  (select 1 from people_member m where m.person_id = s.killid and m.people_id != peopleid);
  get diagnostics rowc = ROW_COUNT;
  raise notice '% persons exits in at least one other people, not removed', rowc;
--  
  create temp table segid on commit drop as
  select distinct s.id from segment s join probandset b on s.probandset_id = b.id 
         where b.people_id = peopleid;
  get diagnostics rowc = ROW_COUNT;
  raise notice 'Dealing with % segments', rowc;
  create unique index on segid(id);
  delete from duo_segment d using segid s where d.segment1_id = s.id;
  get diagnostics rowc = ROW_COUNT;
  raise notice 'Culling % duos 1', rowc;
  delete from duo_segment d using segid s where d.segment2_id = s.id;
  get diagnostics rowc = ROW_COUNT;
  raise notice 'Culling % duos 2', rowc;
  delete from chaseable c using segid s where c.segment_id = s.id;
  get diagnostics rowc = ROW_COUNT;
  raise notice 'Culling % chaseables', rowc;
  
  delete from segment t using segid i where t.id = i.id;
  get diagnostics rowc = ROW_COUNT;
  raise notice 'Culling % segments', rowc;
--
  delete from threshold t where pbsgroupid = any(t.probandset_groups);
  delete from probandset_group_member where group_id = pbsgroupid;
  delete from probandset_group where id = pbsgroupid;
  delete from probandset where people_id = peopleid;
  get diagnostics rowc = ROW_COUNT;
  raise notice 'Culling % probandsets', rowc;
--
  delete from people_member where people_id = peopleid;
  get diagnostics rowc = ROW_COUNT;
  raise notice 'Culling % people members', rowc;
--
  delete from alias where people_id = peopleid;
--
  delete from person p where exists (select 1 from peeps s where s.killid = p.id);
  get diagnostics rowc = ROW_COUNT;
  raise notice 'Culling % persons', rowc;
--
  delete from people where id = peopleid;
end;
$$;


ALTER FUNCTION public.remove_people(pname text) OWNER TO postgres;

--
-- Name: rollback_ma(uuid); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.rollback_ma(procid uuid) RETURNS void
    LANGUAGE plpgsql
    AS $$
declare 
  mksetid uuid;
  peepid uuid;
  rowcount int;
  deletables_found int;
  membercount int;
  setcount int;
  othertypes text;
begin
  select count(*) into rowcount from process where id = procid;
  if rowcount != 1 then
    raise exception 'Did not find exactly ONE process by id %. % actually', procid, rowcount;
  end if;
--
  select array_to_string(array_agg(distinct output_type), ' ,', 'nil') into othertypes
  from process_output 
  where process_id = procid and output_type != 'segmentset';
--
  if not othertypes is null then
    raise exception 'Cannot drop process with output type(s) %', othertypes;
  end if;
  delete from process_output where process_id = procid;  
  get diagnostics rowcount = ROW_COUNT;
  raise notice '%: % outputs dropped', clock_timestamp(), rowcount;
  delete from process_input where process_id = procid;
  get diagnostics rowcount = ROW_COUNT;
  raise notice '%: % inputs dropped', clock_timestamp(), rowcount;
  delete from process_arg where process_id = procid;
  get diagnostics rowcount = ROW_COUNT;
  raise notice '%: % args dropped', clock_timestamp(), rowcount;
  delete from process where id = procid;
--
  raise notice '%: Process % deleted', clock_timestamp(), procid;
end;
$$;


ALTER FUNCTION public.rollback_ma(procid uuid) OWNER TO postgres;

--
-- Name: segment_calls(uuid); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.segment_calls(segid uuid) RETURNS TABLE(name text, firsti integer, lasti integer, calls text)
    LANGUAGE plpgsql
    AS $$
begin
select 
    p.name, 
    s.firstmarker, 
    s.lastmarker, 
    regexp_replace(substr(g.calls,1+(2*s.firstmarker), 2*(s.lastmarker-s.firstmarker+1))::text, '(..)', ' \1','g') as calls
from
    segment s
    join probandset b on s.probandset_id = b.id
    join people l on b.people_id = l.id
    join people_member m on l.id = m.people_id
    join person p on m.person_id = p.id
    join genotype g on g.markerset_id = s.markerset_id and g.person_id = p.id
where s.id = segid;
end;
$$;


ALTER FUNCTION public.segment_calls(segid uuid) OWNER TO postgres;

--
-- Name: threshold_process(uuid, uuid, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.threshold_process(thresholdid uuid, pbsgid uuid, marker_rgx text) RETURNS uuid
    LANGUAGE plpgsql
    AS $$
declare
  pid uuid;
begin
  pid = uuid_generate_v4();
  insert into process values( pid, current_user, 'optimal set generation', clock_timestamp(), inet_client_addr()::text);
  insert into process_input values (uuid_generate_v4(), pid, 'probandset_group', pbsgid);
  insert into process_output values (uuid_generate_v4(), pid, 'optimalset', thresholdid);
  insert into process_arg values (uuid_generate_v4(), pid, 'marker regex', null, null, marker_rgx);
  return pid;
end;
$$;


ALTER FUNCTION public.threshold_process(thresholdid uuid, pbsgid uuid, marker_rgx text) OWNER TO postgres;

--
-- Name: wilson_ci(double precision, bigint, bigint); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.wilson_ci(z double precision, iev bigint, tev bigint) RETURNS double precision[]
    LANGUAGE plpgsql IMMUTABLE STRICT
    AS $$
declare
  p float;
  q float;
  ex1 float;
  ex2 float;
  dex float;
  ucl float;
  lcl float;
  ci float[];
begin
  p := (iev*1.0)/tev;
  ex1 := ((iev) + (power(z,2)/2))/((tev) + power(z,2));
  ex2 := (z/((tev)+power(z,2))) * sqrt((tev)*((p*(1-p)) + (power(z,2)/(4*(tev)))));
  dex = ex1 - ex2;
  if (dex) < 0.0 then ci[1] = 0.0; else ci[1] = dex; end if;
  ci[2] := ex1 + ex2;
  return ci;
end;
$$;


ALTER FUNCTION public.wilson_ci(z double precision, iev bigint, tev bigint) OWNER TO postgres;

--
-- Name: worklist_fill(text, double precision); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.worklist_fill(groupname text, maxpv double precision) RETURNS void
    LANGUAGE plpgsql
    AS $$
declare
  workitems int;
begin
  insert into chaseable
  select uuid_generate_v4() as id, s.id as segment_id , t.id as optimalset_id 
  from segment s
      join probandset p on s.probandset_id = p.id 
      join probandset_group_member m on p.id = m.member_id 
      join probandset_group g on m.group_id = g.id 
      join optimalset t on array[g.id] = t.probandset_groups
  where 
      g.name = groupname
      and (
        pv(s.events_less, s.events_equal, s.events_greater, 0) = 0
        or (
            pv(s.events_less, s.events_equal, s.events_greater, 0) <= maxpv
            and (
                ( pv_upper95(s.events_less, s.events_equal, s.events_greater, 0) > t.significant 
                  and pv_lower95(s.events_less, s.events_equal, s.events_greater, 0) < t.significant
                ) 
                or 
                ( pv_upper95(s.events_less, s.events_equal, s.events_greater, 0) > t.suggestive 
                  and pv_lower95(s.events_less, s.events_equal, s.events_greater, 0) < t.suggestive 
                )
            )
          )
        );
  get diagnostics workitems = ROW_COUNT;
  raise notice 'added % worklist items for %', workitems, groupname;
end;
$$;


ALTER FUNCTION public.worklist_fill(groupname text, maxpv double precision) OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: alias; Type: TABLE; Schema: base; Owner: postgres
--

CREATE TABLE base.alias (
    id uuid NOT NULL,
    person_id uuid NOT NULL,
    aka text NOT NULL,
    people_id uuid,
    akadef_id uuid
);


ALTER TABLE base.alias OWNER TO postgres;

--
-- Name: cytoband; Type: TABLE; Schema: base; Owner: postgres
--

CREATE TABLE base.cytoband (
    chrom text,
    chrom_start integer,
    chrom_end integer,
    name text,
    gie_stain text
);


ALTER TABLE base.cytoband OWNER TO postgres;

--
-- Name: definer; Type: TABLE; Schema: base; Owner: postgres
--

CREATE TABLE base.definer (
    id uuid NOT NULL,
    name text NOT NULL,
    description text
);


ALTER TABLE base.definer OWNER TO postgres;

--
-- Name: definervalue; Type: TABLE; Schema: base; Owner: postgres
--

CREATE TABLE base.definervalue (
    id uuid NOT NULL,
    valuestring text,
    valueint integer,
    valuefloat double precision,
    definer_id uuid NOT NULL,
    description text,
    CONSTRAINT forcenonnull CHECK ((COALESCE((valuefloat)::text, (valueint)::text, valuestring) IS NOT NULL))
);


ALTER TABLE base.definervalue OWNER TO postgres;

--
-- Name: definerview; Type: VIEW; Schema: base; Owner: postgres
--

CREATE VIEW base.definerview AS
 SELECT v.id,
    COALESCE((v.valuefloat)::text, (v.valueint)::text, v.valuestring) AS defvalue,
    d.name
   FROM (base.definer d
     JOIN base.definervalue v ON ((d.id = v.definer_id)))
  ORDER BY d.name, COALESCE((v.valuefloat)::text, (v.valueint)::text, v.valuestring);


ALTER TABLE base.definerview OWNER TO postgres;

--
-- Name: genome_markerset; Type: TABLE; Schema: base; Owner: postgres
--

CREATE TABLE base.genome_markerset (
    id uuid NOT NULL,
    name text NOT NULL,
    description text,
    primary_build integer,
    CONSTRAINT build_check CHECK ((primary_build = ANY (ARRAY[37, 38])))
);


ALTER TABLE base.genome_markerset OWNER TO postgres;

--
-- Name: ld_clique; Type: TABLE; Schema: base; Owner: postgres
--

CREATE TABLE base.ld_clique (
    markerset_id uuid NOT NULL,
    ordinal integer NOT NULL,
    loci_ordinals integer[],
    potential double precision[]
);


ALTER TABLE base.ld_clique OWNER TO postgres;

--
-- Name: marker; Type: TABLE; Schema: base; Owner: postgres
--

CREATE TABLE base.marker (
    id uuid NOT NULL,
    name text NOT NULL,
    chrom integer NOT NULL,
    basepos37 integer,
    alleles character(1)[],
    basepos38 integer,
    unknown_allele text DEFAULT 'nospec'::text
);


ALTER TABLE base.marker OWNER TO postgres;

--
-- Name: markerset; Type: TABLE; Schema: base; Owner: postgres
--

CREATE TABLE base.markerset (
    id uuid NOT NULL,
    name text NOT NULL,
    chrom integer NOT NULL,
    genome_id uuid,
    avg_theta double precision,
    std_theta double precision
);


ALTER TABLE base.markerset OWNER TO postgres;

--
-- Name: markerset_member; Type: TABLE; Schema: base; Owner: postgres
--

CREATE TABLE base.markerset_member (
    markerset_id uuid NOT NULL,
    member_id uuid NOT NULL,
    ordinal integer NOT NULL,
    theta double precision
);


ALTER TABLE base.markerset_member OWNER TO postgres;

--
-- Name: migration; Type: TABLE; Schema: base; Owner: postgres
--

CREATE TABLE base.migration (
    id integer NOT NULL,
    file text,
    project text
);


ALTER TABLE base.migration OWNER TO postgres;

--
-- Name: migration_id_seq; Type: SEQUENCE; Schema: base; Owner: postgres
--

ALTER TABLE base.migration ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME base.migration_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: people; Type: TABLE; Schema: base; Owner: postgres
--

CREATE TABLE base.people (
    id uuid NOT NULL,
    name text NOT NULL,
    typedef_id uuid
);


ALTER TABLE base.people OWNER TO postgres;

--
-- Name: people_member; Type: TABLE; Schema: base; Owner: postgres
--

CREATE TABLE base.people_member (
    id uuid NOT NULL,
    people_id uuid NOT NULL,
    person_id uuid NOT NULL,
    ordinal integer
);


ALTER TABLE base.people_member OWNER TO postgres;

--
-- Name: person; Type: TABLE; Schema: base; Owner: postgres
--

CREATE TABLE base.person (
    id uuid NOT NULL,
    ma uuid,
    pa uuid,
    name text,
    gender character(1),
    CONSTRAINT mfgender CHECK ((gender = ANY (ARRAY['m'::bpchar, 'f'::bpchar])))
);


ALTER TABLE base.person OWNER TO postgres;

--
-- Name: flyway_schema_history; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.flyway_schema_history (
    installed_rank integer NOT NULL,
    version character varying(50),
    description character varying(200) NOT NULL,
    type character varying(20) NOT NULL,
    script character varying(1000) NOT NULL,
    checksum integer,
    installed_by character varying(100) NOT NULL,
    installed_on timestamp without time zone DEFAULT now() NOT NULL,
    execution_time integer NOT NULL,
    success boolean NOT NULL
);


ALTER TABLE public.flyway_schema_history OWNER TO postgres;

--
-- Name: alias alias_pkey; Type: CONSTRAINT; Schema: base; Owner: postgres
--

ALTER TABLE ONLY base.alias
    ADD CONSTRAINT alias_pkey PRIMARY KEY (id);


--
-- Name: alias aliasperpeep; Type: CONSTRAINT; Schema: base; Owner: postgres
--

ALTER TABLE ONLY base.alias
    ADD CONSTRAINT aliasperpeep UNIQUE (aka, people_id);


--
-- Name: definer definer_name_key; Type: CONSTRAINT; Schema: base; Owner: postgres
--

ALTER TABLE ONLY base.definer
    ADD CONSTRAINT definer_name_key UNIQUE (name);


--
-- Name: definer definer_pkey; Type: CONSTRAINT; Schema: base; Owner: postgres
--

ALTER TABLE ONLY base.definer
    ADD CONSTRAINT definer_pkey PRIMARY KEY (id);


--
-- Name: definervalue definervalue_pkey; Type: CONSTRAINT; Schema: base; Owner: postgres
--

ALTER TABLE ONLY base.definervalue
    ADD CONSTRAINT definervalue_pkey PRIMARY KEY (id);


--
-- Name: genome_markerset genome_markerset_pkey; Type: CONSTRAINT; Schema: base; Owner: postgres
--

ALTER TABLE ONLY base.genome_markerset
    ADD CONSTRAINT genome_markerset_pkey PRIMARY KEY (id);


--
-- Name: ld_clique ld_clique_pkey; Type: CONSTRAINT; Schema: base; Owner: postgres
--

ALTER TABLE ONLY base.ld_clique
    ADD CONSTRAINT ld_clique_pkey PRIMARY KEY (markerset_id, ordinal);


--
-- Name: marker marker_name_key; Type: CONSTRAINT; Schema: base; Owner: postgres
--

ALTER TABLE ONLY base.marker
    ADD CONSTRAINT marker_name_key UNIQUE (name);


--
-- Name: marker marker_pkey; Type: CONSTRAINT; Schema: base; Owner: postgres
--

ALTER TABLE ONLY base.marker
    ADD CONSTRAINT marker_pkey PRIMARY KEY (id);


--
-- Name: markerset markerset_pkey; Type: CONSTRAINT; Schema: base; Owner: postgres
--

ALTER TABLE ONLY base.markerset
    ADD CONSTRAINT markerset_pkey PRIMARY KEY (id);


--
-- Name: people_member people_member_pkey; Type: CONSTRAINT; Schema: base; Owner: postgres
--

ALTER TABLE ONLY base.people_member
    ADD CONSTRAINT people_member_pkey PRIMARY KEY (id);


--
-- Name: people people_pkey; Type: CONSTRAINT; Schema: base; Owner: postgres
--

ALTER TABLE ONLY base.people
    ADD CONSTRAINT people_pkey PRIMARY KEY (id);


--
-- Name: person person_pkey; Type: CONSTRAINT; Schema: base; Owner: postgres
--

ALTER TABLE ONLY base.person
    ADD CONSTRAINT person_pkey PRIMARY KEY (id);


--
-- Name: markerset_member set_ordinal; Type: CONSTRAINT; Schema: base; Owner: postgres
--

ALTER TABLE ONLY base.markerset_member
    ADD CONSTRAINT set_ordinal PRIMARY KEY (markerset_id, ordinal);


--
-- Name: marker umbc; Type: CONSTRAINT; Schema: base; Owner: postgres
--

ALTER TABLE ONLY base.marker
    ADD CONSTRAINT umbc UNIQUE (basepos37, chrom);


--
-- Name: flyway_schema_history flyway_schema_history_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.flyway_schema_history
    ADD CONSTRAINT flyway_schema_history_pk PRIMARY KEY (installed_rank);


--
-- Name: def_value; Type: INDEX; Schema: base; Owner: postgres
--

CREATE UNIQUE INDEX def_value ON base.definervalue USING btree (definer_id, COALESCE((valuefloat)::text, (valueint)::text, valuestring));


--
-- Name: ld_clique_ukey; Type: INDEX; Schema: base; Owner: postgres
--

CREATE UNIQUE INDEX ld_clique_ukey ON base.ld_clique USING btree (markerset_id, ordinal);


--
-- Name: ma_idx; Type: INDEX; Schema: base; Owner: postgres
--

CREATE INDEX ma_idx ON base.person USING btree (ma);


--
-- Name: marker_chrom_basepos_idx; Type: INDEX; Schema: base; Owner: postgres
--

CREATE UNIQUE INDEX marker_chrom_basepos_idx ON base.marker USING btree (chrom, basepos37);


--
-- Name: markerset_member_member_id_idx; Type: INDEX; Schema: base; Owner: postgres
--

CREATE INDEX markerset_member_member_id_idx ON base.markerset_member USING btree (member_id);


--
-- Name: pa_idx; Type: INDEX; Schema: base; Owner: postgres
--

CREATE INDEX pa_idx ON base.person USING btree (pa);


--
-- Name: people_member_people_id_idx; Type: INDEX; Schema: base; Owner: postgres
--

CREATE INDEX people_member_people_id_idx ON base.people_member USING btree (people_id);


--
-- Name: people_member_person_id_people_id_idx; Type: INDEX; Schema: base; Owner: postgres
--

CREATE UNIQUE INDEX people_member_person_id_people_id_idx ON base.people_member USING btree (person_id, people_id);


--
-- Name: people_name_idx; Type: INDEX; Schema: base; Owner: postgres
--

CREATE UNIQUE INDEX people_name_idx ON base.people USING btree (name);


--
-- Name: people_name_idx1; Type: INDEX; Schema: base; Owner: postgres
--

CREATE UNIQUE INDEX people_name_idx1 ON base.people USING btree (name);


--
-- Name: flyway_schema_history_s_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX flyway_schema_history_s_idx ON public.flyway_schema_history USING btree (success);


--
-- Name: alias alias_people_id_fkey; Type: FK CONSTRAINT; Schema: base; Owner: postgres
--

ALTER TABLE ONLY base.alias
    ADD CONSTRAINT alias_people_id_fkey FOREIGN KEY (people_id) REFERENCES base.people(id);


--
-- Name: alias alias_person_id_fkey; Type: FK CONSTRAINT; Schema: base; Owner: postgres
--

ALTER TABLE ONLY base.alias
    ADD CONSTRAINT alias_person_id_fkey FOREIGN KEY (person_id) REFERENCES base.person(id);


--
-- Name: alias defcon; Type: FK CONSTRAINT; Schema: base; Owner: postgres
--

ALTER TABLE ONLY base.alias
    ADD CONSTRAINT defcon FOREIGN KEY (akadef_id) REFERENCES base.definervalue(id);


--
-- Name: definervalue definervalue_definer_id_fkey; Type: FK CONSTRAINT; Schema: base; Owner: postgres
--

ALTER TABLE ONLY base.definervalue
    ADD CONSTRAINT definervalue_definer_id_fkey FOREIGN KEY (definer_id) REFERENCES base.definer(id);


--
-- Name: ld_clique ld_clique_markerset_id_fkey; Type: FK CONSTRAINT; Schema: base; Owner: postgres
--

ALTER TABLE ONLY base.ld_clique
    ADD CONSTRAINT ld_clique_markerset_id_fkey FOREIGN KEY (markerset_id) REFERENCES base.markerset(id);


--
-- Name: markerset markerset_genome_id_fkey; Type: FK CONSTRAINT; Schema: base; Owner: postgres
--

ALTER TABLE ONLY base.markerset
    ADD CONSTRAINT markerset_genome_id_fkey FOREIGN KEY (genome_id) REFERENCES base.genome_markerset(id);


--
-- Name: markerset_member markerset_member_markerset_id_fkey; Type: FK CONSTRAINT; Schema: base; Owner: postgres
--

ALTER TABLE ONLY base.markerset_member
    ADD CONSTRAINT markerset_member_markerset_id_fkey FOREIGN KEY (markerset_id) REFERENCES base.markerset(id);


--
-- Name: markerset_member markerset_member_member_id_fkey; Type: FK CONSTRAINT; Schema: base; Owner: postgres
--

ALTER TABLE ONLY base.markerset_member
    ADD CONSTRAINT markerset_member_member_id_fkey FOREIGN KEY (member_id) REFERENCES base.marker(id);


--
-- Name: people_member people_member_people_id_fkey; Type: FK CONSTRAINT; Schema: base; Owner: postgres
--

ALTER TABLE ONLY base.people_member
    ADD CONSTRAINT people_member_people_id_fkey FOREIGN KEY (people_id) REFERENCES base.people(id);


--
-- Name: people_member people_member_person_id_fkey; Type: FK CONSTRAINT; Schema: base; Owner: postgres
--

ALTER TABLE ONLY base.people_member
    ADD CONSTRAINT people_member_person_id_fkey FOREIGN KEY (person_id) REFERENCES base.person(id);


--
-- Name: people people_typedef_id_fkey; Type: FK CONSTRAINT; Schema: base; Owner: postgres
--

ALTER TABLE ONLY base.people
    ADD CONSTRAINT people_typedef_id_fkey FOREIGN KEY (typedef_id) REFERENCES base.definervalue(id);


--
-- Name: person person_ma_fkey; Type: FK CONSTRAINT; Schema: base; Owner: postgres
--

ALTER TABLE ONLY base.person
    ADD CONSTRAINT person_ma_fkey FOREIGN KEY (ma) REFERENCES base.person(id);


--
-- Name: person person_pa_fkey; Type: FK CONSTRAINT; Schema: base; Owner: postgres
--

ALTER TABLE ONLY base.person
    ADD CONSTRAINT person_pa_fkey FOREIGN KEY (pa) REFERENCES base.person(id);


--
-- Name: SCHEMA base; Type: ACL; Schema: -; Owner: postgres
--

GRANT ALL ON SCHEMA base TO sgstemplate;


--
-- Name: SCHEMA bulk; Type: ACL; Schema: -; Owner: postgres
--

GRANT ALL ON SCHEMA bulk TO sgstemplate;


--
-- Name: TABLE alias; Type: ACL; Schema: base; Owner: postgres
--

GRANT ALL ON TABLE base.alias TO sgstemplate;


--
-- Name: TABLE cytoband; Type: ACL; Schema: base; Owner: postgres
--

GRANT ALL ON TABLE base.cytoband TO sgstemplate;


--
-- Name: TABLE definer; Type: ACL; Schema: base; Owner: postgres
--

GRANT ALL ON TABLE base.definer TO sgstemplate;


--
-- Name: TABLE definervalue; Type: ACL; Schema: base; Owner: postgres
--

GRANT ALL ON TABLE base.definervalue TO sgstemplate;


--
-- Name: TABLE definerview; Type: ACL; Schema: base; Owner: postgres
--

GRANT ALL ON TABLE base.definerview TO sgstemplate;


--
-- Name: TABLE marker; Type: ACL; Schema: base; Owner: postgres
--

GRANT ALL ON TABLE base.marker TO sgstemplate;


--
-- Name: TABLE markerset; Type: ACL; Schema: base; Owner: postgres
--

GRANT ALL ON TABLE base.markerset TO sgstemplate;


--
-- Name: TABLE markerset_member; Type: ACL; Schema: base; Owner: postgres
--

GRANT ALL ON TABLE base.markerset_member TO sgstemplate;


--
-- Name: TABLE migration; Type: ACL; Schema: base; Owner: postgres
--

GRANT ALL ON TABLE base.migration TO sgstemplate;


--
-- Name: TABLE people; Type: ACL; Schema: base; Owner: postgres
--

GRANT ALL ON TABLE base.people TO sgstemplate;


--
-- Name: TABLE people_member; Type: ACL; Schema: base; Owner: postgres
--

GRANT ALL ON TABLE base.people_member TO sgstemplate;


--
-- Name: TABLE person; Type: ACL; Schema: base; Owner: postgres
--

GRANT ALL ON TABLE base.person TO sgstemplate;


--
-- PostgreSQL database dump complete
--

