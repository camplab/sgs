create or replace function public.pv(l bigint, e bigint, g bigint, o int) 
returns float
as 
$$
select 1.0*(g+e+o)/(l+e+g+o)::float;
$$
language sql
;

create or replace function public.duopv(p1 float, p2 float)
returns float
as
$$
select p1*p2*(1-ln(p1*p2))::float;
$$
language sql
;

CREATE OR REPLACE FUNCTION public.pv_lower95(l bigint, e bigint, g bigint, o integer)
RETURNS double precision
LANGUAGE plpgsql
AS $function$
declare
  pv double precision;
  ll double precision;
begin
       pv:=1.0*(g+e+o)/(l+e+g+o);
       ll:=pv-1.96*sqrt((pv/(l+e+g))*(1-pv));
       return ll;
end;
$function$
;

CREATE OR REPLACE FUNCTION public.pv_upper95(l bigint, e bigint, g bigint, o integer)
RETURNS double precision
LANGUAGE plpgsql
AS $function$
declare
  pv double precision;
  ul double precision;
begin
       pv:=1.0*(g+e+o)/(l+e+g+o);
       ul:=pv+1.96*sqrt((pv/(l+e+g))*(1-pv));
       return ul;
end;
$function$
;
 
CREATE OR REPLACE FUNCTION public.rand_pv(bounds double precision[])
 RETURNS double precision
 LANGUAGE sql
AS $function$
select random()*(bounds[2]-bounds[1]) + bounds[1];
$function$
;
