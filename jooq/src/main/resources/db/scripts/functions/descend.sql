set search_path = base,public;
create or replace function public.descend(founder uuid)
returns table (id uuid, name text, pa uuid, ma uuid, gender char(1), genr int) as
$$
with recursive generation(id, name, pa, ma, gender, gen) as (
     select p.id, p.name, p.pa, p.ma, p.gender, 0 as gen from person p where p.id = founder
     union
     select b.id, b.name, b.pa, b.ma, b.gender, g.gen+1
     from person b join generation g on (g.gender = 'f' and g.id = b.ma) or (g.gender = 'm' and g.id = b.pa)
)
select * from generation order by gen;
$$
language sql;



create or replace function public.descend_wide(founder uuid)
returns table (id uuid, name text, pa uuid, ma uuid, gender char(1), genr int) as
$$
begin
drop table if exists genbuilder;
create temp table genbuilder as
with recursive generation(id, name, pa, ma, gender, gen) as (
     select p.id, p.name, p.pa, p.ma, p.gender, 0 as gen
     from person p where p.id = founder
     union
     select b.id, b.name, b.pa, b.ma, b.gender, g.gen+1
     from person b join generation g on (g.gender = 'f' and g.id = b.ma) or (g.gender = 'm' and g.id = b.pa)
)
select * from generation order by gen;
return query
select * from genbuilder 
       union distinct 
       select e.id, e.name, e.pa, e.ma, e.gender, g1.gen - 1 
       from  person e join genbuilder g1 on e.id = g1.pa or e.id = g1.ma 
             and not exists (select 1 from genbuilder g2 where g2.id = e.id)
       order by gen;
end;       
$$
language plpgsql;


/* testing

with recursive build (id, name, pa, ma, gender, gen) as (
select * from descend('4830abd8-f182-4b27-a02c-ed0c2b8939e3') 
union 
select p.id, p.name, p.pa, p.ma, p.gender, d.gen - 1 as genr 
from build d join person p on d.ma = p.id
where d.gender = 'f')
select distinct a.aka, b.name, f.name dad, m.name mom, b.gender, b.gen
from build b join alias a on b.id = a.person_id 
     join person f on b.pa = f.id and f.gender = 'm'
     join person m on b.ma = f.id and f.gender = 'f';

*/
