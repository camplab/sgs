create or replace function public.genome_optimalset_mono(pbs_name text, genome_name text, conf float, empirical boolean)
returns uuid
as $$
declare
  genome_markerset_id uuid;
  pid uuid;        
  tid uuid;
  gids uuid[];  -- because threshold record uses an array of probandset group ids
  rows int;
  startAt timestamp;
begin
-- create a optimalset entry and load (mono) optimal set
  select clock_timestamp() into startAt;
  select id into genome_markerset_id from genome_markerset where name = genome_name;
  tid = uuid_generate_v4();
  select array_agg(id) into gids from probandset_group where name = pbs_name; 
  get diagnostics rows = ROW_COUNT;
  if rows = 0 then
     raise exception 'NO PROBANDSET_GROUP for %s', pbs_name;
  else
     raise notice 'group id is %', gids[1];
  end if;
  insert into optimalset(id, suggestive, significant, name, probandset_groups)
         values(tid, 0.0, 0.0, pbs_name, gids);
  get diagnostics rows = ROW_COUNT;
  if rows = 1 then
    raise notice 'New optimalset: %', tid;
  else
    raise exception 'Failed to insert optimalset record';
  end if;
  insert into optimalset_segment(id, optimalset_id, segment_id, pvalue, mu_t)
         select uuid_generate_v4(), tid, f.segment_id, f.pval, null
         from genome_pvalue_mono(pbs_name, genome_markerset_id, conf, empirical) as f;
  get diagnostics rows = ROW_COUNT;
-- process to ties ends together
  select threshold_process(tid, genome_name, empirical) into pid;
  raise notice 'process % generated optval set of % segments at % (duration: %)', pid, rows, clock_timestamp(), clock_timestamp() - startAt;

  return tid;
end;
$$ language plpgsql;

create or replace function public.genome_optimalset_smooth(pbs_name text, genome_name text)
returns uuid
as $$
declare
  tid uuid;
begin
  select genome_optimalset_mono(pbs_name, genome_name, 1.96, false) into tid;
  return tid;
end;
$$ language plpgsql;

create or replace function public.genome_optimalset_result(pbs_name text, genome_name text)
returns uuid
as $$
declare
  tid uuid;
begin
  select genome_optimalset_mono(pbs_name, genome_name, 1.96, true) into tid;
  return tid;
end;
$$ language plpgsql;
;
