-- The following function cannot live with the other pv routines since it uses a
-- table definition known only /after/ the project (e.g. camp.bc) has been set.
create or replace function  __STUDYNAME__.pvr(seg segment, plus float default 0.0)
returns float as $$
  select (((seg.events_equal+seg.events_greater)+plus)/((seg.events_less+seg.events_equal+seg.events_greater)+plus) as result;
$$
language sql;
   
