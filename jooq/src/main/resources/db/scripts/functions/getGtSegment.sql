create or replace function public.getgt_segment(segid uuid, plusone bool = false)
returns table(caseid text, pedname text, chr int, alleles text)
language plpgsql as 
$$
declare
  startindex int;
  endindex int;
  mksid uuid;
  pbids uuid[];
  extra int;
begin
  select s.markerset_id, 
  	 case when plusone then s.firstmarker-1 else s.firstmarker end, 
	 case when plusone then s.lastmarker+1 else s.lastmarker end
  into mksid,startindex,endindex
  from segment s where id = segid;
  select probands into pbids from segment s join probandset b on s.probandset_id = b.id where s.id = segid;
  return query
  -- markerset is indexed from zero; postgres strings are indexed from 1, substring uses offset and length
  select p.name as case
         , l.name as ped
         , m.chrom
         , substring(encode(g.calls,'hex'), 2*startindex+1, 2*(endindex-startindex+1)) as gtype
  from genotype g
       join person p on g.person_id = p.id
       join people l on g.people_id = l.id
       join markerset m on g.markerset_id = m.id
  where g.markerset_id = mksid
        and g.person_id = any(pbids);
end;
$$
;

-- create or replace function public.getgt_markers( first int, last int )
-- returns table(id uuid, person_id uuid, markerset_id uuid, alleles text)
-- language plpgsql as 
-- $$
-- declare
-- begin
--   return query 
--   select g.id
--          , g.person_id
--          , g.markerset_id
--          , substring(encode(g.calls, 'hex'), 2*first+1, (2*(last-first+1)))
--   from genotype g;
-- end;
-- $$
-- ;
