create or replace function public.optimal_pvalue_mono(pbsg_name text, markers_name text, chr int, conf float, empirical boolean default false)
returns table (segment_id uuid, base1 int, pval float)
as $$
-- ------
-- single "people" best segments for given chrom
-- ------
declare
  segp          RECORD;
  mkset         uuid;
  pbsgid        uuid;
  rcount        int;
  mkrcnt        int;
  segsdone      int  :=  0;
  totalinserts  int  :=  0;
-- ------
begin
  select id into mkset from markerset where name = markers_name and chrom = chr; 
  select id into pbsgid from probandset_group where name = pbsg_name;
  if pbsgid is null then
     raise exception 'NO SUCH probandset_group name: %', pbsg_name;
  end if;
  raise notice '%: markerset id is %(%), people (%) id is %', clock_timestamp(), mkset, markers_name, pbsgid, pbsg_name;
--
  drop table if exists collected;	
  drop table if exists mrkidx;
--
  create temp table if not exists imputed_pvalue_t(segment_id uuid, probandset_group_id uuid, ipv float) on commit drop;
  create temp table collected(segid uuid, cpv float) on commit drop;
  create temp table mrkidx on commit drop as
    select member_id as marker_id, ordinal 
    from markerset_member where markerset_id = mkset;
  get diagnostics mkrcnt = ROW_COUNT;
  create unique index on mrkidx(ordinal);
  raise notice '%: working with % markers', clock_timestamp(), mkrcnt;
--
  insert into imputed_pvalue_t (segment_id, probandset_group_id, ipv)
  select s.id,
         pbsgid,
	 case when empirical then pvr(s)
	      else rand_pv( wilson_ci(conf,  s.threshold_events, 1000000))
	 end
  from segment s join probandset b on s.probandset_id = b.id
       join probandset_group_member m on b.id = m.member_id
  where s.markerset_id = mkset 
        and m.group_id = pbsgid;
--
  get diagnostics rcount = ROW_COUNT;
  raise notice '%: added % segments to imputed_pvalue_t', clock_timestamp(), rcount;
--  
  for segp in
    select s.id, s.firstmarker, s.lastmarker, 
           v.ipv,
           array_length(p.probands,1) as pbslen,
           s.lastmarker - s.firstmarker + 1 as mks
    from segment s 
         join imputed_pvalue_t v on s.id = v.segment_id
         join probandset p on s.probandset_id = p.id
         join probandset_group_member m on p.id = m.member_id
    where s.markerset_id = mkset
          and m.group_id = pbsgid
    order by ipv asc, p.meioses desc, pbslen desc, mks desc, p.name asc
  LOOP
    delete from mrkidx where ordinal between segp.firstmarker and segp.lastmarker;
    get diagnostics rcount = ROW_COUNT;
    segsdone = segsdone + 1;
    if rcount > 0 then
       insert into collected values(segp.id, segp.ipv);
       totalinserts = totalinserts + rcount;
       if totalinserts = mkrcnt then  -- really totalDELETES
          raise notice '%: no markers left on %th segment %', clock_timestamp(), segsdone, segp.id;
          exit;
       end if;
    end if;
  end LOOP;
  select count(*) into rcount from mrkidx;
  raise notice '%: finished assessing markers; % leftover', clock_timestamp(), rcount;
  truncate imputed_pvalue_t;
--
  return query
    select s.id as segment_id, s.startbase as base1, c.cpv
    from segment as s join collected as c on s.id = c.segid
    order by s.startbase;
end;
$$ language plpgsql;
