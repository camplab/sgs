create or replace function public.alias_definers(category text, defs text[], verbage text)
returns void as
$$
declare
  defid uuid;
  defval text;
  defparts text[];
begin
  select id into defid from base.definer where name = category;
  if defid is null
  then
    select uuid_generate_v4() into defid;
    insert into base.definer (id, name, description) values (defid, category, verbage);
  end if;
  foreach defval in array defs loop
    select regexp_split_to_array(defval, E';;') into defparts;
    insert into base.definervalue (id, valuestring, description, definer_id) values (uuid_generate_v4(), defparts[1], defparts[2], defid);
  end loop;
end;
$$
language plpgsql;
