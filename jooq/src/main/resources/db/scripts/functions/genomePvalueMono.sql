create or replace function public.genome_pvalue_mono( pbsg_name text, 
       	  	  	   			      markers_id uuid, 
						      conf float,
						      empirical boolean )
returns table (segment_id uuid, base1 int, pval float)
as $$
-- ------
-- single "people" genome wide best segments
-- ------
declare
  loops int := 0;
  mvec RECORD;                
  markerset_id uuid;
  markerset_name text;
begin
  create temp table if not exists goptset(segment_id uuid, startbase int, pvalue float) on commit drop;
  truncate table goptset;
  for mvec in      
    select chrom, name
    from markerset where genome_id = markers_id
    order by chrom
  loop
    loops = loops+1;
    raise notice 'doing chrom %', mvec.chrom;
    insert into goptset
    select * from optimal_pvalue_mono(pbsg_name, mvec.name, mvec.chrom, conf, empirical);
  end loop;
  if loops = 0 then
    raise exception 'No markersets found matching "%"', markers_id;
  end if;
  return query select * from goptset;
end;
$$ language plpgsql;

create or replace function public.genome_pvalue_mono( pbsg_name text, 
       	  	  	   			      markers_name text )
returns table (segment_id uuid, base1 int, pval float)
as $$
-- ------
-- single "people" genome wide best segments
-- ------
declare
  gms_id uuid;
begin
  select id into gms_id from genome_markerset g where g.name = markers_name;
  raise notice 'Defaulting confidence level to 1.96. All p-values will be smoothed';
  return query select * from genome_pvalue_mono(pbsg_name, gms_id, 1.96, true);
end;
$$ language plpgsql;
