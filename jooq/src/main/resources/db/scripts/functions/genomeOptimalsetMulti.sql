create or replace function public.genome_threshold_multi(thresholds uuid[])
returns void
as $$
declare
  thresholdnew  uuid;        
  procid	uuid;
  groups        uuid[];
  tnames        text[];
  rc		int;
begin
  -- create optimalset entry and load duo optimal set
  raise notice '%: === STARTING PAIR "%"', clock_timestamp(), array_to_string(namepair,'/');
  threshid = uuid_generate_v4();
  select array[a.id, b.id] into groupids
  from probandset_group a, probandset_group b 
  where a.name = namepair[1] 
        and b.name = namepair[2];
  insert into optimalset (id, suggestive, significant, name, probandset_groups)
        values (threshid, 0.0, 0.0, array_to_string(namepair,'/'), groupids);
  insert into optimalset_duosegment(id, optimalset_id, segment1_id, segment2_id, duo_optval)
    select uuid_generate_v4(), threshid, o.segment1_id, o.segment2_id, duopv( o.optval1, o.optval2) 
    from genome_pvalue_duo(namepair, mrkr_rgx) as o
    where o.segment1_id is not null;
  get diagnostics rc = row_count;
  procid = uuid_generate_v4();
    insert into process values(procid, current_user, 'optimalset_multi', now(), inet_server_addr()::text);
  insert into process_output values(uuid_generate_v4(), procid, 'optimalset', threshid);
  raise notice '%: added % duo optimals for %', clock_timestamp(), rc, array_to_string(namepair,'/');
end;
$$ language plpgsql;
