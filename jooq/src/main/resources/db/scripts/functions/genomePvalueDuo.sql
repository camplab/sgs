create or replace function public.genome_pvalue_duo(pbs_groups text[], genome_name text)
returns table(segment1_id uuid, ped1_id uuid, optval1 double precision, segment2_id uuid, ped2_id uuid, optval2 double precision, duo_optval double precision)
-- ------
-- multi "people" genome wide best segments
-- ------
AS $function$
declare 
  rcount int := 0;
  markersetnames text[];
  setname text;
begin
  create temp table if not exists gwo(seg1 uuid, ped1 uuid, optval1 float, seg2 uuid, ped2 uuid, optval2 float, fisher_exact float) on commit drop;
--
  create temp table tlist on commit drop as
  select t.id as threshid
  	 , v.id as tsid
	 , v.pvalue
	 , s.id as segid
	 , s.firstmarker
	 , s.lastmarker
	 , s.lastmarker-s.firstmarker+1 as seglength
	 , p.name
 	 , p.meioses
	 , array_length(p.probands,1) as pbs
  from optimalset t
  join optimalset_segment v on t.id = v.optimalset_id
  join segment s on v.segment_id = s.id
  join probandset p on p.id = s.probandset_id
  where t.id = any (tids) and s.markerset_id = mkset;
--
  truncate gwo;
  raise notice 'peoples named:  %\nmarker pattern: %', array_to_string(pbs_groups, ', '), genome_name;
  select array_agg(m.name) into markersetnames
  from markerset m join genome_markerset g on m.genome_id = g.id
  where g.name = genome_name;
--
  foreach setname in array markersetnames loop
    insert into gwo 
    select distinct f.segment1_id
    	   , f.ped1_id
	   , f.optval1
	   , f.segment2_id
	   , f.ped2_id 
	   , f.optval2
	   , duopv(f.optval1, f.optval2) as duo_optval
    from optimal_pvalue_mped(pbs_groups, setname)as f;
  end loop;
  return query select * from gwo;
end;
$function$
language plpgsql;


