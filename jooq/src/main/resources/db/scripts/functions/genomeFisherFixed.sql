CREATE OR REPLACE FUNCTION public.genome_fisher_fixed(maint uuid, tids uuid[])
 RETURNS TABLE(tseg1 uuid, tseg2 uuid, fisher_combined double precision)
 LANGUAGE plpgsql
AS $function$
declare
  rc           int;
  markersdone  int = 0;
  segn         int = 0;
  tid          uuid;
  mkset        uuid;
  fisher       float;
  t1           RECORD;
  t2           RECORD;
--  
begin
    raise notice '%: Off we go', clock_timestamp();
    create temp table fishers(tseg1 uuid, tseg2 uuid, fisher_combined float) on commit drop;
    -- we believe all input optimalset use the same markers
    for chrm in 1..22
    loop
        select distinct s.markerset_id into mkset from segment s 
               where exists(select * from optimalset_segment t where t.segment_id = s.id and t.optimalset_id = maint) and s.chrom = chrm;
--        
        create temp table tlist on commit drop as
        select t.id as threshid, v.id as tsid
               , v.pvalue
               , s.id as segid
               , s.firstmarker
               , s.lastmarker
               , s.lastmarker-s.firstmarker+1 as seglength
               , p.meioses
               , array_length(p.probands,1) as pbs
               , p.name as name
        from optimalset t
             join optimalset_segment v on t.id = v.optimalset_id
             join segment s on v.segment_id = s.id
             join probandset p on p.id = s.probandset_id
        where t.id = any (tids) and s.markerset_id = mkset;
        get diagnostics rc = row_count;
        raise notice '%: Starting % with % optimalset segments across %', clock_timestamp(), chrm, rc, array_to_string(tids,'/');
        create index on tlist(firstmarker, threshid);
        create index on tlist(lastmarker, threshid);
        create index on tlist(tsid);
        raise notice '%: indexing done', clock_timestamp();
--
        drop table if exists mkridx;
        create temp table mkridx on commit drop as
            select m.member_id, m.ordinal from markerset_member m where m.markerset_id = mkset;
        get diagnostics rc = row_count;
        raise notice '%: chromosome % has % markers', clock_timestamp(), chrm, rc;
        create index on mkridx(ordinal);
        --
        for t1 in
            select t.id as threshid
                   , v.id as tsid
                   , v.pvalue
                   , s.id as segid
                   , s.firstmarker
                   , s.lastmarker
                   , s.lastmarker-s.firstmarker+1 as seglength
                   , p.name
                   , p.meioses
                   , array_length(p.probands,1) as pbs
            from optimalset t
            join optimalset_segment v on t.id = v.optimalset_id
            join segment s on v.segment_id = s.id
            join probandset p on p.id = s.probandset_id
            where t.id = maint
            and s.markerset_id = mkset
            

            order by pvalue asc, coalesce(p.meioses,0) desc, pbs desc, seglength desc, p.name asc
        loop
        segn = 0;
--
        for t2 in
             -- collect all optimalset segment for the unfixed list of optimalset which overlap this fixed segment
             select * from tlist l
             where l.lastmarker >= t1.firstmarker and l.firstmarker <= t1.lastmarker
             order by l.pvalue asc, coalesce(l.meioses, 0) desc, l.pbs desc, l.seglength desc, l.name asc
            loop
                segn = segn + 1;
                -- raise notice '%: working with t2=%', clock_timestamp(), t2.tsid;
                -- still some markers for fixed segment
                select count(*) into rc from mkridx x where x.ordinal >= t1.firstmarker and x.ordinal <= t1.lastmarker;  
                if rc > 0 then
                -- if FOUND then
                   -- raise notice '%: Still % markers for boss %', clock_timestamp(), rc, t1.tsid;
                   delete from mkridx x 
                          where (x.ordinal >= t1.firstmarker and x.ordinal >= t2.firstmarker)
                          and (x.ordinal <= t1.lastmarker and x.ordinal <= t2.lastmarker);
                   get diagnostics rc = row_count;
                   if rc > 0 then
                       -- raise notice '%: accounted % for fixed segid = %, other segid = %', clock_timestamp(), rc, t1.segid, t2.segid;
                       markersdone = markersdone + rc;                       
                       fisher = duopv(t1.pvalue, t2.smooth_pvalue);
                       -- raise notice '%: pairing %,% accounts for %, fish = %, markersdone = %',
                       -- clock_timestamp(), t1.segid, t2.segid, rc, fisher, markersdone;
                       insert into fishers values(t1.tsid, t2.tsid, fisher);
                   -- else
                       -- raise notice '%: zero delete %/%', clock_timestamp(), t1.tsid, t2.tsid;
                   end if;
                else
                   -- raise notice 'early exit: zero markers left for %', t1.tsid;
                   exit;
                end if;
            end loop; --t2
            select count(*) into rc from mkridx;
            if rc = 0 then
               raise notice '%: zero markers left after % primary segments', clock_timestamp(), segn;
               exit;
            end if;
        end loop; -- t1
        -- select count(*) into rc from mkridx;
        raise notice '%: finished chromosome %, covered % markers', clock_timestamp(), chrm, markersdone;
        drop table if exists tlist;
    end loop; --chrom
    return query select * from fishers;
end;
$function$
