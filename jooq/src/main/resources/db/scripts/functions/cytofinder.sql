CREATE OR REPLACE FUNCTION public.cytofinder(p_chrom integer, p_start integer, p_end integer)
RETURNS text
LANGUAGE plpgsql
AS $function$
declare 
cyto1 text;
cyto2 text;
begin
-- Look for cytoband that completely encloses segment
  select c.name
  from cytoband c
  into cyto1
  where c.chrom = 'chr'||p_chrom::text
  and c.chrom_start = (select max(chrom_start) from cytoband where chrom_start <= p_start and chrom = 'chr'||p_chrom::text);
  
  select c.name
  from cytoband c
  into cyto2
  where c.chrom = 'chr'||p_chrom::text
   and c.chrom_end = (select min(chrom_end) from cytoband where chrom_end >= p_end and chrom = 'chr'||p_chrom::text);
--
  if(cyto1=cyto2) then
     return p_chrom||cyto1;
   else
     return p_chrom||cyto1||'-'||cyto2;
   end if;
end;
$function$
;
