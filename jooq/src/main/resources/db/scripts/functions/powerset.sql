CREATE OR REPLACE FUNCTION public.powerset(a anyarray)
 RETURNS SETOF anyarray
 LANGUAGE plpgsql
 IMMUTABLE STRICT
AS $function$
declare
retval a%type;
alower	integer := array_lower(a,1);
aupper	integer := array_upper(a,1);
j integer;
k integer;
begin
for i in 0 .. (1 << (aupper - alower + 1)) - 1 LOOP
	retval := '{}';
	j:= alower;
	k :=i;
--	
	while k > 0 loop
		if k & 1 = 1 then
			retval := array_append(retval, a[j]);
		end if;
		
		j := j + 1;
		k := k >> 1;
	end loop;
--
	return next retval;
end loop;
return;
end;
$function$
