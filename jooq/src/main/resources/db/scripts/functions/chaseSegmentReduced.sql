create or replace function public.chase_segment_reduced(segid uuid, extend bool)
returns text as
$function$
declare
  fmark int;
  lmark int;
  chaser text;
begin
  select m.ordinal into fmark 
  from markerset_member m join segment s on m.markerset_id = s.markerset_id 
       join marker k on m.member_id = k.id
  where s.startbase = coalesce(k.basepos37, k.basepos38, -1) and s.id = segid;
  --
  select m.ordinal into lmark 
  from markerset_member m join segment s on m.markerset_id = s.markerset_id 
       join marker k on m.member_id = k.id
  where s.endbase = coalesce(k.basepos37, k.basepos38, -1) and s.id = segid;
  --
  if extend then
    select s.id || ' ' ||
           fmark || ' ' ||
           lmark || ' ' ||
           s.lastmarker - s.firstmarker + 1 || ' '  ||
           s.startbase || ' ' ||
           s.endbase || ' ' ||
           s.events_less || ' ' || 
           s.events_equal || ' ' ||
           s.events_greater || ' '
    into chaser
    from segment s
    where s.id = segid;
  else  
    select s.id || ' ' ||
           fmark || ' ' ||
           lmark || ' ' ||
           s.lastmarker - s.firstmarker + 1 || ' '  ||
           s.startbase || ' ' ||
           s.endbase || ' 0 0 0'
    into chaser
    from segment s
    where s.id = segid;
  end if;
--
  return chaser;        
end;
$function$
language plpgsql;


create or replace function public.chase_segment_finder(pbsg text, chrm int, cutoff float)
returns table(chaser text) as
$$
begin
  return query        
  with chaseable as(
       select s.* 
       from segment s
            join probandset_group_member gm on s.probandset_id = gm.member_id
            join probandset_group g on gm.group_id = g.id
            where s.chrom = chrm 
                  and pv(s.events_less,s.events_equal,s.events_greater,1) < cutoff
                  and g.name = pbsg)
  select 
         s.id || ' ' ||
         s.chrom || ' ' ||
         m1.ordinal || ' ' ||
         m2.ordinal || ' ' ||
         s.lastmarker - s.firstmarker + 1 || ' '  ||
         s.startbase || ' ' ||
         s.endbase || ' ' ||
         s.events_less || ' ' ||
         s.events_equal || ' ' ||
         s.events_greater as chaser
  from chaseable s
       join markerset_member m1 on s.markerset_id = m1.markerset_id join marker k1 on m1.member_id = k1.id
       join markerset_member m2 on s.markerset_id = m2.markerset_id join marker k2 on m2.member_id = k2.id;
end;
$$
language plpgsql;
