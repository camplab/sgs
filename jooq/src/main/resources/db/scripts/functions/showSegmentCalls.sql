create or replace function public.segment_calls(segid uuid)
returns table (name text, firsti int, lasti int, calls text)
as
$$
begin
return query
select 
    p.name, 
    s.firstmarker, 
    s.lastmarker, 
    regexp_replace(substr(encode(g.calls,'hex'),1+(2*s.firstmarker), 2*(s.lastmarker-s.firstmarker+1))::text, '(..)', ' \1','g') as calls
from
    segment s
    join probandset b on s.probandset_id = b.id
    join people l on b.people_id = l.id
    join people_member m on l.id = m.people_id
    join person p on m.person_id = p.id
    join genotype g on g.markerset_id = s.markerset_id and g.person_id = p.id
where s.id = segid;
end;
$$
language plpgsql;
;



drop function if exists base.locustweaker;
create or replace function base.locustweaker(segid uuid, leftmarker int, rightmarker int, changemarker int)
returns table (gtid uuid, pedname text, first int, alleles int, calls text)
as $$
begin
return query
select 
    g.id,
    p.name, 
    1+(2*leftmarker) as leftallele,
    2*(rightmarker - leftmarker + 1 ) as n_alleles,
    regexp_replace(
        substr( encode(g.calls,'hex'), 1+(2*leftmarker), 2*(changemarker-leftmarker))
                || '00' ||
                substr(encode(g.calls,'hex'), 1+(2*(changemarker+1)), 2*(1+rightmarker - (changemarker+1))),
        '(.)', '\1 ','g') as calls
from
    segment s
    join probandset b on s.probandset_id = b.id
    join people l on b.people_id = l.id
    join people_member m on l.id = m.people_id
    join person p on m.person_id = p.id
    join genotype g on g.markerset_id = s.markerset_id and g.person_id = p.id
where s.id = segid;
end;
$$
language plpgsql;
/*
 * We've a need to discount suspect loci        
*/
create or replace function base.locusedit(peop text, markers text, target int)
returns table (person_id uuid, markerset_id uuid, calls bytea, people_id uuid)
as $$
declare
    pid uuid;
    mid uuid;
begin
    select id into mid from markerset where name = markers;
    select id into pid from people where name = peop;
    return query
    select 
        p.id,
        g.markerset_id,
        substring( g.calls, 1, target ) -- one short, since zero index
                || decode('00', 'hex') || -- target
                substring( g.calls, target+2) -- one past target
        as calls,
        l.id
    from people l join people_member m on l.id = m.people_id
        join person p on m.person_id = p.id
        join genotype g on  g.person_id = p.id
    where l.id = pid and g.markerset_id = mid;
end;
$$
language plpgsql;

/*show gt*/
create or replace function base.showgt(segid uuid, locus1 int, locus2 int)
returns table (person_id uuid, markerset_id uuid, calls bytea, people_id uuid)
as $$
declare
    pid uuid;
    mid uuid;
begin
    -- select s.markerset_id into mid from segment s where id = segid;
    -- select b.people_id into pid from segment s join probandset b on s.probandset_id = b.id where s.id = segid;
    return query
    select 
        g.id,
        g.markerset_id,
        substring( g.calls, locus1, locus2-locus1+1 )
        as calls,
        l.id
    from segment s join probandset b on s.probandset_id = b.id
         join people l on b.people_id = l.id
         join genotype g on g.person_id = any(b.probands)
    where g.people_id = l.id and g.markerset_id = s.markerset_id and s.id = segid;
end;
$$
language plpgsql;
