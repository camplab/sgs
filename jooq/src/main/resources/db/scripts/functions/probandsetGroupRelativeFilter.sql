CREATE OR REPLACE FUNCTION public.probandset_group_relative_filter(pbs_group uuid)
 RETURNS integer
 LANGUAGE plpgsql
AS $function$
-- ------
-- For a given probandset group, remove impossible and uninformative parent/child rels
-- ------
declare 
        new_pbg_id uuid;
        new_pbg_name text;
        pbset RECORD;
        orphan_count int;
        ma_count int;
        pa_count int;
        pb_count int;
        child_count int;
        rec_count int;
        process_id uuid;
        existing_count int;

begin
-- Create temp table of good probandsets
create temp table if not exists
new_pbset_group (member_id uuid, group_id uuid)
on commit drop;
truncate table new_pbset_group;

select uuid_generate_v4(), name||'_relfilter'
into new_pbg_id, new_pbg_name
from probandset_group
where id = pbs_group;

select count(*)
into existing_count
from probandset_group
where name = new_pbg_name;

if existing_count > 0 then
        raise notice 'Probandset group already exists with name %s',new_pbg_name;
        return 0;
end if;

-- Look at all probandsets defined for supplied group
-- For each one, see if any individual has a mother
for pbset in
        select pg.name, ps.name as probandset, ps.id, ps.probands
        from probandset_group pg
        join probandset_group_member pgm on pgm.group_id = pg.id
        join probandset ps on ps.id = pgm.member_id
        where pg.id = pbs_group
loop
-- for each of these, look for two cases:
-- 1) there are no probands in this set who are not first-degree relatives.
------ 1a. All full siblings
        select count(distinct ma), count(distinct pa), count(distinct id)
        into ma_count, pa_count, pb_count
        from base.person
        where id in (select unnest(pbset.probands));
------ 1b. We have exactly two sets of parents, and one of our ca
        select count(distinct id)
        into child_count
        from base.person p where p.id in (select unnest(pbset.probands))
        and (p.ma in (select unnest(pbset.probands)) or p.pa in (select unnest(pbset.probands)));

-- 2) if a proband in the set has a mother or father in this probandsetgroup,
---- then they both must be in this set, otherwise we don't add it
        select count(*)
        into orphan_count
        from base.person p_child
        where p_child.id in (select unnest(pbset.probands))
        and exists (
                select 1
                from base.person p_parent
                where (p_parent.id = p_child.ma or p_parent.id = p_child.pa)
                and p_parent.id in (
                        select unnest(ps_all.probands)
                        from probandset ps_all
                        join probandset_group_member pgm_all on pgm_all.member_id = ps_all.id
                        join probandset_group pg_all on pg_all.id = pgm_all.group_id
                        where pg_all.id = pbs_group
                )
        )
        and not exists (
                select 1
                from base.person p_parent
                where (p_parent.id = p_child.ma or p_parent.id = p_child.pa)
                and p_parent.id in (select unnest(pbset.probands))
        );

        if ma_count=1 and pa_count=1 then
                raise notice '% pbset name: %. All probands are siblings, skipping.',pbset.id,pbset.probandset;
        elsif ma_count=2 and pa_count=2 and child_count=pb_count-1 then
                raise notice '% pbset name: %. All probands are first-degree relatives, skipping.',pbset.id,pbset.probandset;
        elsif orphan_count>0 then
                raise notice '% pbset name: %. Found one or more orphans, skipping.',pbset.id,pbset.probandset;
        else
                insert into new_pbset_group (member_id, group_id) values (pbset.id, new_pbg_id);
        end if;
end loop;

select count(*)
into rec_count
from new_pbset_group;

if rec_count = 0 then
        raise notice 'No eligible probandsets found after filtering for probandset group %',pbs_group;
        return 0;
else
        begin
                -- Make process record here
                select uuid_generate_v4()
                into process_id;

                insert into process (id, who, what, runhost) values (process_id, current_user,'Probandset group filtering',inet_client_addr()::text);
                insert into process_output (id, process_id, output_type, output_ref)
                select uuid_generate_v4(), process_id, 'probandset_group', new_pbg_id;

                insert into process_input (id, process_id, input_type, input_ref)
                select uuid_generate_v4(), process_id, 'probandset_group', pbs_group;

                insert into probandset_group (id, name, purpose)
                values(new_pbg_id,new_pbg_name,'Filter out probandset groups with uninformative relationships');

                insert into probandset_group_member(group_id, member_id)
                select group_id, member_id from new_pbset_group;
                return rec_count;
        exception
        when others then
                raise notice 'Error name:%',SQLERRM;
                raise notice 'Error state:%',SQLSTATE;
                return 0;
        end;
end if;
end;

$function$

