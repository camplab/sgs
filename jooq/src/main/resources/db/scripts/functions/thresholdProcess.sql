create or replace function public.threshold_process( optimalset_id uuid, chipdef text, empirical boolean)
returns UUID
language plpgsql
as $$
declare
  pid uuid;
  chipid uuid;
  i uuid;
  purpose text;
  found boolean;
begin
  select id into chipid from genome_markerset where name = chipdef;
  if chipid is null then
    raise exception 'Could not find markerset for %', chipdef;
  end if;
--
  if empirical then
     purpose := 'empirical result set generation';
  else
     purpose := 'threshold optimal set generation';
  end if;
  pid = uuid_generate_v4();
  insert into process values( pid, current_user, purpose, clock_timestamp(), inet_client_addr()::text);
  
  insert into process_input
  select uuid_generate_v4(), pid, 'probandset_group', unnest(probandset_groups)::text
  from optimalset
  where id = optimalset_id;

  insert into process_input values (uuid_generate_v4(), pid, 'genome_markerset', chipid);
  insert into process_output values (uuid_generate_v4(), pid, 'threshold', optimalset_id);
  return pid;
end;
$$;
