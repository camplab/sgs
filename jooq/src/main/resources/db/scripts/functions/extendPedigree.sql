create or replace function public.extendpeople(pnames text[], paliases text[], peep text)
returns void
as $$
declare
  rcount int;
  memberord int;
  idx int;
  aname text;
  nom text;
  personid uuid;
  peepid uuid;
begin
  select id into peepid from people where name = peep;
  select max(ordinal) into memberord from people_member where people_id = peepid;
  if peepid is null then
     raise exception 'Who are the %?', peep;
     return;
  end if;
  if paliases is not null and array_length(pnames, 1) != array_length(paliases, 1) then
     raise exception 'number of names(%) does not match aliases(%)', array_length(pnames), array_length(paliases);
     return;
  end if;
  idx = 0;
  foreach nom in array pnames loop
    aname = null;
    idx = idx + 1;
    select id into personid from person p where p.name = nom;
    if personid is null then
      raise exception '% is unknown', pname;
      -- select addNewPerson(name, paliases[idx], null, null, peepid);
    else
      if memberord is not null then
        memberord = memberord + 1;
      end if;
      insert into people_member values(uuid_generate_v4(), peepid, personid, memberord);
      if paliases is not null then
        aname = paliases[idx];
      end if;
      if aname is null then
        aname = nom;
      end if;
      raise notice 'Aliasing %', aname;
      insert into alias values(uuid_generate_v4(), personid, aname, peepid);
    end if;
  end loop;
end;
$$
language plpgsql;
