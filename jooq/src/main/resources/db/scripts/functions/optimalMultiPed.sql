create or replace function public.optimal_pvalue_mped(group_names text[], markersetname text)
 returns TABLE(marker_id uuid, marker_name text, basepos integer, segment1_id uuid, ped1_id uuid, optval1 double precision, segment2_id uuid, ped2_id uuid, optval2 double precision)
 as $$
-- ------
-- For one chromosome get two best segments per marker
-- -----
declare
  msetid uuid;
  groups uuid[];
  rowscount int;
  markercount int;
  updatecount int := 0;
  seg1count int := 0;
  seg2count int := 0;
  seg record;
begin
  select id into msetid from markerset where name = markersetname;
  select array_agg(id) into groups from probandset_group where name = any(group_names);
-- check length
  raise notice '%: markerset id is %, group ids are %', clock_timestamp(), msetid, array_to_string(groups,'/','nil');
--
  create temp table if not exists optmarkers (
         mkrid uuid,
         marker_name text,
         marker_base integer,
         segment1_id uuid,
         group1_id uuid,  --should be 'group'
         optval1 float,
         segment2_id uuid,
         group2_id uuid,
         optval2 float)on commit drop;
  truncate optmarkers;
  insert into optmarkers
  select m.id as mkrid
    ,m.name as marker_name
    ,m.basepos as marker_base
    ,null::uuid as segment1_id
    ,null::uuid as group1_id
    ,null::float as optval1
    ,null::uuid as segment2_id
    ,null::uuid as group2_id
    ,null::float as optval2
  from marker m join markerset_member mm on m.id = member_id
  where mm.markerset_id = msetid;
  get diagnostics markercount = ROW_COUNT;
  create unique index on optmarkers(marker_base);
  raise notice '%: markerset preped, % markers total', clock_timestamp(), markercount;
--
  for seg in
      select s.id, s.startbase, s.endbase, m.group_id
             , pv(s.events_less, s.events_equal, s.events_greater, 1) as spv
             , array_length(b.probands, 1) as pbs
             , s.endbase - s.startbase as bases
      from segment s 
      	   join probandset b on s.probandset_id = b.id 
 	   join probandset_group_member m on b.id = m.member_id and m.group_id = any(groups)
      where s.markerset_id = msetid
      order by spv, pbs, bases
  loop
    seg1count = seg1count +1;
    update optmarkers as o 
           set segment1_id = seg.id, 
               group1_id = seg.group_id, 
               optval1 = seg.spv
    where o.marker_base >= seg.startbase 
          and o.marker_base <= seg.endbase 
          and o.segment1_id is null;
    get diagnostics rowscount = ROW_COUNT;
    updatecount = updatecount + rowscount;
    if updatecount >= markercount then
      raise notice '%: all markers accounted for once at % segments', clock_timestamp(), seg1count;
      exit;
    end if;
  end loop;
--
  raise notice '%: first pass completed, % markers assigned a pval', clock_timestamp(), updatecount;
  updatecount = 0;
  for seg in
      select s.id, s.startbase, s.endbase, m.group_id
             , pv(s.events_less, s.events_equal, s.events_greater, 1) as spv
             , array_length(b.probands,1) as pbs
             , s.endbase - s.startbase as bases
      from segment s join probandset b on s.probandset_id = b.id join probandset_group_member m on b.id = m.member_id and m.group_id = any(groups)
      where s.markerset_id = msetid
      order by spv, pbs, bases
  loop
    seg2count = seg2count +1;
    update optmarkers as o 
    set segment2_id = seg.id, 
        group2_id = seg.group_id, 
        optval2 = seg.spv
    where o.marker_base between seg.startbase and seg.endbase
       and o.segment2_id is null
       and seg.group_id != o.group1_id;
    get diagnostics rowscount = ROW_COUNT;
    updatecount = updatecount + rowscount;
    if updatecount >= markercount then
      raise notice '%: all markers in accounted for twice after % segments', clock_timestamp(), seg2count;
      exit;
    end if;
  end loop;
  raise notice '%: second pass completed, % markers assigned a pval', clock_timestamp(), updatecount;
--
  return query select * from optmarkers;
end;
$$
language plpgsql;
