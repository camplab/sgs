-- explain analyse select t.name,
--        'SIG' as interest,
--        cytofinder(s.chrom, s.startbase, s.endbase),
--        d.*
--        from optimalset t 
--        join optimalset_duo_segment d on t.id = d.threshold_id
--        join optimalset_segment ts on d.threshold_segment1_id = ts.id
--        join segment s on ts.segment_id = s.id 
-- where d.fisher_pvalue <= t.significant/*interest = 'SIG' */
-- order by t.name, interest;
create or replace function public.interesting_segments()
returns table (name text, interest text, band text, n bigint)
as $$begin	 
  return query
  with interest as (
  select 
       t.id
       ,t.name
       , case when d.fisher_pvalue <= t.significant then 'SIG'
	     when d.fisher_pvalue between t.significant and t.suggestive then 'SUG'
	     end as interest
       , d.optimalset_segment1_id
       from optimalset t 
	    join optimalset_duosegment d on t.id = d.optimalset_id
       where d.fisher_pvalue <= t.suggestive
  )
  select i.name
	 , i.interest
	 , cytofinder(s.chrom, s.startbase, s.endbase) as band
	 ,count(*)
  from interest i 
       join optimalset_segment ts on i.optimalset_segment1_id = ts.id
       join segment s on ts.segment_id = s.id
  group by i.name, band, i.interest;
  return;
end;
$$
language plpgsql;
;
