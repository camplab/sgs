#!/bin/bash -ex
pifdef="clark:sptb//feldkamp:bdef,omph//smith:gev//jorde:als"
function usage() {
    printf "usage: %s versionSQL\n" $1
    printf "be sure you've set PG_SUSER to name of postgres admin account.\n"
    printf "Do not run this in the background (unless you\'re really clever)\n"
    printf "Good-bye\n"
    exit 1
}

function pifHelp {
    printf "\n\texport PIFLIST=PI1:sch1[,sch2...][//PI2:sch1[,sch2...]] (currently %s)" ${PIFLIST:-unset}
    printf "\n\texport DBHOST=your.database.server (currently %s)" ${DBHOST:-unset}
    printf "\n"
    usage $0
}    

function pifCheck {
    printf "Then you'll want to set the following environment variables:\n"
    pifHelp;
    printf "Good-bye\n"
    exit 1
}

function isProjectFunc {
    grep -lq sgstemplate $1
}

if [[ ! -n $PG_SUSER ]]
then
    printf "PG_SUSER is not set\n"
    usage $0
fi

dbdest=${DBHOST:-csgsdb.chpc.utah.edu}
dbport=${DBHOST##*:}
if [[ "$dbport" == "$dbdest" ]]
then 
    dbport=5432
else
    dbdest=${dbdest/:*}
fi

#if [[ -n $PIFLIST ]]; then echo using incoming def: "$PIFLIST"; fi
pifs=( `echo ${PIFLIST:-$pifdef} | sed 's;//;\ ;g'` )

picount=${#pifs[@]}
printf "The targets are:\n"
printf "DBHOST is %s:%d\n" $dbdest $dbport
printf "\t%s\n"  "${pifs[@]}"

g2g=0
read -p "Looks ok? " okcheck
case $okcheck in
    [yY]*)
        g2g=1;;
    *)
        pifCheck;;
esac

sql=$1
if [[ "$sql"x == "x" ]]; then pifHelp; fi

while [[ "$sql"x != "x" ]];
do
    vers=$(basename $sql)
    qsql="'$vers'"
    vpath=$(dirname $sql)
    if [[ $FORCE_ENTRY ]]
    then
	echo COMMENTING SQL, touching original $vers
	sed -i s/\;/\\\\p\\\\r/ $vers
    fi
    echo "************" $vers
    cat $sql
    for (( i=0; i<${picount}; i++ )); 
    do
        pif=${pifs[0]};
        pi=${pif/:*/}
	if isProjectFunc "$sql"
	then
            fsv=${pif/*:/}
            fs=( `echo $fsv | sed s/,/\ /g` )
            fc=${#fs[@]}
            for (( j=0; j<$fc; j++ )); do
		eft=${fs[$j]}
		sed -e s/sgstemplate/$eft/g $sql > sedsql
		qeft="'$eft'"
		printf "\n=== migrating project %s with %s\n" $eft $vers
		echo psql --host=$dbdest --dbname=$pi --user=$PG_SUSER --port=$dbport on $sql
		psql --host=$dbdest --dbname=postgres --user=$PG_SUSER --port=$dbport <<EOF
\c $pi
set search_path=${eft},base,public;
begin;
\i sedsql
insert into base.migration(file,project) values(${qsql}, ${qeft});
commit;
EOF
            done
	else
            cp $sql sedsql;
	    qbase="'base'"
	    echo "====== BASE MIGRATION"
            psql --host=$dbdest --dbname=postgres --user=$PG_SUSER --port=$dbport <<EOF
\c $pi
set search_path=base,public;
begin;
\i sedsql
insert into base.migration(file,project) values(${qsql}, ${qbase});
commit;
EOF
	fi
    done
    shift
    sql=$1
    if [[ -n $sql ]]
    then
	printf "Continue?[y/n]: "
	for i in {1..5};
	do
	    read -t 1 g2g
	    case $g2g in
		[nN]*)
		    exit 2;;
		[yY]*)
		    break;;
		*)
		    printf " ."
		    ;;
	    esac
	done
    fi
done
rm -f sedsql
