#!/bin/bash -e
function usage {
    printf "usage: %s --dbhost|-d <host> --inv|-i <surname> --study|-s <project> --schema-dir|-c <schema dir> --migration <version> [--rege|n-r] [--dba|-a <dba role>]\n"  $1
    printf "\tor\t%s --help|-h \n" $1
    printf "\twhere\n\t%-12s names the machine running postgres \n" "host"
    printf "\t%-12s uniquely identifies the new investigator\n" "surname"
    printf "\t%-12s name for this investigative focus of PI\n" "project"
    printf "\t%-12s is the location of related scripts [default is %s]\n" "schema dir" $progDir
    printf "\t%-12s is the current schema version number (last applied migration eg V5002_addMigrationTable)\n" "migration" 
    printf "\t%-12s toggles regeneration of the DDL\n" "--regen"
    printf "\t%-12s postgres account name with create database privileges [default is postgres]\n" "dba role"
    exit 1
}

function listMigrations {
    local fdir=$1
    local found=
    local comma=""
    if [[ -d $fdir ]]
    then
	for m in $(find $fdir -name V\*.sql)
	do
	    printf "%s%s" $comma $m
	    found=1
	    comma=", "
	done
    fi
    if [[ $found ]]
    then
	printf "\nplease run the migrations named above\n"
    fi
}

prog=$(basename $0)
progDir=$(dirname $(realpath $0))
regen=
sqldir=$progDir
while [[ ${#@} -gt 0 ]]
do
    case $1 in
        -d|--dbh*|--db)
            shift
            dbhost=$1;
            shift;;
        -i|--investigator|--inv*)
            shift
            sgsdb=$1;
            shift;;
        -s|--study|--st*)
            shift
            projname=$1;
            shift;;
        -c|--schema-dir|--sch*)
            shift
            sqldir=$1
	    if [[ ! -e $sqldir ]]
	    then
		printf "non-existent sql dir. [%s]\n" $sqldir
		usage $0
	    fi
            shift;;
	-m|--mi*)
	    shift
	    bv=$1
	    baseversion="'$bv'"
	    shift;;
        -r|--re*)
            regen=1
            shift;;
	-a|--dba)
	    shift;
	    dba=$1;
	    shift;;
	-h|--help)
	    usage $prog;;
	*)
	    echo Unknow arg $1
	    usage $prog;;
	esac
done
if [[ "${dbhost}x" == "x" ]]
then
    echo need to set --dbhost
    usage $prog
fi

if [[ "${sgsdb}x" == "x" ]]
then
    echo need to set --sgsdb
    usage $prog
fi

if [[ "${projname}x" == "x" ]]
then
    echo need to set --name
    usage $prog
fi

if [[ "${sqldir}x" == "x" ]]
then
    echo need to set --schema-dir
    usage $prog
fi

if [[ ! -d $sqldir ]]
then 
    printf "%s (schema-dir) is not a directory??\n" $schema
    usage $prog
fi

funcdir=${sqldir}/functions
PRJ=$projname

if [[ $regen ]]
then
    echo getting definitions from $dbhost
    ${sqldir}/regenerateTemplates.sh $dbhost sgsdev
fi

sed -e s/sgstemplate/$PRJ/g ${sqldir}/baselineSchema.sql > /tmp/createdb-$$_$PRJ.sql
printf "Starting db build: %s::%s\n" $sgsdb $PRJ
psql --user=${dba:=postgres} --dbname postgres --host=$dbhost <<EOF
create database $sgsdb;
\c $sgsdb
\i ${sqldir}/functions/uuidFunctions.sql
\i /tmp/createdb-$$_$PRJ.sql 
insert into base.migration(file,project) values($baseversion, 'base');
EOF
#rm /tmp/createdb-$$_$PRJ.sql

echo '****' loading functions from $funcdir via $funcfileee
funcfile=/tmp/allfunctions.$$
printf "\\c %s;\n" $sgsdb > $funcfile
for f in $(find $funcdir -maxdepth 1 -name \*.sql); 
do
    echo adding function file $f 
    printf "\\i %s\n" $f >> $funcfile
done

for t in $(find ${sqldir}/../tabledata -type f); 
do
    bn=$(basename $t)
    printf "\\copy base.%s from %s csv;\n" ${bn} $t >> $funcfile
done
    
psql --host=$dbhost --user=${dba:=postgres} --file $funcfile
#rm $funcfile


## NOW ADD FIRST STUDY
printf "Adding initial study: %s\n" $projname
${progDir}/createStudy.sh --dbhost $dbhost --study $projname --inv $sgsdb --sqldir $sqldir

printf "\n"
listMigrations ${sqldir}/../migration/project
listMigrations ${sqldir}/../migration/base
