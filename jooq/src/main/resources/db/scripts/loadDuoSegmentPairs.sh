#!/bin/bash -e

dbh=${DBHOST:-campdb.chpc.utah.edu}
pfile=$1
case $pfile in 
    -h|--h*|*help*)
        printf "usage: %s <pairs-of-names file> <markersetpattern> PI project\n" $0
        printf "\t where the <pair-of-names> are written as postgres arrays in braces"
        printf "\t\t{name-one, name two} - N.B. no quotes"
        exit;;
    *)
        ;;
esac
if [[ "$pfile"x == "x" ]]; then echo no name pairs file;exit; fi
shift
echo duo optimals for $pfile
msetpattern=$1
if [[ "$msetpattern"x == "x" ]]; then echo no marker set name patter; exit; fi
shift
prince=$1
if [[ "$prince"x == "x" ]]; then echo no investigator name; exit; fi
shift
proj=$1
if [[ "$proj"x == "x" ]]; then echo no investigator name; exit; fi
shift

while read pair; do
    psql --host=$dbh --dbname=$prince --user=$proj <<EOF
begin;
select load_duosegment_pair('$pair', '$msetpattern');
commit;
EOF
done < $pfile
