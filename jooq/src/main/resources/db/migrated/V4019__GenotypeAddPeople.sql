alter table sgstemplate.genotype add column people_id uuid null;

update sgstemplate.genotype as g set people_id = m.people_id
from base.people_member m
where g.person_id = m.person_id;
alter table sgstemplate.genotype 
      alter column people_id set not null,
      drop constraint genotype_pkey,
      add primary key(person_id, markerset_id,people_id),
      add foreign key(people_id) references base.people(id)
      ;
      

