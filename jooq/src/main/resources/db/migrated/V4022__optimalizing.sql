alter table sgstemplate.threshold rename to optimalset;

alter table sgstemplate.threshold_segment rename column smooth_pvalue to pvalue;
alter table sgstemplate.threshold_segment rename column threshold_id to optimalset_id;
alter table sgstemplate.threshold_segment rename to optimalset_segment;

alter table sgstemplate.threshold_duo_segment rename column threshold_id to optimalset_id;
alter table sgstemplate.threshold_duo_segment rename column threshold_segment1_id to optimalset_segment1_id ;
alter table sgstemplate.threshold_duo_segment rename column threshold_segment2_id to optimalset_segment2_id ;
alter table sgstemplate.threshold_duo_segment rename to optimalset_duosegment;

alter table sgstemplate.duo_chaseable rename column threshold_id to optimalset_id;
