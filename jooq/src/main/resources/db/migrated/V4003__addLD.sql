create table base.ld (
       id uuid primary key
       , markerset_id uuid references base.markerset(id)
       )
;

create table base.ld_clique (
       ldid uuid references base.ld(id)
       , clique_order int
       , clique_loci int[]
       , clique_recomb_fractions float[]
       )
;

create unique index ld_clique_ukey 
       on base.ld_clique(ldid, clique_order)
;
