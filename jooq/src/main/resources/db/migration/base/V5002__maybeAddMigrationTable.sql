create table if not exists base.migration(
       id integer generated always as identity,
       file text,
       project text
);
