-- These are begin renamed, args change
-- genome_pvalue_mono    | people_name text, markers_rx text                                                  
-- genome_pvalue_mono    | people_name text, markers_rx text, conf double precision, maxi integer             
-- genome_threshold_mono | pbs_name text, markers_rx text                                                     
-- genome_threshold_mono | pbs_name text, markers_rx text, conf double precision, maxi integer                
-- optimal_pvalue_mono   | pbsg_name text, markers_name text, chr integer, conf double precision, maxi integer
-- These are begin renamed, args change

drop function genome_pvalue_mono(text,text);
drop function genome_pvalue_mono(text,text,double precision,integer);
drop function genome_threshold_mono(text,text);
drop function genome_threshold_mono(text, text,double precision,integer);
drop function optimal_pvalue_mono(text,text,integer,double precision,integer);

create or replace function public.optimal_pvalue_mono(pbsg_name text, markers_name text, chr int, conf float, empirical boolean default false)
returns table (segment_id uuid, base1 int, pval float)
as $$
-- ------
-- single "people" best segments for given chrom
-- ------
declare
  segp          RECORD;
  mkset         uuid;
  pbsgid        uuid;
  rcount        int;
  mkrcnt        int;
  segsdone      int  :=  0;
  totalinserts  int  :=  0;
-- ------
begin
  select id into mkset from markerset where name = markers_name and chrom = chr; 
  select id into pbsgid from probandset_group where name = pbsg_name;
  if pbsgid is null then
     raise exception 'NO SUCH probandset_group name: %', pbsg_name;
  end if;
  raise notice '%: markerset id is %(%), people (%) id is %', clock_timestamp(), mkset, markers_name, pbsgid, pbsg_name;
--
  drop table if exists collected;	
  drop table if exists mrkidx;
--
  create temp table if not exists imputed_pvalue_t(segment_id uuid, probandset_group_id uuid, ipv float) on commit drop;
  create temp table collected(segid uuid, cpv float) on commit drop;
  create temp table mrkidx on commit drop as
    select member_id as marker_id, ordinal 
    from markerset_member where markerset_id = mkset;
  get diagnostics mkrcnt = ROW_COUNT;
  create unique index on mrkidx(ordinal);
  raise notice '%: working with % markers', clock_timestamp(), mkrcnt;
--
  insert into imputed_pvalue_t (segment_id, probandset_group_id, ipv)
  select s.id,
         pbsgid,
	 case when empirical then pvr(s)
	      else rand_pv( wilson_ci(conf,  s.threshold_events, 1000000))
	 end
  from segment s join probandset b on s.probandset_id = b.id
       join probandset_group_member m on b.id = m.member_id
  where s.markerset_id = mkset 
        and m.group_id = pbsgid;
--
  get diagnostics rcount = ROW_COUNT;
  raise notice '%: added % segments to imputed_pvalue_t', clock_timestamp(), rcount;
--  
  for segp in
    select s.id, s.firstmarker, s.lastmarker, 
           v.ipv,
           array_length(p.probands,1) as pbslen,
           s.lastmarker - s.firstmarker + 1 as mks
    from segment s 
         join imputed_pvalue_t v on s.id = v.segment_id
         join probandset p on s.probandset_id = p.id
         join probandset_group_member m on p.id = m.member_id
    where s.markerset_id = mkset
          and m.group_id = pbsgid
    order by ipv asc, p.meioses desc, pbslen desc, mks desc, p.name asc
  LOOP
    delete from mrkidx where ordinal between segp.firstmarker and segp.lastmarker;
    get diagnostics rcount = ROW_COUNT;
    segsdone = segsdone + 1;
    if rcount > 0 then
       insert into collected values(segp.id, segp.ipv);
       totalinserts = totalinserts + rcount;
       if totalinserts = mkrcnt then  -- really totalDELETES
          raise notice '%: no markers left on %th segment %', clock_timestamp(), segsdone, segp.id;
          exit;
       end if;
    end if;
  end LOOP;
  select count(*) into rcount from mrkidx;
  raise notice '%: finished assessing markers; % leftover', clock_timestamp(), rcount;
  truncate imputed_pvalue_t;
--
  return query
    select s.id as segment_id, s.startbase as base1, c.cpv
    from segment as s join collected as c on s.id = c.segid
    order by s.startbase;
end;
$$ language plpgsql;


create or replace function public.genome_pvalue_mono( people_name text, 
       	  	  	   			      markers_id uuid, 
						      conf float,
						      empirical boolean )
returns table (segment_id uuid, base1 int, pval float)
as $$
-- ------
-- single "people" genome wide best segments
-- ------
declare
  loops int := 0;
  mvec RECORD;                
  markerset_id uuid;
  markerset_name text;
begin
  create temp table if not exists goptset(segment_id uuid, startbase int, pvalue float) on commit drop;
  truncate table goptset;
  for mvec in      
    select chrom, name
    from markerset where genome_id = markers_id
    order by chrom
  loop
    loops = loops+1;
    raise notice 'doing chrom %', mvec.chrom;
    insert into goptset
    select * from optimal_pvalue_mono(people_name, mvec.name, mvec.chrom, conf, empirical);
  end loop;
  if loops = 0 then
    raise exception 'No markersets found matching "%"', markers_id;
  end if;
  return query select * from goptset;
end;
$$ language plpgsql;

create or replace function public.genome_pvalue_mono( people_name text, 
       	  	  	   			      markers_name text )
returns table (segment_id uuid, base1 int, pval float)
as $$
-- ------
-- single "people" genome wide best segments
-- ------
declare
  gms_id uuid;
begin
  select id into gms_id from genome_markerset g where g.name = markers_name;
  raise notice 'Defaulting confidence level to 1.96. All p-values will be smoothed';
  return query select * from genome_pvalue_mono(people_name, gms_id, 1.96, true);
end;
$$ language plpgsql;
--
-- jooq/src/main/resources/db/scripts/functions/genomeOptimalsetMono.sql
--
create or replace function public.genome_optimalset_mono(pbs_name text, genome_name text, conf float, empirical boolean)
returns uuid
as $$
declare
  genome_markerset_id uuid;
  pid uuid;        
  tid uuid;
  gids uuid[];  -- because threshold record uses an array of probandset group ids
  rows int;
  startAt timestamp;
begin
-- create a optimalset entry and load (mono) optimal set
  select clock_timestamp() into startAt;
  select id into genome_markerset_id from genome_markerset where name = genome_name;
  tid = uuid_generate_v4();
  select array_agg(id) into gids from probandset_group where name = pbs_name; 
  get diagnostics rows = ROW_COUNT;
  if rows = 0 then
     raise exception 'NO PROBANDSET_GROUP for %s', pbs_name;
  else
     raise notice 'group id is %', gids[1];
  end if;
-- process to ties ends together
  select threshold_process(tid, genome_name, empirical) into pid;
--
  insert into optimalset(id, suggestive, significant, name, probandset_groups)
         values(tid, 0.0, 0.0, pbs_name, gids);
  get diagnostics rows = ROW_COUNT;
  if rows = 1 then
    raise notice 'New optimalset: %', tid;
  else
    raise exception 'Failed to insert optimalset record';
  end if;
  insert into optimalset_segment(id, optimalset_id, segment_id, pvalue, mu_t)
         select uuid_generate_v4(), tid, f.segment_id, f.pval, null
         from genome_pvalue_mono(pbs_name, genome_markerset_id, conf, empirical) as f;
  get diagnostics rows = ROW_COUNT;
  raise notice 'process % generated optval set of % segments at % (duration: %)', pid, rows, clock_timestamp(), clock_timestamp() - startAt;
  return tid;
end;
$$ language plpgsql;

create or replace function public.genome_optimalset_smooth(pbs_name text, genome_name text)
returns uuid
as $$
declare
  tid uuid;
begin
  select genome_optimalset_mono(pbs_name, genome_name, 1.96, false) into tid;
  return tid;
end;
$$ language plpgsql;

create or replace function public.genome_optimalset_result(pbsname text, genomename text)
returns uuid
as $$
begin
   select genome_threshold_mono(pbsname, genomename, 1.96, true);
end;
$$ language plpgsql;
