package edu.utah.camplab.db.generated.public_.routines;


import edu.utah.camplab.db.generated.public_.Public;

import java.util.UUID;

import javax.annotation.processing.Generated;

import org.jooq.Field;
import org.jooq.Parameter;
import org.jooq.impl.AbstractRoutine;
import org.jooq.impl.Internal;
import org.jooq.impl.SQLDataType;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "https://www.jooq.org",
        "jOOQ version:3.16.5",
        "schema version:5002"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Issorteduuids extends AbstractRoutine<Boolean> {

    private static final long serialVersionUID = 1L;

    /**
     * The parameter <code>public.issorteduuids.RETURN_VALUE</code>.
     */
    public static final Parameter<Boolean> RETURN_VALUE = Internal.createParameter("RETURN_VALUE", SQLDataType.BOOLEAN, false, false);

    /**
     * The parameter <code>public.issorteduuids.uset</code>.
     */
    public static final Parameter<UUID[]> USET = Internal.createParameter("uset", SQLDataType.UUID.getArrayDataType(), false, false);

    /**
     * Create a new routine call instance
     */
    public Issorteduuids() {
        super("issorteduuids", Public.PUBLIC, SQLDataType.BOOLEAN);

        setReturnParameter(RETURN_VALUE);
        addInParameter(USET);
    }

    /**
     * Set the <code>uset</code> parameter IN value to the routine
     */
    public void setUset(UUID[] value) {
        setValue(USET, value);
    }

    /**
     * Set the <code>uset</code> parameter to the function to be used with a
     * {@link org.jooq.Select} statement
     */
    public Issorteduuids setUset(Field<UUID[]> field) {
        setField(USET, field);
        return this;
    }
}
