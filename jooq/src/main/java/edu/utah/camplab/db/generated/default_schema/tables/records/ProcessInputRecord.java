package edu.utah.camplab.db.generated.default_schema.tables.records;


import edu.utah.camplab.db.generated.default_schema.tables.ProcessInput;

import java.util.UUID;

import javax.annotation.processing.Generated;

import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record4;
import org.jooq.Row4;
import org.jooq.impl.UpdatableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "https://www.jooq.org",
        "jOOQ version:3.16.5",
        "schema version:5002"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class ProcessInputRecord extends UpdatableRecordImpl<ProcessInputRecord> implements Record4<UUID, UUID, String, String> {

    private static final long serialVersionUID = 1L;

    /**
     * Setter for <code>process_input.id</code>.
     */
    public ProcessInputRecord setId(UUID value) {
        set(0, value);
        return this;
    }

    /**
     * Getter for <code>process_input.id</code>.
     */
    public UUID getId() {
        return (UUID) get(0);
    }

    /**
     * Setter for <code>process_input.process_id</code>.
     */
    public ProcessInputRecord setProcessId(UUID value) {
        set(1, value);
        return this;
    }

    /**
     * Getter for <code>process_input.process_id</code>.
     */
    public UUID getProcessId() {
        return (UUID) get(1);
    }

    /**
     * Setter for <code>process_input.input_type</code>.
     */
    public ProcessInputRecord setInputType(String value) {
        set(2, value);
        return this;
    }

    /**
     * Getter for <code>process_input.input_type</code>.
     */
    public String getInputType() {
        return (String) get(2);
    }

    /**
     * Setter for <code>process_input.input_ref</code>.
     */
    public ProcessInputRecord setInputRef(String value) {
        set(3, value);
        return this;
    }

    /**
     * Getter for <code>process_input.input_ref</code>.
     */
    public String getInputRef() {
        return (String) get(3);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    @Override
    public Record1<UUID> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Record4 type implementation
    // -------------------------------------------------------------------------

    @Override
    public Row4<UUID, UUID, String, String> fieldsRow() {
        return (Row4) super.fieldsRow();
    }

    @Override
    public Row4<UUID, UUID, String, String> valuesRow() {
        return (Row4) super.valuesRow();
    }

    @Override
    public Field<UUID> field1() {
        return ProcessInput.PROCESS_INPUT.ID;
    }

    @Override
    public Field<UUID> field2() {
        return ProcessInput.PROCESS_INPUT.PROCESS_ID;
    }

    @Override
    public Field<String> field3() {
        return ProcessInput.PROCESS_INPUT.INPUT_TYPE;
    }

    @Override
    public Field<String> field4() {
        return ProcessInput.PROCESS_INPUT.INPUT_REF;
    }

    @Override
    public UUID component1() {
        return getId();
    }

    @Override
    public UUID component2() {
        return getProcessId();
    }

    @Override
    public String component3() {
        return getInputType();
    }

    @Override
    public String component4() {
        return getInputRef();
    }

    @Override
    public UUID value1() {
        return getId();
    }

    @Override
    public UUID value2() {
        return getProcessId();
    }

    @Override
    public String value3() {
        return getInputType();
    }

    @Override
    public String value4() {
        return getInputRef();
    }

    @Override
    public ProcessInputRecord value1(UUID value) {
        setId(value);
        return this;
    }

    @Override
    public ProcessInputRecord value2(UUID value) {
        setProcessId(value);
        return this;
    }

    @Override
    public ProcessInputRecord value3(String value) {
        setInputType(value);
        return this;
    }

    @Override
    public ProcessInputRecord value4(String value) {
        setInputRef(value);
        return this;
    }

    @Override
    public ProcessInputRecord values(UUID value1, UUID value2, String value3, String value4) {
        value1(value1);
        value2(value2);
        value3(value3);
        value4(value4);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached ProcessInputRecord
     */
    public ProcessInputRecord() {
        super(ProcessInput.PROCESS_INPUT);
    }

    /**
     * Create a detached, initialised ProcessInputRecord
     */
    public ProcessInputRecord(UUID id, UUID processId, String inputType, String inputRef) {
        super(ProcessInput.PROCESS_INPUT);

        setId(id);
        setProcessId(processId);
        setInputType(inputType);
        setInputRef(inputRef);
    }

    /**
     * Create a detached, initialised ProcessInputRecord
     */
    public ProcessInputRecord(edu.utah.camplab.db.generated.default_schema.tables.pojos.ProcessInput value) {
        super(ProcessInput.PROCESS_INPUT);

        if (value != null) {
            setId(value.getId());
            setProcessId(value.getProcessId());
            setInputType(value.getInputType());
            setInputRef(value.getInputRef());
        }
    }
}
