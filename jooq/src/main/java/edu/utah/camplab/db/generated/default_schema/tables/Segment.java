package edu.utah.camplab.db.generated.default_schema.tables;


import edu.utah.camplab.db.generated.base.tables.Markerset;
import edu.utah.camplab.db.generated.default_schema.DefaultSchema;
import edu.utah.camplab.db.generated.default_schema.Indexes;
import edu.utah.camplab.db.generated.default_schema.Keys;
import edu.utah.camplab.db.generated.default_schema.tables.records.SegmentRecord;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import javax.annotation.processing.Generated;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Index;
import org.jooq.Name;
import org.jooq.Record;
import org.jooq.Row12;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.TableOptions;
import org.jooq.UniqueKey;
import org.jooq.impl.DSL;
import org.jooq.impl.SQLDataType;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "https://www.jooq.org",
        "jOOQ version:3.16.5",
        "schema version:5002"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Segment extends TableImpl<SegmentRecord> {

    private static final long serialVersionUID = 1L;

    /**
     * The reference instance of <code>segment</code>
     */
    public static final Segment SEGMENT = new Segment();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<SegmentRecord> getRecordType() {
        return SegmentRecord.class;
    }

    /**
     * The column <code>segment.id</code>.
     */
    public final TableField<SegmentRecord, UUID> ID = createField(DSL.name("id"), SQLDataType.UUID.nullable(false), this, "");

    /**
     * The column <code>segment.chrom</code>.
     */
    public final TableField<SegmentRecord, Integer> CHROM = createField(DSL.name("chrom"), SQLDataType.INTEGER.nullable(false), this, "");

    /**
     * The column <code>segment.markerset_id</code>.
     */
    public final TableField<SegmentRecord, UUID> MARKERSET_ID = createField(DSL.name("markerset_id"), SQLDataType.UUID.nullable(false), this, "");

    /**
     * The column <code>segment.probandset_id</code>.
     */
    public final TableField<SegmentRecord, UUID> PROBANDSET_ID = createField(DSL.name("probandset_id"), SQLDataType.UUID.nullable(false), this, "");

    /**
     * The column <code>segment.startbase</code>.
     */
    public final TableField<SegmentRecord, Integer> STARTBASE = createField(DSL.name("startbase"), SQLDataType.INTEGER.nullable(false), this, "");

    /**
     * The column <code>segment.endbase</code>.
     */
    public final TableField<SegmentRecord, Integer> ENDBASE = createField(DSL.name("endbase"), SQLDataType.INTEGER.nullable(false), this, "");

    /**
     * The column <code>segment.firstmarker</code>.
     */
    public final TableField<SegmentRecord, Integer> FIRSTMARKER = createField(DSL.name("firstmarker"), SQLDataType.INTEGER.nullable(false), this, "");

    /**
     * The column <code>segment.lastmarker</code>.
     */
    public final TableField<SegmentRecord, Integer> LASTMARKER = createField(DSL.name("lastmarker"), SQLDataType.INTEGER.nullable(false), this, "");

    /**
     * The column <code>segment.events_less</code>.
     */
    public final TableField<SegmentRecord, Long> EVENTS_LESS = createField(DSL.name("events_less"), SQLDataType.BIGINT.nullable(false).defaultValue(DSL.field("0", SQLDataType.BIGINT)), this, "");

    /**
     * The column <code>segment.events_equal</code>.
     */
    public final TableField<SegmentRecord, Long> EVENTS_EQUAL = createField(DSL.name("events_equal"), SQLDataType.BIGINT.nullable(false).defaultValue(DSL.field("0", SQLDataType.BIGINT)), this, "");

    /**
     * The column <code>segment.events_greater</code>.
     */
    public final TableField<SegmentRecord, Long> EVENTS_GREATER = createField(DSL.name("events_greater"), SQLDataType.BIGINT.nullable(false).defaultValue(DSL.field("0", SQLDataType.BIGINT)), this, "");

    /**
     * The column <code>segment.threshold_events</code>.
     */
    public final TableField<SegmentRecord, Integer> THRESHOLD_EVENTS = createField(DSL.name("threshold_events"), SQLDataType.INTEGER, this, "");

    private Segment(Name alias, Table<SegmentRecord> aliased) {
        this(alias, aliased, null);
    }

    private Segment(Name alias, Table<SegmentRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, DSL.comment(""), TableOptions.table());
    }

    /**
     * Create an aliased <code>segment</code> table reference
     */
    public Segment(String alias) {
        this(DSL.name(alias), SEGMENT);
    }

    /**
     * Create an aliased <code>segment</code> table reference
     */
    public Segment(Name alias) {
        this(alias, SEGMENT);
    }

    /**
     * Create a <code>segment</code> table reference
     */
    public Segment() {
        this(DSL.name("segment"), null);
    }

    public <O extends Record> Segment(Table<O> child, ForeignKey<O, SegmentRecord> key) {
        super(child, key, SEGMENT);
    }

    @Override
    public Schema getSchema() {
        return aliased() ? null : DefaultSchema.DEFAULT_SCHEMA;
    }

    @Override
    public List<Index> getIndexes() {
        return Arrays.asList(Indexes.SEGMENT_MARKERSET_ID_IDX, Indexes.SEGMENT_PROBANDSET_ID_IDX, Indexes.USEG);
    }

    @Override
    public UniqueKey<SegmentRecord> getPrimaryKey() {
        return Keys.SEGMENT_PKEY;
    }

    @Override
    public List<ForeignKey<SegmentRecord, ?>> getReferences() {
        return Arrays.asList(Keys.SEGMENT__SEGMENT_MARKERSET_ID_FKEY, Keys.SEGMENT__SEGMENT_PROBANDSET_ID_FKEY);
    }

    private transient Markerset _markerset;
    private transient Probandset _probandset;

    /**
     * Get the implicit join path to the <code>base.markerset</code> table.
     */
    public Markerset markerset() {
        if (_markerset == null)
            _markerset = new Markerset(this, Keys.SEGMENT__SEGMENT_MARKERSET_ID_FKEY);

        return _markerset;
    }

    /**
     * Get the implicit join path to the <code>sgstemplate.probandset</code>
     * table.
     */
    public Probandset probandset() {
        if (_probandset == null)
            _probandset = new Probandset(this, Keys.SEGMENT__SEGMENT_PROBANDSET_ID_FKEY);

        return _probandset;
    }

    @Override
    public Segment as(String alias) {
        return new Segment(DSL.name(alias), this);
    }

    @Override
    public Segment as(Name alias) {
        return new Segment(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public Segment rename(String name) {
        return new Segment(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public Segment rename(Name name) {
        return new Segment(name, null);
    }

    // -------------------------------------------------------------------------
    // Row12 type methods
    // -------------------------------------------------------------------------

    @Override
    public Row12<UUID, Integer, UUID, UUID, Integer, Integer, Integer, Integer, Long, Long, Long, Integer> fieldsRow() {
        return (Row12) super.fieldsRow();
    }
}
