package edu.utah.camplab.db.generated.base.tables.records;


import edu.utah.camplab.db.generated.base.tables.Marker;

import java.util.UUID;

import javax.annotation.processing.Generated;

import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record7;
import org.jooq.Row7;
import org.jooq.impl.UpdatableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "https://www.jooq.org",
        "jOOQ version:3.16.5",
        "schema version:5002"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class MarkerRecord extends UpdatableRecordImpl<MarkerRecord> implements Record7<UUID, String, Integer, Integer, String[], Integer, String> {

    private static final long serialVersionUID = 1L;

    /**
     * Setter for <code>base.marker.id</code>.
     */
    public MarkerRecord setId(UUID value) {
        set(0, value);
        return this;
    }

    /**
     * Getter for <code>base.marker.id</code>.
     */
    public UUID getId() {
        return (UUID) get(0);
    }

    /**
     * Setter for <code>base.marker.name</code>.
     */
    public MarkerRecord setName(String value) {
        set(1, value);
        return this;
    }

    /**
     * Getter for <code>base.marker.name</code>.
     */
    public String getName() {
        return (String) get(1);
    }

    /**
     * Setter for <code>base.marker.chrom</code>.
     */
    public MarkerRecord setChrom(Integer value) {
        set(2, value);
        return this;
    }

    /**
     * Getter for <code>base.marker.chrom</code>.
     */
    public Integer getChrom() {
        return (Integer) get(2);
    }

    /**
     * Setter for <code>base.marker.basepos37</code>.
     */
    public MarkerRecord setBasepos37(Integer value) {
        set(3, value);
        return this;
    }

    /**
     * Getter for <code>base.marker.basepos37</code>.
     */
    public Integer getBasepos37() {
        return (Integer) get(3);
    }

    /**
     * Setter for <code>base.marker.alleles</code>.
     */
    public MarkerRecord setAlleles(String[] value) {
        set(4, value);
        return this;
    }

    /**
     * Getter for <code>base.marker.alleles</code>.
     */
    public String[] getAlleles() {
        return (String[]) get(4);
    }

    /**
     * Setter for <code>base.marker.basepos38</code>.
     */
    public MarkerRecord setBasepos38(Integer value) {
        set(5, value);
        return this;
    }

    /**
     * Getter for <code>base.marker.basepos38</code>.
     */
    public Integer getBasepos38() {
        return (Integer) get(5);
    }

    /**
     * Setter for <code>base.marker.unknown_allele</code>.
     */
    public MarkerRecord setUnknownAllele(String value) {
        set(6, value);
        return this;
    }

    /**
     * Getter for <code>base.marker.unknown_allele</code>.
     */
    public String getUnknownAllele() {
        return (String) get(6);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    @Override
    public Record1<UUID> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Record7 type implementation
    // -------------------------------------------------------------------------

    @Override
    public Row7<UUID, String, Integer, Integer, String[], Integer, String> fieldsRow() {
        return (Row7) super.fieldsRow();
    }

    @Override
    public Row7<UUID, String, Integer, Integer, String[], Integer, String> valuesRow() {
        return (Row7) super.valuesRow();
    }

    @Override
    public Field<UUID> field1() {
        return Marker.MARKER.ID;
    }

    @Override
    public Field<String> field2() {
        return Marker.MARKER.NAME;
    }

    @Override
    public Field<Integer> field3() {
        return Marker.MARKER.CHROM;
    }

    @Override
    public Field<Integer> field4() {
        return Marker.MARKER.BASEPOS37;
    }

    @Override
    public Field<String[]> field5() {
        return Marker.MARKER.ALLELES;
    }

    @Override
    public Field<Integer> field6() {
        return Marker.MARKER.BASEPOS38;
    }

    @Override
    public Field<String> field7() {
        return Marker.MARKER.UNKNOWN_ALLELE;
    }

    @Override
    public UUID component1() {
        return getId();
    }

    @Override
    public String component2() {
        return getName();
    }

    @Override
    public Integer component3() {
        return getChrom();
    }

    @Override
    public Integer component4() {
        return getBasepos37();
    }

    @Override
    public String[] component5() {
        return getAlleles();
    }

    @Override
    public Integer component6() {
        return getBasepos38();
    }

    @Override
    public String component7() {
        return getUnknownAllele();
    }

    @Override
    public UUID value1() {
        return getId();
    }

    @Override
    public String value2() {
        return getName();
    }

    @Override
    public Integer value3() {
        return getChrom();
    }

    @Override
    public Integer value4() {
        return getBasepos37();
    }

    @Override
    public String[] value5() {
        return getAlleles();
    }

    @Override
    public Integer value6() {
        return getBasepos38();
    }

    @Override
    public String value7() {
        return getUnknownAllele();
    }

    @Override
    public MarkerRecord value1(UUID value) {
        setId(value);
        return this;
    }

    @Override
    public MarkerRecord value2(String value) {
        setName(value);
        return this;
    }

    @Override
    public MarkerRecord value3(Integer value) {
        setChrom(value);
        return this;
    }

    @Override
    public MarkerRecord value4(Integer value) {
        setBasepos37(value);
        return this;
    }

    @Override
    public MarkerRecord value5(String[] value) {
        setAlleles(value);
        return this;
    }

    @Override
    public MarkerRecord value6(Integer value) {
        setBasepos38(value);
        return this;
    }

    @Override
    public MarkerRecord value7(String value) {
        setUnknownAllele(value);
        return this;
    }

    @Override
    public MarkerRecord values(UUID value1, String value2, Integer value3, Integer value4, String[] value5, Integer value6, String value7) {
        value1(value1);
        value2(value2);
        value3(value3);
        value4(value4);
        value5(value5);
        value6(value6);
        value7(value7);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached MarkerRecord
     */
    public MarkerRecord() {
        super(Marker.MARKER);
    }

    /**
     * Create a detached, initialised MarkerRecord
     */
    public MarkerRecord(UUID id, String name, Integer chrom, Integer basepos37, String[] alleles, Integer basepos38, String unknownAllele) {
        super(Marker.MARKER);

        setId(id);
        setName(name);
        setChrom(chrom);
        setBasepos37(basepos37);
        setAlleles(alleles);
        setBasepos38(basepos38);
        setUnknownAllele(unknownAllele);
    }

    /**
     * Create a detached, initialised MarkerRecord
     */
    public MarkerRecord(edu.utah.camplab.db.generated.base.tables.pojos.Marker value) {
        super(Marker.MARKER);

        if (value != null) {
            setId(value.getId());
            setName(value.getName());
            setChrom(value.getChrom());
            setBasepos37(value.getBasepos37());
            setAlleles(value.getAlleles());
            setBasepos38(value.getBasepos38());
            setUnknownAllele(value.getUnknownAllele());
        }
    }
}
