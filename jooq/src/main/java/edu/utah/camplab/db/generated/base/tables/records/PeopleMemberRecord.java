package edu.utah.camplab.db.generated.base.tables.records;


import edu.utah.camplab.db.generated.base.tables.PeopleMember;

import java.util.UUID;

import javax.annotation.processing.Generated;

import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record4;
import org.jooq.Row4;
import org.jooq.impl.UpdatableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "https://www.jooq.org",
        "jOOQ version:3.16.5",
        "schema version:5002"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class PeopleMemberRecord extends UpdatableRecordImpl<PeopleMemberRecord> implements Record4<UUID, UUID, UUID, Integer> {

    private static final long serialVersionUID = 1L;

    /**
     * Setter for <code>base.people_member.id</code>.
     */
    public PeopleMemberRecord setId(UUID value) {
        set(0, value);
        return this;
    }

    /**
     * Getter for <code>base.people_member.id</code>.
     */
    public UUID getId() {
        return (UUID) get(0);
    }

    /**
     * Setter for <code>base.people_member.people_id</code>.
     */
    public PeopleMemberRecord setPeopleId(UUID value) {
        set(1, value);
        return this;
    }

    /**
     * Getter for <code>base.people_member.people_id</code>.
     */
    public UUID getPeopleId() {
        return (UUID) get(1);
    }

    /**
     * Setter for <code>base.people_member.person_id</code>.
     */
    public PeopleMemberRecord setPersonId(UUID value) {
        set(2, value);
        return this;
    }

    /**
     * Getter for <code>base.people_member.person_id</code>.
     */
    public UUID getPersonId() {
        return (UUID) get(2);
    }

    /**
     * Setter for <code>base.people_member.ordinal</code>.
     */
    public PeopleMemberRecord setOrdinal(Integer value) {
        set(3, value);
        return this;
    }

    /**
     * Getter for <code>base.people_member.ordinal</code>.
     */
    public Integer getOrdinal() {
        return (Integer) get(3);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    @Override
    public Record1<UUID> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Record4 type implementation
    // -------------------------------------------------------------------------

    @Override
    public Row4<UUID, UUID, UUID, Integer> fieldsRow() {
        return (Row4) super.fieldsRow();
    }

    @Override
    public Row4<UUID, UUID, UUID, Integer> valuesRow() {
        return (Row4) super.valuesRow();
    }

    @Override
    public Field<UUID> field1() {
        return PeopleMember.PEOPLE_MEMBER.ID;
    }

    @Override
    public Field<UUID> field2() {
        return PeopleMember.PEOPLE_MEMBER.PEOPLE_ID;
    }

    @Override
    public Field<UUID> field3() {
        return PeopleMember.PEOPLE_MEMBER.PERSON_ID;
    }

    @Override
    public Field<Integer> field4() {
        return PeopleMember.PEOPLE_MEMBER.ORDINAL;
    }

    @Override
    public UUID component1() {
        return getId();
    }

    @Override
    public UUID component2() {
        return getPeopleId();
    }

    @Override
    public UUID component3() {
        return getPersonId();
    }

    @Override
    public Integer component4() {
        return getOrdinal();
    }

    @Override
    public UUID value1() {
        return getId();
    }

    @Override
    public UUID value2() {
        return getPeopleId();
    }

    @Override
    public UUID value3() {
        return getPersonId();
    }

    @Override
    public Integer value4() {
        return getOrdinal();
    }

    @Override
    public PeopleMemberRecord value1(UUID value) {
        setId(value);
        return this;
    }

    @Override
    public PeopleMemberRecord value2(UUID value) {
        setPeopleId(value);
        return this;
    }

    @Override
    public PeopleMemberRecord value3(UUID value) {
        setPersonId(value);
        return this;
    }

    @Override
    public PeopleMemberRecord value4(Integer value) {
        setOrdinal(value);
        return this;
    }

    @Override
    public PeopleMemberRecord values(UUID value1, UUID value2, UUID value3, Integer value4) {
        value1(value1);
        value2(value2);
        value3(value3);
        value4(value4);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached PeopleMemberRecord
     */
    public PeopleMemberRecord() {
        super(PeopleMember.PEOPLE_MEMBER);
    }

    /**
     * Create a detached, initialised PeopleMemberRecord
     */
    public PeopleMemberRecord(UUID id, UUID peopleId, UUID personId, Integer ordinal) {
        super(PeopleMember.PEOPLE_MEMBER);

        setId(id);
        setPeopleId(peopleId);
        setPersonId(personId);
        setOrdinal(ordinal);
    }

    /**
     * Create a detached, initialised PeopleMemberRecord
     */
    public PeopleMemberRecord(edu.utah.camplab.db.generated.base.tables.pojos.PeopleMember value) {
        super(PeopleMember.PEOPLE_MEMBER);

        if (value != null) {
            setId(value.getId());
            setPeopleId(value.getPeopleId());
            setPersonId(value.getPersonId());
            setOrdinal(value.getOrdinal());
        }
    }
}
