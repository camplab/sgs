package edu.utah.camplab.db.generated.default_schema.tables;


import edu.utah.camplab.db.generated.default_schema.DefaultSchema;
import edu.utah.camplab.db.generated.default_schema.Indexes;
import edu.utah.camplab.db.generated.default_schema.Keys;
import edu.utah.camplab.db.generated.default_schema.tables.records.ProcessArgRecord;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import javax.annotation.processing.Generated;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Index;
import org.jooq.Name;
import org.jooq.Record;
import org.jooq.Row6;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.TableOptions;
import org.jooq.UniqueKey;
import org.jooq.impl.DSL;
import org.jooq.impl.SQLDataType;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "https://www.jooq.org",
        "jOOQ version:3.16.5",
        "schema version:5002"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class ProcessArg extends TableImpl<ProcessArgRecord> {

    private static final long serialVersionUID = 1L;

    /**
     * The reference instance of <code>process_arg</code>
     */
    public static final ProcessArg PROCESS_ARG = new ProcessArg();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<ProcessArgRecord> getRecordType() {
        return ProcessArgRecord.class;
    }

    /**
     * The column <code>process_arg.id</code>.
     */
    public final TableField<ProcessArgRecord, UUID> ID = createField(DSL.name("id"), SQLDataType.UUID.nullable(false), this, "");

    /**
     * The column <code>process_arg.process_id</code>.
     */
    public final TableField<ProcessArgRecord, UUID> PROCESS_ID = createField(DSL.name("process_id"), SQLDataType.UUID.nullable(false), this, "");

    /**
     * The column <code>process_arg.argname</code>.
     */
    public final TableField<ProcessArgRecord, String> ARGNAME = createField(DSL.name("argname"), SQLDataType.CLOB.nullable(false), this, "");

    /**
     * The column <code>process_arg.argvalue_int</code>.
     */
    public final TableField<ProcessArgRecord, Long> ARGVALUE_INT = createField(DSL.name("argvalue_int"), SQLDataType.BIGINT, this, "");

    /**
     * The column <code>process_arg.argvalue_float</code>.
     */
    public final TableField<ProcessArgRecord, Double> ARGVALUE_FLOAT = createField(DSL.name("argvalue_float"), SQLDataType.DOUBLE, this, "");

    /**
     * The column <code>process_arg.argvalue_text</code>.
     */
    public final TableField<ProcessArgRecord, String> ARGVALUE_TEXT = createField(DSL.name("argvalue_text"), SQLDataType.CLOB, this, "");

    private ProcessArg(Name alias, Table<ProcessArgRecord> aliased) {
        this(alias, aliased, null);
    }

    private ProcessArg(Name alias, Table<ProcessArgRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, DSL.comment(""), TableOptions.table());
    }

    /**
     * Create an aliased <code>process_arg</code> table reference
     */
    public ProcessArg(String alias) {
        this(DSL.name(alias), PROCESS_ARG);
    }

    /**
     * Create an aliased <code>process_arg</code> table reference
     */
    public ProcessArg(Name alias) {
        this(alias, PROCESS_ARG);
    }

    /**
     * Create a <code>process_arg</code> table reference
     */
    public ProcessArg() {
        this(DSL.name("process_arg"), null);
    }

    public <O extends Record> ProcessArg(Table<O> child, ForeignKey<O, ProcessArgRecord> key) {
        super(child, key, PROCESS_ARG);
    }

    @Override
    public Schema getSchema() {
        return aliased() ? null : DefaultSchema.DEFAULT_SCHEMA;
    }

    @Override
    public List<Index> getIndexes() {
        return Arrays.asList(Indexes.ARG_PROC);
    }

    @Override
    public UniqueKey<ProcessArgRecord> getPrimaryKey() {
        return Keys.PROCESS_ARG_PKEY;
    }

    @Override
    public List<ForeignKey<ProcessArgRecord, ?>> getReferences() {
        return Arrays.asList(Keys.PROCESS_ARG__PROCESS_ARG_PROCESS_ID_FKEY);
    }

    private transient Process _process;

    /**
     * Get the implicit join path to the <code>sgstemplate.process</code> table.
     */
    public Process process() {
        if (_process == null)
            _process = new Process(this, Keys.PROCESS_ARG__PROCESS_ARG_PROCESS_ID_FKEY);

        return _process;
    }

    @Override
    public ProcessArg as(String alias) {
        return new ProcessArg(DSL.name(alias), this);
    }

    @Override
    public ProcessArg as(Name alias) {
        return new ProcessArg(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public ProcessArg rename(String name) {
        return new ProcessArg(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public ProcessArg rename(Name name) {
        return new ProcessArg(name, null);
    }

    // -------------------------------------------------------------------------
    // Row6 type methods
    // -------------------------------------------------------------------------

    @Override
    public Row6<UUID, UUID, String, Long, Double, String> fieldsRow() {
        return (Row6) super.fieldsRow();
    }
}
