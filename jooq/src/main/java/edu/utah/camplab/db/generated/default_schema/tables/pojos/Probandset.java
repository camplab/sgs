package edu.utah.camplab.db.generated.default_schema.tables.pojos;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;

import java.io.Serializable;
import java.util.Arrays;
import java.util.UUID;

import javax.annotation.processing.Generated;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "https://www.jooq.org",
        "jOOQ version:3.16.5",
        "schema version:5002"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
@JsonTypeInfo(use=JsonTypeInfo.Id.MINIMAL_CLASS, include=JsonTypeInfo.As.PROPERTY, property="class")
@JsonTypeName("edu.utah.camplab.db.generated.default_schema.tables.pojos.tables.pojos.Probandset")
@JsonFormat(shape=JsonFormat.Shape.ARRAY)
@JsonPropertyOrder(alphabetic=true)
public class Probandset implements Serializable {

    private static final long serialVersionUID = 1L;

    private UUID    id;
    private String  name;
    private UUID[]  probands;
    private Integer meioses;
    private Double  minKincoef;
    private Double  maxKincoef;
    private UUID    peopleId;

    public Probandset() {}

    public Probandset(Probandset value) {
        this.id = value.id;
        this.name = value.name;
        this.probands = value.probands;
        this.meioses = value.meioses;
        this.minKincoef = value.minKincoef;
        this.maxKincoef = value.maxKincoef;
        this.peopleId = value.peopleId;
    }

    public Probandset(
        UUID    id,
        String  name,
        UUID[]  probands,
        Integer meioses,
        Double  minKincoef,
        Double  maxKincoef,
        UUID    peopleId
    ) {
        this.id = id;
        this.name = name;
        this.probands = probands;
        this.meioses = meioses;
        this.minKincoef = minKincoef;
        this.maxKincoef = maxKincoef;
        this.peopleId = peopleId;
    }

    /**
     * Getter for <code>probandset.id</code>.
     */
    public UUID getId() {
        return this.id;
    }

    /**
     * Setter for <code>probandset.id</code>.
     */
    public Probandset setId(UUID id) {
        this.id = id;
        return this;
    }

    /**
     * Getter for <code>probandset.name</code>.
     */
    public String getName() {
        return this.name;
    }

    /**
     * Setter for <code>probandset.name</code>.
     */
    public Probandset setName(String name) {
        this.name = name;
        return this;
    }

    /**
     * Getter for <code>probandset.probands</code>.
     */
    public UUID[] getProbands() {
        return this.probands;
    }

    /**
     * Setter for <code>probandset.probands</code>.
     */
    public Probandset setProbands(UUID[] probands) {
        this.probands = probands;
        return this;
    }

    /**
     * Getter for <code>probandset.meioses</code>.
     */
    public Integer getMeioses() {
        return this.meioses;
    }

    /**
     * Setter for <code>probandset.meioses</code>.
     */
    public Probandset setMeioses(Integer meioses) {
        this.meioses = meioses;
        return this;
    }

    /**
     * Getter for <code>probandset.min_kincoef</code>.
     */
    public Double getMinKincoef() {
        return this.minKincoef;
    }

    /**
     * Setter for <code>probandset.min_kincoef</code>.
     */
    public Probandset setMinKincoef(Double minKincoef) {
        this.minKincoef = minKincoef;
        return this;
    }

    /**
     * Getter for <code>probandset.max_kincoef</code>.
     */
    public Double getMaxKincoef() {
        return this.maxKincoef;
    }

    /**
     * Setter for <code>probandset.max_kincoef</code>.
     */
    public Probandset setMaxKincoef(Double maxKincoef) {
        this.maxKincoef = maxKincoef;
        return this;
    }

    /**
     * Getter for <code>probandset.people_id</code>.
     */
    public UUID getPeopleId() {
        return this.peopleId;
    }

    /**
     * Setter for <code>probandset.people_id</code>.
     */
    public Probandset setPeopleId(UUID peopleId) {
        this.peopleId = peopleId;
        return this;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Probandset (");

        sb.append(id);
        sb.append(", ").append(name);
        sb.append(", ").append(Arrays.toString(probands));
        sb.append(", ").append(meioses);
        sb.append(", ").append(minKincoef);
        sb.append(", ").append(maxKincoef);
        sb.append(", ").append(peopleId);

        sb.append(")");
        return sb.toString();
    }
}
