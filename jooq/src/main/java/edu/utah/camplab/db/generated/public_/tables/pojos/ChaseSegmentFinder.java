package edu.utah.camplab.db.generated.public_.tables.pojos;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;

import java.io.Serializable;

import javax.annotation.processing.Generated;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "https://www.jooq.org",
        "jOOQ version:3.16.5",
        "schema version:5002"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
@JsonTypeInfo(use=JsonTypeInfo.Id.MINIMAL_CLASS, include=JsonTypeInfo.As.PROPERTY, property="class")
@JsonTypeName("edu.utah.camplab.db.generated.public_.tables.pojos.tables.pojos.ChaseSegmentFinder")
@JsonFormat(shape=JsonFormat.Shape.ARRAY)
@JsonPropertyOrder(alphabetic=true)
public class ChaseSegmentFinder implements Serializable {

    private static final long serialVersionUID = 1L;

    private String chaser;

    public ChaseSegmentFinder() {}

    public ChaseSegmentFinder(ChaseSegmentFinder value) {
        this.chaser = value.chaser;
    }

    public ChaseSegmentFinder(
        String chaser
    ) {
        this.chaser = chaser;
    }

    /**
     * Getter for <code>public.chase_segment_finder.chaser</code>.
     */
    public String getChaser() {
        return this.chaser;
    }

    /**
     * Setter for <code>public.chase_segment_finder.chaser</code>.
     */
    public ChaseSegmentFinder setChaser(String chaser) {
        this.chaser = chaser;
        return this;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("ChaseSegmentFinder (");

        sb.append(chaser);

        sb.append(")");
        return sb.toString();
    }
}
