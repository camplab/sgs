package edu.utah.camplab.db.generated.base.tables;


import edu.utah.camplab.db.generated.base.Base;
import edu.utah.camplab.db.generated.base.Keys;
import edu.utah.camplab.db.generated.base.tables.records.DefinervalueRecord;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import javax.annotation.processing.Generated;

import org.jooq.Check;
import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Name;
import org.jooq.Record;
import org.jooq.Row6;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.TableOptions;
import org.jooq.UniqueKey;
import org.jooq.impl.DSL;
import org.jooq.impl.Internal;
import org.jooq.impl.SQLDataType;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "https://www.jooq.org",
        "jOOQ version:3.16.5",
        "schema version:5002"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Definervalue extends TableImpl<DefinervalueRecord> {

    private static final long serialVersionUID = 1L;

    /**
     * The reference instance of <code>base.definervalue</code>
     */
    public static final Definervalue DEFINERVALUE = new Definervalue();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<DefinervalueRecord> getRecordType() {
        return DefinervalueRecord.class;
    }

    /**
     * The column <code>base.definervalue.id</code>.
     */
    public final TableField<DefinervalueRecord, UUID> ID = createField(DSL.name("id"), SQLDataType.UUID.nullable(false), this, "");

    /**
     * The column <code>base.definervalue.valuestring</code>.
     */
    public final TableField<DefinervalueRecord, String> VALUESTRING = createField(DSL.name("valuestring"), SQLDataType.CLOB, this, "");

    /**
     * The column <code>base.definervalue.valueint</code>.
     */
    public final TableField<DefinervalueRecord, Integer> VALUEINT = createField(DSL.name("valueint"), SQLDataType.INTEGER, this, "");

    /**
     * The column <code>base.definervalue.valuefloat</code>.
     */
    public final TableField<DefinervalueRecord, Double> VALUEFLOAT = createField(DSL.name("valuefloat"), SQLDataType.DOUBLE, this, "");

    /**
     * The column <code>base.definervalue.definer_id</code>.
     */
    public final TableField<DefinervalueRecord, UUID> DEFINER_ID = createField(DSL.name("definer_id"), SQLDataType.UUID.nullable(false), this, "");

    /**
     * The column <code>base.definervalue.description</code>.
     */
    public final TableField<DefinervalueRecord, String> DESCRIPTION = createField(DSL.name("description"), SQLDataType.CLOB, this, "");

    private Definervalue(Name alias, Table<DefinervalueRecord> aliased) {
        this(alias, aliased, null);
    }

    private Definervalue(Name alias, Table<DefinervalueRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, DSL.comment(""), TableOptions.table());
    }

    /**
     * Create an aliased <code>base.definervalue</code> table reference
     */
    public Definervalue(String alias) {
        this(DSL.name(alias), DEFINERVALUE);
    }

    /**
     * Create an aliased <code>base.definervalue</code> table reference
     */
    public Definervalue(Name alias) {
        this(alias, DEFINERVALUE);
    }

    /**
     * Create a <code>base.definervalue</code> table reference
     */
    public Definervalue() {
        this(DSL.name("definervalue"), null);
    }

    public <O extends Record> Definervalue(Table<O> child, ForeignKey<O, DefinervalueRecord> key) {
        super(child, key, DEFINERVALUE);
    }

    @Override
    public Schema getSchema() {
        return aliased() ? null : Base.BASE;
    }

    @Override
    public UniqueKey<DefinervalueRecord> getPrimaryKey() {
        return Keys.DEFINERVALUE_PKEY;
    }

    @Override
    public List<ForeignKey<DefinervalueRecord, ?>> getReferences() {
        return Arrays.asList(Keys.DEFINERVALUE__DEFINERVALUE_DEFINER_ID_FKEY);
    }

    private transient Definer _definer;

    /**
     * Get the implicit join path to the <code>base.definer</code> table.
     */
    public Definer definer() {
        if (_definer == null)
            _definer = new Definer(this, Keys.DEFINERVALUE__DEFINERVALUE_DEFINER_ID_FKEY);

        return _definer;
    }

    @Override
    public List<Check<DefinervalueRecord>> getChecks() {
        return Arrays.asList(
            Internal.createCheck(this, DSL.name("forcenonnull"), "((COALESCE((valuefloat)::text, (valueint)::text, valuestring) IS NOT NULL))", true)
        );
    }

    @Override
    public Definervalue as(String alias) {
        return new Definervalue(DSL.name(alias), this);
    }

    @Override
    public Definervalue as(Name alias) {
        return new Definervalue(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public Definervalue rename(String name) {
        return new Definervalue(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public Definervalue rename(Name name) {
        return new Definervalue(name, null);
    }

    // -------------------------------------------------------------------------
    // Row6 type methods
    // -------------------------------------------------------------------------

    @Override
    public Row6<UUID, String, Integer, Double, UUID, String> fieldsRow() {
        return (Row6) super.fieldsRow();
    }
}
