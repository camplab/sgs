package edu.utah.camplab.db.generated.base.tables.pojos;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;

import java.io.Serializable;
import java.util.Arrays;
import java.util.UUID;

import javax.annotation.processing.Generated;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "https://www.jooq.org",
        "jOOQ version:3.16.5",
        "schema version:5002"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
@JsonTypeInfo(use=JsonTypeInfo.Id.MINIMAL_CLASS, include=JsonTypeInfo.As.PROPERTY, property="class")
@JsonTypeName("edu.utah.camplab.db.generated.base.tables.pojos.tables.pojos.LdClique")
@JsonFormat(shape=JsonFormat.Shape.ARRAY)
@JsonPropertyOrder(alphabetic=true)
public class LdClique implements Serializable {

    private static final long serialVersionUID = 1L;

    private UUID      markersetId;
    private Integer   ordinal;
    private Integer[] lociOrdinals;
    private Double[]  potential;

    public LdClique() {}

    public LdClique(LdClique value) {
        this.markersetId = value.markersetId;
        this.ordinal = value.ordinal;
        this.lociOrdinals = value.lociOrdinals;
        this.potential = value.potential;
    }

    public LdClique(
        UUID      markersetId,
        Integer   ordinal,
        Integer[] lociOrdinals,
        Double[]  potential
    ) {
        this.markersetId = markersetId;
        this.ordinal = ordinal;
        this.lociOrdinals = lociOrdinals;
        this.potential = potential;
    }

    /**
     * Getter for <code>base.ld_clique.markerset_id</code>.
     */
    public UUID getMarkersetId() {
        return this.markersetId;
    }

    /**
     * Setter for <code>base.ld_clique.markerset_id</code>.
     */
    public LdClique setMarkersetId(UUID markersetId) {
        this.markersetId = markersetId;
        return this;
    }

    /**
     * Getter for <code>base.ld_clique.ordinal</code>.
     */
    public Integer getOrdinal() {
        return this.ordinal;
    }

    /**
     * Setter for <code>base.ld_clique.ordinal</code>.
     */
    public LdClique setOrdinal(Integer ordinal) {
        this.ordinal = ordinal;
        return this;
    }

    /**
     * Getter for <code>base.ld_clique.loci_ordinals</code>.
     */
    public Integer[] getLociOrdinals() {
        return this.lociOrdinals;
    }

    /**
     * Setter for <code>base.ld_clique.loci_ordinals</code>.
     */
    public LdClique setLociOrdinals(Integer[] lociOrdinals) {
        this.lociOrdinals = lociOrdinals;
        return this;
    }

    /**
     * Getter for <code>base.ld_clique.potential</code>.
     */
    public Double[] getPotential() {
        return this.potential;
    }

    /**
     * Setter for <code>base.ld_clique.potential</code>.
     */
    public LdClique setPotential(Double[] potential) {
        this.potential = potential;
        return this;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("LdClique (");

        sb.append(markersetId);
        sb.append(", ").append(ordinal);
        sb.append(", ").append(Arrays.toString(lociOrdinals));
        sb.append(", ").append(Arrays.toString(potential));

        sb.append(")");
        return sb.toString();
    }
}
