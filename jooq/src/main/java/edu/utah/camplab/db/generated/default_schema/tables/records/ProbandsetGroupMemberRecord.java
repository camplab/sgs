package edu.utah.camplab.db.generated.default_schema.tables.records;


import edu.utah.camplab.db.generated.default_schema.tables.ProbandsetGroupMember;

import java.util.UUID;

import javax.annotation.processing.Generated;

import org.jooq.Field;
import org.jooq.Record2;
import org.jooq.Row2;
import org.jooq.impl.UpdatableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "https://www.jooq.org",
        "jOOQ version:3.16.5",
        "schema version:5002"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class ProbandsetGroupMemberRecord extends UpdatableRecordImpl<ProbandsetGroupMemberRecord> implements Record2<UUID, UUID> {

    private static final long serialVersionUID = 1L;

    /**
     * Setter for <code>probandset_group_member.group_id</code>.
     */
    public ProbandsetGroupMemberRecord setGroupId(UUID value) {
        set(0, value);
        return this;
    }

    /**
     * Getter for <code>probandset_group_member.group_id</code>.
     */
    public UUID getGroupId() {
        return (UUID) get(0);
    }

    /**
     * Setter for <code>probandset_group_member.member_id</code>.
     */
    public ProbandsetGroupMemberRecord setMemberId(UUID value) {
        set(1, value);
        return this;
    }

    /**
     * Getter for <code>probandset_group_member.member_id</code>.
     */
    public UUID getMemberId() {
        return (UUID) get(1);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    @Override
    public Record2<UUID, UUID> key() {
        return (Record2) super.key();
    }

    // -------------------------------------------------------------------------
    // Record2 type implementation
    // -------------------------------------------------------------------------

    @Override
    public Row2<UUID, UUID> fieldsRow() {
        return (Row2) super.fieldsRow();
    }

    @Override
    public Row2<UUID, UUID> valuesRow() {
        return (Row2) super.valuesRow();
    }

    @Override
    public Field<UUID> field1() {
        return ProbandsetGroupMember.PROBANDSET_GROUP_MEMBER.GROUP_ID;
    }

    @Override
    public Field<UUID> field2() {
        return ProbandsetGroupMember.PROBANDSET_GROUP_MEMBER.MEMBER_ID;
    }

    @Override
    public UUID component1() {
        return getGroupId();
    }

    @Override
    public UUID component2() {
        return getMemberId();
    }

    @Override
    public UUID value1() {
        return getGroupId();
    }

    @Override
    public UUID value2() {
        return getMemberId();
    }

    @Override
    public ProbandsetGroupMemberRecord value1(UUID value) {
        setGroupId(value);
        return this;
    }

    @Override
    public ProbandsetGroupMemberRecord value2(UUID value) {
        setMemberId(value);
        return this;
    }

    @Override
    public ProbandsetGroupMemberRecord values(UUID value1, UUID value2) {
        value1(value1);
        value2(value2);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached ProbandsetGroupMemberRecord
     */
    public ProbandsetGroupMemberRecord() {
        super(ProbandsetGroupMember.PROBANDSET_GROUP_MEMBER);
    }

    /**
     * Create a detached, initialised ProbandsetGroupMemberRecord
     */
    public ProbandsetGroupMemberRecord(UUID groupId, UUID memberId) {
        super(ProbandsetGroupMember.PROBANDSET_GROUP_MEMBER);

        setGroupId(groupId);
        setMemberId(memberId);
    }

    /**
     * Create a detached, initialised ProbandsetGroupMemberRecord
     */
    public ProbandsetGroupMemberRecord(edu.utah.camplab.db.generated.default_schema.tables.pojos.ProbandsetGroupMember value) {
        super(ProbandsetGroupMember.PROBANDSET_GROUP_MEMBER);

        if (value != null) {
            setGroupId(value.getGroupId());
            setMemberId(value.getMemberId());
        }
    }
}
