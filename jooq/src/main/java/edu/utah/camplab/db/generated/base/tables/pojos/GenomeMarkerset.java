package edu.utah.camplab.db.generated.base.tables.pojos;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;

import java.io.Serializable;
import java.util.UUID;

import javax.annotation.processing.Generated;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "https://www.jooq.org",
        "jOOQ version:3.16.5",
        "schema version:5002"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
@JsonTypeInfo(use=JsonTypeInfo.Id.MINIMAL_CLASS, include=JsonTypeInfo.As.PROPERTY, property="class")
@JsonTypeName("edu.utah.camplab.db.generated.base.tables.pojos.tables.pojos.GenomeMarkerset")
@JsonFormat(shape=JsonFormat.Shape.ARRAY)
@JsonPropertyOrder(alphabetic=true)
public class GenomeMarkerset implements Serializable {

    private static final long serialVersionUID = 1L;

    private UUID    id;
    private String  name;
    private String  description;
    private Integer primaryBuild;

    public GenomeMarkerset() {}

    public GenomeMarkerset(GenomeMarkerset value) {
        this.id = value.id;
        this.name = value.name;
        this.description = value.description;
        this.primaryBuild = value.primaryBuild;
    }

    public GenomeMarkerset(
        UUID    id,
        String  name,
        String  description,
        Integer primaryBuild
    ) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.primaryBuild = primaryBuild;
    }

    /**
     * Getter for <code>base.genome_markerset.id</code>.
     */
    public UUID getId() {
        return this.id;
    }

    /**
     * Setter for <code>base.genome_markerset.id</code>.
     */
    public GenomeMarkerset setId(UUID id) {
        this.id = id;
        return this;
    }

    /**
     * Getter for <code>base.genome_markerset.name</code>.
     */
    public String getName() {
        return this.name;
    }

    /**
     * Setter for <code>base.genome_markerset.name</code>.
     */
    public GenomeMarkerset setName(String name) {
        this.name = name;
        return this;
    }

    /**
     * Getter for <code>base.genome_markerset.description</code>.
     */
    public String getDescription() {
        return this.description;
    }

    /**
     * Setter for <code>base.genome_markerset.description</code>.
     */
    public GenomeMarkerset setDescription(String description) {
        this.description = description;
        return this;
    }

    /**
     * Getter for <code>base.genome_markerset.primary_build</code>.
     */
    public Integer getPrimaryBuild() {
        return this.primaryBuild;
    }

    /**
     * Setter for <code>base.genome_markerset.primary_build</code>.
     */
    public GenomeMarkerset setPrimaryBuild(Integer primaryBuild) {
        this.primaryBuild = primaryBuild;
        return this;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("GenomeMarkerset (");

        sb.append(id);
        sb.append(", ").append(name);
        sb.append(", ").append(description);
        sb.append(", ").append(primaryBuild);

        sb.append(")");
        return sb.toString();
    }
}
