import edu.utah.camplab.db.generated.base.tables.records.*;
import edu.utah.camplab.jps.SGSFormatter;
import edu.utah.camplab.jps.SGSLDModel;
import edu.utah.camplab.jps.SGSParameterData;
import edu.utah.camplab.sgs.SGSChase;

import edu.utah.camplab.tools.S3FileInputStream;
import jpsgcs.pedmcmc.LDModel;
import jpsgcs.linkage.*;

import org.jooq.*;
import org.jooq.exception.DataAccessException;
import org.jooq.impl.DSL;

import java.io.*;
import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import joptsimple.BuiltinHelpFormatter;
import joptsimple.OptionParser;
import joptsimple.OptionSet;
import joptsimple.OptionSpec;

import static edu.utah.camplab.db.generated.base.Tables.*;

public class TestReduceChromosome extends SGSChase {

  // CLI options
  private final OptionSpec<String>	parfileSpec;
  private final OptionSpec<String>	pedfileSpec; 

  private File dbReduced;
  private File fileReduced;
  private String s3pedFilename;
  private String s3parFilename;
  private LinkageParameterData fileParData;
  private LinkagePedigreeData filePedData;

  public TestReduceChromosome(String[] args) {
    super();
    parfileSpec = op.accepts("s3par").withRequiredArg().ofType(String.class).required();
    pedfileSpec = op.accepts("s3ped").withRequiredArg().ofType(String.class).required();
  }

  @Override
  protected void init(OptionSet opSet) throws Exception {
    super.init(opSet);
    s3parFilename = parfileSpec.value(opSet);
    s3pedFilename = pedfileSpec.value(opSet);
  }

  public void init(String[] args) throws Exception {
    init(op.parse(args));
  }

  public static void main(String[] args) {
    try {
      TestReduceChromosome trc = new TestReduceChromosome(args);
      trc.init(args);
      trc.load();
    }
    catch (Exception e){
      e.printStackTrace();
    }
}

  public void load() throws Exception{
    requestSegment();
    reduceFromFile();
    reduceFromDb();
    compare();
  }

  private void reduceFromFile() throws Exception{
    fileReduced = new File(getJsonDir(), getSegmentId()+".fileReduced.ld");
    String[] s3uriParts = parseS3URI(s3parFilename);
    LinkageFormatter formatter = new LinkageFormatter(S3FileInputStream.getS3Reader( s3uriParts[0]+ "/" + s3uriParts[1]), s3uriParts[1]);
    fileParData = new LinkageParameterData(formatter, false);
    ldModel = null;
    ldModel = new LDModel(formatter,  0.0);

    s3uriParts = parseS3URI(s3pedFilename);
    formatter = new SGSFormatter(s3uriParts[0], s3uriParts[1]);
    filePedData = new LinkagePedigreeData(formatter, fileParData);
    //ldModel = new LDModel(formatter);
    buildLinkageDataSet(fileParData, filePedData); // trample on SGSRun, but that's ok, right
    reducedLINKAGE RL = reduceChromosome(getOriginalSegment().getFirstmarker());
    PrintStream ls = new PrintStream(new BufferedOutputStream(new FileOutputStream(fileReduced)));
    RL.model().writeTo(ls);
    ls.close();
  }

  private void reduceFromDb() throws Exception{
    dbReduced = new File(getJsonDir(), getSegmentId() + ".dbReduced.ld");
    buildLinkageDataSet();
    reducedLINKAGE reduced = reduceChromosome();
    PrintStream dbps = new PrintStream(new BufferedOutputStream(new FileOutputStream(dbReduced)));
    reduced.model().writeTo(dbps);
    dbps.close();
  }

  private boolean compare() throws Exception{
    BufferedReader fileFileReader = new BufferedReader(new FileReader(fileReduced));
    BufferedReader dbFileReader = new BufferedReader(new FileReader(dbReduced));
    String fileLine = fileFileReader.readLine();
    String dbLine = dbFileReader.readLine();
    int lineno = 0;
    boolean good = true;
    while (fileLine != null && dbLine != null) {
      lineno++;
      if ( ! fileLine.equals(dbLine) ) {
	System.err.println(String.format("Trouble on line %d\nfile: %s\ndb:%s", lineno, fileLine, dbLine));
	good = false;
	break;
      }
      fileLine = fileFileReader.readLine();
      dbLine = dbFileReader.readLine();
    }
    return good;
  }
    
  private String[] parseS3URI(String sarg) {
    String retval[] = new String[2];
    int idx = sarg.indexOf('/',5);
    //int idx = sarg.lastIndexOf('/');
    String bucket = sarg.substring(0,idx);
    String path = sarg.substring(idx+1);
    retval[0] = bucket;
    retval[1] = path;
    return retval;
  }
}
