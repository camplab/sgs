
import edu.utah.camplab.db.generated.base.tables.records.*;
import edu.utah.camplab.jps.SGSFormatter;
import edu.utah.camplab.jps.SGSLDModel;
import edu.utah.camplab.jps.SGSParameterData;
import edu.utah.camplab.jx.SGSFullContext;

import jpsgcs.pedmcmc.LDModel;

import org.jooq.*;
import org.jooq.exception.DataAccessException;
import org.jooq.impl.DSL;

import java.io.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static edu.utah.camplab.db.generated.base.Tables.*;

public class TestLoadLDLI
{
  private String basename;
  private String dbUrl = String.format("jdbc:postgresql://%s/%s", "localhost", "version4000");

  private Connection dbconn;
  private DSLContext globalContext;
  private SGSFormatter sgsFormatter;
  private String parin;

  public TestLoadLDLI() {
    //load(argv);
  }

  public static void main(String[] args) {
    try {
      TestLoadLDLI ldli = new TestLoadLDLI();
      ldli.conn(args);
      ldli.load();
      ldli.writeParfile();
    }
    catch (Exception e){
      e.printStackTrace();
    }
}

  public void load() {
    try {
      globalContext.transaction(localconf -> {
        DSLContext lct = DSL.using(localconf);
        sgsFormatter.setDbContext(lct);

        SGSParameterData lpd = new SGSParameterData(sgsFormatter);
        GenomeMarkersetRecord gmRec = lct.selectFrom(GENOME_MARKERSET).where(GENOME_MARKERSET.NAME.like("%GSA%")).fetchOne();
        if (gmRec == null) {
          gmRec = lct.newRecord(GENOME_MARKERSET);
          gmRec.setId(UUID.randomUUID());
          gmRec.setName("GSA-disposable");
          gmRec.insert();
        }
        MarkersetRecord mkrset = lct.selectFrom(MARKERSET).where(MARKERSET.NAME.equal(parin)).fetchOne();
        if (mkrset == null) {
          mkrset = lct.newRecord(MARKERSET);
          mkrset.setId(UUID.randomUUID());
          mkrset.setName(basename);
          mkrset.setChrom(9);
          mkrset.setGenomeId(gmRec.getId());
          mkrset.insert();
        }
        stowMarkerData(lpd.getMarkerTuples(), mkrset, lct);
      });
      LDModel ld = null;
      ld = new SGSLDModel(sgsFormatter,  0.0);
      ld.report();
      ld.writeTo(new PrintStream(new BufferedOutputStream(new FileOutputStream("/tmp/regen.ld"))));
      
    }
    catch (DataAccessException | IOException e)
   {
    e.printStackTrace();
    System.exit(2);
   }
}


  private void conn(String[] avec) throws IOException, SQLException {
    dbconn = DriverManager.getConnection(dbUrl, "viv", "viv_notnull");
    globalContext = SGSFullContext.DSLContextFactory(dbconn);

    parin = avec[0];
    setBasename(avec[0]);
    String[] s3uriParts = parseS3URI(avec[0]);
    sgsFormatter = new SGSFormatter(s3uriParts[0], s3uriParts[1]);
    sgsFormatter.setDbContext(globalContext);
  }

  private void writeParfile() throws IOException {
    PrintStream ps = new PrintStream(new BufferedOutputStream(new FileOutputStream("/tmp/regenDB.ld")));
    Result<?>markerinfo = writePar(ps);
    writeLd(ps, markerinfo);
  }
  private Result<?> writePar(PrintStream ps) {
    GenomeMarkersetRecord gmRec = sgsFormatter.getDbContext().selectFrom(GENOME_MARKERSET).where(GENOME_MARKERSET.NAME.equal("GSA")).fetchOne();
    MarkersetRecord msRec = sgsFormatter.getDbContext().selectFrom(MARKERSET).where(MARKERSET.GENOME_ID.equal(gmRec.getId())).fetchOne();
       //check for null
      Result<?> markerdata = sgsFormatter.getDbContext()
        .select(MARKERSET_MEMBER.asterisk(), MARKER.asterisk())
        .from(MARKERSET_MEMBER
          .join(MARKER).on(MARKERSET_MEMBER.MEMBER_ID.equal(MARKER.ID)))
        .where(MARKERSET_MEMBER.MARKERSET_ID.equal(msRec.getId()))
        .orderBy(MARKERSET_MEMBER.ORDINAL)
        .fetch();
      StringBuffer sbuf = new StringBuffer();

      ps.printf("%d 0 0 5 \n%d %3.1f %3.1f %d \n", markerdata.size(), 0, 0.0, 0.0, 0);
      for (int m = 1; m <= markerdata.size(); m++) {
        ps.printf("%d ", m);
      }
      ps.println();

      for( org.jooq.Record mline : markerdata) {
        int alen = mline.getValue(MARKER.ALLELES) == null ? 2 : mline.getValue(MARKER.ALLELES).length;
        ps.printf("3 %d     %s|%d|%d\n", alen, mline.getValue(MARKER.NAME), mline.getValue(MARKER.BASEPOS37), mline.getValue(MARKER.CHROM));
        sbuf.append(String.format("%s ", mline.getValue(MARKERSET_MEMBER.THETA)));
        for (int i = 0; i < alen; i++) {
          ps.printf(" %8.6f ", (1.0 / alen));
        }
        ps.println();
      }
      ps.printf("0 0 \n%s\n1 5.0 0.2     0.1\n", sbuf.toString());
      ps.flush();

    return markerdata;
  }

  private void writeLd(PrintStream ps, Result<?> markers) {
    UUID markersetId = markers.get(1).getValue(MARKERSET_MEMBER.MARKERSET_ID);
    List<LdCliqueRecord> cliquesRed =
      sgsFormatter.getDbContext().select()
        .from(LD_CLIQUE.join(MARKERSET).on(MARKERSET.ID.equal(LD_CLIQUE.MARKERSET_ID)))
        .where(MARKERSET.ID.equal(markersetId))
        .orderBy(LD_CLIQUE.ORDINAL).fetch(r -> r.into(LD_CLIQUE));

    for (org.jooq.Record mdata : markers) {
      ps.printf(" %d", mdata.getValue(MARKER.ALLELES) == null ? 2 : mdata.getValue(MARKER.ALLELES).length);
    }
    ps.println();
    for (LdCliqueRecord crec : cliquesRed) {
      for(int ord : crec.getLociOrdinals()) {
        ps.printf(" %d", ord);
      }
      ps.println();
      for(double d : crec.getPotential()){
        ps.printf(" %s", d);
      }
      ps.println();
    }
    ps.flush();
  }
  private void setBasename(String fullname) {
    basename = fullname.substring(fullname.lastIndexOf('/')+1);
  }

  private String[] parseS3URI(String sarg) {
    String retval[] = new String[2];
    int idx = sarg.indexOf('/',5);
    //int idx = sarg.lastIndexOf('/');
    String bucket = sarg.substring(0,idx);
    String path = sarg.substring(idx+1);
    retval[0] = bucket;
    retval[1] = path;
    return retval;
  }

  private void stowMarkerData(ArrayList<SGSParameterData.MarkerTuple> tuples, MarkersetRecord msr, DSLContext ltx) {
    int blocksize = 1000;
    ArrayList<MarkerRecord> mkrset = new ArrayList<>();
    ArrayList<MarkersetMemberRecord> mmbset = new ArrayList<>();

    for (SGSParameterData.MarkerTuple tup : tuples) {
      tup.mark.setId(UUID.randomUUID());
      tup.memb.setMemberId(tup.mark.getId());
      tup.memb.setMarkersetId(msr.getId());
      mkrset.add(tup.mark);
      mmbset.add(tup.memb);
      if (mkrset.size() == blocksize) {
        saveBlock(mkrset, mmbset, ltx);
        mkrset = new ArrayList<>();
        mmbset = new ArrayList<>();

      }
    }
    if (mkrset.size() > 0) {
      saveBlock(mkrset, mmbset, ltx);
    }
  }
  private void saveBlock(ArrayList<MarkerRecord> klist, ArrayList<MarkersetMemberRecord> blist, DSLContext dsl) {
    writeMarkers(klist,dsl);
    writeMembers(blist,dsl);
  }
  private void writeMarkers(ArrayList<MarkerRecord> markerList, DSLContext ctx) {
    ctx.batchStore(markerList).execute();
  }

  private void writeMembers(ArrayList<MarkersetMemberRecord> mrecList, DSLContext ctx) {
    ctx.batchStore(mrecList).execute();
  }
}
