package edu.utah.camplab.server;

import org.apache.catalina.Context;
import org.apache.catalina.LifecycleException;
import org.apache.catalina.Service;
import org.apache.catalina.connector.Connector;
import org.apache.catalina.startup.Tomcat;
import org.apache.tomcat.util.descriptor.web.ContextResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Properties;

public class SGSSelector implements Runnable {
  private final static Logger logger = LoggerFactory.getLogger(SGSSelector.class);

  private int           tomcatPort;
  private String        dbUrlTemplate    = null;
  private String        emergencyJsonDir = "";
  private String        dbHost;
  private int           dbPort           = 5432;
  private String        dbTestUser       = null;
  private String        dbTestDb         = null;
  private Tomcat        embeddedTomcat   = null;

  public static void main(String[] args) {
    logger.info("Loading selector props from {}", args[0]);

    try {
      File configFile = new File(args[0]);
      if (!configFile.exists()) {
        throw new RuntimeException("Can't find configuration file at " + configFile.getCanonicalPath());
      }
      InputStream inStream = new FileInputStream(configFile);
      System.getProperties().load(inStream);
    } catch (IOException e) {
      e.printStackTrace();
    }

    SGSSelector selector = new SGSSelector(System.getProperties());  // or no-arg constuctor...
    logger.info("Initiating SGS: agent db={} failoverDir={}", selector.getDbUrlTemplate(), selector.getEmergencyJsonDir());
    new Thread(selector).start();
  }

  public SGSSelector(Properties props) {
    logger.debug("Constructing selector");
    tomcatPort = Integer.parseInt(props.getProperty("SGSSRVR_socketPort", "15002"));
    setEmergencyJsonDir(props.getProperty("SGSSRVR_emergencyJsonDir", "/tmp/SGS.crashpad"));
    setDbTestUser(props.getProperty("SGSSRVR_dbTestUser"));
    setDbTestDb(props.getProperty("SGSSRVR_dbTestDb"));
    setDbUrlTemplate(props.getProperty("SGSSRVR_jdbcConnection"));
    setDbHost(props.getProperty("SGSSRVR_databaseHost"));
    setDbPort(Integer.parseInt(props.getProperty("SGSSRVR_databasePort", "5432")));
    //There are others accessed elsewhere.
    System.getProperties().putAll(props);
  }

  public String getDbUrlTemplate() {
    return dbUrlTemplate;
  }

  public void setDbUrlTemplate(String template) {
    dbUrlTemplate = template;
  }

  public String getDbTestUser() {
    return dbTestUser;
  }

  public void setDbTestUser(String testUser) {
    dbTestUser = testUser;
  }

  public String getDbTestDb() {
    return dbTestDb;
  }

  public void setDbTestDb(String testDb) {
    dbTestDb = testDb;
  }

  public void setDbHost(String host) {
    dbHost = host;
  }

  public String getDbHost() {
    return dbHost;
  }

  public void setDbPort(int port) {
    dbPort = port;
  }

  public int getDbPort() {
    return dbPort;
  }

  public String getEmergencyJsonDir() {
    return emergencyJsonDir;
  }

  public void setEmergencyJsonDir(String jsonDir) {
    emergencyJsonDir = jsonDir;
    File efile = new File(emergencyJsonDir);
    if (!efile.exists()) {
      if (!efile.mkdirs()) {
        throw new RuntimeException("Could not find/make emergency landing strip in " + jsonDir);
      }
    }
  }

  public void kickOff() throws LifecycleException, InterruptedException, ServletException {
    if (preflight()) {
      embeddedTomcat = new Tomcat();
      logger.debug("handing in CATALINA_HOME as " + System.getenv("CATALINA_HOME"));
      embeddedTomcat.setPort(tomcatPort);
      embeddedTomcat.enableNaming();
      Service service = embeddedTomcat.getService();
      Connector connector = null;
      if (System.getProperty("SGSSRVR_keystoreFile").length() > 0) {
	connector = addTLSConnector(tomcatPort);
      }
      else {
	connector = addClearConnector(tomcatPort);
      }
      logger.error("This connector is {}secure", connector.getSecure() ? "" : "NOT ");
      service.addConnector(connector);

      logger.info("tomcatd listening on port " + tomcatPort);
      String contextRootPath = System.getProperty("SGSSRVR_ContextRootDir");
      logger.debug("contextRootPath is {}", contextRootPath);
      File sgsPath = new File(contextRootPath + "/sgs");
      if ( ! sgsPath.exists()) {
        System.err.println( "***********\nWebapp not installed at " + sgsPath.getAbsolutePath() + "\n**********\n");
        System.exit(2);
      }
          
      Context contextTomcat = embeddedTomcat.addContext("", sgsPath.getAbsolutePath());
      logger.debug("host: {}, general context basename {}", embeddedTomcat.getHost(), contextTomcat.getBaseName());
      //TODO: cut from here ...
      java.time.LocalDateTime bootDT = java.time.LocalDateTime.now();
      Tomcat.addServlet(contextTomcat, "monitor", new HttpServlet() {
        @Override
        protected void doGet(HttpServletRequest req, HttpServletResponse resp)
          throws ServletException, IOException {
          ServletOutputStream out = resp.getOutputStream();
          out.write("SGS Agent monitor: SGS_OK\n".getBytes());
          out.write(("Up since " + bootDT.toString()).getBytes());
          out.write(("\nTime now " + java.time.LocalDateTime.now().toString()).getBytes());
          out.flush();
          out.close();
        }
      });
      contextTomcat.addServletMappingDecoded("/monitor", "monitor");

      logger.debug("deploy sgs: {}", contextRootPath + "/sgs/sgs.war");
      Context sgsContext = embeddedTomcat.addWebapp("/sgs", contextRootPath + "/sgs"); // /sgs.war
      logger.debug("sgsContext base: {}", sgsContext.getBaseName());
      // addDataSource(sgsContext); // now using context.xml

      embeddedTomcat.start();
      embeddedTomcat.getServer().await();
    }
    else {
      System.err.println("Missing critical piece(s): db reachable? crashpad there?");
      System.exit(2);
    }
  }

  public void run() {
    try {
      logClasspathString();
      kickOff();
    } catch (Throwable t) {
      t.printStackTrace();
      String emsg = "SGS Agent hit a snag: " + t.getMessage();
      logger.error(emsg);
      throw new RuntimeException(emsg);
    }
    System.out.println("good bye");
  }

  private String bufferRangeAsString(ByteBuffer bb, int startIndex, int sizeWanted) {
    byte[] justTheBytes = Arrays.copyOfRange(bb.array(), startIndex, startIndex + sizeWanted);
    String s = new String(justTheBytes, java.nio.charset.StandardCharsets.UTF_8);
    return s;
  }

  private boolean preflight() {
    if (!haveEmergencyJsonDir()) {
      logger.error("no crash pad");
      return false;
    }
    if (!haveBouncer()) {
      logger.error("can't find pgbouncer");
      return false;
    }
    return true;
  }

  private boolean haveEmergencyJsonDir() {
    boolean jsonDirFount = true;
    File edir = new File(emergencyJsonDir);
    if (!edir.exists()) {
      if (!edir.mkdir()) {
        jsonDirFount = false;
        logger.error("could not make emergency json directory: {}", edir);
      }
    }
    else if (!edir.isDirectory()) {
      jsonDirFount = false;
      logger.error("emergency landing strip {} exists, but is not a directory", edir);
    }
    return jsonDirFount;
  }

  // At AWS we don't actually have pgBouncer in play (yet).  Relying
  // on tomcat datasource pools.
  private boolean haveBouncer() {
    boolean gotContext;
    String url = null;
    try {
      url = String.format(dbUrlTemplate, getDbHost(), getDbPort(), dbTestDb);
      trialConnect(url, dbTestUser);
      gotContext = true;
    } catch (Exception rex) {
      logger.error("Blow out on db connection to {}; {}", url, rex.getMessage(), rex);
      gotContext = false;
      throw new RuntimeException("Blow out on db connection", rex);
    }
    return gotContext;
  }

  private void trialConnect(String dbUrl, String namer) throws SQLException {
    String fullUrl = buildDBUrlString(dbUrl);
    try {
      Connection testCon = DriverManager.getConnection(fullUrl, namer, namer + System.getProperty("SGSSRVR_roleExtension"));
      testCon.close();
    }
    catch (SQLException pse) {
      logger.debug("No pg-ssl here {}", dbUrl);
    }
    logger.debug("Selector found db at {}", dbUrl);
  }

  private String buildDBUrlString( String dbUrl) {
    // This will blow up if no db available _immediately
    String mode = System.getProperty("SGSSRVR_databaseSSLMode", "verify-full");
    String cert = System.getProperty("SGSSRVR_databaseSSLCert", "");
    StringBuilder sb = new StringBuilder(dbUrl);
    sb.append("?sslmode=").append(mode);
    if ( cert.length() > 0) {
      sb.append("&sslkey=").append(cert);
    }
    return sb.substring(0);
  }

  private void logClasspathString() {
    ClassLoader applicationClassLoader = this.getClass().getClassLoader();
    if (applicationClassLoader == null) {
      applicationClassLoader = ClassLoader.getSystemClassLoader();
    }
    for (Package p : applicationClassLoader.getDefinedPackages()) {
      logger.error("CP entry: {}", p.getName());
    }
  }

  // We've switched to context.xml/web.xml for this
  public void addDataSource(Context ctx) {
    ContextResource contextResource = null;
    contextResource = new ContextResource();
    contextResource.setName("jdbc/sgsdb");
    String dburl = String.format(dbUrlTemplate, dbHost, dbPort, dbTestDb);
    logger.debug("formatted datasource url = {}", dburl);
    contextResource.setProperty("url",                                dburl);
    contextResource.setProperty("driverClassName",                    "org.postgresql.Driver");
    contextResource.setProperty("type",                               "javax.sql.DataSource");
    contextResource.setProperty("factory",                            "org.apache.tomcat.jdbc.pool.DataSourceFactory");
    contextResource.setProperty("testWhileIdle",                      "false");
    contextResource.setProperty("testOnBorrow",                       "true");
    contextResource.setProperty("validationQuery",                    "SELECT 1");
    contextResource.setProperty("testOnReturn",                       "false");
    contextResource.setProperty("validationInterval",                 "30000");
    contextResource.setProperty("timeBetweenEvictionRunsMillis",      "30000");
    contextResource.setProperty("maxActive",                          "50");
    contextResource.setProperty("initialSize",                        "5");
    contextResource.setProperty("maxWait",                            "10000");
    contextResource.setProperty("removeAbandonedTimeout",             "60");
    contextResource.setProperty("minEvictableIdleTimeMillis",         "30000");
    contextResource.setProperty("minIdle",                            "3");
    contextResource.setProperty("maxIdle",                            "10");
    contextResource.setProperty("logAbandoned",                       "true");
    contextResource.setProperty("removeAbandoned",                    "true");

    ctx.getNamingResources().addResource(contextResource);
  }

  private Connector addClearConnector(int tcport) {
    Connector connector = new Connector();
    connector.setPort(tcport);
    connector.setSecure(false);
    addBaseConnectorConfig(connector);

    return connector;
  }

  private Connector addTLSConnector(int tcport) {
    File keyFile = new File (System.getProperty("SGSSRVR_keystoreFile"));
    if (! keyFile.exists()) throw new RuntimeException("where's the keystore?");
    
    String trustStore = System.getProperty("SGSSRVR_truststoreFile");
    boolean haveTrustStore = false;
    File trustFile = null;
    if ( trustStore != null ) {
      trustFile = new File(trustStore);
      if (! trustFile.exists()) throw new RuntimeException("where's the truststore?  " + trustStore);
      haveTrustStore = true;
    }
    Connector connector = new Connector();
    connector.setPort(tcport);
    connector.setSecure(true);
    addBaseConnectorConfig(connector);
    connectorSetTest(connector, "SSLEnabled", "true");
    connectorSetTest(connector, "sslProtocol", "TLS");
    connectorSetTest(connector, "keyAlias", System.getProperty("SGSSRVR_keystoreAlias"));
    connectorSetTest(connector, "keystorePass", System.getProperty("SGSSRVR_keystorePwd"));
    connectorSetTest(connector, "keystoreFile", keyFile.getAbsolutePath());
    connectorSetTest(connector, "keystoreType", System.getProperty("SGSSRVR_storeType"));

    return connector;
  }

  private void addBaseConnectorConfig(Connector connector) {
    connector.setScheme(System.getProperty("SGSSRVR_scheme"));
    connectorSetTest(connector, "clientAuth", "false");
    connectorSetTest(connector, "maxThreads", "200");
    connectorSetTest(connector, "address",System.getProperty("SGSSRVR_hostaddr"));
  }

  private void connectorSetTest(Connector conn, String prop, String val ) {
    boolean ok = conn.setProperty(prop, val);
    if (! ok) {
      logger.error("trouble with prop %s / %s", prop, val);
    }
  }
}
