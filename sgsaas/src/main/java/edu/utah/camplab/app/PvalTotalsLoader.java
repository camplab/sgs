package edu.utah.camplab.app;

import edu.utah.camplab.db.generated.default_schema.tables.Segment;
import edu.utah.camplab.jx.SGSFullContext;
import static edu.utah.camplab.db.generated.base.Tables.MARKERSET;
import static edu.utah.camplab.db.generated.base.Tables.PEOPLE;
import static edu.utah.camplab.db.generated.default_schema.Tables.PROBANDSET;
import static edu.utah.camplab.db.generated.default_schema.Tables.PROBANDSET_GROUP;
import static edu.utah.camplab.db.generated.default_schema.Tables.PROBANDSET_GROUP_MEMBER;
import static edu.utah.camplab.db.generated.default_schema.Tables.PROJECTFILE;
import static edu.utah.camplab.db.generated.default_schema.Tables.SEGMENT;
import static edu.utah.camplab.db.generated.default_schema.Tables.SEGMENTSET;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.jooq.*;
import org.jooq.Record;
import org.jooq.impl.DSL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.utah.camplab.db.crafted.ProbandsetExtended;
import edu.utah.camplab.db.crafted.ProcessExtended;
import edu.utah.camplab.db.generated.base.tables.records.MarkersetRecord;
import edu.utah.camplab.db.generated.base.tables.records.PeopleRecord;
import edu.utah.camplab.db.generated.default_schema.tables.records.ProbandsetGroupMemberRecord;
import edu.utah.camplab.db.generated.default_schema.tables.records.ProbandsetGroupRecord;
import edu.utah.camplab.db.generated.default_schema.tables.records.ProbandsetRecord;
import edu.utah.camplab.db.generated.default_schema.tables.records.ProjectfileRecord;
import edu.utah.camplab.db.generated.default_schema.tables.records.SegmentRecord;
import edu.utah.camplab.db.generated.default_schema.tables.records.SegmentsetRecord;
import edu.utah.camplab.sgs.SGSRunInterface;
import edu.utah.camplab.tools.LinkageIdSet;
import edu.utah.camplab.tools.DecompressReader;
import edu.utah.camplab.tools.MeiosesCounter;
import edu.utah.camplab.tools.NamePassPrompter;
import edu.utah.camplab.tools.PCPP;
import edu.utah.camplab.tools.PedTrimmer;
import joptsimple.BuiltinHelpFormatter;
import joptsimple.OptionParser;
import joptsimple.OptionSet;
import joptsimple.OptionSpec;
import jpsgcs.linkage.LinkageDataSet;
import jpsgcs.linkage.LinkageFormatter;
import jpsgcs.linkage.LinkageIndividual;
import jpsgcs.linkage.LinkageInterface;
import jpsgcs.linkage.LinkageParameterData;
import jpsgcs.linkage.LinkagePedigreeData;

public class PvalTotalsLoader {
    private final static Logger     logger   = LoggerFactory.getLogger(PvalTotalsLoader.class);

    public static final String SPEC_EFFORT   = "effortFile";
    public static final String SPEC_PCPP     = "pcppFile";
    public static final String SPEC_SEGMIN   = "segMinSize";
    public static final String SPEC_WORKDIR  = "workingDir";
    public static final String SPEC_ASTGROUP = "group";
    public static final String SPEC_MARKERS  = "markersetStem";

    private DSLContext                    ctx               = null; // Use it or loose it?
    private Map<Integer, PCPP>            pcppMap           = new HashMap<>();
    private Map<Integer, Map<String, File>> effortMap        = new HashMap<>();
    private Map<Integer, MarkersetRecord> chromMarkersetMap = null;
    private String                        hostport          = null;
    private String                        investigator      = null;
    private String                        markersetGroup;
    private int                           minSegSize;
    private File                          workingDir;
    private File                          trimFile          = null;


    public static void main(String args[]) {
        PvalTotalsLoader loader = new PvalTotalsLoader();
        loader.handleArgs(args);
        loader.go();
    }

    public String getHostport() {
        return hostport;
    }

    public String getInvestigator() {
        return investigator;
    }

    public void go() {
        Map<String, String>npMap = NamePassPrompter.prompt();
        String dburl = String.format("jdbc:postgresql://%s/%s", getHostport(), getInvestigator());
        try (Connection dbconn = DriverManager.getConnection(dburl, npMap.get("name"), npMap.get("password"))) { 
	  ctx = SGSFullContext.DSLContextFactory(dbconn);
            for(Map.Entry<Integer, Map<String, File>> isfME : effortMap.entrySet()) {
                Integer chrom = isfME.getKey();
                MarkersetRecord markersetRec = getChromosomeMarkersetMap(markersetGroup).get(chrom);
                Map<String,File> sfMap = isfME.getValue();
                PCPP pcpp = pcppMap.get(chrom);
                if (pcpp == null) {
                    logger.error("Presumably we're intentionally skipping chromosome {}, no pcpp found", chrom);
                    continue;
                }
                logger.info("Starting segment loader for chromosome {} pedigree {} with {} files", chrom, pcpp.peopleName, sfMap.size());
                load(pcpp, sfMap, markersetRec, chrom);
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public void handleArgs(String[] args) {
        OptionParser op = new OptionParser();

        //TODO: use sysprop for default db?
        OptionSpec<File>    wkdirSpec  = op.accepts(PvalTotalsLoader.SPEC_WORKDIR,  
						    "directory containing pval totals files").withRequiredArg().ofType(File.class).required();
        OptionSpec<Integer> minsegSpec = op.accepts(PvalTotalsLoader.SPEC_SEGMIN,   
						    "smallest acceptable segment marker count").withRequiredArg().ofType(Integer.class).defaultsTo(1);
        OptionSpec<File>    effortSpec = op.accepts(PvalTotalsLoader.SPEC_EFFORT,   
						    "file of effort definitions").withRequiredArg().ofType(File.class).required();
        OptionSpec<File>    pcppSpec   = op.accepts(PvalTotalsLoader.SPEC_PCPP,     
						    "fof: pop chrom pedfile parfile").withRequiredArg().ofType(File.class).required();
        OptionSpec<String>  trimSpec   = op.accepts(SGSRunInterface.SGS_OPT_TRIM,
						    "file of probands to trim pedigree to").withRequiredArg().ofType(String.class);
        OptionSpec<String>  piSpec     = op.accepts(SGSRunInterface.SGS_OPT_DBNAME,
						    "typically the PI (camp, coon, clark, dere, feldkamp)").withRequiredArg().ofType(String.class).required();
        OptionSpec<String>  dbSpec     = op.accepts(PedParLoader.SPEC_DBHOST, 
						    "'dbhost:port' (:port otptional)").withRequiredArg().ofType(String.class).defaultsTo("campdb.chpc.utah.edu:5432");
        OptionSpec<String>  astSpec    = op.accepts(PvalTotalsLoader.SPEC_ASTGROUP,
                                                    "pet name for probandset grouping").withRequiredArg().ofType(String.class);
        OptionSpec<String>  markerSpec = op.accepts(PvalTotalsLoader.SPEC_MARKERS,
                                                    "common stem of markerset names").withRequiredArg().ofType(String.class).required();
        OptionSpec<Void>    helpSpec   = op.accepts(PedParLoader.SPEC_HELP);
        
        OptionSet opSet =  null;
        try {
            opSet = op.parse(args);
            op.formatHelpWith(new BuiltinHelpFormatter(120, 4));
            if (opSet.has(helpSpec)) {
                op.printHelpOn(System.out);
                System.exit(0);
            }
            
        }

        catch(Exception ex) {
            ex.printStackTrace();
            try {
            	op.printHelpOn(System.out);
            }
            catch (Exception e) {
            	;
            }
            System.exit(2);
        }
        workingDir = opSet.valueOf(wkdirSpec);
        hostport = opSet.valueOf(dbSpec);
        investigator = opSet.valueOf(piSpec);
        setEffortMap(opSet.valueOf(effortSpec));
        setPCPPMap(opSet.valueOf(pcppSpec), opSet.valueOf(astSpec));
        markersetGroup = (opSet.valueOf(markerSpec));
        minSegSize = opSet.valueOf(minsegSpec);
        if (opSet.has(trimSpec)) {
            trimFile = new File(opSet.valueOf(trimSpec));
        }
        
    }

    // run per chromosome
    public void load(PCPP pcpp, Map<String, File> probandsFileMap, MarkersetRecord markersetRec, int chrm) {
        ctx.transaction(conf -> {
                ProjectfileRecord pedRec = readFileRec(new File(pcpp.pedfileName));
                ProjectfileRecord parRec = readFileRec(new File(pcpp.parfileName));
                PeopleRecord peopleRec = readPeople(pcpp.peopleName);
                Map<String,SegmentRecord> preExistingSegmentMap = readExistingSegments(peopleRec, markersetRec);
                ProcessExtended processEx = setupProcess(pedRec, parRec);
                LinkageInterface LI = buildLinkageDataSet(pcpp.pedfileName, pcpp.parfileName);
                SegmentsetRecord ssetRec = stowSegmentSet(processEx, pedRec, parRec, pcpp);
                Set<ProbandsetRecord> denovoProbandsetList = new HashSet<>();
                ProbandsetGroupRecord probandsetGroupRec = maybeStowProbandsetGroup(processEx, peopleRec, pcpp);
		//List<ProbandsetRecord> knownGroupMembers = ProbandsetExtended.getProbandsetGroupMembership(ctx, probandsetGroupRec);
                Map<UUID, ProbandsetGroupMemberRecord> groupMemberMap = readGroupMembers(probandsetGroupRec);
                Set<SegmentRecord> segmentList = new HashSet<>();
                Set<SegmentRecord> updateableSegmentList = new HashSet<>();
                logger.debug("file map for chrom {} has {} entries ", pcpp.chromosome, probandsFileMap.size());
                for (Map.Entry<String, File> sfME : probandsFileMap.entrySet()) {
                    LinkageIdSet lis = new LinkageIdSet(sfME.getKey());
                    LinkageIdSet.resetProbands(LI, lis);
                    MeiosesCounter mc = new MeiosesCounter(LI);
                    Integer pbsetMeioses = mc.getCount(0);  // Expect only one pedigree per ped file
                    ProbandsetRecord pbsRec = getProbandsetRecord(lis, peopleRec, pbsetMeioses);
                    if (pbsRec.changed()) {
                        denovoProbandsetList.add(pbsRec);
                    }
                    try {
                        DecompressReader dReader = new DecompressReader(sfME.getValue(), pbsRec, markersetRec, minSegSize);
                        List<SegmentRecord> newfoundSegments = dReader.readSegments(ctx);
                        List<SegmentRecord> cullable = new ArrayList<>();
                        for (SegmentRecord newfoundSeg: newfoundSegments) {
                            String surrogate = surrogateKey(newfoundSeg);
                            SegmentRecord known = preExistingSegmentMap.get(surrogate);
                            if (known != null) {
                                mergeSegmentEvents(known, newfoundSeg);
                                updateableSegmentList.add(known);
                                preExistingSegmentMap.remove(surrogate);
                                cullable.add(newfoundSeg);
                            }
                        }
                        newfoundSegments.removeAll(cullable);
                        segmentList.addAll(newfoundSegments);
                        logger.info("read in {} new segment definitions and {} updateable segments", segmentList.size(), cullable.size());

                    }
                    catch (IOException ioe) {
                        throw new RuntimeException("can't read totals file: " + sfME.getValue(), ioe);
                    }
                }

                ctx.batchStore(denovoProbandsetList).execute();
                ctx.batchStore(segmentList).execute();
                ctx.batchStore(updateableSegmentList).execute();
                logger.info("Added {} probandsets, {} segments, updated {}", denovoProbandsetList.size(), segmentList.size(), updateableSegmentList.size());
                             
                ProbandsetRecord superset = null;
                if (probandsetGroupRec.getProbandSupersetId() == null) {
                    superset = grabProbandSuperset(processEx, peopleRec, LI);
                    probandsetGroupRec.setProbandSupersetId(superset.getId());
                    if (superset.changed()) {
                        denovoProbandsetList.add(superset);
                    }
                }
                maybeExtendProbandsetGroup(groupMemberMap, probandsetGroupRec, denovoProbandsetList);
                processEx.startSave();
                processEx.addOutput("segmentset", ssetRec.getId());
                processEx.addOutput("probandsetGroup", probandsetGroupRec.getId());
                processEx.finishSave();
            });
    }
    
    //TODO: collect these somewhere...
    public PeopleRecord readPeople(String name) {
        PeopleRecord peopleRec = ctx.selectFrom(PEOPLE).where(PEOPLE.NAME.equal(name)).fetchOne();
        if (peopleRec == null) {
            throw new RuntimeException(name + " is not a known people");
        }
        return peopleRec;
    }

    private Map<UUID, ProbandsetGroupMemberRecord> readGroupMembers(ProbandsetGroupRecord groupRec) {
        return ctx.selectFrom(PROBANDSET_GROUP_MEMBER)
            .where(PROBANDSET_GROUP_MEMBER.GROUP_ID.equal(groupRec.getId()))
            .fetchMap(PROBANDSET_GROUP_MEMBER.MEMBER_ID);
    }
    
    private ProcessExtended setupProcess(ProjectfileRecord ped, ProjectfileRecord par) throws UnknownHostException {
        InetAddress machine = InetAddress.getLocalHost();
        ProcessExtended px = new ProcessExtended(ctx, getClass().getName(), machine, UUID.randomUUID());
        px.addInput("pedfile", ped.getId().toString());
        px.addInput("parfile", par.getId().toString());
        return px;
    }
    
    private ProbandsetGroupRecord maybeStowProbandsetGroup(ProcessExtended procRec,
                                                           PeopleRecord peopleRec,
							   PCPP planner) {
	String pbsgName = String.format("%s_%s", peopleRec.getName(), planner.assignedType);
        ProbandsetGroupRecord probandsetGroupRec =
            ctx.selectFrom(PROBANDSET_GROUP).where(PROBANDSET_GROUP.NAME.equal(pbsgName)).fetchOne();
        if (probandsetGroupRec == null) {
            probandsetGroupRec = ctx.newRecord(PROBANDSET_GROUP);
            probandsetGroupRec.setId(UUID.randomUUID());
            probandsetGroupRec.setName(pbsgName);
            probandsetGroupRec.insert();
            procRec.addOutput("proband group", probandsetGroupRec.getId());
        }
	return probandsetGroupRec;
    }
    
    private SegmentsetRecord stowSegmentSet(ProcessExtended procRec, ProjectfileRecord pedr, ProjectfileRecord parr, PCPP pc) {
        SegmentsetRecord segmentsetRec = ctx.newRecord(SEGMENTSET);
        segmentsetRec.setId(UUID.randomUUID());
        // segmentsetRec.setPedfileId(pedr.getId());
        // segmentsetRec.setParfileId(parr.getId());
        segmentsetRec.setName(String.format("%s and %s%s", 
                                            pedr.getName(), 
                                            parr.getName(), 
                                            pc.assignedType == null ? "" : (" for " + pc.assignedType)));
        segmentsetRec.insert();
        return segmentsetRec;
    }

    private ProjectfileRecord readFileRec(File f) {
        //TODO: return to uri after testing
    	//ProjectfileRecord rec = ctx.selectFrom(PROJECTFILE).where(PROJECTFILE.URI.equal(f.toURI().toString())).fetchOne();
    	ProjectfileRecord rec = ctx.selectFrom(PROJECTFILE).where(PROJECTFILE.NAME.equal(f.getName())).fetchOne();
        if (rec == null) {
            throw new RuntimeException("No project file for " + f.toURI().toString());
        }
        return rec;
    }

    private ProbandsetRecord grabProbandSuperset(ProcessExtended processEx, PeopleRecord peopleRec, LinkageInterface linkageInterface ) {
        List<String> idList = new ArrayList<>();  //really a name list
        // Since a) we have both ped and par in hand and b) we might have .gz files, we'll use
        // the linkage formatter to find the probands
        for (LinkageIndividual indiv : linkageInterface.raw().getPedigreeData().getIndividuals()) {
            if (indiv.getProband() == 1) {
                idList.add(indiv.id.toString());
            }
        }
        if (idList.size() == 0) {
            throw new RuntimeException(String.format("Pedfile %s has no probands!", peopleRec.getName()));
        }
        ProbandsetRecord psetRec = ProbandsetExtended.findProbandsetByNames(ctx, idList, peopleRec);
        return psetRec;
    }

    private Map<Integer, MarkersetRecord> getChromosomeMarkersetMap(String stem) {
        if (chromMarkersetMap == null) {
            chromMarkersetMap = findMarkersetMap(stem);
        }
        return chromMarkersetMap;
    }

    private Map<Integer,MarkersetRecord> findMarkersetMap(String namestem) {
        Map<Integer,MarkersetRecord> chromSetMap = new HashMap<>();
        chromSetMap = ctx.selectFrom(MARKERSET)
            .where(MARKERSET.NAME.contains(namestem))
            .fetchMap(MARKERSET.CHROM);
        return chromSetMap;
    }
    
    private void maybeExtendProbandsetGroup(Map<UUID, ProbandsetGroupMemberRecord> groupMap,
                                            ProbandsetGroupRecord groupRec,
                                            Set<ProbandsetRecord> denovoList) {
        //TODO: this should get people_id?
        for (ProbandsetRecord pbsr : denovoList) {
            if (groupMap.get(pbsr.getId()) == null) {
                ProbandsetGroupMemberRecord pgRec = ctx.newRecord(PROBANDSET_GROUP_MEMBER);
                pgRec.setGroupId(groupRec.getId());
                pgRec.setMemberId(pbsr.getId());
                groupMap.put(pbsr.getId(), pgRec);
            }
        }
        storeNewGroupMembers(groupMap);
    }

    private void setEffortMap(File effortsFile) {
        try (BufferedReader reader = new BufferedReader(new FileReader(effortsFile))) {
            String line = null;
            while ((line = reader.readLine()) != null) {
                if (line.startsWith("#")) {
                    continue;
                }
                String[] parts = line.split("\\s");
                File totalsDir = new File(workingDir, "chrom" + parts[1]);
                File pvalfile = new File(totalsDir, parts[0]+".pval.total.gz");
                if (! pvalfile.exists() ) {
                    throw new RuntimeException("missing pval file " + pvalfile.getCanonicalPath());
                }
                Integer chrom = Integer.parseInt(parts[1]);
                Map<String, File> sfmap = effortMap.get(chrom);
                if (sfmap == null) {
                    sfmap = new HashMap<>();
                    effortMap.put(chrom, sfmap);
                }
                String probandCSV = parts[2].replace("-", ",");
                File oldfile = sfmap.put(probandCSV, pvalfile);
                if (oldfile != null) {
                    throw new RuntimeException("Found second file for subset " + parts[2]);
                }
            }
        }
        catch (IOException ioe) {
            throw new RuntimeException("trouble reading efforts file", ioe);
        }
    }

    private void setPCPPMap(File pcppFile, String astName) {
        try (BufferedReader reader = new BufferedReader(new FileReader(pcppFile))) {
	    String line = null;
	    while ((line = reader.readLine()) != null) {
		if (line.startsWith("#")) {
		    continue;
		}
		PCPP pcpp = new PCPP(line);
		pcpp.assignedType = astName;
		pcppMap.put(pcpp.chromosome, pcpp);
	    }
	}
        catch (IOException ioe) {
            throw new RuntimeException("trouble reading pcpp file", ioe);
        }
    }

    private LinkageInterface buildLinkageDataSet(String pedf, String parf) {
        LinkageFormatter lf = null;
        LinkagePedigreeData peddata = null;
        LinkageParameterData pardata = null;
        try {
            lf  = new LinkageFormatter(pedf);
            peddata = new LinkagePedigreeData(lf, null, false);  // relying here on single pedigree per file
            if (trimFile != null) {
                Set<String> probands = PedTrimmer.getProbandIdsFromListFile(trimFile);
                LinkageIdSet lidset = new LinkageIdSet(probands);
                peddata.resetProbands(lidset.asList());
            }
            lf = new LinkageFormatter(parf);
            pardata = new LinkageParameterData(lf, false);
        }
        catch (IOException ioe) {
            throw new RuntimeException("trouble building LI", ioe);
        }
        LinkageDataSet linkageDataSet = new LinkageDataSet(pardata, peddata);
        LinkageInterface li = new LinkageInterface(linkageDataSet);
        return li;
    }

    private void storeNewGroupMembers(Map<UUID, ProbandsetGroupMemberRecord> gmMap) {
        Set<ProbandsetGroupMemberRecord> news = new HashSet<>();
        for (ProbandsetGroupMemberRecord pgmRec : gmMap.values()) {
            if (pgmRec.changed()) {
                news.add(pgmRec);
            }
        }
        logger.debug("found {} new group members of {} total", news.size(), gmMap.size());
        ctx.batchStore(news).execute();
    }

    private ProbandsetRecord getProbandsetRecord(LinkageIdSet lis, PeopleRecord peeps, Integer pbsMeioses) {
	//We'll get clever here, someday
	return ProbandsetExtended.findOrMakeProbandset(ctx, lis.asStringList(), peeps, pbsMeioses);
    }

    private Map<String, SegmentRecord> readExistingSegments(PeopleRecord peeps, MarkersetRecord markers) {
        Map<String, SegmentRecord> segMap = new HashMap<>();

        Result<Record> list = ctx.select(SEGMENT.asterisk())
          .from(SEGMENT)
          .join(PROBANDSET).on(SEGMENT.PROBANDSET_ID.equal(PROBANDSET.ID))
          .join(PEOPLE).on(PROBANDSET.PEOPLE_ID.equal(PEOPLE.ID))
            .where(SEGMENT.MARKERSET_ID.equal(markers.getId())
                   .and(PEOPLE.ID.equal(peeps.getId())))
            .fetch();
        for (Record r : list) {
          SegmentRecord seg = r.into(new Segment());
          String surrogate= surrogateKey(seg);
          segMap.put(surrogate,seg);
        }
        return segMap;
    }
        
    private void mergeSegmentEvents(SegmentRecord starter, SegmentRecord add) {
        starter.setEventsLess(starter.getEventsLess() + add.getEventsLess());
        starter.setEventsEqual(starter.getEventsEqual() + add.getEventsEqual());
        starter.setEventsGreater(starter.getEventsGreater() + add.getEventsGreater());
    }

    private String surrogateKey(SegmentRecord seg) {
        return String.format("%s@%don%d", seg.getProbandsetId().toString(), seg.getStartbase(),  seg.getChrom());
    }
}
