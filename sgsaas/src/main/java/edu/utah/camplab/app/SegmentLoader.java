package edu.utah.camplab.app;

import org.jooq.impl.DSL;
import org.jooq.util.postgres.PostgresDSL;

import static org.jooq.impl.DSL.*;
import org.jooq.*;

import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.*;
import java.io.File;
import java.io.IOException;
import java.net.*;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

import static edu.utah.camplab.db.generated.base.Tables.*;
import static edu.utah.camplab.db.generated.default_schema.Tables.*;
import edu.utah.camplab.db.generated.base.tables.records.*;
import edu.utah.camplab.db.generated.default_schema.tables.records.*;
import edu.utah.camplab.jx.SGSFullContext;

import edu.utah.camplab.db.crafted.ProcessExtended;
import edu.utah.camplab.tools.DecompressVisitor;
import edu.utah.camplab.tools.NamePassPrompter;

import jpsgcs.linkage.*;

public class SegmentLoader {

    private Connection            dbconn             = null;
    private DSLContext            ctx                = null; // Use it or loose it?
    private File                  pedfile            = null;
    private File                  parfile            = null;
    private File                  workdir            = null;
    private int                   chromosome         = 0;
    private int                   minSegSize;
    private String                filePattern;
    private String                peopleName;

    private String                projectName        = null;
    private PeopleRecord          peopleRec          = null;
    private ProbandsetGroupRecord probandsetGroupRec = null;

    public SegmentLoader(Connection c, File ped, File par, File dir, int chr, int ms, String prj, String regx, String peepname) {
        dbconn = c;
        pedfile = ped;
        parfile = par;
        workdir = dir;
        chromosome = chr;
        minSegSize = ms;
        projectName = prj;
        filePattern = regx;
        peopleName = peepname;
    }

    public static void main(String args[]) {
        if (args.length != 6) {
            System.out.println("usage: connection pedparFile dataDir minSegmentSize project filePattern");
            System.exit(0);
        }
        Map<String, String>npMap = NamePassPrompter.prompt();
    	String url = args[0]; // "jdbc:postgresql://kitsegas:5432/sgs";
    	File pedParList = new File(args[1]);
    	try(Connection c = DriverManager.getConnection(url, npMap.get("name"), npMap.get("password"));
            BufferedReader reader = new BufferedReader(new FileReader(pedParList)) ) {
                File dir = new File(args[2]);
                int minsize = Integer.parseInt(args[3]);
                String projname = args[4];
                String pattern = args[5];

                String line = null;
                Map<Integer, MarkersetRecord> markersetChromMap = findMarkersetMap(c, projname);
                // A separate tx for each line of the control file in new instance of SegmentLoader
                while ((line = reader.readLine()) != null) {
                    if (line.startsWith("#"))
                        continue;
                    String[] parts = line.split("\\s");
                    int chrom = Integer.parseInt(parts[0]);
                    String pedName = parts[1];
                    File pedf  = new File(parts[2]);
                    File parf  = new File(parts[3]);
                    MarkersetRecord msRec = markersetChromMap.get(chrom);
                    SegmentLoader segLoader = new SegmentLoader(c, pedf, parf, dir, chrom, minsize, projname, pattern, pedName);
                    segLoader.load(msRec);
                }
            }
    	catch( Exception e ) {
            e.printStackTrace();
    	}
    }

    public void load(MarkersetRecord markersetRec) {
      ctx = SGSFullContext.DSLContextFactory(dbconn);
      // What else should be done here?
      setPeople(peopleName);
      ctx.transaction(conf -> {
	  ProcessExtended processEx = setupProcess();
	  SegmentsetRecord segmentsetRec = stowSegmentSet(processEx);
	  if (probandsetGroupRec == null) {
	    maybeStowProbandsetGroup(processEx);
	  }
	  stowMultiAssessResults(processEx, segmentsetRec, markersetRec);
	});
    }
    
    //TODO: collect these somewhere...
    public void setPeople(String name) {
        peopleRec = ctx.selectFrom(PEOPLE).where(PEOPLE.NAME.equal(name)).fetchOne();
        if (peopleRec == null) {
            throw new RuntimeException(name + " is not a known people");
        }
    }

    private ProcessExtended setupProcess() throws UnknownHostException {
        InetAddress machine = InetAddress.getLocalHost();
        ProcessExtended px = new ProcessExtended(ctx, getClass().getName(), machine, UUID.randomUUID()); 
//        pedRec = readFileRec(pedfile);
//        parRec = readFileRec(parfile);
//        px.addInput("pedfile", pedRec.getId().toString());
//        px.addInput("parfile", parRec.getId().toString());
        return px;
    }
    
    private void maybeStowProbandsetGroup(ProcessExtended procRec) {
        probandsetGroupRec = ctx.selectFrom(PROBANDSET_GROUP).where(PROBANDSET_GROUP.NAME.equal(peopleRec.getName())).fetchOne();
        if (probandsetGroupRec == null) {
            probandsetGroupRec = ctx.newRecord(PROBANDSET_GROUP);
            probandsetGroupRec.setId(UUID.randomUUID());
            probandsetGroupRec.setProbandSupersetId(grabProbandSuperset(procRec).getId());
            probandsetGroupRec.setName(pedfile.getName());
            probandsetGroupRec.insert();
            //reed it out?  TODO: confirm with jOOQ that this isn't necessary
            ProbandsetGroupMemberRecord supersetMember = ctx.newRecord(PROBANDSET_GROUP_MEMBER);
            supersetMember.setGroupId(probandsetGroupRec.getId());
            supersetMember.setMemberId(probandsetGroupRec.getProbandSupersetId());
            supersetMember.insert();
            procRec.addOutput("proband group", probandsetGroupRec.getId());
        }
    }
    
    private SegmentsetRecord stowSegmentSet(ProcessExtended procRec) {
        SegmentsetRecord segmentsetRec = ctx.newRecord(SEGMENTSET);
        segmentsetRec.setId(UUID.randomUUID());
        // add people, markerset it
        if (peopleRec == null) {
          throw new RuntimeException("add people, markerset to segment set record");
        }
        segmentsetRec.insert();
        procRec.addOutput("segment set", segmentsetRec.getId()); //move to setup?
        return segmentsetRec;
    }

    private ProjectfileRecord readFileRec(File f) {
    	ProjectfileRecord rec = ctx.selectFrom(PROJECTFILE).where(PROJECTFILE.URI.equal(f.toURI().toString())).fetchOne();
        if (rec == null) {
            throw new RuntimeException("No project file for " + f.toURI().toString());
        }
        return rec;
    }

    private ProbandsetRecord grabProbandSuperset(ProcessExtended processEx) {
        List<String> idList = new ArrayList<>();  //really a name list
        // Since a) we have both ped and par in hand and b) we might have .gz files, we'll use
        // the linkage formatter to find the probands
        try (LinkageFormatter parin = new LinkageFormatter(parfile.getCanonicalPath());
             LinkageFormatter pedin = new LinkageFormatter(pedfile.getCanonicalPath()))
                {
                    LinkageParameterData par = new LinkageParameterData(parin);
                    LinkagePedigreeData ped = new LinkagePedigreeData(pedin, par, new HashSet<String>());
                    for (LinkageIndividual indiv : ped.getIndividuals()) {
                        if (indiv.getProband() == 1) {
                            idList.add(indiv.id.toString());
                        }
                    }
                }
        catch (IOException ioe) {
            //logger.error("Trouble reading ped/par: {} {}", pedfile, parfile);
            throw new RuntimeException("ped par problem: " + ioe.getMessage(), ioe);
        }
        if (idList.size() == 0) {
            throw new RuntimeException(String.format("Pedfile %s has no probands!", pedfile.getName()));
        }
                                       
        UUID newsetId = UUID.randomUUID();
        //TODO: use lesson from Lukas.
        ProbandsetRecord psetRec =
            ctx.selectFrom(PROBANDSET)
            .where(PROBANDSET.PROBANDS.equal(PostgresDSL.array(select(PERSON.ID)
                                                               .from(PERSON)
                                                               .join(ALIAS).on(PERSON.ID.equal(ALIAS.PERSON_ID))
                                                               .where(PERSON.NAME.in(idList).and(ALIAS.PEOPLE_ID.equal(peopleRec.getId())))
                                                               .orderBy(PERSON.ID))))
            .fetchOne();
        if (psetRec == null) {
            // working with Lukas here
            ctx.insertInto(PROBANDSET)
                .set(PROBANDSET.ID, newsetId)
                .set(PROBANDSET.NAME, pedfile.getName())
                .set(PROBANDSET.PROBANDS,
                     ctx.select(arrayAgg(PERSON.as("tid").ID)).from(ctx.select(PERSON.ID  )
                                                                    .from(PERSON)
                                                                    .join(ALIAS).on(PERSON.ID.equal(ALIAS.PERSON_ID))
                                                                    .where(PERSON.NAME.in(idList).and(ALIAS.PEOPLE_ID.equal(peopleRec.getId())))
                                                                    .orderBy(PERSON.ID).asTable("tid")))
                .set(PROBANDSET.PEOPLE_ID, peopleRec.getId())
                .execute();
            psetRec = ctx.selectFrom(PROBANDSET).where(PROBANDSET.ID.equal(newsetId)).fetchOne();
            processEx.addOutput("proband set", psetRec.getId());
        }
        return psetRec;
    }

    private static Map<Integer,MarkersetRecord> findMarkersetMap( Connection channel, String prjname ) {
      DSLContext lookup = SGSFullContext.DSLContextFactory(channel);
        Map<Integer,MarkersetRecord> chromSetMap = new HashMap<>();
        String[] parTypes = new String[]{"ld","par"};
        chromSetMap = lookup.select(MARKERSET.fields())
            .from(MARKERSET.join(PROJECTFILE)
                  .on(MARKERSET.NAME.equal(PROJECTFILE.NAME)))
            .where(MARKERSET.NAME.equal(PROJECTFILE.NAME)).and(PROJECTFILE.FILETYPE.in(parTypes))
            .fetchMap(MARKERSET.CHROM, r->r.into(MARKERSET));
        return chromSetMap;
    }

    private void stowMultiAssessResults(ProcessExtended pRec, SegmentsetRecord ssRec, MarkersetRecord msRec) {
        long start = System.currentTimeMillis();
        String pattern = String.format(filePattern, chromosome);
        pRec.startSave();
        try {
            FileVisitor<Path>visitor = new DecompressVisitor(ctx, pRec, msRec, ssRec, probandsetGroupRec, projectName, pattern, minSegSize, peopleRec);
            Files.walkFileTree(workdir.toPath(), visitor);
        }
        catch (IOException ioe) {
            throw new RuntimeException(ioe);
        }
        pRec.finishSave();
        System.err.println(" Segment visitor done in " + (System.currentTimeMillis() - start) + "ms");
    }
}
