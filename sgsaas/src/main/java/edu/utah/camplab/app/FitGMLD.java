package edu.utah.camplab.app;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;

import joptsimple.BuiltinHelpFormatter;
import joptsimple.OptionParser;
import joptsimple.OptionSet;
import joptsimple.OptionSpec;
import jpsgcs.genio.GeneticDataSource;
import jpsgcs.graph.Network;
import jpsgcs.graph.NetworkMask;
import jpsgcs.jtree.JTree;
import jpsgcs.jtree.JTreeSampler;
import jpsgcs.jtree.MultinomialMLEScorer;
import jpsgcs.jtree.OrderedJTree;
import jpsgcs.jtree.ProperIntervalJTree;
import jpsgcs.linkage.LinkageDataSet;
import jpsgcs.linkage.LinkageFormatter;
import jpsgcs.linkage.LinkageInterface;
import jpsgcs.linkage.LinkageParameterData;
import jpsgcs.linkage.LinkagePedigreeData;
import jpsgcs.markov.Function;
import jpsgcs.markov.Variable;
import jpsgcs.pedapps.CompleteHaplotypes;
import jpsgcs.pedmcmc.LDModel;
import jpsgcs.util.Monitor;
import jpsgcs.viewgraph.GraphFrame;

import edu.utah.camplab.sgs.SharingRun;

public class FitGMLD {
    private boolean              visualize             = false;
    private boolean              proper                = false;

    private int                  maxlink               = 35;
    private int                  windowwidth           = 250;
    private int                  gibbsperwindow        = 50;
    private int                  optgibbsperwindow     = 10;
    private int                  metropergibbsperlocus = 200;

    private double               penalty               = 0.25;
    private double               eprob                 = 0.001;

    private PrintStream          outStream             = null;

    private LinkageParameterData par                   = null;
    private LinkagePedigreeData  ped                   = null;
    private LinkageInterface     geneticDataSource     = null;
    private Random               rng                   = new Random();


    public FitGMLD(File parFile, File pedFile) {
        try {
            par = new LinkageParameterData(new LinkageFormatter(openBufferedReader(parFile), parFile.getName()));
            ped = new LinkagePedigreeData(new LinkageFormatter(openBufferedReader(pedFile), pedFile.getName()), par);
            geneticDataSource = new LinkageInterface(new LinkageDataSet(par, ped));
        }
        catch (IOException ioe) {
            ioe.printStackTrace();
            System.exit(2);
        }
    }

    public FitGMLD(LinkageParameterData parData, LinkagePedigreeData pedData) {
	par = parData;
	ped = pedData;
	geneticDataSource = new LinkageInterface(new LinkageDataSet(par, ped));
    }
    
    public static void main(String[] args) {
        OptionParser op = new OptionParser();
        op.formatHelpWith(new BuiltinHelpFormatter(80, 4));

	int runStatus = 0;

        FitGMLD fitgmld = null;
        try {
            // Set parameters to default values.
            Monitor.quiet(false);

            OptionSpec<File> pedFileSpec = op.accepts("d").withRequiredArg().ofType(File.class).describedAs("ped file name").required();
            OptionSpec<File> parFileSpec = op.accepts("p").withRequiredArg().ofType(File.class).describedAs("par file name").required();
            OptionSpec<File> outFileSpec = op.accepts("l").withRequiredArg().ofType(File.class).describedAs("ld output file name").required();
            OptionSpec<File> reducerSpec = op.accepts("r").withRequiredArg().ofType(File.class).describedAs("reducing specification file (segment defn.)");

            OptionSpec<Integer> maxlinkSpec = op.accepts("m", "maximum links").withOptionalArg().ofType(Integer.class).defaultsTo(35);
            OptionSpec<Integer> windowSpec = op.accepts("w", "window width").withOptionalArg().ofType(Integer.class).defaultsTo(250);
            OptionSpec<Integer> gibbsSpec = op.accepts("g", "gibbs per window").withOptionalArg().ofType(Integer.class).defaultsTo(50);
            OptionSpec<Integer> optSpec = op.accepts("o", "optimal gibbs per windo").withOptionalArg().ofType(Integer.class).defaultsTo(10);
            OptionSpec<Integer> metroSpec = op.accepts("e", "metro per gibbs per Locus").withOptionalArg().ofType(Integer.class).defaultsTo(200);

            OptionSpec<Double> penaltySpec = op.accepts("cost", "penalty").withOptionalArg().ofType(Double.class).defaultsTo(0.25);
            OptionSpec<Double> eprobSpec = op.accepts("error-rate", "error rate").withOptionalArg().ofType(Double.class).defaultsTo(0.001);

            OptionSpec<Void> visSpec = op.accepts("visualize", "visualization on");
            OptionSpec<Void> properSpec = op.accepts("proper", "proper tree on");

            List<String> helps = new ArrayList<String>();
            helps.add("help");
            helps.add("h");
            OptionSpec<Void> helpSpec = op.acceptsAll(helps);

            OptionSet opSet = op.parse(args);

            if (opSet.has(helpSpec)) {
                cliHelp(op, null);
            }

            fitgmld = new FitGMLD(parFileSpec.value(opSet), pedFileSpec.value(opSet));
            if (opSet.has(reducerSpec)) {
                File segFile = reducerSpec.value(opSet);
                fitgmld.reduceChromosome(segFile);
            }

            fitgmld.setOutStream(new PrintStream(outFileSpec.value(opSet)));
            fitgmld.setMaxlink(maxlinkSpec.value(opSet));
            fitgmld.setWindowwidth(windowSpec.value(opSet));
            fitgmld.setGibbsperwindow(gibbsSpec.value(opSet));
            fitgmld.setOptgibbsperwindow(optSpec.value(opSet));
            fitgmld.setMetropergibbsperlocus(metroSpec.value(opSet));
            fitgmld.setPenalty(penaltySpec.value(opSet));
            fitgmld.setEprob(eprobSpec.value(opSet));
            fitgmld.setVisualize(opSet.has(visSpec));
            fitgmld.setProper(opSet.has(properSpec));

        }
        catch (FileNotFoundException | RuntimeException oe) {
            System.out.println(oe.getMessage());
            cliHelp(op, oe);
        }
        try {
            fitgmld.run();
            Monitor.show("Done  =   ");
        }
        catch (ConcurrentModificationException cme) {
	    runStatus = 2;
            cme.printStackTrace();
        }
        catch (Exception e) {
            System.err.println("Caught in FitGMLD.main()");
            e.printStackTrace();
	    runStatus = 2;
        }
	System.exit(runStatus);
    }

    public static void cliHelp(OptionParser op, Throwable t) {
        if (null != t) {
            t.printStackTrace();
        }
        try {
            op.printHelpOn(System.out);
            System.exit(2);
        }
        catch (IOException ioe) {
            ;
        }
        System.exit(3);

    }

    public void run() {
        // Initial model.
        LDModel ld = new LDModel(geneticDataSource);

        // Set the initial graphical model to be have Markov structure with
        // specified lag.

        Variable[] loc = ld.getLocusVariables();

        Network<Variable, Object> g = new Network<Variable, Object>();
        for (int i = 0; i < loc.length; i++) {
            g.add(loc[i]);
        }

        int lag = 2;
        for (int i = 0; i < loc.length; i++) {
            int low = i - lag;
            if (low < 0) {
                low = 0;
            }
            for (int j = low; j < i; j++) {
                g.connect(loc[j], loc[i]);
            }
        }

        // Make a graph viewer if required.
        GraphFrame<Variable, Object> frame = (visualize ? new GraphFrame<Variable, Object>(g) : null);

        // Run the windowed MCMC scheme to fit a Markov graph.

        runWindowMCMC(ld, g);
        // Fit the parameters of the LD model for the graph.

        EMFitLDModelForGraph(ld, g, geneticDataSource, 10, eprob, rng);

        // Output the model.

        par.writeTo(outStream);
        ld.writeStateSizes(outStream);
        ld.writeFunctions(outStream);

    }

    public void runWindowMCMC(LDModel ld, Network<Variable, Object> gg) {
        boolean[] both = { true, false };

        ld.clear();
        Variable[] loci = ld.getLocusVariables();

        NetworkMask<Variable, Object> g = new NetworkMask<Variable, Object>(gg);
        g.hideAll();

        // Find and read in initial window of data.
        int held = 3 * windowwidth / 2 + 2 * 2 * maxlink;
        if (held > loci.length) {
            held = loci.length;
        }

        // Make and initialize haplotypes in the window.
        CompleteHaplotypes haps = new CompleteHaplotypes(geneticDataSource, eprob, held, rng);
        ld.initialize(0, held);
        haps.setModel(ld);
        haps.update(true);
        MultinomialMLEScorer scorer = new MultinomialMLEScorer(loci, haps, penalty * 0.5 * Math.log(haps.nRows()), rng);

        // Go into main MCMC loop.
        Monitor.show("Setup time = ");
        int expectedLoops = (loci.length + windowwidth) / (windowwidth / 2);
        System.err.println(String.format("Loops to run = %d +/- 1", expectedLoops));
	int loopCount = 1;
	System.err.println("timestamp,loop,position,locus");
        for (int low = -windowwidth; low < loci.length; low += windowwidth / 2) {
            int get = low + 3 * windowwidth / 2 + 2 * maxlink - held;
            if (get + held > loci.length) {
                get = loci.length - held;
            }

            if (get > 0) {
                ld.initialize(held, get);
                haps.shift(get);
                haps.setModel(ld);
                haps.update(true);
                scorer.clear();
                held += get;
            }

            // Now do the sampling.
            for (boolean random : both) {
                int ll = random ? low + windowwidth / 2 : low;
                Set<Variable> order = new LinkedHashSet<Variable>();
                Set<Variable> wind = showWindow(g, loci, ll, order);
                if (wind.isEmpty()) {
                    continue;
                }

                JTree<Variable> jt = new JTree<Variable>(g, order, new Random());

                JTreeSampler<Variable> jts = null;

                if (proper) {
                    jts = new ProperIntervalJTree<Variable>(jt, loci, maxlink, wind, scorer);
                }
                else {
                    jts = new OrderedJTree<Variable>(jt, loci, maxlink, wind, scorer);
                }

                jts.setTemperature(random ? 1 : 0);

                int nits = random ? gibbsperwindow : optgibbsperwindow;

                for (int i = 0; i < nits; i++) {
                    ld.clear();
                    for (Function f : scorer.fitModel(jt).getFunctions()) {
                        ld.add(f);
                    }
                    haps.setModel(ld);
                    haps.update(random);
                    scorer.clear();

                    for (int j = 0; j < metropergibbsperlocus * windowwidth; j++) {
                        if (Math.random() < 0.5) {
                            jts.randomConnection();
                        }
                        else {
                            jts.randomDisconnection();
                        }
                    }

                    jts.randomize();
                }

                hideWindow(g, loci, ll);
            }
	    reportLoop(low, loopCount, ld);
	    loopCount++;
        }
        System.err.println();

        Monitor.show("Main loop done  =  ");
    }

    public void hideWindow(NetworkMask<Variable, Object> g, Variable[] loci, int low) {
        int high = low + windowwidth;
        if (high > loci.length) {
            high = loci.length;
        }

        int ll = low - 2 * maxlink;
        if (ll < 0) {
            ll = 0;
        }

        int hh = high + 2 * maxlink;
        if (hh > loci.length) {
            hh = loci.length;
        }

        for (int i = ll; i < hh; i++) {
            g.hide(loci[i]);
        }
    }

    public Set<Variable> showWindow(NetworkMask<Variable, Object> g, Variable[] loci, int low, Set<Variable> order) {
        int high = low + windowwidth;
        if (high > loci.length) {
            high = loci.length;
        }

        Set<Variable> wind = new LinkedHashSet<Variable>();
        for (int i = (low < 0 ? 0 : low); i < high; i++) {
            wind.add(loci[i]);
        }

        int ll = low - 2 * maxlink;
        if (ll < 0) {
            ll = 0;
        }

        int hh = high + 2 * maxlink;
        if (hh > loci.length) {
            hh = loci.length;
        }

        for (int i = ll; i < hh; i++) {
            g.show(loci[i]);
            order.add(loci[i]);
        }

        return wind;
    }

    public boolean isVisualize() {
        return visualize;
    }

    public void setVisualize(boolean visualize) {
        this.visualize = visualize;
    }

    public boolean isProper() {
        return proper;
    }

    public void setProper(boolean proper) {
        this.proper = proper;
    }

    public int getMaxlink() {
        return maxlink;
    }

    public void setMaxlink(int maxlink) {
        this.maxlink = maxlink;
    }

    public int getWindowwidth() {
        return windowwidth;
    }

    public void setWindowwidth(int windowwidth) {
        this.windowwidth = windowwidth;
    }

    public int getGibbsperwindow() {
        return gibbsperwindow;
    }

    public void setGibbsperwindow(int gibbsperwindow) {
        this.gibbsperwindow = gibbsperwindow;
    }

    public int getOptgibbsperwindow() {
        return optgibbsperwindow;
    }

    public void setOptgibbsperwindow(int optgibbsperwindow) {
        this.optgibbsperwindow = optgibbsperwindow;
    }

    public int getMetropergibbsperlocus() {
        return metropergibbsperlocus;
    }

    public void setMetropergibbsperlocus(int metropergibbsperlocus) {
        this.metropergibbsperlocus = metropergibbsperlocus;
    }

    public double getPenalty() {
        return penalty;
    }

    public void setPenalty(double penalty) {
        this.penalty = penalty;
    }

    public double getEprob() {
        return eprob;
    }

    public void setEprob(double eprob) {
        this.eprob = eprob;
    }

    public PrintStream getOutStream() {
        return outStream;
    }

    public void setOutStream(PrintStream outStream) {
        this.outStream = outStream;
    }

    public LinkageParameterData getPar() {
        return par;
    }

    public void setPar(LinkageParameterData par) {
        this.par = par;
    }

    public LinkagePedigreeData getPed() {
        return ped;
    }

    public void setPed(LinkagePedigreeData ped) {
        this.ped = ped;
    }

    public GeneticDataSource getGeneticDataSource() {
        return geneticDataSource;
    }

    // public void setGeneticDataSource(GeneticDataSource geneticDataSource) {
    //     this.geneticDataSource = geneticDataSource;
    // }

    private BufferedReader openBufferedReader(File file) {
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(file));
        }
        catch (FileNotFoundException fnf) {
            fnf.printStackTrace();
            System.exit(2);
        }
        return br;
    }

    private void reduceChromosome(File defn) {
        //TODO: undo this cut/paste
        TreeSet<Integer> retainedMarkers = new TreeSet<>();
        int lastMarkerIndex = getGeneticDataSource().nLoci() - 1;
        for (SharingRun srun : getReducingSegmentList(defn)) {
            // We want to include the bounding markers, if available
            int upperBound = (srun.getLastmarker() < lastMarkerIndex ? srun.getLastmarker()+1 : srun.getFirstmarker());
            for (int i = (srun.getFirstmarker() == 0 ? srun.getFirstmarker() : srun.getFirstmarker() - 1) ;
                 i < upperBound;
                 i++) {
                retainedMarkers.add(i);
            }
        }
        // TODO: use set to array op?
        int[] retainedIndices = new int[retainedMarkers.size()];
        //logger.error("{}: retained {} of {} markers", getTag(), retainedMarkers.size(), lastMarkerIndex);
        int reti = 0;
        for (Integer origIndex  : retainedMarkers) {
            retainedIndices[reti++] = origIndex;
        }
        LinkageDataSet reducedLinkageDataSet = new LinkageDataSet(geneticDataSource.raw(), retainedIndices);
        geneticDataSource = new LinkageInterface(reducedLinkageDataSet);
    }

    private List<SharingRun> getReducingSegmentList(File segsFile) {
        //TODO: more cut/past
    	ArrayList<SharingRun> seglist = new ArrayList<>();
    	try (BufferedReader segReader = new BufferedReader( new FileReader(segsFile))) {
            String line = segReader.readLine();
            while ( line != null ) {
	      String markers[] = line.split("\\s+");
                SharingRun segdef = new SharingRun();
                segdef.setFirstmarker(Integer.parseInt(markers[1]));
                segdef.setLastmarker(Integer.parseInt(markers[2]));
                segdef.setEndbase(Integer.parseInt(markers[5]));
                segdef.setStartbase(Integer.parseInt(markers[4]));
		segdef.setEventsLess(Long.parseLong(markers[6]));
		segdef.setEventsEqual(Long.parseLong(markers[7]));
                segdef.setEventsGreater(Long.parseLong(markers[8]));
                seglist.add(segdef);
                line = segReader.readLine();
            }
    	}
    	catch(IOException ioe) {
            throw new RuntimeException("trouble reading segment file", ioe);
    	}
    	// if (seglist.size() > 1) {
    	// 	throw new UnsupportedOperationException("We currently accept only a single segment definition: " + segsFile.getCanonicalPath());
    	// }
    	return seglist;
    }

    private void EMFitLDModelForGraph(LDModel ld, Network<Variable,Object> g, GeneticDataSource x, int nits, double eprob, Random rand)
    {
        boolean[] both = {true, false};

        ld.initialize();
        CompleteHaplotypes haps = new CompleteHaplotypes(x,ld,eprob,rand);
        haps.simulate();

        MultinomialMLEScorer scorer= new MultinomialMLEScorer(ld.getLocusVariables(),haps,0,rand);

        JTree<Variable> jt = new JTree<Variable>(g,rand);

        for (boolean random : both)
            {
                for (int i=0; i<nits; i++)
                    {
                        // M step.

                        scorer.clear();
                        ld.clear();
                        for (Function f : scorer.fitModel(jt).getFunctions())
                            ld.add(f);

                        // E step.

                        haps.setModel(ld);
                        haps.update(random);

                        System.err.print("-");
                    }
			
                System.err.println();
            }

        scorer.clear();
        ld.clear();
        for (Function f : scorer.fitModel(jt).getFunctions())
            ld.add(f);
    }

  private void reportLoop(int position, int loopn, LDModel ldmodel) {
    String report = String.format("%d: %d,%d,%s\n", System.currentTimeMillis(), loopn, position, par.getLocus(position < 0 ? 0 : position).locName());
    System.err.print(report);
  }
}
