package edu.utah.camplab.app;

import java.io.IOException;
import java.util.Collection;
import java.util.TreeSet;
import java.util.UUID;

import edu.utah.camplab.sgs.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SGSPValue {
    final static Logger logger = LoggerFactory.getLogger(SGSPValue.class);

    // Probably should be in LinkageFormatter...
    public static final int PEDFILE            = 0;
    public static final int PARFILE            = 1;

    public static final String SPEC_CREDS = "credentials";
    public static final String SPEC_DBNAME = "dbname";

    private final UUID      tag;

    public static void main(String[] args) {
        try {
            //TODO: separate constructor from actions
            new SGSPValue(args);
        }
        catch (Exception e) {
            System.out.println("Whoa! " + e.getMessage());
            e.printStackTrace();
        }
    }

    public SGSPValue(String[] args) {
        tag = UUID.randomUUID();
        
        //find out which analysis to run
        int operation = -1;
        try {
            operation = extractOperation(args, AbstractSGSRun.SPEC_OP);
        }
        catch (NumberFormatException nfe) {
            System.out.println("Could not parse the arguments for program/operation:\n\t" + args);
            logger.error("Could not parse the arguments for program/operation: {}", args.toString());
        }
        
        SGSRunInterface delegate = null;
        
        try {
            switch (operation) {
            case SGSChase.ANALYSIS_PVALUE:
                delegate = new SGSChase();
                break;
            case SGSChase.ANALYSIS_THRESHOLD:
                delegate = new SGSSigThreshold();
                break;
            case SGSChase.ANALYSIS_MULTIASSESS:
                delegate = new SGSMultiAssess();
                break;
            case SGSChase.ANALYSIS_CASECONTROL:
                delegate = new SGSCaseVsControl();
                break;
            default:
            	delegate = new SGSChase();
                throw new RuntimeException("Which operation?\n");
            }
            delegate.init(args);
            delegate.run();
        }
        catch (Exception ex) {
            System.out.println(ex.getMessage());
            delegate.formatHelp();
            try {
                delegate.printHelp();
                ex.printStackTrace();
                System.exit(2);
            }
            catch (IOException ioe) {
                logger.error("{}: No help in sight", tag, ioe);
            }
            System.exit(3);
        }
    }

    private int extractOperation(String[] args, Collection<String> flags) throws NumberFormatException {
        String option = null;
        int bestmatchop = 0;
        if (args.length == 1) {
            //Let's just assume help is needed
            return 0;
        }
        for (int i = 0; i < args.length; i++) {
            String currentArg = args[i];
            // For OPERATION, aka PROGRAM, "op" or "pr" will suffice.  A change in the total options list may affect this
            if (currentArg.startsWith("--op") || currentArg.startsWith("--prog")){ 
                String[] parts = currentArg.substring(2).split("=");
                // Operation specified as next argument
                if (parts.length != 2) {
                    parts = new String[] {currentArg.substring(2), args[i+1]};
                }
                try {
                    return Integer.parseInt(parts[1]);
                }
                catch (Exception exc) {
                    option = currentArg;
                    bestmatchop = 0;
                    break;
                }
            }
            else if (currentArg.equals("-o")) {
                try {
                    return Integer.parseInt(args[i+1]);
                }
                catch (Exception exc) {
                    option = "-o";
                    bestmatchop = 0;
                    break;
                }
            }
        }
        if (option != null) {
            throw new RuntimeException("Missing required argument for " + option);
        }
        return bestmatchop;
    }
}
