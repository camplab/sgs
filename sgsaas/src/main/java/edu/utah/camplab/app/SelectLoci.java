package edu.utah.camplab.app;

import jpsgcs.linkage.LinkageDataSet;
import java.io.PrintStream;
import java.util.Vector;

/**
 * This program selects a subset of loci from LINKAGE parameter and pedigree
 * files.
 * 
 * <ul>
 * Usage : <b> java SelectLoci input.par input.ped output.par output.ped l1 [l2]
 * ...</b> </li>
 * </ul>
 * where
 * <ul>
 * <li><b> input.par </b> is the original LINKAGE parameter file</li>
 * <li><b> input.ped </b> is the original LINKAGE pedigree file</li>
 * <li><b> output.par </b> is the file where the selected loci will be put.</li>
 * <li><b> output.ped </b> is the file where the matching pedigree data will be
 * put.</li>
 * <li><b> l1 [l2] ... </b> is a list of at least one locus index. The indexes
 * match the order of the loci in the input parameter file. The first locus has
 * index 0, the last of n has index n-1.</li>
 * </ul>
 */

public class SelectLoci {
    public static void main(String[] args)
    {
        try
            {
                if (args.length < 5)
                    System.err.println("Usage: java SelectLoci input.par input.ped output.par output.ped l1 [l2] ...");

                LinkageDataSet l = new LinkageDataSet(args[0],args[1]);
                PrintStream parout = new PrintStream(args[2]);
                PrintStream pedout = new PrintStream(args[3]);
                
                Vector<Integer> out = new Vector<Integer>();
                for (int i=4; i<args.length; i++)
                    {
                        if (args[i].contains("-"))
                            {
                                String[] s = args[i].split("-");
                                int a = Integer.valueOf(s[0]);
                                int b = 0;
                                if (s.length == 2)
                                    b = Integer.valueOf(s[1]).intValue();
                                else
                                    b = l.getParameterData().nLoci()-1;

                                if (b > l.getParameterData().nLoci()-1)
                                    b = l.getParameterData().nLoci()-1;

                                for (int k=a; k<=b; k++)
                                    out.add(Integer.valueOf(k));
                            }
                        else
                            out.add(Integer.valueOf(args[i]));
                    }

                int[] x = new int[out.size()];
                for (int i=0; i<x.length; i++)
                    x[i] = out.get(i).intValue();

                l = new LinkageDataSet(l,x);
                l.getParameterData().writeTo(parout);
                l.getPedigreeData().writeTo(pedout);
            }
        catch (Exception e)
            {
                System.err.println("Caught in SelectLoci.main()");
                e.printStackTrace();
            }
    }
}
