package edu.utah.camplab.app;

import jpsgcs.linkage.LinkageParameterData;
import jpsgcs.linkage.LinkagePedigreeData;
import jpsgcs.linkage.LinkageIndividual;
import jpsgcs.linkage.LinkageLocus;
import jpsgcs.util.Main;

import java.io.PrintStream;

public class DownCodeAndCount {
    public static void main(String[] args) {
        try {
            int harddowncode = 2;
            boolean markersonly = true;

            String[] bargs = Main.strip(args, "-markersonly");
            if (bargs != args) {
                markersonly = false;
                args = bargs;
            }

            switch (args.length) {
            case 1:
            case 0:
                System.err
                    .println("Usage: java pipe.DownCodeAlleles input.par input1.ped input2.ped ... [-markersonly]");
                System.exit(0);
            }

            LinkageParameterData par = new LinkageParameterData(args[0]);

            LinkagePedigreeData[] ped = new LinkagePedigreeData[args.length - 1];
            for (int i = 1; i < args.length; i++)
                ped[i - 1] = new LinkagePedigreeData(args[i], par);

            if (markersonly) {
                int nmarks = 0;
                int[] x = new int[par.nLoci()];

                for (int i = 0; i < x.length; i++) {
                    if (par.getLocus(i).getType() == LinkageLocus.NUMBERED_ALLELES) {
                        x[i] = 1;
                        nmarks++;
                    } else {
                        System.err
                            .println("Warning: Removing non marker locus at position "
                                     + i + ".");
                    }
                }

                if (nmarks < par.nLoci()) {
                    int[] y = new int[nmarks];
                    nmarks = 0;
                    for (int i = 0; i < x.length; i++)
                        if (x[i] == 1)
                            y[nmarks++] = i;

                    par = new LinkageParameterData(par, y);
                    for (int i = 0; i < ped.length; i++)
                        ped[i] = new LinkagePedigreeData(ped[i], y);
                }
            }

            int ninds = 0;
            for (int i = 0; i < ped.length; i++)
                ninds += ped[i].nIndividuals();

            LinkageIndividual[] all = new LinkageIndividual[ninds];
            ninds = 0;

            for (int i = 0; i < ped.length; i++) {
                LinkageIndividual[] indi = ped[i].getIndividuals();
                for (int j = 0; j < indi.length; j++)
                    all[ninds++] = indi[j];
            }

            for (int i = 0; i < par.nLoci(); i++) {
                par.getLocus(i).downCode(all, i, harddowncode);
                par.getLocus(i).countAlleleFreqs(all, i);
            }

            PrintStream s = new PrintStream(args[0] + ".down");
            par.writeTo(s);
            s.flush();
            s.close();

            for (int i = 0; i < ped.length; i++) {
                s = new PrintStream(args[i + 1] + ".down");
                ped[i].writeTo(s);
                s.flush();
                s.close();
            }
        } catch (Exception e) {
            System.err.println("Caught in pipe.DownCodeAndCount:main()");
            e.printStackTrace();
        }
    }
}
