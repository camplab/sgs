package edu.utah.camplab.app;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.UnknownHostException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.zip.GZIPInputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import edu.utah.camplab.jx.AbstractPayload;
import edu.utah.camplab.jx.SegmentBlock;
import edu.utah.camplab.sgs.AbstractSGSRun;
import joptsimple.BuiltinHelpFormatter;
import joptsimple.OptionParser;
import joptsimple.OptionSet;
import joptsimple.OptionSpec;

public class LoadCrashpadJson {

  final static Logger     logger   = LoggerFactory.getLogger(LoadCrashpadJson.class);

  public static final String SPEC_FILE   = "file";
  public static final String SPEC_ACHOST = "accumulator";
  public static final String SPEC_FOF    = "using-fof";
  public static final String SPEC_DELAY  = "delay";
  public static final String SPEC_ROLE  = "role";
  public static final String SPEC_PI  = "pi";

  private List<File> jsonFiles   = null;
  private String     accumulator = null;
  private String     dbname = null;
  private String     dbrole = null;
  private int        msMuxDelayMilli  = 3600000;


  public static void main(String[] args) {
    LoadCrashpadJson loader = new LoadCrashpadJson();
    try {
      loader.handleArgs(args);
      loader.load();
    }
    catch (UnknownHostException uhe) {
      logger.error("bad database connection information: {}", uhe.getMessage(), uhe);
    }
    catch(IOException ioe) {
      logger.error("file handling issues: {}", ioe.getMessage(), ioe);
    }
    catch(SQLException sqle) {
      logger.error("query or connection failure: {}", sqle.getMessage(), sqle);
    }
  }

  public void handleArgs(String[] cli) throws SQLException,IOException {
    OptionParser op = new OptionParser();
    op.formatHelpWith(new BuiltinHelpFormatter(120, 4));

    OptionSpec<File>    jsonSpec  = op.accepts(LoadCrashpadJson.SPEC_FILE, "file (or file of files) from db save failure").withRequiredArg().ofType(File.class).required();
    OptionSpec<String>  accuSpec  = op.accepts(LoadCrashpadJson.SPEC_ACHOST, "host:port for the accumulator").withRequiredArg().ofType(String.class).required();
    OptionSpec<String>  piSpec    = op.accepts(LoadCrashpadJson.SPEC_PI, "Principal investigator cum dbname").withRequiredArg().ofType(String.class).required();
    OptionSpec<String>  roleSpec  = op.accepts(LoadCrashpadJson.SPEC_ROLE, "user cum project").withRequiredArg().ofType(String.class).required();
    OptionSpec<Void>    fofSpec   = op.accepts(LoadCrashpadJson.SPEC_FOF, "interpret file as a file of filenames");
    OptionSpec<Integer> delaySpec = op.accepts(LoadCrashpadJson.SPEC_DELAY, "inter-file delay, seconds").withRequiredArg().ofType(Integer.class).defaultsTo(3600);
    OptionSpec<Void>    helpSpec  = op.accepts(PedParLoader.SPEC_HELP);

    OptionSet opSet =  null;
    try {
      opSet = op.parse(cli);
      if (opSet.has(helpSpec)) {
	op.printHelpOn(System.out);
	System.exit(0);
      }
    }
    catch(Exception ex) {
      ex.printStackTrace();
      op.printHelpOn(System.out);
      System.exit(2);
    }

    jsonFiles = new ArrayList<File>();
    File jsonFile = opSet.valueOf(jsonSpec);
    if (opSet.has(fofSpec)) {
      BufferedReader r = new BufferedReader(new FileReader(jsonFile));
      String file;
      while ((file = r.readLine()) != null && file.length() > 0) {
	if (file.startsWith("#")) {
	  continue;
	}
	jsonFiles.add(new File(file));
      }
      r.close();
    }
    else {
      jsonFiles.add(jsonFile);
    }
    accumulator = opSet.valueOf(accuSpec);
    dbname = opSet.valueOf(piSpec);
    dbrole = opSet.valueOf(roleSpec);

    if (opSet.has(delaySpec)) {
      msMuxDelayMilli = opSet.valueOf(delaySpec) * 1000;
    }
  }
    
  public void load() throws IOException {
    long sleep = 1000;
    boolean isList = jsonFiles.size() > 1;
    for (File jsonFile : jsonFiles) {
      for (int att = 0; att < 10; att++) {
	try {
	  Thread.sleep(sleep);
	} catch (InterruptedException ex) {
	  logger.error("Failed to sleep (attempt " + att + "/10): {}", ex.getMessage(), ex);
	} 
      }
      if (isList) {
	System.out.println(String.format("loading crash file " + jsonFile.getCanonicalPath()));
      }
      sleep = load(jsonFile);
    }
  }

  public long load(File jsonf) {
    long sleep = 1;
    logger.info("Crashpad import commencing.");

    ObjectMapper jsonMapper = JsonMapper.builder().addModule(new JavaTimeModule()).build();
    jsonMapper.setSerializationInclusion(Include.NON_NULL);
    try {
      GZIPInputStream zstream = new GZIPInputStream(new FileInputStream(jsonf));
      BufferedReader reader = new BufferedReader(new InputStreamReader(zstream));

      AbstractPayload crashPayload = jsonMapper.readValue(reader, AbstractPayload.class);
      MinimalSGSRun minRun = new MinimalSGSRun(accumulator, crashPayload, jsonf);
      minRun.setDbName(dbname);
      minRun.setProjectName(dbrole);
      minRun.run();

      if (crashPayload.getProcessBlock().getProcess().getWhat().toLowerCase().contains(AbstractPayload.PAYLOADTYPE_MULTIASSESS)) {
	sleep = msMuxDelayMilli; // defaulted to 1 hour
      }
      else {// PayloadFromMux
	sleep = 1000; // 1 second
      }
      logger.debug("Loaded crashpad json: {}", jsonf.getCanonicalPath());
      reader.close();
      String jsonPath = jsonf.getCanonicalPath();
      Files.move(FileSystems.getDefault().getPath(jsonPath), FileSystems.getDefault().getPath(jsonPath+".processed"), StandardCopyOption.REPLACE_EXISTING);
      logger.info("Marked crashpad json as processed: {}", jsonPath);
    }
    catch (NumberFormatException nfe) {
      logger.error("bad accumulator: {}", nfe.getMessage(), nfe);
    }
    catch(IOException ioe) {
      logger.error("file handling issues: {}", ioe.getMessage(), ioe);
    }
    return sleep;
  }
    
  private class MinimalSGSRun extends AbstractSGSRun {
    private AbstractPayload crashPayload;
    private File sourceJson;
    public MinimalSGSRun(String accumulator, AbstractPayload payload, File inputJson) {
      super(accumulator);
      sourceJson = inputJson;
      crashPayload = payload;
    }

    @Override
    public void run() {
      shipPayload(crashPayload);
    }

    @Override
    public SegmentBlock buildSegmentBlock() {
      return null;
    }

    @Override
    public void init(String[] args) throws Exception {
      ;
    }

    @Override public UUID getPedigreeId() { return null;}
    @Override public void setPedigreeId(UUID id) { }
    @Override public UUID getMarkersetId() {return null;}
    @Override public void setMarkersetId(UUID id) {}
    
    @Override
    public File getJsonDir() {
      return sourceJson.getParentFile();
    }

    public void buildLinkageDataSet() {
      ;
    }
  }
}
