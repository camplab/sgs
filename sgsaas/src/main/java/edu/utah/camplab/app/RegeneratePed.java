package edu.utah.camplab.app;

import static edu.utah.camplab.db.generated.default_schema.Tables.GENOTYPE;
import static edu.utah.camplab.db.generated.base.Tables.MARKER;
import static edu.utah.camplab.db.generated.base.Tables.MARKERSET;
import static edu.utah.camplab.db.generated.base.Tables.MARKERSET_MEMBER;
import static edu.utah.camplab.db.generated.base.Tables.PEOPLE;
import static edu.utah.camplab.db.generated.base.Tables.PEOPLE_MEMBER;
import static edu.utah.camplab.db.generated.base.Tables.PERSON;

import static org.jooq.impl.DSL.inline;

import java.io.BufferedReader;
import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.ArrayList;
import java.util.UUID;

import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;
import org.postgresql.copy.CopyManager;
import org.postgresql.core.BaseConnection;

import edu.utah.camplab.db.generated.default_schema.tables.records.GenotypeRecord;
import edu.utah.camplab.db.generated.base.tables.records.MarkerRecord;
import edu.utah.camplab.db.generated.base.tables.records.MarkersetRecord;
import edu.utah.camplab.db.generated.base.tables.records.MarkersetMemberRecord;
import edu.utah.camplab.db.generated.base.tables.records.PeopleRecord;
import edu.utah.camplab.db.generated.base.tables.records.PeopleMemberRecord;
import edu.utah.camplab.db.generated.base.tables.records.PersonRecord;
import edu.utah.camplab.jx.SGSFullContext;

import jpsgcs.linkage.LinkageFormatter;
import jpsgcs.linkage.LinkageId;
import jpsgcs.linkage.LinkageIndividual;
import jpsgcs.linkage.LinkageLocus;
import jpsgcs.linkage.LinkageParameterData;
import jpsgcs.linkage.LinkagePedigreeData;
import jpsgcs.linkage.LinkagePhenotype;
import jpsgcs.linkage.NumberedAllelePhenotype;

public class RegeneratePed {

    public static final void main(String[] args) throws SQLException {
        String pedName = args[0];
        String markersetName = args[1];
        Connection dbconn = DriverManager.getConnection(args[0],args[1],args[2]);
        DSLContext ctx = SGSFullContext.DSLContextFactory(dbconn);
        Map<UUID,PersonRecord> personMap = new HashMap<>(); 
        personMap.putAll(ctx.select(PERSON.fields())
                         .from(PERSON.join(PEOPLE_MEMBER)
                               .on(PERSON.ID.eq(PEOPLE_MEMBER.PERSON_ID))
                               .join(PEOPLE).on(PEOPLE_MEMBER.PEOPLE_ID.eq(PEOPLE.ID)))
                         .where(PEOPLE.NAME.eq(pedName))
                         .groupBy(PERSON.NAME, PERSON.ID)
                         .fetchMap(PERSON.ID , r-> r.into(PERSON)));
        System.err.println(String.format("Found %d people for ped  = %s", personMap.size(), pedName));
        MarkersetRecord markersetRec = ctx.selectFrom(MARKERSET).where(MARKERSET.NAME.eq(markersetName)).fetchOne();
        List<MarkerRecord> markerList = new ArrayList<>();
        markerList = ctx.select(MARKER.fields())
            .from(MARKERSET
                  .join(MARKERSET_MEMBER).on(MARKERSET.ID.eq(MARKERSET_MEMBER.MARKERSET_ID))
                  .join(MARKER).on(MARKERSET_MEMBER.MEMBER_ID.eq(MARKER.ID)))
            .where(MARKERSET.NAME.eq(markersetName))
            .orderBy(MARKERSET_MEMBER.ORDINAL)
            .fetch(r->r.into(MARKER));
        int markerCount = markerList.size();
        StringBuilder zeroBuilder = new StringBuilder();
        for (int i = 0; i < markerCount; i++) {
            zeroBuilder.append(" 0 0");
        }

        System.err.println(String.format("Found %d markers for set = %s", markerCount, markersetName));
        // For writing pedfiles we need only the number of markers!
        // Get the genotypes
        Map<UUID, GenotypeRecord> gtMap = ctx.select(GENOTYPE.fields())
            .from(PEOPLE
                  .join(PEOPLE_MEMBER).on(PEOPLE.ID.eq(PEOPLE_MEMBER.PEOPLE_ID))
                  .join(GENOTYPE).on(PEOPLE_MEMBER.PERSON_ID.eq(GENOTYPE.PERSON_ID)))
            .where(PEOPLE.NAME.eq(pedName).and(GENOTYPE.MARKERSET_ID.eq(markersetRec.getId())))
            .fetchMap(GENOTYPE.PERSON_ID, r -> r.into(GENOTYPE));
        System.err.println(String.format("%d people have genotypes", gtMap.size()));

        for (Map.Entry<UUID, PersonRecord> personME : personMap.entrySet()) {
            System.gc();
            UUID personId = personME.getKey();
            PersonRecord personRec = personME.getValue();
            GenotypeRecord personGt = gtMap.get(personId);
            PersonRecord paRec = personMap.get(personRec.getPa()); 
            PersonRecord maRec = personMap.get(personRec.getMa()); 
            StringBuilder builder = new StringBuilder(zeroBuilder.length() + 50);
            builder.append(String.format("%s %10s %10s %10s 0 0 0 %d %d",
                                         pedName,
                                         personRec.getName(),
                                         paRec == null ? "0" : paRec.getName(),
                                         maRec == null ? "0" : maRec.getName(),
                                         personRec.getGender().equals("m") ? 1 : 2,
                                         personGt == null ? 0 : 1));
            if (personGt == null) {
                builder.append(zeroBuilder.toString());
            }
            else {
                for (int i = 0; i < personGt.getCalls().length; i++ ) {
                    byte c = personGt.getCalls()[i];
                    builder.append(" " + c);                         
                }
            }
            System.out.println(builder.toString());
        }
    }
}
