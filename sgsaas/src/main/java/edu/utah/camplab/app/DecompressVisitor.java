package edu.utah.camplab.tools;

import static edu.utah.camplab.db.generated.base.Tables.PERSON;
import static edu.utah.camplab.db.generated.default_schema.Tables.SEGMENT;
import static edu.utah.camplab.db.generated.default_schema.Tables.PROBANDSET;
import static edu.utah.camplab.db.generated.default_schema.Tables.PROBANDSET_GROUP_MEMBER;
import static org.jooq.impl.DSL.*;

import edu.utah.camplab.db.generated.default_schema.tables.Probandset;
import org.jooq.util.postgres.PostgresDSL;
import org.jooq.DSLContext;

import java.io.IOException;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.Files;
import java.nio.file.FileVisitResult;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;

import edu.utah.camplab.db.crafted.ProcessExtended;
import edu.utah.camplab.db.generated.default_schema.tables.records.*;
import edu.utah.camplab.db.generated.base.tables.records.*;



public class DecompressVisitor extends SimpleFileVisitor<Path> {

    private DSLContext tranny;
    private ProcessExtended processRec = null;
    private PeopleRecord peopleRec = null;
    private BufferedReader reader = null;
    private boolean isSubdir = false;

    private Pattern CHROMPAT = null;
    private MarkersetRecord markersetRec;
    private SegmentsetRecord segmentsetRec;
    private ProbandsetGroupRecord probandsetGroupRec;
    
    private int minSegmentSize;
    private int segmentSetIndex = 0;
    private String projectName;

    public DecompressVisitor(DSLContext dbctx, ProcessExtended proc, MarkersetRecord msr, SegmentsetRecord ssr, ProbandsetGroupRecord psr, String prj, String filePattern, int minseg, PeopleRecord peeps) {
        tranny = dbctx;
        minSegmentSize = minseg;
        markersetRec = msr;
        segmentsetRec = ssr;
        probandsetGroupRec = psr;
        projectName = prj;
        processRec = proc;
        peopleRec = peeps;
        CHROMPAT = Pattern.compile(filePattern);
    }

    @Override
    public FileVisitResult postVisitDirectory(Path dir, IOException exc) {
        return FileVisitResult.CONTINUE;
    }
    
    @Override
    public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) {
        if (! isSubdir)
            return FileVisitResult.CONTINUE;
        else {
            isSubdir = true;
            return FileVisitResult.SKIP_SUBTREE;
        }
    }

    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
    	if (attrs.isDirectory()) {
            return FileVisitResult.CONTINUE;
    	}

        // TODO check this assumption in compressLand
        if ( ! file.toString().contains(".pval")) {
            return FileVisitResult.CONTINUE;
    	}

        Matcher matcher = CHROMPAT.matcher(file.toString());
        if (! matcher.find() ) {
            return FileVisitResult.CONTINUE;
        }

        //TODO: where to put these "arg type" strings
        processRec.addArg("pval file", file.toUri().toString());
        // or use magic number...?
        if (file.toString().endsWith(".gz")) {
            GZIPInputStream zstream = new GZIPInputStream(new FileInputStream(file.toString()));
            InputStreamReader rstream = new InputStreamReader(zstream);
            reader = new BufferedReader(rstream);
        }
        else {
            reader = Files.newBufferedReader(file);
        }

        ArrayList<SegmentRecord> newSegments = new ArrayList<>();
        try {
            parseFile(newSegments);
        }
        catch (UnsupportedOperationException uoe) {
            throw new RuntimeException(file.toString(), uoe);
        }
        stowSegments(newSegments);
        
        return FileVisitResult.CONTINUE;
    }

    private void parseFile(ArrayList<SegmentRecord> segList) throws IOException {
         
        String dataline = null;
        dataline = reader.readLine();
        ProbandsetRecord psetRec = null;
        //MarkersetRecord chromosomeMarkerset = null;
        while (dataline.startsWith("#")) {
            String lowline = dataline.toLowerCase();
            if (lowline.contains("proband")) {
                psetRec = findOrStowProbandset(dataline);
            }
            dataline = reader.readLine();
        }
        
        // or use dropEval...
        while (dataline != null) {
            String[] parts = dataline.split("\\s");
            if (parts.length != 9) {
                throw new UnsupportedOperationException("Wrong format: " + dataline);
            }
            int segSize = Integer.parseInt(parts[3]);
            if (segSize >= minSegmentSize) {
                SegmentRecord s = tranny.newRecord(SEGMENT);
                s.setId(UUID.randomUUID());
                s.setProbandsetId(psetRec.getId());
                s.setMarkersetId(markersetRec.getId()); //TODO: focus on set
                s.setChrom(Integer.parseInt(parts[0]));
                s.setFirstmarker(Integer.parseInt(parts[1]));
                s.setLastmarker(Integer.parseInt(parts[2]));
                s.setStartbase(Integer.parseInt(parts[4]));
                s.setEndbase(Integer.parseInt(parts[5]));
                s.setEventsLess(Long.parseLong(parts[6]));
                s.setEventsEqual(Long.parseLong(parts[7]));
                s.setEventsGreater(Long.parseLong(parts[8]));
                segList.add(s);
            }
            dataline = reader.readLine();
        }
    }

    private ProbandsetRecord findOrStowProbandset(String probandLine) {
        ProbandsetRecord pRec = null;
        String[] parts = probandLine.split("\\s");
        String[] namelist = parts[parts.length-1].split(",");
        
        pRec = tranny.selectFrom(PROBANDSET)
            .where(PROBANDSET.PROBANDS.equal(PostgresDSL.array(select(PERSON.ID)
                                                               .from(PERSON)
                                                               .where(PERSON.NAME.in(namelist))
                                                               .orderBy(PERSON.ID))))
            .fetchOne();

        // TODO: undo the postgres specific array trick. Maybe.
        if (pRec == null) {
            UUID newsetId = UUID.randomUUID();
            tranny.insertInto(PROBANDSET)
                .set(PROBANDSET.ID, newsetId)
                .set(PROBANDSET.NAME, listToString(namelist))
                .set(PROBANDSET.PROBANDS,
                     PostgresDSL.array(select(PERSON.ID).from(PERSON).where(PERSON.NAME.in(namelist)).orderBy(PERSON.ID)))
                .set(PROBANDSET.PEOPLE_ID, peopleRec.getId())
                .execute();
            // Not sure this is necessary, sure ain't fastest
            pRec = tranny.selectFrom(PROBANDSET).where(PROBANDSET.ID.equal(newsetId)).fetchOne();
            extendProbandsetGroup(pRec);
        }
        return pRec;
    }

    private void stowSegments(ArrayList<SegmentRecord> segs) {
        // This used to make the segmentset_members, can go away
        tranny.batchStore(segs).execute();
        System.out.println("wrote " + segs.size() + " segments");
    }

    private void extendProbandsetGroup(ProbandsetRecord pbsRec) {
        //TODO: this should get people_id
        ProbandsetGroupMemberRecord pgRec = tranny.newRecord(PROBANDSET_GROUP_MEMBER);
        //pgRec.setId(UUID.randomUUID());
        pgRec.setGroupId(probandsetGroupRec.getId());
        pgRec.setMemberId(pbsRec.getId());
        pgRec.insert();
        System.out.println("added probandset to group " + probandsetGroupRec.getId().toString());
    }

    private String listToString(String[] slist) {
        String comma = "";
        StringBuffer buf = new StringBuffer();
        for (String s : slist) {
            buf.append(comma+s);
            comma = ",";
        }
        return buf.toString();
    }
}
