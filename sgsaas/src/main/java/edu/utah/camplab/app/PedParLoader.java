package edu.utah.camplab.app;
// REMEMBER: PedParLoader.SPEC_ALLELE + ") may be collected from the 'downcode.sh' stage,
// typically in DownCodeAlleles.log. These may also be generated retrospectively" + " by
// running DownCodeAlleles on the CHECKFORMATTED.ped and .par files per chromosome. At
// this time there is no standalone tool to import those after project initialization. SQL
// is your friend." + "\nNote that the --" +

import com.jonoler.longpowerset.LongPowerSet;
import com.jonoler.longpowerset.LongSet;
import edu.utah.camplab.db.crafted.ProbandsetExtended;
import edu.utah.camplab.db.crafted.ProcessExtended;
import edu.utah.camplab.db.generated.base.tables.records.*;
import edu.utah.camplab.db.generated.default_schema.tables.*;
import edu.utah.camplab.db.generated.default_schema.tables.records.*;
import edu.utah.camplab.jps.SGSFormatter;
import edu.utah.camplab.jps.SGSLDModel;
import edu.utah.camplab.jps.SGSParameterData;
import edu.utah.camplab.sgs.AbstractSGSRun;
import edu.utah.camplab.tools.*;
import edu.utah.camplab.jx.SGSFullContext;

import java.io.*;
import java.io.File;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.*;
import java.util.*;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.TimeUnit;

import joptsimple.BuiltinHelpFormatter;
import joptsimple.OptionParser;
import joptsimple.OptionSet;
import joptsimple.OptionSpec;

import jpsgcs.linkage.*;

import static org.jooq.impl.DSL.*;
import org.jooq.*;
import org.jooq.conf.Settings;
import org.jooq.impl.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static edu.utah.camplab.db.generated.base.Tables.*;
import static edu.utah.camplab.db.generated.default_schema.Tables.*;

public class PedParLoader {

  private class FOFInput {
    public String pedFile = null;
    public String parFile = null;
    public String alleleFile = null;
    public String popName = null;
    public Integer chromosome = null;

    public FOFInput(String[] lineParts) {
      pedFile = lineParts[FOF_COL_PEDFILE];
      popName = lineParts[FOF_COL_PEDNAME];
      parFile = lineParts[FOF_COL_PARFILE];
      alleleFile = lineParts[FOF_COL_ALLFILE];
      chromosome = Integer.parseInt(lineParts[FOF_COL_CHRMNUM]);
    }
  }

  final static Logger logger = LoggerFactory.getLogger(PedParLoader.class);

  public static final Integer FOF_COLUMNS = 5;
  public static final Integer FOF_COL_PEDFILE = 0;
  public static final Integer FOF_COL_PEDNAME = 1;
  public static final Integer FOF_COL_PARFILE = 2;
  public static final Integer FOF_COL_ALLFILE = 3;
  public static final Integer FOF_COL_CHRMNUM = 4;

  // required
  public static final String SPEC_GENOME = "genome";
  public static final String SPEC_PEDPARFOF = "pedparfof";
  public static final String SPEC_DBHOST = "dbaddr";
  public static final String SPEC_DBOWN = "investigator";
  // options
  public static final String SPEC_DOSUBS = "no-subsets";
  public static final String SPEC_SUBSFILE = "probandsets";
  public static final String SPEC_TRIM = "trimfile";
  public static final String SPEC_MEIOSES = "no-meioses-count";
  public static final String SPEC_BUILD = "build";
  public static final String SPEC_BATCH = "batch-size";
  public static final String SPEC_LOGFILE = "log";
  public static final String SPEC_JOOQ = "jooqsql";
  public static final String SPEC_FS = "filesystem";
  public static final String SPEC_HELP = "help";

  private DSLContext	ctx	       = null;
  private File		fofFile	       = null;
  //private File explicitProbandSets   = null;
  private String	genomeName     = null;
  private int		genomeBuild;
  private String	projectName    = null;
  private boolean	noSubsets      = false;
  private boolean	noMeiosesCount = false;
  private boolean	isS3	       = true;
  private Integer	pbsBlockSize   = null;
  private String	dbUrl	       = null;

  private ProcessExtended		processEx    = null;
  private GenomeMarkersetRecord		genomeRec    = null;
  //private PeopleRecord              peopleRec      = null;
  //private ProbandsetGroupRecord probandsetGroupRec = null;
  private DefinervalueRecord		defvalRec    = null;
  private ConnectingThreadPoolExecutor	pbsExecutor;

  private Map<PeopleRecord,	Set<PersonRecord>>      peopleProbandsMap  = new HashMap<>();
  private Map<PeopleRecord,	LinkagePedigreeData>	peopleLinkageMap   = new HashMap<>();
  private Map<String,		Set<String>>            trimProbandMap	   = new HashMap<>();
  private Map<PeopleRecord,	Set<String>>            pedSupersetMap	   = new HashMap<>();
  private Map<Integer,		Map<String, String[]>>  markerAlleleMap	   = null;
  private Map<String,		PeopleRecord>           knownNamePeopleMap = new HashMap<>();
  private Map<Integer,		MarkersetRecord>        chromMarkersetMap  = null;

  private PrintStream printStream;


  private PedParLoader() {
  }

  public static void main(String[] args) {
    PedParLoader ppl = new PedParLoader();
    try {
      ppl.handleArgs(args);
      ppl.preload();
      ppl.load();
      ppl.generateProbandsets();
    } catch (UnknownHostException uhe) {
      logger.error("bad database connection information: {}", uhe.getMessage(), uhe);
    } catch (IOException ioe) {
      logger.error("file handling issues: {}", ioe.getMessage(), ioe);
    } catch (SQLException sqle) {
      logger.error("query or connection failure: {}", sqle.getMessage(), sqle);
    }
  }

  public void handleArgs(String[] cli) throws SQLException, IOException {
    OptionParser op = new OptionParser();
    op.formatHelpWith(new BuiltinHelpFormatter(120, 4));

    OptionSpec<File>	pedparSpec    = op.accepts(PedParLoader.SPEC_PEDPARFOF, "csv file: pedfile, pedname, parfile, allelefile, chromosome number").withRequiredArg().ofType(File.class).required();
    OptionSpec<String>	genomeSpec    = op.accepts(PedParLoader.SPEC_GENOME, "name for CHIP defining project markers [consider build number]").withRequiredArg().ofType(String.class).required();
    OptionSpec<Integer> buildSpec     = op.accepts(PedParLoader.SPEC_BUILD, "genome build: 37 or 38").withRequiredArg().ofType(Integer.class).defaultsTo(37);
    OptionSpec<File>	trimSpec      = op.accepts(PedParLoader.SPEC_TRIM, "file of pedigree trim files").withRequiredArg().ofType(File.class);
    OptionSpec<Void>	nosubSpec     = op.accepts(PedParLoader.SPEC_DOSUBS, "turn off ProbandSet generation");
    OptionSpec<Void>	noMeiosesSpec = op.accepts(PedParLoader.SPEC_MEIOSES, "don't calculate meioses for each ProbandSet");
    OptionSpec<String>	dbPI	      = op.accepts(PedParLoader.SPEC_DBOWN, "typically the PI surname").withRequiredArg().ofType(String.class).required();
    OptionSpec<String>	dbSpec	      = op.accepts(PedParLoader.SPEC_DBHOST, "db connect info as 'host:port'").withRequiredArg().ofType(String.class).required();
    OptionSpec<Integer> batchSpec     = op.accepts(PedParLoader.SPEC_BATCH, "size of batch in bulk load").withRequiredArg().ofType(Integer.class).defaultsTo(10000);
    OptionSpec<File>	logSpec	      = op.accepts(PedParLoader.SPEC_LOGFILE, "logfile name").withRequiredArg().ofType(File.class);
    OptionSpec<Void>    logJooqSpec   = op.accepts(PedParLoader.SPEC_JOOQ, "log jooq sql");
    OptionSpec<String>  fsSpec        = op.accepts(PedParLoader.SPEC_FS, "filesystem (s3, disc").withRequiredArg().ofType(String.class).defaultsTo("S3");
    OptionSpec<Void>	helpSpec      = op.accepts(PedParLoader.SPEC_HELP);
    //OptionSpec<File>    subsSpec      = op.accepts(PedParLoader.SPEC_SUBSFILE,  "file of selected proband set definitions; requires " + PedParLoader.SPEC_DOSUBS).withRequiredArg().ofType(File.class);

    OptionSet opSet = null;
    try {
      opSet = op.parse(cli);
      if (opSet.has(helpSpec)) {
        SGSHelpPrinter helpPrinter = new SGSHelpPrinter(op);
        helpPrinter.printHelp();
      }
    } catch (Exception ex) {
      SGSHelpPrinter helpPrinter = new SGSHelpPrinter(op, false);
      ex.printStackTrace();
      helpPrinter.printHelp();
      System.err.println("***** Encountered processing exception during setup *****");
      System.exit(2);
    }

    genomeName = opSet.valueOf(genomeSpec);
    genomeBuild = opSet.valueOf(buildSpec);
    fofFile = opSet.valueOf(pedparSpec);
    printStream = opSet.has(logSpec) ? setPrintStream(opSet.valueOf(logSpec)) : System.out;
    pbsBlockSize = opSet.valueOf(batchSpec);
    trimProbandMap = opSet.has(trimSpec) ? buildTrimSet(opSet.valueOf(trimSpec)) : null;
    String dbcli = opSet.valueOf(dbSpec);
    noSubsets = opSet.has(nosubSpec);
    noMeiosesCount = opSet.has(noMeiosesSpec);
    isS3 = opSet.has(fsSpec) && opSet.valueOf(fsSpec).equalsIgnoreCase("s3");
    //explicitProbandSets = opSet.has(subsSpec) ? opSet.valueOf(subsSpec) : null;
    String principle = opSet.valueOf(dbPI);
    dbUrl = String.format("jdbc:postgresql://%s/%s", dbcli, principle);

    String prop = System.getProperty("upass");
    String upass = null;
    String uname = null;
    if (prop == null) {
      Map<String, String> npMap = NamePassPrompter.prompt();
      upass = npMap.get("password");
      uname = npMap.get("name");
      System.setProperty("upass", String.format("%s:%s", uname, upass));
    }
    else {
      String[] parts = prop.split(":");
      uname = parts[0];
      upass = parts[1];
    }
    Connection dbconn = DriverManager.getConnection(dbUrl, uname, upass.length() < 1 ? null : upass);
    Settings settings = new Settings();
    settings.withExecuteLogging(opSet.has(logJooqSpec));
    ctx = SGSFullContext.DSLContextFactory(dbconn, settings);
    projectName = uname;
  }

  public void preload() {
    knownNamePeopleMap = ctx.selectFrom(PEOPLE).fetchMap(PEOPLE.NAME);
    genomeRec = findOrCreateGenome(genomeName, genomeBuild);
    chromMarkersetMap = initChromMarkersetMap(genomeRec);
    printStream.println(String.format("From the get go we know\nthese markersets (for genome %s:", genomeRec.getName()));
    for (Map.Entry<Integer, MarkersetRecord> me : chromMarkersetMap.entrySet()) {
      printStream.printf("\t|%2d| %s\n", me.getKey(), me.getValue().getId());
    }
    printStream.println("\n----\nthese peoples:");
    for (Map.Entry<String, PeopleRecord> me : knownNamePeopleMap.entrySet()) {
      PeopleRecord prec = me.getValue();
      printStream.printf("\t|%s| %s\n", prec.getName(), prec.getId());
    }
  }

  public void load() throws FileNotFoundException, IOException, UnknownHostException, SQLException {
    InetAddress machine = InetAddress.getLocalHost();
    for (FOFInput fofInput : fofReader(fofFile)) {
      PeopleRecord currentPeople = knownNamePeopleMap.get(fofInput.popName);
      boolean isNewPeople = currentPeople == null;
      MarkersetRecord currentMarkerset = chromMarkersetMap.get(fofInput.chromosome);
      boolean isNewMarkerset = currentMarkerset == null;
      printStream.printf("Treating pedigree %s as %s markerset %s as %s)\n", 
			 fofInput.popName, 
			 isNewPeople ? "NEW" : "KNOWN",
			 fofInput.parFile.substring(fofInput.parFile.lastIndexOf("/")+1), 
			 isNewMarkerset ? "NEW" : "KNOWN");

      if (!isNewCombination(currentPeople, currentMarkerset, fofInput)) {
        printStream.println(String.format("Nothing to see here folks (%s, %s). Move along", fofInput.popName, fofInput.parFile));
        continue;
      }

      ctx.transaction(ltx -> {
        DSLContext localtx = DSL.using(ltx);
        SGSFormatter formatter = getFormatter(fofInput.parFile);
        formatter.setDbContext(localtx);
        processEx = new ProcessExtended(localtx, "pedpar loader", machine, UUID.randomUUID());
        processEx.addJarsUsed(AbstractSGSRun.addManifest());
        maybeStowProject(projectName);
        SGSParameterData param = new SGSParameterData(formatter); // All marker related saves done within
        MarkersetRecord readMarkerset = chromMarkersetMap.get(fofInput.chromosome);
        if (isNewMarkerset) {
          readMarkerset = maybeStowMarkerset(localtx, fofInput); // TODO: nothing "maybe" about it; get rid of db re-read
          formatter.setContextId(readMarkerset.getId());
          HashMap<String, String[]> markerAlleleMap = buildMarkerAlleleMap(fofInput.alleleFile);
          stowMarkerData(localtx, param, readMarkerset, markerAlleleMap);
          SGSLDModel ldm = new SGSLDModel(formatter, 0.0d); // Stows clique data
          chromMarkersetMap.put(readMarkerset.getChrom(), readMarkerset);
        }
        ProjectfileRecord markerFileRec = localtx.selectFrom(PROJECTFILE).where(PROJECTFILE.URI.equal(fofInput.parFile)).fetchOne();
        if (markerFileRec == null) {
          markerFileRec = stowFile(fofInput.parFile, null);
        }
        processEx.addInput(PROJECTFILE.getQualifiedName().last(), markerFileRec.getId().toString());
        loadPeople(localtx, fofInput, param, readMarkerset, isNewPeople);
        processEx.startSave();
        processEx.finishSave();
      });
    }
  }

  private void loadPeople(DSLContext ltx, FOFInput fofInput, LinkageParameterData paramData, MarkersetRecord msRec, boolean newPeople) throws FileNotFoundException, IOException {
    String s3ped = fofInput.pedFile;
    String popName = fofInput.popName;
    Map<String, LinkageIndividual> lidMap = new HashMap<>();
    long ptran = System.currentTimeMillis();
    PeopleRecord peopleRec = maybeStowPeople(ltx, popName);
    printStream.println("Now we know these peoples:\n\t" + knownNamePeopleMap.keySet().toString());

    ProjectfileRecord projectFileRec = ltx.selectFrom(PROJECTFILE).where(PROJECTFILE.URI.equal(s3ped)).fetchOne();
    if (projectFileRec == null) {
      projectFileRec = stowFile(s3ped, peopleRec);
    }
    processEx.addInput(PROJECTFILE.getQualifiedName().last(), projectFileRec.getId().toString());

    LinkageFormatter f = getFormatter(s3ped);
    LinkagePedigreeData pedData = new LinkagePedigreeData(f, paramData, LinkConstants.STANDARD);
    peopleLinkageMap.put(peopleRec, pedData); // should over-write the peddata for this people
    Set<String> probandNameSet = new HashSet<>();

    Map<String, PersonRecord> alreadyIndb = null;
    HashSet<String> incomingAliases = new HashSet<>();
    if (newPeople) {
      printStream.println("Add new people: " + peopleRec.getName());
      for (LinkageIndividual ldiv : pedData.getIndividuals()) {
        lidMap.put(ldiv.id.toString(), ldiv);
        if (ldiv.proband == 1) {
          if (trimProbandMap == null || trimProbandMap.get(popName).contains(ldiv.id.toString())) {
            // fullProbandSet.put(ldiv.id.toString(), ldiv);
            probandNameSet.add(ldiv.id.toString());
          }
          else {
            ldiv.proband = 0;
          }
        }
      }
      alreadyIndb = readKnowPersons(lidMap);
      printStream.println(String.format("Start with %d known people", alreadyIndb.size()));

      Map<String, PersonRecord> newPersons = stowNewPersons(alreadyIndb, peopleRec, lidMap);
      printStream.println(String.format("Add %d new people", newPersons.size()));
      // Merge and update persons
      alreadyIndb.putAll(newPersons);
      prepProbandsetHandling(peopleRec, alreadyIndb, probandNameSet);
      updateParents(alreadyIndb, lidMap);
      stowPopulation(ltx, peopleRec, alreadyIndb.values());
    }
    else {
      alreadyIndb = readPeoplePerson(ltx, peopleRec);
    }
    stowGenotypes(ltx, alreadyIndb, msRec, peopleRec, pedData);
    logger.info("people load done in {} ms ", (System.currentTimeMillis() - ptran));
  }

  private Map<String, PersonRecord> readPeoplePerson(DSLContext ltx, PeopleRecord thePeople) {
    // this only works because we name == aka...
    Map<String, PersonRecord> precMap;
    precMap = ltx.select(PERSON.fields())
      .from(PERSON)
      .join(PEOPLE_MEMBER).on(PEOPLE_MEMBER.PERSON_ID.eq(PERSON.ID))
      .where(PEOPLE_MEMBER.PEOPLE_ID.eq(thePeople.getId()))
      .fetchMap(PERSON.NAME, r -> r.into(PERSON));
    return precMap;
  }

  private boolean isNewCombination(PeopleRecord peeps, MarkersetRecord marks, FOFInput fof) {
    if (peeps == null || marks == null) {
      return true;
    }
    if (peeps != null && marks != null) {
      ProcessInput pi1 = PROCESS_INPUT.as("pi1");
      ProcessInput pi2 = PROCESS_INPUT.as("pi2");
      Projectfile f1 = PROJECTFILE.as("f1");
      Projectfile f2 = PROJECTFILE.as("f2");
      ArrayList<String> parTypes = new ArrayList<>(Arrays.asList("par", "ld"));
      ProcessRecord proc = ctx.select(PROCESS.fields())
        .from(PROCESS)
        .join(pi1).on(PROCESS.ID.equal(pi1.PROCESS_ID))
        .join(f1).on(f1.ID.equal(pi1.INPUT_REF.cast(UUID.class)).and(f1.FILETYPE.equal("ped")))
        .join(pi2).on(pi2.PROCESS_ID.equal(PROCESS.ID))
        .join(f2).on(f2.ID.equal(pi2.INPUT_REF.cast(UUID.class)).and(f2.FILETYPE.in(parTypes)))
        .where(f1.URI.equal(fof.pedFile).and(f2.URI.equal(fof.parFile)))
        .fetchOne(r -> r.into(PROCESS));
      return proc == null;
    }
    return false;
  }

  private void stowMarkerData(DSLContext ltx, SGSParameterData param, MarkersetRecord msr,
                              HashMap<String, String[]> alleleMap) {
    ArrayList<SGSParameterData.MarkerTuple> tuples = param.getMarkerTuples();
    int blocksize = 1000;
    ArrayList<MarkerRecord> mkrset = new ArrayList<>();
    ArrayList<MarkersetMemberRecord> mmbset = new ArrayList<>();

    for (SGSParameterData.MarkerTuple tup : tuples) {
      tup.mark.setId(UUID.randomUUID());
      tup.mark.setAlleles(alleleMap.get(tup.mark.getName()));
      tup.mark.setUnknownAllele("0");
      if (genomeBuild == 37) {
        // the import fills 38, the default
        tup.mark.setBasepos37(tup.mark.getBasepos38());
        tup.mark.setBasepos38(null);
      }
      tup.memb.setMemberId(tup.mark.getId());
      tup.memb.setMarkersetId(msr.getId());
      mkrset.add(tup.mark);
      mmbset.add(tup.memb);
      if (mkrset.size() == blocksize) {
        saveBlock(ltx, mkrset, mmbset);
        mkrset = new ArrayList<>();
        mmbset = new ArrayList<>();
      }
    }
    if (mkrset.size() > 0) {
      saveBlock(ltx, mkrset, mmbset);
    }
  }

  private void saveBlock(DSLContext dsl, ArrayList<MarkerRecord> markerList, ArrayList<MarkersetMemberRecord> memberList) {
    dsl.batchStore(markerList).execute();
    dsl.batchStore(memberList).execute();
  }

  /*
   * If newPeople, We want to use pre-existing persons in the map
   * people mapping as well as those discovered in the .ped file
   */

  private void stowGenotypes(DSLContext ltx, Map<String, PersonRecord> knownPersons, MarkersetRecord markerset, PeopleRecord people, LinkagePedigreeData genotypeData) {

    int pcount = 0;
    for (LinkageIndividual indiv : genotypeData.getIndividuals()) {
      if (indiv.proband == 0) {
        continue;
      }
      pcount++;
      StringBuffer calls = new StringBuffer("");
      PersonRecord reci = knownPersons.get(indiv.id.stringGetId());
      for (LinkagePhenotype ph : indiv.pheno) {
        calls.append(ph.getAllele(0) + 1).append(ph.getAllele(1) + 1);
      }
      GenotypeRecord gtRec = ltx.newRecord(GENOTYPE);
      gtRec.setId(UUID.randomUUID());
      gtRec.setPersonId(reci.getId());
      gtRec.setMarkersetId(markerset.getId());
      gtRec.setCalls(compressAlleles(calls.toString()));
      gtRec.setPeopleId(people.getId());
      gtRec.insert();
      processEx.addOutput(GENOTYPE.getUnqualifiedName().last(), gtRec.getId());
    }
    printStream.println(String.format(">>>> wrote genotypes for %d probands on %s", pcount, markerset.getName()));
  }

  private byte[] compressAlleles(String gtLine) {
    byte[] fullLine = gtLine.getBytes();
    byte[] compLine = new byte[fullLine.length / 2];
    byte shift = (byte) 48;
    int j = 0;
    for (int i = 0; i < fullLine.length - 1; i += 2) {
      byte call1 = (byte) (fullLine[i] - shift);
      call1 = (byte) (call1 << 4);
      byte call2 = (byte) (fullLine[i + 1] - shift);
      compLine[j++] = (byte) (call1 | call2);
    }
    return compLine;
  }

  // We use the collected maps to write the 2**N-(N+1) probandset
  // definitions, replete with meioses counts and kinship ranges
  // This is a separate transaction from ped/par loading
  private void generateProbandsets() throws SQLException {
    if (noSubsets) {
      return;
    }
    printStream.println(String.format("Begin probandset work with %d peds", peopleProbandsMap.size()));
    initExecutor();
    Map<PeopleRecord, Set<String>> peopleStagesMap = new HashMap<>();
    // Call the powerset, send blocks to executor
    for (Map.Entry<PeopleRecord, Set<PersonRecord>> me : peopleProbandsMap.entrySet()) {
      Set<String> stagingTableSet = new HashSet<>();
      stagingTableSet.addAll(partitionProbandsetHandling(me, peopleLinkageMap.get(me.getKey())));
      peopleStagesMap.put(me.getKey(), stagingTableSet);
    }
    // We're waiting for all the bulk tables to be loaded.  Should we?
    pbsExecutor.shutdown();
    while (!pbsExecutor.isTerminated()) {
      ;
    }
    completeProbandsetLoad(peopleStagesMap);
  }

  // Send one pedigree to pbs generator. The job is people/probands, jooq-style
  private Set<String> partitionProbandsetHandling(Map.Entry<PeopleRecord, Set<PersonRecord>> job, LinkagePedigreeData lpd) throws SQLException {
    HashSet<String> stageNameSet = new HashSet<>();
    PeopleRecord peopleRec = job.getKey();

    // As we will hand off to a "tool" we don't want JOOQ pieces
    Map<String, UUID> nameIdMap = new HashMap<>();
    for (PersonRecord p : job.getValue()) {
      nameIdMap.put(p.getName(), p.getId());
    }
    int probandCount = nameIdMap.size();
    long subsetCount = Math.round(Math.pow(2.0, 1.0 * probandCount)) - probandCount - 1;
    long batchCount = subsetCount / pbsBlockSize + (subsetCount % pbsBlockSize == 0 ? 0 : 1);
    logger.error("Dealing with {} probands, expect {} subsets in {} batches", probandCount, subsetCount, batchCount);

    LongSet<Set<String>> powerSet = LongPowerSet.create(nameIdMap.keySet());
    Set<Set<String>> pbsSubset = new HashSet<>();
    int partition = 0;
    ProbandsetWriterThread pwt;
    for (Set<String> sids : powerSet) {
      if (sids.size() < 2) {
        continue;
      }
      if (sids.size() == probandCount) {
        pedSupersetMap.put(peopleRec, sids);
      }
      pbsSubset.add(sids);
      if (pbsSubset.size() == pbsBlockSize) {
        String tableName = createBulkPartTable(peopleRec, ++partition);
        stageNameSet.add(tableName);
        pwt = prepWriterThread(nameIdMap, peopleRec, tableName);
        pwt.setOrdinal(partition);
        pwt.localizePedigree(lpd);
        sendBatch(pwt, pbsSubset, peopleRec);
        pbsSubset = new HashSet<>();
        logger.error("Sent part {} now have {} active, and {} completed",
          partition,
          pbsExecutor.getActiveCount(),
          pbsExecutor.getCompletedTaskCount());
      }
    }
    if (pbsSubset.size() > 0) {
      String tableName = createBulkPartTable(peopleRec, ++partition);
      stageNameSet.add(tableName);
      pwt = prepWriterThread(nameIdMap, peopleRec, tableName);
      pwt.setOrdinal(partition);
      pwt.localizePedigree(lpd);
      sendBatch(pwt, pbsSubset, peopleRec);
    }
    return stageNameSet;
  }

  // Move bulk save table data into pbs & pbsgMember
  private void completeProbandsetLoad(Map<PeopleRecord, Set<String>> stagings) throws SQLException {
    for (Map.Entry<PeopleRecord, Set<String>> me : stagings.entrySet()) {
      PeopleRecord peopleRec = me.getKey();
      logger.info("Begin transfer from bulk {} tables for {} into project tables", stagings.size(), peopleRec.getName());
      Map<PeopleRecord, ProbandsetGroupRecord> peopleGroupMap = new HashMap<>();
      for (String stagingTable : me.getValue()) {
        ctx.transaction(t1 -> {
	    if (!peopleGroupMap.containsKey(peopleRec)) {
	      peopleGroupMap.put(peopleRec, skeletonProbandsetGroup(peopleRec));
	    }
	    ProbandsetGroupRecord pbsgRec = peopleGroupMap.get(peopleRec);
	    ctx.createIndex(stagingTable).on("id");
	    String sql = String.format("insert into probandset select * from %s", stagingTable);
	    long startAt = System.currentTimeMillis();
	    ctx.query(sql).execute();
	    sql = String.format("insert into probandset_group_member select '%s', id from %s",
				pbsgRec.getId(),
				stagingTable);
	    ctx.query(sql).execute();
	    logger.debug("transfer sql [{}] took {}s ", stagingTable, (System.currentTimeMillis() - startAt) / 1000);
	    dropStagingTable(stagingTable);
	  });
      }
      setGroupSuperset(peopleRec, peopleGroupMap);
    }
  }

  private void setGroupSuperset(PeopleRecord peopleRec, Map<PeopleRecord, ProbandsetGroupRecord> groupMap) {
    //It might be possible for the partitioner to remember this particular id when generated but
    //this one lookup won't be the long pole in the tent
    ProbandsetGroupRecord groupRec = groupMap.get(peopleRec);
    ProbandsetRecord pbsRec = ProbandsetExtended.findByProbandIds(ctx, peopleRec, peopleProbandsMap.get(peopleRec));
    groupRec.setProbandSupersetId(pbsRec.getId());
    groupRec.store();
  }

  private UUID[] fakeSortedUUIDs(TreeSet<String> tree) {
    UUID[] fake = new UUID[tree.size()];
    int i = 0;
    for (String sid : tree) {
      fake[i++] = UUID.fromString(sid);
    }
    return fake;
  }

  private String treeAsCSV(Set<String> names) {
    Iterator<String> niter = names.iterator();
    StringBuffer buf = new StringBuffer(niter.next());
    while (niter.hasNext()) {
      buf.append(",").append(niter.next());
    }
    return buf.toString();
  }

  private List<FOFInput> fofReader(File fof) { 
    List<FOFInput> lines = new ArrayList<>();
    try (BufferedReader reader = new BufferedReader(new FileReader(fof))) {
      String line;

      while ((line = reader.readLine()) != null) {
        if (line.startsWith("#")) {
          continue;
        }
        line.replaceAll(",,", ", , ");
        String[] parts = line.split(",");
        if (parts.length != PedParLoader.FOF_COLUMNS) {
          throw new RuntimeException("malformatted input file: " + fof.getName());
        }
        lines.add(new FOFInput(parts));
      }
    } catch (IOException ex) {
      logger.error("file of file reader problem: {} {}", fof, ex.getMessage(), ex);
      throw new RuntimeException("file of file reader problem", ex);
    }
    return lines;
  }

  // TODO: rename this
  private void maybeStowProject(String projectName) {
    DefinerRecord aliasDefinerRec = ctx.selectFrom(DEFINER).where(DEFINER.NAME.equal("aliases")).fetchOne();
    if (aliasDefinerRec == null) {
      aliasDefinerRec = ctx.newRecord(DEFINER);
      aliasDefinerRec.setId(UUID.randomUUID());
      aliasDefinerRec.setName("aliases");
      aliasDefinerRec.store();
    }
    defvalRec = ctx.selectFrom(DEFINERVALUE)
      .where(DEFINERVALUE.VALUESTRING.equal(projectName)
        .and(DEFINERVALUE.DEFINER_ID.equal(aliasDefinerRec.getId())))
      .fetchOne();
    if (defvalRec == null) {
      defvalRec = ctx.newRecord(DEFINERVALUE);
      defvalRec.setId(UUID.randomUUID());
      defvalRec.setDefinerId(aliasDefinerRec.getId());
      defvalRec.setValuestring(projectName);
      defvalRec.store();
    }
  }

  private PeopleRecord maybeStowPeople(DSLContext local, String populationName) {
    PeopleRecord pRec = knownNamePeopleMap.get(populationName);
    if (pRec == null) {
      pRec = ctx.newRecord(PEOPLE);
      pRec.setId(UUID.randomUUID());
      pRec.setName(populationName);
      pRec.insert();
      knownNamePeopleMap.put(populationName, pRec);
    }
    return pRec;
  }

  private MarkersetRecord maybeStowMarkerset(DSLContext ltx, FOFInput inline) {
    MarkersetRecord msRec = chromMarkersetMap.get(inline.chromosome);
    if (msRec == null) {
      String setName = pullBaseObject(inline.parFile);
      msRec = ctx.newRecord(MARKERSET);
      msRec.setId(UUID.randomUUID());
      msRec.setGenomeId(genomeRec.getId());
      msRec.setChrom(inline.chromosome);
      msRec.setName(setName);
      msRec.insert();
      chromMarkersetMap.put(inline.chromosome, msRec);
    }
    return msRec;
  }

  private void stowPopulation(DSLContext ltx, PeopleRecord peoplerec, Collection<PersonRecord> persons) {
    logger.error("Writing {} new people membership for {}: ", persons.size(), peoplerec.getName());
    //ctx.batchStore(persons).execute();
    List<PeopleMemberRecord> memberList = new ArrayList<>();
    for (PersonRecord person : persons) {
      PeopleMemberRecord pmr = ltx.newRecord(PEOPLE_MEMBER);
      pmr.setId(UUID.randomUUID());
      pmr.setPeopleId(peoplerec.getId());
      pmr.setPersonId(person.getId());
      memberList.add(pmr);
    }
    ltx.batchStore(memberList).execute();
  }

  private ProjectfileRecord stowFile(String f, PeopleRecord p) {
    ProjectfileRecord pf = ctx.newRecord(PROJECTFILE);
    pf.setId(UUID.randomUUID());
    pf.setName(f.substring(f.lastIndexOf("/") + 1));
    pf.setUri(f);
    pf.setFiletype(extractExtension(f));
    if (p != null) // should check that this is pedfile
    {
      pf.setPeopleId(p.getId());
    }
    pf.insert();
    return pf;
  }

   /*
   * We believe all current person.name values are also in alias.aka
   */
  private Map<String, PersonRecord> readKnowPersons(Map<String, LinkageIndividual> pedIndivMap) {
    //Apologies for the selectDistinct.  If pedigrees overlap ALIAS may have multiple records
    //for one person, all with the same AKA - one per overlapped pedigree.  ALIAS is unique on
    //id/people_id.
    CommonTableExpression<Record2<String, UUID>> aka = name("aka")/*.fields(ALIAS.AKA, ALIAS.PERSON_ID )*/
      .as(selectDistinct(ALIAS.AKA, ALIAS.PERSON_ID)
	  .from(ALIAS)
	  .where(ALIAS.AKA.in(pedIndivMap.keySet())));

    Map<String, PersonRecord> fullMap = new HashMap<>();
    fullMap.putAll(ctx.with(aka)
		   .select() 
		   .from(PERSON)
		   .join(aka).on(PERSON.ID.equal(aka.field(ALIAS.PERSON_ID)))
		   .fetchMap(aka.field(ALIAS.AKA), r -> r.into(PERSON)));
    return fullMap;
  }

  private String extractExtension(String f) {
    String name = f;
    if (name.endsWith(".gz")) {
      name = name.substring(0, name.length() - 3);
    }
    int index = name.lastIndexOf(".") + 1;
    return name.substring(index);
  }

  private HashMap<String, PersonRecord> stowNewPersons(Map<String, PersonRecord> knownPersonsMap, PeopleRecord peepRec, Map<String, LinkageIndividual> lidMap) {
    HashMap<String, PersonRecord> newPersons = new HashMap<>();
    Set<AliasRecord> aliasSet = new HashSet<>();
    for (Map.Entry<String, LinkageIndividual> me : lidMap.entrySet()) {
      LinkageIndividual ldiv = me.getValue();
      String gdr = null;
      switch (ldiv.sex) {
        case 0:
          gdr = null;
          break;
        case 1:
          gdr = "m";
          break;
        case 2:
          gdr = "f";
          break;
        default:
          throw new RuntimeException("Invalid gender ");
      }
      PersonRecord prec = knownPersonsMap.get(me.getKey());
      if (prec == null) {
        prec = ctx.newRecord(PERSON);
        prec.setId(UUID.randomUUID());
        prec.setGender((String)gdr);
        prec.setName(ldiv.id.toString());
        newPersons.put(ldiv.id.toString(), prec);
      }
      AliasRecord arec = ctx.newRecord(ALIAS);
      arec.setId(UUID.randomUUID());
      arec.setAka(ldiv.id.toString());
      arec.setAkadefId(defvalRec.getId());
      arec.setPersonId(prec.getId());
      arec.setPeopleId(peepRec.getId());
      aliasSet.add(arec);
    }
    ctx.batchStore(newPersons.values()).execute();
    ctx.batchStore(aliasSet).execute();
    return newPersons;
  }

  private List<String> pullBaseName(List<String> linkageNames) {
    List<String> rsOnly = new ArrayList<>();
    for (String l : linkageNames) {
      rsOnly.add(l.substring(0, l.indexOf('|')));
    }
    return rsOnly;
  }

  private Integer pullBasePosition(String linkageName) {
    String[] parts = linkageName.split("\\|");
    return Integer.parseInt(parts[1]);
  }

  private List<Integer> pullBasePositions(List<String> linkageNames) {
    List<Integer> posOnly = new ArrayList<>();
    for (String l : linkageNames) {
      String[] parts = l.split("\\|");
      posOnly.add(Integer.parseInt(parts[1]));
    }
    return posOnly;
  }

  private ProbandsetWriterThread prepWriterThread(Map<String, UUID> probandNames, PeopleRecord peepRec, String tableName) throws SQLException {
    ProbandsetWriterThread pwt = new ProbandsetWriterThread(probandNames, peepRec.getId());
    pwt.setBulkTablename(tableName);
    pwt.setName("pbs." + peepRec.getId().toString()); //thread name
    return pwt;
  }

  private String createStagingTableName(PeopleRecord peopleRec) {
    StringBuffer bulktableName = new StringBuffer("bulk.pbs_" + peopleRec.getName());
    return bulktableName.toString();
  }

  private void dropStagingTable(String tableName) {
    logger.info("dropping COPY target {}", tableName);
    String sql = String.format("drop table %s", tableName);
    ctx.query(sql).execute();
  }

  private ProbandsetGroupRecord skeletonProbandsetGroup(PeopleRecord peopleRec) {
    ProbandsetGroupRecord groupRec = ctx.newRecord(PROBANDSET_GROUP);
    groupRec.setId(UUID.randomUUID());
    groupRec.setName(peopleRec.getName() + ".pbs");
    groupRec.store();

    return groupRec;
  }

  private HashMap<String, Set<String>> buildTrimSet(File trimfile) {
    HashMap<String, Set<String>> trimmers = new HashMap<>();
    try (BufferedReader fofreader = new BufferedReader(new FileReader(trimfile))) {
      String fofline;
      while ((fofline = fofreader.readLine()) != null) {
        String[] fofparts = fofline.split("\\s");
        String pedName = fofparts[0];
        try (BufferedReader pbreader = new BufferedReader(new FileReader(fofparts[1]))) {
          Set<String> idset = new HashSet<>();
          String pbline;
          while ((pbline = pbreader.readLine()) != null) {
            String[] ids = pbline.split("[\\s,|]");
            idset.addAll(Arrays.asList(ids));
          }
          trimmers.put(pedName, idset);
        } catch (IOException ie) {
          throw new RuntimeException(ie.getMessage());
        }
      }
    } catch (IOException ioe) {
      logger.error("trouble in pedparLoader trimming", ioe);
      ioe.printStackTrace();
      throw new RuntimeException(ioe.getMessage(), ioe);
    }
    return trimmers;
  }

  private void extendPedAlias(Collection<PersonRecord> knownPersons, PeopleRecord peep) {
    if (knownPersons == null || knownPersons.size() == 0) {
      return;
    }
    ArrayList<String> names = new ArrayList<>();
    for (PersonRecord persRec : knownPersons) {
      names.add(persRec.getName());
    }
    List<PersonRecord> existingAlias = new ArrayList<>();
    existingAlias.addAll(ctx.select(PERSON.fields())
      .from(PERSON.join(ALIAS)
        .on(PERSON.ID.equal(ALIAS.PERSON_ID).and(ALIAS.PEOPLE_ID.equal(peep.getId()))))
      .where(PERSON.NAME.in(names))
      .fetch(r -> r.into(PERSON)));
    logger.info("found {} persons needing alias for people {}", existingAlias.size(), peep.getName());
    knownPersons.removeAll(existingAlias);

    List<AliasRecord> extension = new ArrayList<>();
    for (PersonRecord perRec : knownPersons) {
      AliasRecord aliRec = ctx.newRecord(ALIAS);
      aliRec.setId(UUID.randomUUID());
      aliRec.setPersonId(perRec.getId());
      aliRec.setPeopleId(peep.getId());
      aliRec.setAka(perRec.getName());
      extension.add(aliRec);
    }
    ctx.batchStore(extension).execute();
  }

  private void updateParents(Map<String, PersonRecord> knowns, Map<String, LinkageIndividual> lid) {
    Set<PersonRecord> updatePersons = new HashSet<>();
    boolean toUpdate = false;
    for (Map.Entry<String, PersonRecord> maprec : knowns.entrySet()) {
      LinkageIndividual ldiv = lid.get(maprec.getKey());
      if (ldiv == null) {
        printStream.println(String.format("Didn't find %s in this .ped file", maprec.getKey()));
        continue;
      }
      PersonRecord personRec = maprec.getValue();
      if (ldiv.maid != null) {
        PersonRecord mom = knowns.get(ldiv.maid.toString());
        if (mom == null) {
          logger.info("No known mom for {}", ldiv.maid.toString());
        }
        else if (personRec.getMa() == null) {
          personRec.setMa(mom.getId());
          toUpdate = true;
        }
        else if (!personRec.getMa().equals(mom.getId())) {
          String msg = String.format("Ped file and database disagree on mother of %s: %s(%s) vs %s(%s)", personRec.getId(), personRec.getMa(), personRec.getMa().variant(), mom.getId(), mom.getId().variant());
          throw new RuntimeException(msg);
        }
      }
      if (ldiv.paid != null) {
        PersonRecord dad = knowns.get(ldiv.paid.toString());
        if (dad == null) {
          logger.info("No known dad for {}", ldiv.paid.toString());
        }
        else if (personRec.getPa() == null) {
          personRec.setPa(dad.getId());
          toUpdate = true;
        }
        else if (!personRec.getPa().equals(dad.getId())) {
          String msg = String.format("Ped file and database disagree on father of %s: %s(%s) vs %s(%s)", personRec.getId(), personRec.getPa(), personRec.getPa().variant(), dad.getId(), dad.getId().variant());
          throw new RuntimeException(msg);
        }
      }
      if (toUpdate) { //We don't want to do two updates, so we wait 'til here
        logger.info("updating parents of {}", personRec.getId());
        updatePersons.add(personRec);
      }
      toUpdate = false; // redundant
    }
    logger.info("need to update parents of {} people", updatePersons.size());
    ctx.batchStore(updatePersons).execute();
  }

  private void prepProbandsetHandling(PeopleRecord peopleRec, Map<String, PersonRecord> currentPersonsMap, Set<String>pbnameSet) {
    HashSet<PersonRecord> pbPersonSet = new HashSet<>();
    for (String inName : pbnameSet) {
      PersonRecord perp = currentPersonsMap.get(inName);
      if (perp == null) {
	throw new RuntimeException(String.format("Could not find %s as proband for %s", inName, peopleRec.getName()));
      }
      pbPersonSet.add(perp);
    }
    peopleProbandsMap.put(peopleRec, pbPersonSet);
  }

  public Map<Integer, Map<String, String[]>> getMarkerAlleleMap() {
    if (markerAlleleMap == null) {
      markerAlleleMap = new HashMap<>();
    }
    return markerAlleleMap;
  }

  private HashMap<String, String[]> buildMarkerAlleleMap(String s3allele) throws IOException {
    HashMap<String, String[]> nameAlleleMap = new HashMap<>();
    if (s3allele != null && s3allele.trim().length() > 0) {
      try (BufferedReader reader = getReader(s3allele)) {
        String alleleLine;
        while ((alleleLine = reader.readLine()) != null) {
          String[] parts = alleleLine.split("[:|,]\\s*");
          //String[] alleles = {parts[(parts.length() - 2)], parts[(parts.length() - 1)]}; // Why does this not work???
	  if (parts.length != 5) {
	    throw new RuntimeException("Malformed alleles file: " + s3allele + " line is " + alleleLine);
	  }
          String[] alleles = {parts[3].trim(), parts[4].trim()};
          nameAlleleMap.put(parts[0], alleles);
        }
        logger.error("loaded allele file: {}", s3allele);
      }
    }
    return nameAlleleMap;
  }

  private MarkersetRecord findMarkerset(String markerName) {
    MarkersetRecord msRec;
    msRec = ctx.select(MARKERSET.fields())
      .from(MARKERSET)
      .join(MARKERSET_MEMBER).on(MARKERSET.ID.equal(MARKERSET_MEMBER.MARKERSET_ID))
      .join(MARKER).on(MARKERSET_MEMBER.MEMBER_ID.equal(MARKER.ID))
      .where(MARKER.NAME.equal(markerName))
      .fetchOne(r -> r.into(MARKERSET));
    return msRec;
  }

  private List<MarkerRecord> findSetMarkers(MarkersetRecord msRec) {
    List<MarkerRecord> mkList;
    mkList = ctx.select(MARKER.fields())
      .from(MARKER).join(MARKERSET_MEMBER).on(MARKER.ID.equal(MARKERSET_MEMBER.MEMBER_ID))
      .where(MARKERSET_MEMBER.MARKERSET_ID.equal(msRec.getId()))
      .fetch(r -> r.into(MARKER));
    return mkList;
  }

  // per chromosome here
  private void updateAlleles(List<MarkerRecord> markers, Map<String, String[]> alleles) {
    for (MarkerRecord mRec : markers) {
      mRec.setAlleles(alleles.get(mRec.getName()));
    }
    ctx.batchStore(markers).execute();
  }

  //A pair of candidates for a tool??
  private void showHelpAndExit(OptionParser op) throws IOException {
    printStream.print(justifyHelp(getHelpDescription(), 100));
    op.printHelpOn(System.out);
    System.exit(0);
  }

  private String justifyHelp(String description, int width) {
    int position = 0;
    int endpoint = 0;
    StringBuilder sb = new StringBuilder();
    while (position + width < description.length() - 1) {
      endpoint += width;
      while (description.charAt(endpoint) != ' ') {
        endpoint--;
      }
      String chunk = description.substring(position, endpoint);
      int nlAt = chunk.indexOf(System.getProperty("line.separator"));
      if (nlAt > -1) {
        sb.append(chunk.substring(0, nlAt + 1));
        position += nlAt + 1;
      }
      else {
        position = 1 + endpoint;
        sb.append(chunk + "\n");
      }
    }
    sb.append(description.substring(position)).append("\n");
    return sb.toString();
  }

  private void sendBatch(ProbandsetWriterThread pswThread,
                         Set<Set<String>> pbsSubset,
                         PeopleRecord peopleRec) throws SQLException {
    pswThread.setPowerset(pbsSubset);
    pswThread.setIsMeiosesCounting(!noMeiosesCount);
    pswThread.prepPeopleInfo(peopleRec.getName(), peopleRec.getId());
    pbsExecutor.execute(pswThread);
    logger.error("Added part {}(size {}) to queue for {}", pswThread.getOrdinal(), pbsSubset.size(), peopleRec.getName());
  }

  // private Connection reconnect(String url) throws SQLException {
  //     String prop = System.getProperty("upass");
  //     String[] parts = null;
  //     if ( prop == null ) {
  //         logger.error("We expect 'upass' property for thread work");
  //         System.exit(2);
  //     }
  //     else {
  //         parts = prop.split(":");
  //     }
  //     return DriverManager.getConnection(url, parts[0], parts[1]);
  // }

  private String createBulkPartTable(PeopleRecord peopleRec, int partno) {
    String btableName = String.format("bulk.pbs_%s_p%04d", peopleRec.getName(), partno);
    String sql = String.format("create table %s as select * from %s.probandset where 1 = 2", btableName, projectName);
    ctx.query(sql).execute();
    return btableName;
  }

  private String getHelpDescription() {
    String sb = "\n" + getClass().getName() + " is an essential component in project initialization." + "  It operates on file-of-filenames as input for the two main components (people (in pedigrees), marker sets (with LD information)). Both are required." +
      "Optionally, allele definitions files (--" +
      PedParLoader.SPEC_SUBSFILE + " option (used when wishing to specify a particular collection of proband set definitions) requires --" +
      PedParLoader.SPEC_DOSUBS + " to ensure that the entire power set of all probands is NOT generated." +
      "\n=== This is a '2-tier' application.  It reads from and writes to the database ===\n";
    return sb;
  }

  private void initExecutor() {
    ArrayBlockingQueue<Runnable> pbsQueue = new ArrayBlockingQueue<>(100); //TODO: needs a param?
    int availableProcessors = Runtime.getRuntime().availableProcessors();
    pbsExecutor = new ConnectingThreadPoolExecutor(availableProcessors * 3 / 4,
      availableProcessors * 2,
      10, TimeUnit.SECONDS,
      pbsQueue);
    pbsExecutor.setTemplateURL(dbUrl);
  }

  private String pullBaseObject(String s3url) {
    int idx = s3url.lastIndexOf('/');
    return s3url.substring(idx + 1);
  }

  private GenomeMarkersetRecord findOrCreateGenome(String gname, int gbuild) {
    // singleton transaction.
    GenomeMarkersetRecord gmRec = ctx.selectFrom(GENOME_MARKERSET).where(GENOME_MARKERSET.NAME.equal(gname)).fetchOne();
    if (gmRec == null) {
      gmRec = ctx.newRecord(GENOME_MARKERSET);
      gmRec.setId(UUID.randomUUID());
      gmRec.setName(genomeName);
      gmRec.setPrimaryBuild(gbuild);
      gmRec.insert();
    }
    return gmRec;
  }

  private Map<Integer, MarkersetRecord> initChromMarkersetMap(GenomeMarkersetRecord gmr) {
    Map<Integer, MarkersetRecord> mmap = new HashMap<>();
    mmap.putAll(ctx.selectFrom(MARKERSET)
      .where(MARKERSET.GENOME_ID.equal(gmr.getId()))
      .fetchMap(MARKERSET.CHROM, r -> r.into(MARKERSET)));

    return mmap;
  }

  private PrintStream setPrintStream(File logfile) throws IOException {
    if (! logfile.exists()) {
      logfile.getParentFile().mkdirs();
    }
    if (! logfile.createNewFile()) {
      throw new IOException("Cannot open log file: " + logfile.getAbsolutePath());
    }
    return new PrintStream(logfile);
  }

  private SGSFormatter getFormatter(String source) throws IOException{
    if (isS3) {
      // This constructor expects source to be a "url" into AWS.
      return new SGSFormatter(source);
    }
    else {
      return new SGSFormatter(new BufferedReader(new InputStreamReader(new FileInputStream(source))), "pedpar");
    }
  }

  private BufferedReader getReader(String name ) throws FileNotFoundException {
    if (isS3) {
      return new BufferedReader(S3FileInputStream.getS3Reader(name));
    }
    else {
      return new BufferedReader(new FileReader(name));
    }
  }
}
