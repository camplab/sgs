package edu.utah.camplab.tools;

import org.jooq.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.UUID;
import java.util.zip.GZIPInputStream;

import static edu.utah.camplab.db.generated.default_schema.Tables.*;
import edu.utah.camplab.db.generated.default_schema.tables.records.*;
import edu.utah.camplab.db.generated.base.tables.records.*;

//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;

public class DecompressReader {
    //private final static Logger logger = LoggerFactory.getLogger(DecompressReader.class);

    private File pvalTotalFile = null;
    private BufferedReader reader = null;

    private MarkersetRecord markersetRec;
    private ProbandsetRecord probandsetRec;
    private int minSegmentSize;
    
    public DecompressReader(File pvfile, ProbandsetRecord psetRec, MarkersetRecord msr, int minseg) throws IOException {
        pvalTotalFile = pvfile;
        minSegmentSize = minseg;
        markersetRec = msr;
        probandsetRec = psetRec;
        
        if (pvalTotalFile.toString().endsWith(".gz")) {
            GZIPInputStream zstream = new GZIPInputStream(new FileInputStream(pvalTotalFile.toString()));
            InputStreamReader rstream = new InputStreamReader(zstream);
            reader = new BufferedReader(rstream);
        }
        else {
            reader = Files.newBufferedReader(pvalTotalFile.toPath());
        }
    }

    public ArrayList<SegmentRecord> readSegments(DSLContext dbtran) throws IOException {
        ArrayList<SegmentRecord> segmentsRead = new ArrayList<>();

        String dataline = null;
        dataline = reader.readLine();
        while (dataline.startsWith("#") && dataline != null) {
            dataline = reader.readLine();
        }
        
        while (dataline != null) {
            String[] parts = dataline.split("\\s");
            if (parts.length != 9) {
                throw new UnsupportedOperationException("Wrong format: " + dataline);
            }
            int segSize = Integer.parseInt(parts[3]);
            if (segSize >= minSegmentSize) {
                SegmentRecord s = dbtran.newRecord(SEGMENT);
                s.setId(UUID.randomUUID());
                s.setProbandsetId(probandsetRec.getId());
                s.setMarkersetId(markersetRec.getId()); //TODO: focus on set
                s.setChrom(Integer.parseInt(parts[0]));
                s.setFirstmarker(Integer.parseInt(parts[1]));
                s.setLastmarker(Integer.parseInt(parts[2]));
                s.setStartbase(Integer.parseInt(parts[4]));
                s.setEndbase(Integer.parseInt(parts[5]));
                s.setEventsLess(Long.parseLong(parts[6]));
                s.setEventsEqual(Long.parseLong(parts[7]));
                s.setEventsGreater(Long.parseLong(parts[8]));
                segmentsRead.add(s);
            }
            dataline = reader.readLine();
        }
        return segmentsRead;
    }
}
