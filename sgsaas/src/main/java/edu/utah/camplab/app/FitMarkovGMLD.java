package edu.utah.camplab.app;

import java.util.Random;

import jpsgcs.genio.GeneticDataSource;
import jpsgcs.linkage.LinkageParameterData;
import jpsgcs.linkage.LinkagePedigreeData;
import jpsgcs.linkage.LinkageInterface;
import jpsgcs.linkage.LinkageDataSet;
import jpsgcs.linkage.LinkageFormatter;
import jpsgcs.pedmcmc.LDModel;
import jpsgcs.pedapps.CompleteHaplotypes;
import jpsgcs.markov.Function;
import jpsgcs.markov.Variable;
import jpsgcs.jtree.MultinomialMLEScorer;
import jpsgcs.jtree.JTree;
import jpsgcs.graph.Network;
import jpsgcs.util.Monitor;

public class FitMarkovGMLD {
    public static void main(String[] args) {
        try {
            Random rand = new Random();

            Monitor.quiet(false);

            // Set defaults and read the data.

            double eprob = 0.001;
            int lag = 0;
            int totalits = 10;

            GeneticDataSource x = null;
            LinkageParameterData par = null;
            LinkagePedigreeData ped = null;

            switch (args.length) {
            case 4: 
	      totalits = Integer.valueOf(args[3]);
            case 3: 
	      lag = Integer.valueOf(args[2]);
            case 2: 
                par = new LinkageParameterData(new LinkageFormatter(args[0]));
                ped = new LinkagePedigreeData(new LinkageFormatter(args[1]),par);
                x = new LinkageInterface(new LinkageDataSet(par,ped));
                break;
            default:
                System.err.println("Usage: java FitMarkovGMLD in.par in.ped [lag|0] [totalits|10]");
                System.exit(1);
            }

            Monitor.show("Input read");

            // Set up the LD model and get its variables.

            LDModel ld = new LDModel(x);
            Variable[] loci = ld.getLocusVariables();

            Network<Variable,Object> g = new Network<Variable,Object>();
            for (Variable v : loci)
                g.add(v);

            for (int i=0; i<loci.length; i++)
                for (int j=i-lag; j<i; j++)
                    if (j >= 0)
                        g.connect(loci[j],loci[i]);

            EMFitLDModelForGraph(ld,g,x,totalits,eprob,rand);

            par.writeTo(System.out);
            ld.writeStateSizes(System.out);
            ld.writeFunctions(System.out);

            Monitor.show("Done");
        }
        catch (Exception e) {
            System.err.println("Caught in FitMarkovGMLD.main()");
            e.printStackTrace();
        }
    }

    static public void EMFitLDModelForGraph(LDModel ld, Network<Variable,Object> g, GeneticDataSource x, int nits, double eprob,Random rand) {
        boolean[] both = {true, false};

        ld.initialize();
        CompleteHaplotypes haps = new CompleteHaplotypes(x,ld,eprob,rand);
        haps.simulate();

        MultinomialMLEScorer scorer= new MultinomialMLEScorer(ld.getLocusVariables(),haps,0,rand);

        JTree<Variable> jt = new JTree<Variable>(g,rand);

        for (boolean random : both) {
            for (int i=0; i<nits; i++) {
                // M step.

                scorer.clear();
                ld.clear();
                for (Function f : scorer.fitModel(jt).getFunctions())
                    ld.add(f);

                // E step.

                haps.setModel(ld);
                haps.update(random);

                System.err.print("-");
            }
			
            System.err.println();
        }

        scorer.clear();
        ld.clear();
        for (Function f : scorer.fitModel(jt).getFunctions())
            ld.add(f);
    }
}
