package edu.utah.camplab.sgs;

import java.io.Serializable;
import java.util.UUID;

import edu.utah.camplab.db.generated.default_schema.tables.pojos.Segment;

//Could easily put this inside AlleleSharing
public class SharingRun extends Segment implements Comparable<SharingRun>, Serializable {
  //Edging away from SharingRun
  // But debugging requires more info
  private String probands;
  private static final long serialVersionUID = -7150770119527322689L;
  
  public SharingRun() {
    setEventsLess(0L);
    setEventsEqual(0L);
    setEventsGreater(0L);
  }
  
  public SharingRun( Segment orig, int rebase ) {
    // A copy constructor! used to retain starting point
    super(orig);
    int segmentLength = runLength();
    setFirstmarker(rebase);
    setLastmarker(rebase + segmentLength - 1 );
  }
  public String getProbands() { return probands;}
  public void setProbands(String pset) {probands = pset;}
  public int runLength() {
    return getLastmarker() - getFirstmarker() + 1;
  }
      
  // perhaps this should be reversed to isSubsetOf and incr(event) if true
  public boolean contains(SharingRun other) {
    return (getFirstmarker() <= other.getFirstmarker() && getLastmarker() >= other.getLastmarker());
  }

  public long minEvents2Tail() {
    long tail = getEventsGreater() > getEventsLess() ? getEventsLess() : getEventsGreater();
    return (getEventsEqual()/2) + tail;
  }

  public long minEvents1Tail() {
    return getEventsEqual() + getEventsGreater();
  }

  public long totalEvents() {
    return getEventsLess() + getEventsEqual() + getEventsGreater();
  }

  public void plusRun(Segment adder) {
    if (runLength() != adder.getLastmarker() - adder.getFirstmarker() + 1) {
      throw new RuntimeException(String.format ("mis-match: sharing runs of different length! %d v. %d and %d v. %d",
						getFirstmarker(), adder.getFirstmarker(), getLastmarker(), adder.getLastmarker()));
    }
    setEventsGreater(getEventsGreater() + adder.getEventsGreater());
    setEventsEqual(getEventsEqual() + adder.getEventsEqual());
    setEventsLess(getEventsLess() + adder.getEventsLess());
  }

  public void plusRun(long lesser, long equal, long greater) {
    // TODO Pass in a Segment
    // if (getFirstmarker() != addr.getFirstmarker() || getLastmarker() != addr.getLastmarker() ) {
    //   throw new RuntimeException(String.format ("mis-match sharing runs don't add! %d v. %d and %d v. %d",
    // 						getFirstmarker(), addr.getFirstmarker(), getLastmarker(), addr.getLastmarker()));
    // }
    setEventsLess(getEventsLess() + lesser);
    setEventsEqual(getEventsEqual() + equal);
    setEventsGreater(getEventsGreater() + greater);
  }

  @Override
  public int compareTo(SharingRun other) {
    if (equals(other)) {
      return 0;
    }
    else if (getFirstmarker() < other.getFirstmarker()) {
      return -1;
    }
    else if (getFirstmarker() == other.getFirstmarker()) {
      return getLastmarker() < other.getLastmarker() ? -1 : 1;
    }
    else //if (getFirstmarker() > other.getFirstmarker()) {
      return 1;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (!(obj instanceof SharingRun))
      return false;
    SharingRun other = (SharingRun) obj;
    if (getLastmarker() != other.getLastmarker())
      return false;
    if (getFirstmarker() != other.getFirstmarker())
      return false;
    return true;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + getLastmarker();
    result = prime * result + getFirstmarker();
    return result;
  }

  public boolean equals(SharingRun other) {
    // we only work on one chromosome at a time
    return (getFirstmarker() == other.getFirstmarker()
      && getLastmarker() == other.getLastmarker()
      && getProbandsetId() == other.getProbandsetId());
  }

  public String toString() {
    return String.format("mrkRange: [%06d, %06d] baseRange: [%09d, %09d] L: %d E: %d G: %d",
			 getFirstmarker(), getLastmarker(), getStartbase(), getEndbase(), getEventsLess(), getEventsEqual(), getEventsGreater());
  }
}
