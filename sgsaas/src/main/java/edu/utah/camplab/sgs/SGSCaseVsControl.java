package edu.utah.camplab.sgs;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.UUID;

import edu.utah.camplab.tools.LinkageIdSet;
import joptsimple.OptionSet;
import joptsimple.OptionSpec;

import edu.utah.camplab.jx.AbstractPayload;
import edu.utah.camplab.jx.SegmentBlock;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

// just for a static?

public class SGSCaseVsControl extends AbstractSGSRun {
    
    public static final Logger logger = LoggerFactory.getLogger(SGSCaseVsControl.class);

    // The distilled result set

    private DropEvaluation     caseEvaluation;
    private DropEvaluation     ctrlEvaluation;
    private DropEvaluation     diffEvaluation;
    private int[]              observedNrunDeltas;
    private Float[]            observedPairedDeltas;
    private Float[]            observedWeightedDeltas;
    private float              caseKinshipCoeff;
    private float              ctrlKinshipCoeff;
    private float              casePedigreeWeight;
    private float              ctrlPedigreeWeight;

    private LinkageIdSet caseSet;
    private LinkageIdSet ctrlSet;

    private OptionSpec<String> caseIdsSpec;
    private OptionSpec<String> ctrlIdsSpec;
    
    public SGSCaseVsControl() {
        super();
        caseIdsSpec = op.accepts("case-ids", "comma separated list of cases").withOptionalArg().ofType(String.class);
        ctrlIdsSpec = op.accepts("ctrl-ids", "comma separated list of controls").withOptionalArg().ofType(String.class);
    }

  @Override public UUID getPedigreeId() { return null;}
  @Override public void setPedigreeId(UUID id) { }
  @Override public UUID getMarkersetId() {return null;}
  @Override public void setMarkersetId(UUID id) {}
  
  public void buildLinkageDataSet() {
    ;
  }
    //TODO: When restoring this, think about wall time
    @Override
    protected void init(OptionSet opSet) throws Exception {
        super.init(opSet);
        setGeneDropper();
        logger.error("Someone is trying CaseVsControl!");
        throw new UnsupportedOperationException("CvC currently unsupported - would be cool though");
    }
    
    public void init(String[] args) throws Exception {
        OptionSet opSet = op.parse(args);
        caseSet = new LinkageIdSet(caseIdsSpec.value(opSet));
        ctrlSet = new LinkageIdSet(ctrlIdsSpec.value(opSet));
        init(opSet);
    }

    @Override
    public void run() throws Exception {
        initVectors();
        resetProbands(caseSet);
        caseKinshipCoeff = AlleleSharing.accumulatedPedigreeKinship(linkageInterface);
        casePedigreeWeight = AlleleSharing.pedigreeWeight(linkageInterface);

        resetProbands(ctrlSet);
        ctrlKinshipCoeff = AlleleSharing.accumulatedPedigreeKinship(linkageInterface);
        ctrlPedigreeWeight = AlleleSharing.pedigreeWeight(linkageInterface);

        setObservedDelta();

        for (int i = 0; i < getSimulationCount(); i++) {
            System.out.print(".");
            getGeneDropper().geneDrop();
            resetProbands(caseSet);
            caseEvaluation.assessSimulation(linkageInterface, getMinimumSegmentSize());

            resetProbands(ctrlSet);
            ctrlEvaluation.assessSimulation(linkageInterface, getMinimumSegmentSize());
            evaluateByDifference();
        }
        //reportAll();
    }

    private void initVectors() {
      caseEvaluation = null;//new DropEvaluation();
      ctrlEvaluation = null;//new DropEvaluation();
      diffEvaluation = null;//new DropEvaluation();

        observedNrunDeltas = new int[linkageInterface.nLoci()];
        observedPairedDeltas = new Float[linkageInterface.nLoci()];
        observedWeightedDeltas = new Float[linkageInterface.nLoci()];
    }

    private void setObservedDelta() {
        // TODO sharing
        // for (int i = 0; i < linkageDataSet.getParameterData().nLoci(); i++) {
        //     observedNrunDeltas[i] = caseObserved.getSharingVector(0)[i] - ctrlObserved.getSharingVector(0)[i];
        //     observedPairedDeltas[i] = caseObserved.getPaired(i) - ctrlObserved.getPaired(i);
        //     observedWeightedDeltas[i] = ctrlPedigreeWeight * caseObserved.getWeightedPaired(i) - casePedigreeWeight * ctrlObserved.getWeightedPaired(i);
        // }
    }

    private void evaluateByDifference() {

        // int[] caseRun = caseObserved.getSharingVector(0);
        // int[] ctrlRun = ctrlObserved.getSharingVector(0);
        // int[] caseSim = caseSimulated.getSharingVector(0);
        // int[] ctrlSim = ctrlSimulated.getSharingVector(0);

        // int i = 0;
        // for (SGSMarkerResult mr : diffEvaluation.getMarkerList()) {
        //     int observedDiff = caseRun[i] - ctrlRun[i];
        //     int simulateDiff = caseSim[i] - ctrlSim[i];
        //     // SGSMarkerResult.StatHolder evalStat = mr.getSharings().get(0);
        //     // if (observedDiff <= simulateDiff) {
        //     //     evalStat.incr();
        //     // }
        //     // if (observedPairedDeltas[i] <= caseSimulated.getPaired(i) - ctrlSimulated.getPaired(i)) {
        //     //     mr.incrPaired();
        //     // }
        //     // if (observedWeightedDeltas[i] <= ctrlPedigreeWeight * caseSimulated.getWeightedPaired(i) - casePedigreeWeight * ctrlSimulated.getWeightedPaired(i)) {
        //     //     mr.incrWeighted();
        //     // }
        //     i++;
        // }
    }

    // private void reportAll() throws IOException {
    //     BufferedWriter writer = new BufferedWriter(new FileWriter(getJsonDir()));
    //     writer.write(makeHeaders());
    //     float denominator = 1.0f * (1 + getSimulationCount());

    //     for (int i = 0; i < linkageInterface.nLoci(); i++) {
    //         String[] nameParts = getLocusNameParts(i);
    //         writer.write(String.format("%s\t%s\t%s", nameParts[2], nameParts[0], nameParts[1]));
    //         // writer.write("\t" + caseObserved.getHetSharing(i));
    //         writeStats(writer, caseEvaluation, i, denominator);
    //         writeStats(writer, ctrlEvaluation, i, denominator);
    //         writeDiff(writer, i, denominator);
    //         writer.write("\n");
    //         writer.flush();
    //     }
    // }

    private void writeStats(BufferedWriter bw, DropEvaluation eval, int i, float denom)
        throws IOException {
        bw.write("");
    }

    private void writeDiff(BufferedWriter bw, int i, float denom) throws IOException {
        bw.write("");
    }

    public SegmentBlock buildSegmentBlock() {
        return null;
    }
}
