package edu.utah.camplab.sgs;

public class SGSShutdownHandler extends Thread {
  Thread mainThread = null;
  public SGSShutdownHandler(Thread main) {
    mainThread = main;
    setUncaughtExceptionHandler( new UncaughtExceptionHandler() {  
	@Override
	public void uncaughtException(Thread t, Throwable e) {
	  String emsg = String.format("hiding exeption %s from %s", e.getMessage(), t.getName());
	  System.err.println(emsg);
	}
      });
  }
  @Override
  public void run() {
    System.err.println(" ** Received notice! **");
    SGSChase.SGS_NOKILL = false;
  }
}
