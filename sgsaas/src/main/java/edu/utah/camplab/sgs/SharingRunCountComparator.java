package edu.utah.camplab.sgs;

import java.util.Comparator;

public class SharingRunCountComparator implements Comparator<SharingRun> {
    private SharingRun srun = null;

    public SharingRunCountComparator() {
        ;
    }

    public SharingRunCountComparator(SharingRun run) {
        srun = run;
    }

    public int compare(SharingRun left, SharingRun right) {
        if (left == null && right != null) {
            return -1;
        }
        if (left != null && right == null) {
            return 1;
        }
        long leftMin = left.minEvents1Tail(); 
        long rightMin = right.minEvents1Tail();
        if (leftMin == rightMin) {
            return 0;
        }
        else {
            return leftMin < rightMin ? -1 : 1;
        }
    }

    @Override
    public boolean equals(Object other) {
        if(srun == null && other == null) {
            return true;
        }
        if (srun == null  || other == null) {
            return false;
        }
        if (this == other) {
            return true;
        }
        if (other instanceof SharingRun) {
            SharingRun otherRun = (SharingRun)other;
            return srun.getEventsLess() == otherRun.getEventsLess()
                && srun.getEventsEqual() == otherRun.getEventsEqual()
                && srun.getEventsGreater() == otherRun.getEventsGreater();
        }
        else {
            return false;
        }
    }
}
