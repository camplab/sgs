package edu.utah.camplab.sgs;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import edu.utah.camplab.jps.SGSFormatter;
import edu.utah.camplab.jx.*;
//import edu.utah.camplab.servlet.AbstractSGSServlet;
import edu.utah.camplab.tools.InterimFileManagerInterface;
import edu.utah.camplab.tools.LinkageIdSet;

import joptsimple.BuiltinHelpFormatter;
import joptsimple.OptionParser;
import joptsimple.OptionSet;
import joptsimple.OptionSpec;
import jpsgcs.linkage.*;
import jpsgcs.pedapps.GeneDropper;
import jpsgcs.pedmcmc.LDModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.http.*;
import java.net.*;

import java.util.*;
import java.util.function.Supplier;
import java.util.jar.Attributes;
import java.util.jar.JarFile;
import java.util.jar.Manifest;
import java.util.zip.GZIPOutputStream;

public abstract class AbstractSGSRun implements SGSRunInterface {
  public static final Logger logger = LoggerFactory.getLogger(AbstractSGSRun.class);
    
  public static final int ANALYSIS_PVALUE      = 1;
  public static final int ANALYSIS_THRESHOLD   = 2;
  public static final int ANALYSIS_MULTIASSESS = 3;
  public static final int ANALYSIS_CASECONTROL = 4;

  public static final int REDUCE_INIT_STEP = 5; 
  public static final int REDUCE_INCR_STEP = 5; 
  public static final int REDUCE_MAX_LOOPS = 20;

  public static final String  S3SGS_ROOT = "691459864434-sgs-";

  protected long                        maxWallclock       = 0;
  protected long                        startWallclock     = (System.currentTimeMillis() / 1000);
  private long                          nsims              = 1000;  //Absurdly low default request; corrected to actual sims performed
  private int                           minimumSegmentSize = 0;
  private String                        chromosome         = "";        //TODO: remove chromosome
  private File                          jsonDir            = new File("/tmp/sgs-crash");


  private Map<String,   Object>         optionValueMap     = null;      //Only holds options used or defaulted
  private Float                         threshold          = null;
  private int                           confidence         = 0;

  protected LinkageInterface            linkageInterface   = null;
  protected LinkageDataSet              linkageDataSet     = null;
  protected LDModel                     ldModel            = null;
  protected int[]                       retainedIndices    = null;
  protected Set<String>                 trimmingProbands   = null;

  private GeneDropper                   geneDropper        = null;
  private UUID                          tag                = null;
  private InterimFileManagerInterface   interimManager     = null;
  // context of this run
  private String                        projectName;
  private String                        dbName;
  private String                        writerHost         = null;
  private int                           writerPort         = -1;
  private HttpClient                    httpClient         = null;

  // CLI options
  public static final List<String> SPEC_OP = Arrays.asList("o", "operation");
  protected  OptionParser               op;
  protected  OptionSet                  opSet;
  protected  OptionSpec<Void>           helpSpec;
  protected  OptionSpec<File>           jsonDirSpec;
  protected  OptionSpec<Long>           simSpec;
  protected  OptionSpec<Integer>        skipSpec;
  protected  OptionSpec<String>         accumulatorSpec;
  protected  OptionSpec<String>         effortSpec;
  protected  OptionSpec<Integer>        operationSpec;
  protected  OptionSpec<String>         dbnameSpec;
  
  protected AbstractSGSRun(String accumulator) {
    String[] hostPort = accumulator.split(":");
    setAccumulationHost(hostPort[0]);
    setAccumulationPort(Integer.parseInt(hostPort[1]));
  }
    
  protected AbstractSGSRun() {
    op = new OptionParser();
        
    //help
    helpSpec        = op.acceptsAll(Arrays.asList("h", "help"));
    //which analysis to run, included here for completeness, but it's actually dealt with in SGSPValue
    operationSpec   = op.acceptsAll(SPEC_OP, "analysis to run:  1..4 {Segment(default), Threshold, MultiAssess, CaseControl}").withRequiredArg().ofType(Integer.class).required();
    //options global to all operations
    //where to write in not the database
    jsonDirSpec = op.acceptsAll(Arrays.asList("f", "json-dir"),"client crashpad").withRequiredArg().ofType(File.class).required();
    //where to write in the database
    accumulatorSpec = op.accepts("accumulator", "hostname:port of accumulation service").withRequiredArg().ofType(String.class).required();
    effortSpec      = op.accepts("effortName", "the study (project) at issue; also a db role").withRequiredArg().ofType(String.class).required();
    dbnameSpec      = op.accepts("dbname", "PI specific database name ").withRequiredArg().ofType(String.class).required();
    // defaulted
    simSpec         = op.acceptsAll(Arrays.asList("n", "simscount"), "number of simulations").withOptionalArg().ofType(Long.class).defaultsTo((long)1000);
    skipSpec        = op.accepts("skipLength", "minimum size of analyzable segment").withOptionalArg().ofType(Integer.class).defaultsTo(SGSRunInterface.ARGS_MINSPAN);
  }

  /**
   * Gets the value of maxWallclock
   *
   * @return the value of maxWallclock
   */
  public final long getMaxWallclock() {
    return maxWallclock;
  }

  /**
   * Sets the value of maxWallclock
   *
   * @param argMaxWallclock Value to assign to maxWallclock
   */
  public final void setMaxWallclock(final long argMaxWallclock) {
    maxWallclock = argMaxWallclock;
  }

  /**
   * Gets the value of startWallclock
   *
   * @return the value of startWallclock
   */
  public final long getStartWallclock() {
    return startWallclock;
  }

  /**
   * Sets the value of startWallclock
   *
   * @param argStartWallclock Value to assign to startWallclock
   */
  public final void setStartWallclock(final long argStartWallclock) {
    startWallclock = argStartWallclock;
  }

  /**
   * Gets the value of retainedIndices
   *
   * @return the value of retainedIndices
   */
  public final int[] getRetainedIndices() {
    return retainedIndices;
  }

  /**
   * Sets the value of retainedIndices
   *
   * @param argRetainedIndices Value to assign to retainedIndices
   */
  public final void setRetainedIndices(final int[] argRetainedIndices) {
    retainedIndices = argRetainedIndices;
  }

  /**
   * Gets the value of writerHost
   *
   * @return the value of writerHost
   */
  public final String getWriterHost() {
    return writerHost;
  }

  /**
   * Sets the value of writerHost
   *
   * @param argWriterHost Value to assign to writerHost
   */
  public final void setWriterHost(final String argWriterHost) {
    writerHost = argWriterHost;
  }

  /**
   * Gets the value of writerPort
   *
   * @return the value of writerPort
   */
  public final int getWriterPort() {
    return writerPort;
  }

  /**
   * Sets the value of writerPort
   *
   * @param argWriterPort Value to assign to writerPort
   */
  public final void setWriterPort(final int argWriterPort) {
    writerPort = argWriterPort;
  }

  /**
   * Sets the value of httpClient
   *
   * @param argHttpClient Value to assign to httpClient
   */
  public final void setHttpClient(final HttpClient argHttpClient) {
    httpClient = argHttpClient;
  }

  public HttpClient getHttpClient() {
    if (httpClient == null) {
      httpClient = HttpClient.newBuilder().build();
    }
    return httpClient;
  }

  /**
   * Gets the value of op
   *
   * @return the value of op
   */
  public final OptionParser getOp() {
    return op;
  }

  /**
   * Sets the value of op
   *
   * @param argOp Value to assign to op
   */
  public final void setOp(final OptionParser argOp) {
    op = argOp;
  }

  /**
   * Gets the value of opSet
   *
   * @return the value of opSet
   */
  public final OptionSet getOpSet() {
    return opSet;
  }

  /**
   * Sets the value of opSet
   *
   * @param argOpSet Value to assign to opSet
   */
  public final void setOpSet(final OptionSet argOpSet) {
    opSet = argOpSet;
  }

  /**
   * Gets the value of s3client
   *
   * @return the value of s3client
   */
  public final InterimFileManagerInterface getInterimManager() {
    return interimManager;
  }

//  public String getS3Base() {
//    return s3Base;
//  }
//
//  public void setS3Base(String s3Base) {
//    this.s3Base = s3Base;
//  }
//
  protected void init(OptionSet ops) throws Exception {
    opSet = ops;
        
    if (opSet.has(helpSpec)) {
      printHelp();
      //throw new RuntimeException("Help wanted");
      System.exit(0);
    }

    if (opSet.has(jsonDirSpec)) {
      setJsonDir(jsonDirSpec.value(opSet));
    }

    setAccumulator(accumulatorSpec);
    //We don't look at these unless accumulating centrally
    if (! (opSet.has(effortSpec) && opSet.has(dbnameSpec)) ) {
      throw new RuntimeException("When using accumulator, we need both of --effort and --dbname");
    }
    setProjectName(effortSpec.value(opSet));
    setDbName(dbnameSpec.value(opSet));
    if (! canConnect() ) {
      logger.error("cannot get to saver, trying {}:{}", getAccumulationHost(), getAccumulationPort());
      throw new RuntimeException(String.format("Could not establish connection to %s:%d", getAccumulationHost(), getAccumulationPort()));
    }

    // Add our psuedo-threadId
    setTag(tag);
    Map<String, Object> argMap = preamble();
    logger.info("{}: delegate to {}", tag, getClass().getName());
    setOptions(argMap);
    initSimulationCount();
    setMinimumSegmentSize();
  }

  @Override
  public long getSimulationCount() {
    return nsims;
  }

  public void initSimulationCount() {
    if  ( ! opSet.has(simSpec)) {
      getOptions().put("simscount", 1000L);
    }
    long simsreq = simSpec.value(opSet);
    if (simsreq == 0) {
      throw new RuntimeException("Zero simulations!");
    }
    setSimulationCount(simsreq);
  }

  @Override
  public File getJsonDir() {
    return jsonDir;
  }

  @Override
  public void setJsonDir(File f) {
    jsonDir = f;
  }

  @Override
  public void setSimulationCount(long n) {
    nsims = n;
  }

  @Override
  public void setOptions(Map<String,Object> omap) {
    optionValueMap = omap;
  }

  @Override
  public Map<String,Object> getOptions() {
    return optionValueMap;
  }

  private File openExistingFile(File filespec, String suffix) {
    File openedFile = filespec;
    if (!openedFile.exists()) {
      String filename = filespec.getAbsolutePath();
      if (!filename.endsWith(suffix)) {
	openedFile = new File(filename + "." + suffix);
	if (!openedFile.exists()) {
	  throw new RuntimeException(String.format("Could not find file for\n\t%s\n\twith or without suffixt = %s", filename, suffix));
	}
      }
    }
    return openedFile;
  }
    
  private void setAccumulator(OptionSpec<String> accumSpec) {
    String full = accumSpec.value(opSet);
    String[] parts = full.split(":");
    if (parts.length != 2) {
      throw new RuntimeException("malformed accumulator specification: " + full);
    }
    setAccumulationHost(parts[0]);
    setAccumulationPort(Integer.parseInt(parts[1]));
  }
    
  protected boolean canConnect() {
    boolean retval = false;
    String weburl = String.format("https://%s:%d", getAccumulationHost(), getAccumulationPort());

    try {
      HttpRequest request = HttpRequest.newBuilder()
	.header("dbrole", getProjectName())
	.header("dbname", getDbName())
	.header("dbhost", System.getProperty("SGSSRVR_databaseHost", "localhost"))
	.uri(URI.create(weburl+"/sgs/webmonitor"))
	.build();
      HttpResponse response = getHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
      retval = response.statusCode() == 200;
    }
    catch (IOException  | InterruptedException ie) {
      retval = false;
      ie.printStackTrace();
    }
    return retval;
  }
    
  private Map<String, Object> preamble() {
    Map<OptionSpec<?>, List<?>> specMap = opSet.asMap();
    Map<String, Object> argMap = new HashMap<>();
    for (Map.Entry<OptionSpec<?>, List<?>> entry : specMap.entrySet()) {
      if (!opSet.has(entry.getKey())) {
	logger.debug("{}: Not using {}", tag, entry.getKey());
	continue;
      }
      //TODO: just the args we care about?
      logger.debug("{} sgs args: arg = {} value = {}", tag, entry.getKey(), entry.getValue());
            
      String key = null;
      Object value;
            
      Collection<String> opstrings = entry.getKey().options();
      //We only need to know which option was in effect, not
      //all possible variations.  We could just grab first
      //one but let's shoot for something more meaningful
      for (String tkey : opstrings) {
	if ( tkey.length() > 1) { // skip the monos
	  key = tkey;
	  break;
	}
      }
      if (key == null) {
	key = opstrings.iterator().next();
      }
      List<?> values = entry.getValue();
      if (values == null || values.size() == 0) {
	value = true;
      }
      else if (values.size() > 1) {
	value = values.toString();
	logger.debug("{} has a list of options: {}", key, value);
      }
      else {
	value = values.get(0);
      }
      argMap.put(key, value);
    }
    logger.debug("{}: user: {}", tag, System.getProperty("user.name"));
    argMap.put("user.name", System.getProperty("user.name"));
        
    return argMap;
  }

  public static List<String> addManifest() {
    ArrayList<String> versionBuffer = new ArrayList<>();
    String classpath = System.getProperty("java.class.path");
    String[] pathparts = classpath.split(System.getProperty("path.separator"));
    List<String> jarList = new ArrayList<>();
    try {
      // look for the pathing jar
      for (String pathEntry : pathparts) {
	if (pathEntry.contains("sgs-pathing")) {
	  JarFile jar = new JarFile(new File(pathEntry));
	  Manifest manifest = jar.getManifest();
	  classpath = manifest.getMainAttributes().getValue(Attributes.Name.CLASS_PATH);
	  for (String ajar : classpath.split("\\s")) {
	    jarList.add(ajar);
	  }
	  jar.close();
	}
	else if (new File(pathEntry).exists()) {
	  jarList.add(pathEntry);
	}
	else {
	  // Do we need to do this at startup?
	  logger.warn("bogus entry in CLASSPATH: {}", pathEntry);
	}
      }

      for (String path : jarList) {
	if (path.contains(".jar")) {
	  JarFile jar = new JarFile(new File(path));
	  Manifest manifest = jar.getManifest();
	  String version = ""+manifest.getMainAttributes().getValue(Attributes.Name.IMPLEMENTATION_VERSION);
	  String component = ""+manifest.getMainAttributes().getValue(Attributes.Name.IMPLEMENTATION_TITLE);
	  versionBuffer.add(String.format("# %s (%s: %s)", path, component, version));
	  jar.close();
	}
	else if (path.contains("build")) {
	  versionBuffer.add(String.format("# WARNING: in devland: %s", path));
	}
      }
    }
    catch (IOException ioe) {
      logger.error("Couldn't handle manifest.", ioe);
    }
    return versionBuffer;
  }

  public final LDModel getLdModel() {
    return ldModel;
  }

  public final void setLdModel(final LDModel argLdModel) {
    ldModel = argLdModel;
  }

  public final String getChromosome() {
    return chromosome;
  }

  public final void setChromosome(final String argChromosome) {
    chromosome = argChromosome;
  }

  public final LinkageDataSet getLinkageDataSet() {
    return linkageDataSet;
  }

  public final void setLinkageDataSet(final LinkageDataSet argLinkageDataSet) {
    linkageDataSet = argLinkageDataSet;
  }

  public final GeneDropper getGeneDropper() {
    return geneDropper;
  }

  public final void setGeneDropper() {
    //todo: check for nulls?
    geneDropper = new GeneDropper(linkageInterface, ldModel, new Random());
  }

  // When chasing, we don't touch the original LI, LD
  public final void setGeneDropper(LinkageInterface LI, LDModel LD) {
    //todo: check for nulls?
    geneDropper = new GeneDropper(LI, LD, new Random());
  }

  public final void setGeneDropper(GeneDropper dropper) {
    //todo: check for nulls?
    geneDropper = dropper;
  }

  public final void setLinkageInterface(final LinkageInterface argLinkageInterface) {
    linkageInterface = argLinkageInterface;
  }

  public LinkageInterface getLinkageInterface() {
    return linkageInterface;
  }
    
  protected int getMeiosesCount() {
    return linkageInterface.raw().getPedigreeData().countMeioses();        
  }

  protected String[] getLocusNameParts(int index) {
    // TODO: do we need to cache the parts?
    return getLocusNameParts(linkageInterface.raw().getParameterData().getLoci()[index]);
  }

  protected String[] getLocusNameParts(LinkageLocus locus) {
    // TODO: do we need to cache the parts?
    String[] nameParts = locus.locName().split("\\|");
    if (nameParts.length < 3) {
      String[] fullname = new String[3];
      fullname[0] = nameParts[0];
      fullname[1] = "0";
      fullname[2] = "nil";
      if (nameParts.length == 2) {
	fullname[1] = nameParts[1];
      }
      nameParts = fullname;
    }
    return nameParts;
  }

  public String probandListAsCsv() {
    LinkageIdSet lis = new LinkageIdSet(linkageInterface.getProbandIdSet());
    return lis.asCSV();
  }

  @Override
  public UUID getTag() {
    if (tag == null) {
      tag = UUID.randomUUID();
    }
    return tag;
  }

  @Override
  public void setTag(UUID t) {
    tag = t;
  }

  public void setMinimumSegmentSize() {
    minimumSegmentSize = skipSpec.value(opSet);
    if ( !  opSet.has(skipSpec)) {
      getOptions().put("skipLength", SGSRunInterface.ARGS_MINSPAN);
    }
  }
        
  @Override
  public void setMinimumSegmentSize(int size) {
    minimumSegmentSize = size;
  }
    
  @Override
  public int getMinimumSegmentSize() {
    return minimumSegmentSize;
  }

  @Override
  public Float getThreshold() {
    return threshold;
  }

  @Override
  public void setThreshold(float t) {
    threshold = t;
  }

  @Override
  public int getConfidence() {
    return confidence;
  }

  @Override
  public void setConfidence(int c) {
    confidence = c;
  }

  @Override
  public void setProjectName(String name) {
    projectName = name;
  }

  @Override
  public String getProjectName() {
    return projectName;
  }

  @Override
  public void setDbName(String name) {
    dbName = name;
  }

  @Override
  public String getDbName() {
    return dbName;
  }

  @Override 
  public String getAccumulationHost() {
    return writerHost;
  }
    
  @Override
  public void setAccumulationHost(String s) {
    writerHost = s;
  }
    
  @Override
  public int getAccumulationPort() {
    return writerPort;
  }

  @Override
  public void setAccumulationPort(int p) {
    writerPort = p;
  }
    
  @Override
  public void formatHelp() {
    op.formatHelpWith(new BuiltinHelpFormatter(120, 4));
  }
    
  @Override
  public void printHelp() throws IOException {

    try {
      op.printHelpOn(System.out);
    }
    catch(RuntimeException mroe) {
      if (! opSet.has(helpSpec) ) {
	mroe.printStackTrace();
      }
    }
  }

  public String getDateTime() {
    Calendar calendarNow = Calendar.getInstance();
    return String.format("%4d.%02d.%02d-%02d:%02d:%02d",
			 calendarNow.get(Calendar.YEAR),
			 calendarNow.get(Calendar.MONTH) + 1,
			 calendarNow.get(Calendar.DAY_OF_MONTH),
			 calendarNow.get(Calendar.HOUR_OF_DAY),
			 calendarNow.get(Calendar.MINUTE),
			 calendarNow.get(Calendar.SECOND));
  }

  protected void buildLinkageDataSet(LinkageParameterData pardata, LinkagePedigreeData peddata) {
    linkageDataSet = new LinkageDataSet(pardata, peddata);
    linkageInterface = new LinkageInterface(linkageDataSet);
  }

  // Now customers deal with these in there own way
  public abstract void setMarkersetId(UUID mid);
  public abstract UUID getMarkersetId();
  public abstract void setPedigreeId(UUID mid);
  public abstract UUID getPedigreeId();

  public abstract void buildLinkageDataSet() throws Exception;
  public abstract SegmentBlock buildSegmentBlock();


  public  void setInterimManager(InterimFileManagerInterface interimFileManager) {
    interimManager = interimFileManager;
  }

  protected void recordAllOptions(StringBuffer heads) {
    TreeSet<String> orderedKeys = new TreeSet<>(getOptions().keySet());
    for (String key : orderedKeys) {
      Object o = getOptions().get(key);
      String mapped;
      if (o instanceof Float )
	mapped = String.format("[%5.3e]", (Float)o);
      else if (o instanceof String)
	mapped = String.format("[\"%s\"]", o);
      else
	mapped = String.format("[%s]", o.toString());
      heads.append(String.format("# SGS_ARG::%s=%s\n", key, mapped));
    }
  }

  // TODO: push to LinkageInterface or thereabouts.
  protected void resetProbands(LinkageIdSet probandSet) {
    if (probandSet.size() == 0) {
      return;
    }
    List<LinkageId<?>> pbList = probandSet.asList();
    linkageDataSet.getPedigreeData().resetProbands(pbList);
    linkageInterface.setProbandsCounted(pbList.size());
  }

  protected void resetProbands() {
    if (trimmingProbands == null || trimmingProbands.size() == 0) {
      logger.error("{}: No id list by which to reset probands");
      return;
    }
    LinkageIdSet lis = new LinkageIdSet( new ArrayList<String>(trimmingProbands));
    resetProbands(lis);
  }

  // TODO: push to LinkageInterface or thereabouts.
  protected static void resetProbands(LinkageIdSet probandSet, LinkageInterface explicitLinkageInterface) {
    List<LinkageId<?>> pbList = probandSet.asList();
    explicitLinkageInterface.raw().getPedigreeData().resetProbands(pbList);
    explicitLinkageInterface.setProbandsCounted(pbList.size());
  }
 
  public AbstractPayload buildPayload(AbstractPayload payload) {
    payload.setClientUsername(System.getProperty("user.name", "UNKNOWN"));
    payload.setProjectName(getProjectName()); //from "effort" arg, TOOD: extend spec with projectname
    payload.setRunTag(getTag());
    payload.setDbName(getDbName()); // aka Principal investigator
    payload.setProcessBlock(buildProcessBlock(payload.getClass().getSimpleName().toLowerCase()));
    payload.setSegmentBlock(buildSegmentBlock());
    //payload.setFilesetBlock(buildFilesetBlock());
    return payload;
  }

  public void shipPayload(AbstractPayload clientPayload) {
    logger.error("in shipper for {}", getTag());
    HttpURLConnection httpConn = null;
    try {
      ObjectMapper objectMapper = JsonMapper.builder().addModule(new JavaTimeModule()).build();
      objectMapper.configure(SerializationFeature.INDENT_OUTPUT, false);
      objectMapper.configure(JsonGenerator.Feature.AUTO_CLOSE_TARGET, false);
      objectMapper.setDefaultPropertyInclusion(JsonInclude.Value.construct(JsonInclude.Include.NON_EMPTY, JsonInclude.Include.ALWAYS));

      //URI payloadURI = URI.create(String.format("http://%s:%d/sgs/payloadsave", getAccumulationHost(),getAccumulationPort()));
      URL payloadURL = new URL(String.format("https://%s:%d/sgs/payloadsave", getAccumulationHost(), getAccumulationPort()));
      httpConn = (HttpURLConnection) payloadURL.openConnection();
      httpConn.setDoOutput(true);
      httpConn.setDoInput(true);
      httpConn.setRequestMethod("POST");
      httpConn.setRequestProperty("User-Agent", "Java Client");
      httpConn.setRequestProperty("Content-Type", "application/json");
      httpConn.addRequestProperty("dbrole", getProjectName());
      httpConn.addRequestProperty("dbname", getDbName());

      DataOutputStream dostream = new DataOutputStream(httpConn.getOutputStream());
      objectMapper.writeValue((OutputStream)dostream, clientPayload);
      if (httpConn.getResponseCode() != 200) {
        logger.error("response {}: {}", httpConn.getResponseCode(), httpConn.getResponseMessage());
        File abortFile = writeLocalJson(clientPayload);
        logger.error("Wrote json for {} to {}", getTag(), abortFile);
	getInterimManager().write(abortFile);
      }
      String httpMsg = httpConn.getResponseCode() == 200 ? "Payload saved" : httpConn.getResponseMessage();
      logger.info("response {}: {}", httpConn.getResponseCode(), httpMsg);
    }
    catch (IOException |  NullPointerException ioe) {
      logger.error("Do we know about host {}? losing {} because {}", getAccumulationHost(), getTag(), ioe.getMessage());
      throw new RuntimeException("Failed to send " +  getTag().toString());
    }
    finally {
      if (httpConn != null) {
        httpConn.disconnect();
      }
    }
  }

  public LinkageParameterData requestParameterData(String target, UUID targetId) throws IOException {
    HttpRequest request = HttpRequest.newBuilder()
      .header(/*AbstractSGSServlet.SGSServlet_HEADER_DBROLE*/"dbrole", getProjectName())
      .header(/*AbstractSGSServlet.SGSServlet_HEADER_DBNAME*/"dbname", getDbName())
      .uri(URI.create(String.format("https://%s:%d/sgs/parfile?%s=%s",
				    getAccumulationHost(), getAccumulationPort(),
				    target, targetId)))
      .build();

    try {
      HttpResponse<InputStream> response = getHttpClient().send(request, HttpResponse.BodyHandlers.ofInputStream());
      if (response.statusCode() != 200) {
        logger.error(response.toString());
      }
      SGSFormatter formatter = new SGSFormatter(response.body(), getMarkersetId());
      LinkageParameterData lparam = new LinkageParameterData(formatter, false);
      setLdModel(new LDModel(formatter));
      return lparam;
    }
    catch (InterruptedException ie) {
      logger.error("{}", ie);
      throw new RuntimeException(ie.getMessage(), ie);
    }
  }

  public LinkagePedigreeData requestPedigreeData(LinkageParameterData param, String pedurl) throws IOException, InterruptedException {
    HttpRequest request = HttpRequest.newBuilder()
      .header("dbrole", getProjectName())
      .header("dbname", getDbName())
      .uri(URI.create(pedurl))
      .build();

    HttpResponse<InputStream> response = getHttpClient().send(request, HttpResponse.BodyHandlers.ofInputStream());
    if (response.statusCode() != 200) {
      logger.error(response.toString());
      throw (new RuntimeException("Failed to get genotypes with at "+pedurl));
    }
    SGSFormatter formatter = new SGSFormatter(response.body(), getMarkersetId());
    LinkagePedigreeData ped = new LinkagePedigreeData(formatter, param);
    if (getPedigreeId() == null) {
      setPedigreeId((UUID) ped.firstPedid().getId());
    }
    return ped;
  }
  
  public File writeLocalJson(AbstractPayload payload) {
    //S3?
    File effectiveJsonDir = getJsonDir().isDirectory() ? getJsonDir() : getJsonDir().getParentFile();
    String filenameFormat = "%s_%s_%02d.%s.%d.json.gz";  //STATIC?
    String filename = String.format(filenameFormat,
				    getDbName(),
				    getProjectName(),
				    Integer.parseInt(getChromosome()),
				    getTag().toString(),
				    getSimulationCount());
    File jsonFile = new File(effectiveJsonDir, filename);
    if (jsonFile.exists()) {
      return jsonFile;
    }

    try (FileOutputStream jsonStream = new FileOutputStream(jsonFile);
         GZIPOutputStream gzipper = new GZIPOutputStream(jsonStream)) {
      payload.asJson(gzipper);
    }
    catch (IOException ioe) {
      logger.error ("Complete write failure of payload send {}", payload.getRunTag(), ioe);
      throw new RuntimeException("hoping for oldstyle save", ioe);
    }
    return jsonFile;
  }

  public String getPedigreeName() {
    // relies on ped id as uuid
    String fullname = getPedigreeId().toString();
    return fullname.substring(0, fullname.indexOf("."));
  }

  private Supplier<InputStream> payloadAsStream(AbstractPayload payload, PipedInputStream pis) throws IOException {
    return new Supplier<InputStream>() {
      @Override
      public InputStream get() {
        try {
          ObjectMapper objectMapper = new ObjectMapper();
          PipedOutputStream pos = new PipedOutputStream();
          objectMapper.writeValue(pos, payload);
          pos.connect(pis);
          return pis;
        } catch (IOException ioe) {
          ioe.printStackTrace();
        }
        return pis;
      };
    };
  }

  private ProcessBlock buildProcessBlock(String procType) {
    ProcessBlock procblock = new ProcessBlock();
    try {
      Set<String> pedparTags = new HashSet<>(Arrays.asList("d", "pedfile", "p", "parfile"));

      procblock.initProcess(procType);
      //TODO: get the ped par ids
      // procblock.addInput("pedfile", getPedFile().toURI().toString());
      // procblock.addInput("parfile", getParFile().toURI().toString());
      for (Map.Entry<String, Object> mapEntry : getOptions().entrySet()) {
	if ( mapEntry.getValue() instanceof String ) {
	  procblock.addTextArg( mapEntry.getKey(), (String) mapEntry.getValue() );
	}
	else if ( mapEntry.getValue() instanceof Integer || mapEntry.getValue() instanceof Long) {
	  procblock.addIntArg( mapEntry.getKey(), mapEntry.getValue().toString() );
	}
	else if ( mapEntry.getValue() instanceof Float) {
	  procblock.addFloatArg( mapEntry.getKey(), mapEntry.getValue().toString() );
	}
	else if (mapEntry.getValue() instanceof UUID ) {
	  if (! pedparTags.contains(mapEntry.getKey()) ) {
	    procblock.addTextArg( mapEntry.getKey(), mapEntry.getValue().toString() );
	  }
	}
	else { // Void
	  procblock.addVoidArg( mapEntry.getKey() );
	}
      }
      for (String jarname : addManifest()) {
	procblock.addTextArg("classpath entry", jarname);
      }
    }
    catch (Exception e) {
      logger.error("payload fail in process block", e);
    }
    return procblock;
  }
}
