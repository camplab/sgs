package edu.utah.camplab.sgs;

import com.jonoler.longpowerset.LongPowerSet;
import com.jonoler.longpowerset.LongSet;

import java.io.*;
import java.util.*;

import edu.utah.camplab.jx.PayloadFromMux;
import edu.utah.camplab.tools.LinkageIdSet;
import edu.utah.camplab.tools.SGSDebug;
import edu.utah.camplab.tools.SGSDebugEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jpsgcs.linkage.LinkageId;
import jpsgcs.linkage.LinkageInterface;
import jpsgcs.linkage.LinkagePedigreeData;
import jpsgcs.linkage.LinkageParameterData;
import jpsgcs.pedapps.ThreadedGeneDropper;

import edu.utah.camplab.jx.AbstractPayload;
import edu.utah.camplab.jx.SegmentBlock;

import edu.utah.camplab.tools.MeiosesCounter;

import joptsimple.OptionSet;
import joptsimple.OptionSpec;

public class SGSMultiAssess extends AbstractSGSOptions {

  public static final Logger logger = LoggerFactory.getLogger(SGSMultiAssess.class);
  public static final int MUX_CUTOFF = 4; // multi-thread with at least this many

  private File		lastLocalJson  = null;
  private File          probandSetsFile;
  private Integer       minMeioses     = null;
  private long          requestedSims  = 0;
  private long          interimSize    = 0;
  private int           coresAvailable = 0;
  private int           muxCutoff      = 0;

  private UUID markersetId;
  private UUID pedigreeId;
    
  private List<LinkageIdSet>                   probandSets         = null;
  private List<LinkageId<?>>                   originalProbandIds  = null;
  private HashMap<LinkageIdSet,DropEvaluation> sgsrunEvaluationMap = null;

  private OptionSpec<Integer>  minMeiosesSpec;
  private OptionSpec<File>     probandSetsSpec;
  private OptionSpec<Integer>  muxCutoffSpec;
  private OptionSpec<String>   markersetIdSpec;
  private OptionSpec<String>   pedigreeIdSpec;

  public SGSMultiAssess() {
    super();
    minMeiosesSpec  = op.accepts(SGS_OPT_MEIOSES, "MultiAssess: minimum number of meioses amongst probands").withRequiredArg().ofType(Integer.class).defaultsTo(0);
    probandSetsSpec = op.accepts(SGS_OPT_PROBANDSETS, "name of file of proband sets").withRequiredArg().ofType(File.class);
    muxCutoffSpec   = op.accepts(SGS_OPT_CUTOFFMULTI, "minimum proband count forcing multithreaded assessment").withRequiredArg().ofType(Integer.class).defaultsTo(SGSMultiAssess.MUX_CUTOFF);
    markersetIdSpec = op.accepts(SGS_OPT_MARKERSET, "database id of the markerset for this run").withRequiredArg().ofType(String.class).required();
    pedigreeIdSpec  = op.accepts(SGS_OPT_PEDIGREE, "database id of the pedigree for this run").withRequiredArg().ofType(String.class).required();
    //resultDirSpec   = op.acceptsAll(Arrays.asList("f", "report"), "interim file directory").withRequiredArg().ofType(File.class);
  }

  @Override
  protected void init(OptionSet opSet) throws Exception {
    super.init(opSet);
    setMaxWallclock(opSet.has(clockSpec) ? clockSpec.value(opSet) : 0);
    setMuxCutoff( opSet.has(muxCutoffSpec) ? muxCutoffSpec.value(opSet) : SGSMultiAssess.MUX_CUTOFF);
    coresAvailable = Runtime.getRuntime().availableProcessors();
    setInterimSize(getSimulationCount());
    setMinMeioses(opSet.has(minMeiosesSpec) ? minMeiosesSpec.value(opSet) : 0);
    setMarkersetId(UUID.fromString(markersetIdSpec.value(opSet)));
    setPedigreeId(UUID.fromString(pedigreeIdSpec.value(opSet)));
    //setInterimManager("interim");
  }

  public void init(String[] args) throws Exception {
    init(op.parse(args));
  }
    
  /**
   * Gets the value of marksetName
   *
   * @return the value of marksetName
   */
  public final UUID getMarkersetId() {
    return markersetId;
  }

  /**
   * Sets the value of marksetName
   *
   * @param mid Value to assign to marksetName
   */
  public final void setMarkersetId(final UUID mid) {
    markersetId = mid;
  }

  /**
   * Gets the value of pedigreeName
   *
   * @return the value of pedigreeName
   */
  public final UUID getPedigreeId() {
    return pedigreeId;
  }

  /**
   * Sets the value of pedigreeName
   *
   * @param pid Value to assign to pedigreeName
   */
  public final void setPedigreeId(final UUID pid) {
    pedigreeId = pid;
  }

  public void setInterimSize(long simsRequested) {
    interimSize = simsRequested > 10 ? (simsRequested / 10) : 1;
  }

  public long getInterimSize() {
    if ( interimSize == 0 ) {
      setInterimSize(getSimulationCount());
    }
    return interimSize;
  }

  @Override
  public void run() throws Exception {
    buildLinkageDataSet();
//    if (isDebug()) {
//      writeGendropData(0);
//    }
    originalProbandIds = getLinkageInterface().getProbandIdSet();  // "original" post trim
    logger.info("Begin Multi-Assess with {} probands.", originalProbandIds.size());
    requestedSims = getSimulationCount();  // we tromp on the real one throughout
    initResultVector();

    logger.info("Start MultiAssessment tag={}", getTag());
    setChromosome(getLocusNameParts(getLinkageInterface().raw().getParameterData().getLoci()[0])[2]); //just rediculous
    runSimulations();
  }

  public void buildLinkageDataSet() throws IOException, InterruptedException {
    LinkageParameterData param = requestParameterData();
    LinkagePedigreeData ped = requestPedigreeData(param);
    buildLinkageDataSet(param, ped);
    // trimming
    if ( opSet.has(probandSetsSpec)) {
      LinkageIdSet lis = new LinkageIdSet(linkageInterface.getProbandIdSet());
      if (opSet.has(trimSpec)) {
	logger.error("{}: using both --trim and --probandsets", getTag());
	//trimmings set in super
	if (! trimmingProbands.containsAll(lis.asSet())) {
	  logger.error("{}: --trim probands {} does not include all probands in --probandSets {}", getTag(), trimmingProbands, lis);
	  throw new RuntimeException("--trim probands does not include all probands in --probandSets");
	}
      }
    }
    if (trimmingProbands != null && trimmingProbands.size() > 0) {
      resetProbands(new LinkageIdSet(new ArrayList<>(trimmingProbands)));
    }
  }

  public void setMinMeioses(int m) {
    minMeioses = m;
  }

  public int getMinMeioses() {
    return minMeioses == null ? 0 : minMeioses;
  }

  public void reportAll(long sims) {
    AbstractPayload payload = null;
    try {
      payload = buildPayload(new PayloadFromMux());
      if (payload == null) {
	logger.error("{}: JSON PAYLOAD NOT (errorlessly) GENERATED. Writing locally at {}", getTag(), getJsonDir());
	//try the old fashioned way
	return;
      }
      payload.setMarkersetId(getMarkersetId());
      payload.setPeopleId(getPedigreeId());

      if ( maxTimeExceeded() ) {
	payload.getProcessBlock().addIntArg("wall-clock simulations", Long.toString(sims));
      }
      shipPayload(payload);
    }
    catch (Exception e) {
      logger.error("Trouble in reportAll: {}", e.getMessage(), e);
      if (payload == null) {
	// try the old fashioned way AGAIN
	logger.error("{}: JSON PAYLOAD NOT GENERATED (without error). Writing locally at {}", getTag(), getJsonDir(), e);
      }
      else {
	File jfile = writeLocalJson(payload);
	getInterimManager().write(jfile);
	logger.error("{}: payload written to {}", getTag(), getJsonDir());
      }
    }
    resetProbands(new LinkageIdSet(originalProbandIds));
  }

  public static DropEvaluation createDropEval(LinkageIdSet probandSet, File outDir, LinkageInterface li, int smallest) {
    return new DropEvaluation(li, smallest);
  }

  @Override
  public SegmentBlock buildSegmentBlock() {
    SegmentBlock segblock = new SegmentBlock();
    int totalsegments = 0;
    for (Map.Entry<LinkageIdSet, DropEvaluation> entry : sgsrunEvaluationMap.entrySet()) {
      LinkageIdSet ids = entry.getKey();
      DropEvaluation drop = entry.getValue();
      int chrnum = Integer.parseInt(getChromosome());
      String segsetName = String.format("%s:%d", ids.asCSV(), chrnum);
      segblock.addSegmentset(ids.asCSV(), segsetName);
      totalsegments += drop.getSharingRunList().size();
      for (SharingRun srun : drop.getSharingRunList()) {
	segblock.addSegment(ids.asCSV(),
			    chrnum,
			    srun.getStartbase(),
			    srun.getEndbase(),
			    srun.getFirstmarker(),
			    srun.getLastmarker(),
			    srun.getEventsLess(),
			    srun.getEventsEqual(),
			    srun.getEventsGreater(),
			    (int)(srun.getEventsEqual() + srun.getEventsGreater()));
      }
    }
    logger.debug("{} has total of {} segments", getTag(), totalsegments);
    return segblock;
  }

  public void setMuxCutoff(int probandCount) {
    // The decision on using F/J is based on the number of subsets
    muxCutoff = (int)(Math.pow(2,probandCount)) - (probandCount+1);
  }

  private void runSimulations() {
    setGeneDropper(new ThreadedGeneDropper(getLinkageInterface(),
					   getLdModel(),
					   new Random(),
					   coresAvailable));
    int reportInterval = 10000;  //TODO: make a property for this...

    int jobSize = probandSets.size()/coresAvailable + 1;
    long analStart = 0;
    long analTime = 0;

    // just for peace of mind
    int indivs = getLinkageInterface().raw().getPedigreeData().getIndividuals().length;
    long simStart = 0;
    long simTime = 0;
    logger.info("{}: Begin simulations on {} probands amongst {} individuals", getTag(), getLinkageInterface().nProbands(), indivs);
    logger.info("{}: maxtime = {}", getTag(), getMaxWallclock());
    if (SGSDebug.isDebug(SGSDebugEnum.WRITELINKAGE)) {
      writeParameterData(getLinkageDataSet().getParameterData());
    }
    for (int i = 1; i <= requestedSims; i++) {
      if (sgsrunEvaluationMap.size() == 0) {
	break;
      }
      if (maxTimeExceeded()) {
	// we hit the wall clock
	logger.error("{}: hit max time {} on simulation {}", getTag(), getMaxWallclock(), i);
	break;
      }
      simStart = System.currentTimeMillis();
      getGeneDropper().geneDrop();
      if (SGSDebug.isDebug(SGSDebugEnum.WRITEGDROP)) {
        writeGendropData(i);
      }
      simTime += System.currentTimeMillis() - simStart;
      analStart = System.currentTimeMillis();
      if (sgsrunEvaluationMap.entrySet().size() >= muxCutoff) {
	if (i == 1) {
	  logger.debug("begin F/J based analysis on {} people, {} probands", indivs, getLinkageInterface().nProbands());
	}
	LITuple.resetLinkage(getLinkageInterface());
	MultiAssessExecutor maex = new MultiAssessExecutor(probandSets, sgsrunEvaluationMap, 0, probandSets.size(), jobSize, getMinimumSegmentSize());
	maex.assessAll();
      }
      else {
	for (Map.Entry<LinkageIdSet, DropEvaluation> entry : sgsrunEvaluationMap.entrySet()) {
	  resetProbands(entry.getKey());
	  collectDropStatistics(entry.getValue());
	}
      }
      analTime += System.currentTimeMillis() - analStart;
      setSimulationCount(i);
      if (i % reportInterval == 0) {  
	logger.info("{}: avg times simulation:{}ms collect:{} ms; on drop #{}", getTag(), simTime/reportInterval, analTime*1.0/reportInterval, i);
	simTime = 0;
	analTime = 0;
      }
      if (isIntermimDrop()) {
	reportInterim(i);
      }
    }
    logger.info("{}: Finished simulations", getTag());
    reportAll(getSimulationCount());
  }
  private void writeGendropData(int rep) {
    try (PrintStream ps = new PrintStream(new BufferedOutputStream(new FileOutputStream(new File(getJsonDir(), "sim."+rep))))) {
      getLinkageDataSet().getPedigreeData().writeTo(ps);
      ;
    }
    catch (IOException ioe) {
      ;
    }
  }

  private void writeParameterData(LinkageParameterData lpard) {
    try (PrintStream ps = new PrintStream(new BufferedOutputStream(new FileOutputStream(new File(getJsonDir(), "pardata.ld"))))) {
      getLinkageDataSet().getParameterData().writeTo(ps);
      getLdModel().writeTo(ps);
    }
    catch(IOException exception  ) {
      ;
    }
  }

  private LinkageParameterData requestParameterData() throws IOException {
    return requestParameterData("markerset", getMarkersetId());
  }
    
  private LinkagePedigreeData requestPedigreeData(LinkageParameterData param) throws IOException, InterruptedException {
    String dbUrl = String.format("https://%s:%d/sgs/pedfile?people=%s&markerset=%s",
				 getAccumulationHost(),
				 getAccumulationPort(),
				 getPedigreeId(),
				 getMarkersetId());
    
    return requestPedigreeData(param, dbUrl);
  }

  private void reportInterim(int simsCompleted) {
    long writetime = System.currentTimeMillis();
    AbstractPayload payload = buildPayload(new PayloadFromMux());
    File currentLocalJson = writeLocalJson(payload);
    if (lastLocalJson != null && !currentLocalJson.equals(lastLocalJson) && lastLocalJson.exists()) {
      lastLocalJson.delete();
      //TODO delete from s3
    }
    lastLocalJson = currentLocalJson;
    resetProbands(new LinkageIdSet(originalProbandIds));
    getInterimManager().write(currentLocalJson);
    logger.info("{}: Writing interim files after {} simulations takes {} ms", getTag(), simsCompleted, System.currentTimeMillis() - writetime);
  }

  private void collectDropStatistics(DropEvaluation eval) {
    eval.assessSimulation(linkageInterface, getMinimumSegmentSize());
  }

  private static boolean hasMinMeioses(LinkageIdSet idSet, LinkageInterface li, int minMeioses) {
    if (minMeioses <= 1) {
      return true;
    }
    AbstractSGSRun.resetProbands(idSet, li);
    MeiosesCounter cm = new MeiosesCounter(li);
    if (cm.getCount(0) < minMeioses) {
      return false;
    }
    return true;
  }

  private static List<LinkageIdSet> generateProbandSets(LinkageInterface li, int minMeioses) {
    List<LinkageIdSet> probandSets = new ArrayList<>();
    LinkageIdSet lis = new LinkageIdSet(li.raw().getPedigreeData());
    LongSet<Set<LinkageId<?>>> powerSet = LongPowerSet.create(lis.asSet());
    for (Set<LinkageId<?>> pset : powerSet) {
      List<LinkageId<?>> idList = new ArrayList<LinkageId<?>>();
      idList.addAll(pset);
      LinkageIdSet probandSet = new LinkageIdSet(idList);
      if (probandSet.size() > 1 && hasMinMeioses(probandSet, li, minMeioses)) {
	probandSets.add(probandSet);
      }
    }
    return probandSets;
  }

  private static List<LinkageIdSet> loadProbandsFromSetsFile(File setsFile, LinkageInterface li, int minMeioses) {
    assert setsFile != null;

    List<LinkageIdSet> probandSets = new ArrayList<>();
    try (BufferedReader reader = new BufferedReader(new FileReader(setsFile))) {
      String inline = reader.readLine();
      while (inline != null) {
	LinkageIdSet idset = new LinkageIdSet(inline);
	if (idset.size() > 1 && hasMinMeioses(idset, li, minMeioses)) {
	  probandSets.add(idset);
	}
	inline = reader.readLine();
      }
    }
    catch (IOException ioe) {
      logger.error(ioe.getMessage());
      throw new RuntimeException("Failed to load proband sets from sets file.", ioe);
    }

    return probandSets;
  }

  private void loadProbandSets() {
    if (probandSetsFile != null) {
      probandSets = loadProbandsFromSetsFile(probandSetsFile, linkageInterface, getMinMeioses());
      resetProbands(new LinkageIdSet(trimmingProbands));
    } else {
      probandSets = generateProbandSets(linkageInterface, getMinMeioses());
    }
  }

  private void initResultVector() throws IOException {
    long startTime = System.currentTimeMillis();
    loadProbandSets();

    logger.debug("Initialize result vector from {} individuals, {} probands", getLinkageInterface().nIndividuals(), getLinkageInterface().nProbands());
    LITuple.resetLinkage(getLinkageInterface());
    sgsrunEvaluationMap = new HashMap<LinkageIdSet, DropEvaluation> ();
    if (probandSets.size() < muxCutoff) {  //We've recently switched to preferring mux'ing at 4 probands
      logger.info("NOT Using MT processing");
      for (LinkageIdSet lis : probandSets) {
	DropEvaluation evaluator = null;
	resetProbands(lis, getLinkageInterface());
	evaluator = createDropEval(lis, getJsonDir(), getLinkageInterface(), getMinimumSegmentSize());
	sgsrunEvaluationMap.put(lis, evaluator);
      }
    }
    else {
      logger.info("Using MT processing");
      MATupleInitExecutor matr = new MATupleInitExecutor(probandSets, 0, probandSets.size(),
							 probandSets.size() > coresAvailable ? probandSets.size()/coresAvailable : 1,
							 getJsonDir(), getMinimumSegmentSize());
      sgsrunEvaluationMap = matr.generate();
    }
    logger.info("{}: total result vector ({} elements) setup takes {} ms", getTag(),  sgsrunEvaluationMap.size(), System.currentTimeMillis() - startTime);
  }

  private boolean isIntermimDrop()  { return getSimulationCount() % getInterimSize() == 0;}
  private boolean maxTimeExceeded() {return getMaxWallclock() > 0 && (((System.currentTimeMillis() / 1000) - startWallclock) >= maxWallclock);}
}
