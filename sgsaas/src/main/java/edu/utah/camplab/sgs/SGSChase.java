package edu.utah.camplab.sgs;

import com.fasterxml.jackson.databind.ObjectMapper;

import edu.utah.camplab.db.crafted.SegmentExtended;
import edu.utah.camplab.db.generated.default_schema.tables.pojos.Segment;
import edu.utah.camplab.jps.SGSFormatter;
import edu.utah.camplab.jx.SegmentBlock;
import edu.utah.camplab.jx.AbstractPayload;
import edu.utah.camplab.jx.PayloadFromChase;

//import edu.utah.camplab.servlet.AbstractSGSServlet;
import edu.utah.camplab.tools.SGSDebug;
import edu.utah.camplab.tools.SGSDebugEnum;
import joptsimple.OptionSet;
import joptsimple.OptionSpec;

import jpsgcs.linkage.LinkageDataSet;
import jpsgcs.linkage.LinkageInterface;
import jpsgcs.linkage.LinkageParameterData;
import jpsgcs.linkage.LinkagePedigreeData;
import jpsgcs.pedmcmc.LDModel;
import jpsgcs.markov.MultiVariableMaxStatesException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.URI;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodySubscriber;
import java.net.http.HttpResponse.BodySubscribers;
import java.util.*;
import java.util.function.Supplier;


//TODO: rename to SGSChase, make a separate genotype-generator, old-style "run"(maybe)
public class SGSChase extends AbstractSGSOptions {

  final static Logger		logger		 = LoggerFactory.getLogger(SGSChase.class);

  //extern kill check
  public static volatile Boolean SGS_NOKILL = true;

  // The distilled result
  private DropEvaluation	dropEvaluation = null;

  private Segment		originalSegment;
  private ObjectMapper          objectMapper = null;

  // The position of the first marker of the segment in the reduced
  // LinkageDataSet supplied from the database
  private int reindexFirstMarker = 0;
    
  private  BufferedWriter	resultWriter     = null;
  private  UUID			segmentId	 = null;
  private  UUID			markersetId	 = null;
  private  UUID			pedigreeId	 = null;

  // CLI options
  private final OptionSpec<String>	segmentSpec;
  private final OptionSpec<Integer>	confidenceSpec;
  private final OptionSpec<Float>	thresholdSpec;

  public record reducedLINKAGE(LinkageInterface iface, LDModel model) {}

  public SGSChase() {
    super();
    segmentSpec = op.accepts(SGS_OPT_SEGMENT, "UUID of segment").withRequiredArg().ofType(String.class).required();
    confidenceSpec = op.accepts(SGS_OPT_CONFIDENCE, "Confidence level for p-value bounds:1=95%;2=99%;3=99.9")
      .withOptionalArg().ofType(Integer.class).defaultsTo(1);
    thresholdSpec = op.accepts(SGS_OPT_THRESHOLD, "Genome-wide threshold (suggestive or significant) against which we compare p-values")
      .withRequiredArg().ofType(Float.class);
  }
    
  @Override
  protected void init(OptionSet opSet) throws Exception {
    super.init(opSet);
        
    setMaxWallclock(opSet.has(clockSpec) ? clockSpec.value(opSet) : 0);
    setSegmentId(UUID.fromString(segmentSpec.value(opSet)));
    maybeSetThreshold(thresholdSpec, confidenceSpec);
    //setInterimManager("chase");
  }
    
  public void init(String[] args) throws Exception {
    init(op.parse(args));
  }


  public UUID getSegmentId() {
    return segmentId;
  }

  public void setSegmentId(UUID id) { 
    segmentId = id;
  }

  public final UUID getMarkersetId() {
    return markersetId;
  }

  public final void setMarkersetId(final UUID markersetId) {
    this.markersetId = markersetId;
  }

  public final UUID getPedigreeId() {
    return pedigreeId;
  }

  public final void setPedigreeId(final UUID pedigreeId) {
    this.pedigreeId = pedigreeId;
  }

  public Segment getOriginalSegment() {
    return originalSegment;
  }

  private void maybeSetThreshold(OptionSpec<Float> tspec, OptionSpec<Integer>cspec) {
    if (cspec != null && tspec == null) {
      logger.error("{}: Must use --threshold if using --confidence.", getTag());
      throw new RuntimeException("Must use --threshold if using --confidence.");
    }
    if (opSet.has(tspec)) {
      setThreshold(tspec.value(opSet));
      if (opSet.has(cspec)) {
	if ( 0 < cspec.value(opSet) && cspec.value(opSet) < 4)
	  setConfidence(cspec.value(opSet));
	else
	  throw new RuntimeException("Illegate choice of confidence interval" + cspec.value(opSet));
      }
      // force in confidence default
      getOptions().put("confidence", 1);
    }
  }

  @Override
  public void run() throws Exception {
    float secPerMin = 60.0f;
    // Trap SIGTERM
    SGSShutdownHandler shutdowner = new SGSShutdownHandler(Thread.currentThread());
    Runtime.getRuntime().addShutdownHook(shutdowner);
    shutdowner.join();
    // Hit the database
    requestSegment();
    buildLinkageDataSet();
    if (SGSDebug.isDebug(SGSDebugEnum.WRITELINKAGE)) {
      try (PrintStream peds = new PrintStream(new BufferedOutputStream(new FileOutputStream(new File(getJsonDir(),getOriginalSegment().getId()+"-chase.ped"))));
	   PrintStream pars = new PrintStream(new BufferedOutputStream(new FileOutputStream(new File(getJsonDir(),getOriginalSegment().getId()+"-chase.par"))))) {
	getLinkageDataSet().getPedigreeData().writeTo(peds);
	getLinkageDataSet().getParameterData().writeTo(pars);
	getLdModel().writeTo(pars);
      }
      catch (IOException ioe) {
	;
      }
    }
    // Set inital state from the data
    reducedLINKAGE reduced = reduceChromosome();
    gatherInitialObserved(reduced);
    // call simulation loop (sets the genedropper)
    SimLoopReport simLoopReport = simLoop(reduced);
    //Done simulating
    // reset nsims to that actually done in this session
    setSimulationCount(simLoopReport.currentSim - getTotalSims(originalSegment));
    extractOriginalCounts();

    logger.info("{}: {} simulations generated in {} minutes", 
		getTag(), 
		getSimulationCount(), 
		(System.currentTimeMillis()/1000 - startWallclock) / secPerMin);
    AbstractPayload payload = buildPayload(new PayloadFromChase());
    payload.setPeopleId(getPedigreeId());
    payload.setMarkersetId(originalSegment.getMarkersetId());
    if (SGSDebug.isDebug(SGSDebugEnum.DROPEVAL)) {
      File jsonFile = writeLocalJson(payload);
      getInterimManager().write(jsonFile);
    }
    shipPayload(payload);

    logger.info("{}: Finished run. ", getTag());
  }

  public reducedLINKAGE reduceChromosome(int fullIndex) { // for full ped/par from file...
    reindexFirstMarker = fullIndex;
    return reduceChromosome();
  }

  public reducedLINKAGE reduceChromosome() {
    //
    boolean isReduced = false;
    int tries = 0;
    long startMillis = System.currentTimeMillis();
    SharingRun srun = new SharingRun(originalSegment, originalSegment.getFirstmarker());  // Leave the original alone
    //We know the segment is within the padded bounds of the supplied LinkageDataSet
    int lastMarkerIndex = getLinkageInterface().nLoci() - 1;
    int upperBound = srun.getLastmarker() == lastMarkerIndex ? lastMarkerIndex : srun.getLastmarker() + 1;
    // get the stop marker at pter of segment or zero
    int lowerBound = srun.getFirstmarker() == 0 ? 0 : srun.getFirstmarker() - 1;
    reducedLINKAGE reducer = null;
    while (! isReduced && tries++ < 3) {
      // We'll use this for a wider starting clump; NOTE reduce number of loops by one
      TreeSet<Integer> retainedMarkers = new TreeSet<>();
      // We want to include the bounding markers, if available
      for (int i = lowerBound; i <= upperBound; i++) {
        retainedMarkers.add(i);
      }
      // TODO: use set to array op?
      int[] retainedIndices = new int[retainedMarkers.size()];
      logger.info("{}: retained {} of {} markers", getTag(), retainedMarkers.size(), lastMarkerIndex+1);
      int reti = 0;
      for (Integer origIndex  : retainedMarkers) {
        retainedIndices[reti++] = origIndex;
      }
      try {
        LinkageDataSet reducedDataSet = new LinkageDataSet(linkageDataSet, retainedIndices);
        LinkageInterface reducedInterface = new LinkageInterface(reducedDataSet);
        // Next two lines are equivalent in effect. Take your pick.  Here we choose the destructive option.
        // ldModel.peelTo(retainedIndices);
        LDModel reducedLDModel = new LDModel(ldModel, retainedIndices);
        if (SGSDebug.isDebug(SGSDebugEnum.WRITEPEEL)) {
          try (PrintStream ps = new PrintStream(new BufferedOutputStream(new FileOutputStream(new File(getJsonDir(), getTag()+"-peel.ped"))));
               PrintStream ls = new PrintStream(new BufferedOutputStream(new FileOutputStream(new File(getJsonDir(), getTag()+"-peel.ld")))))
            {
              reducedDataSet.getPedigreeData().writeTo(ps);
              ps.close();
              reducedDataSet.getParameterData().writeTo(ls);
              reducedLDModel.writeTo(ls);
              ls.close();
            }
          catch(IOException ioe) {
            ;
          }
        }
        logger.debug("{}: chromosome reduction took {}ms", getTag(), System.currentTimeMillis() - startMillis);
        isReduced = true;
        reducer = new reducedLINKAGE(reducedInterface, reducedLDModel);
      }
      catch (MultiVariableMaxStatesException uoe) {
        logger.error("Failed try #{} on segment {}: {}", tries, originalSegment.getId(), uoe);
        // reset the ends
      }
      upperBound += REDUCE_INCR_STEP;
      if (upperBound > lastMarkerIndex) {upperBound = lastMarkerIndex;}
      lowerBound -= REDUCE_INCR_STEP;
      if (lowerBound < 0) {lowerBound = 0;}
    }
    if (! isReduced ) {
      String msg = String.format("INTRACTABLE SEGMENT ped=%s, par=%s, segment=%s",
                                 getPedigreeId(),
                                 getMarkersetId(),
                                 getSegmentId());
      logger.error(msg);
      throw new RuntimeException(msg);
    }
    return reducer;
  }

  public BufferedWriter getResultWriter() throws IOException {
    if (resultWriter == null) {
      if (getJsonDir() == null) {
  	// Lord knows where...
  	return openBufferedWriter();
      }
    }
    resultWriter =  openBufferedWriter(getJsonDir());
    return resultWriter;
  }

  public void requestSegment() {
    HttpRequest request = HttpRequest.newBuilder()
      .header("Accept", "application/json")
      .header("dbrole", getProjectName())
      .header("dbname", getDbName())
      .uri(URI.create(String.format("https://%s:%d/sgs/segment?segmentid=%s",
				    getAccumulationHost(),
				    getAccumulationPort(),
				    getSegmentId())))
      .build();

    try {
      HttpResponse<Supplier<SegmentExtended>> response =
	getHttpClient().send(request, responseInfo -> asJSON(SegmentExtended.class));

      if (response.statusCode() != 200) {
	logger.error(response.toString());
	throw new InterruptedException("trouble getting segment: " + response.statusCode());
      }
      else {
	SegmentExtended sext = response.body().get();
	originalSegment = sext.getSegment();
	setMarkersetId(originalSegment.getMarkersetId());
	setPedigreeId(sext.getPeople().getId());
	setChromosome(originalSegment.getChrom() + "");
      }
    }
    catch(IOException | InterruptedException ioe) {
      logger.error("cannot get segment for {} {}", getSegmentId(), ioe);
      System.exit(2);
    }
  }

  public <W> BodySubscriber<Supplier<W>> asJSON(Class<W> targetType) {
     BodySubscriber<InputStream> upstream = BodySubscribers.ofInputStream();

     BodySubscriber<Supplier<W>> downstream =
       BodySubscribers.mapping(upstream, (InputStream is) -> () -> {
               try (InputStream stream = is) {
                   return getObjectMapper().readValue(stream, targetType);
               } catch (IOException e) {
                   throw new UncheckedIOException(e);
               }
           });
    return downstream;
  } 
  
  private LinkagePedigreeData requestPedigreeData(LinkageParameterData param) throws IOException, InterruptedException {
    String agentUrl = String.format("https://%s:%d/sgs/pedfile?segment=%s",
                                    getAccumulationHost(),
                                    getAccumulationPort(),
                                    getSegmentId());
    return requestPedigreeData(param, agentUrl);
  }

  public void gatherInitialObserved(reducedLINKAGE RL) {
    int originalLength = originalSegment.getLastmarker() - originalSegment.getFirstmarker() + 1;
    dropEvaluation = new DropEvaluation(RL.iface, originalLength);
    initializeOriginalCounts();
  }

  private void initializeOriginalCounts() { 
    SharingRun testSegment = dropEvaluation.getSharingRunList().get(0);
    // We're touching the first and only segment in the dropEvaluation vector
    testSegment.plusRun(originalSegment);
  }

  private BufferedWriter openBufferedWriter() throws IOException {
    File lfile = genTempFile("sgs", ".pval");
    logger.error("No output file specified, using {}", lfile.getAbsolutePath());
    return openBufferedWriter(lfile, false);
  }

  private BufferedWriter openBufferedWriter(File file) throws IOException {
    return openBufferedWriter(file, false);
  }

  private BufferedWriter openBufferedWriter(File file, boolean appending) throws IOException {
    return new BufferedWriter(new FileWriter(file, appending));
  }

  private File genTempFile(String stub, String suffix) throws IOException{
    File pwd = new File(System.getProperty("user.dir", "."));
    return File.createTempFile(stub, suffix, pwd);
  }

  private void printSimGenotypes(LinkageInterface li) {
    try {
      BufferedWriter gtWriter = openBufferedWriter(genTempFile("simulated.", ".ped"));
      int lociCount = li.nLoci();
      StringBuffer buf;
      // if (false)
      { // TODO: make this CLI option?
	buf = new StringBuffer("#ped\tego\tpa\tma\tnms\tnps\tfos\tgender\tproband");
	gtWriter.write(buf.toString());
	for (int l = 0; l < lociCount; l++) {
	  String lname = getLocusNameParts(l)[0];
	  gtWriter.write("\t" + lname);
	}
	gtWriter.write("\n");
	gtWriter.flush();
      }

      int indivCount = li.nIndividuals();
      for (int j = 0; j < indivCount; j++) {
	String pedline = String.format("%s\t%s\t%s\t%s\t0\t0\t0\t%d\t%d", li.pedigreeName(j), li.individualName(j), li.pa(j) == -1 ? 0
				       : li.individualName(li.pa(j)), li.ma(j) == -1 ? 0 : li.individualName(li.ma(j)), li.isMale(j) ? 1
				       : 2, li.proband(j));

	buf = new StringBuffer(pedline);
	for (int i = 0; i < lociCount; i++) {
	  buf.append("\t").append(1 + linkageInterface.getAllele(i, j, 0));
	  buf.append("\t").append(1 + linkageInterface.getAllele(i, j, 1));
	}
	buf.append("\n");
	gtWriter.write(buf.toString());
	gtWriter.flush();
      }
    }

    catch (IOException ioe) {
      System.out.println("Trouble writing genotypes: \n\t" + ioe.getMessage());
    }
  }

  private float getZvalue() {
    float z = 1.959964f;
    switch(getConfidence()) {
    case 0:
    case 1:
      break;
    case 2:
      z = 2.575829f;
      break;
    case 3:
      z = 3.290527f;
      break;
    default:
      String msg = "Invalid choice of confidence interval: ";
      logger.error( msg + "{}", getConfidence());
      throw new RuntimeException(msg + getConfidence());
    }
    return z;
  }

  protected Boolean thresholdAccomplished(float zval) {
    Boolean accomplished = null;
    // We believe in one segment only
    SharingRun segment = dropEvaluation.getSharingRunList().get(0);
    long egcount = segment.minEvents1Tail() == 0 ? 1 : segment.minEvents1Tail();
    float npv = (1.0f * egcount)/segment.totalEvents();
    float delta = zval*(float)Math.sqrt((npv/segment.totalEvents()) * (1-npv));
    float lcl = npv - delta;
    float ucl = npv + delta;
    logger.debug("{}: t-test {} vs {} and {} after {}", getTag(), getThreshold(), lcl, ucl, segment.totalEvents());
    if (lcl > getThreshold()) {
      logger.info("{}: segment '{}' lower bound {} fails threshhold {} (conf {})", getTag(), segment.toString(), lcl, getThreshold(), zval);
      accomplished = false;
    }
    if (ucl < getThreshold()) {
      logger.info("{}: segment '{}' upper bound {} achieves threshhold {} (conf {})", getTag(), segment.toString(), ucl, getThreshold(), zval);
      accomplished = true;
    }
    return accomplished;
  }


  // TODO: This relies on 'full' as alias for memory in SRL[0]
  private void extractOriginalCounts() {
    SharingRun full = dropEvaluation.getSharingRunList().get(0);
    full.setEventsLess(full.getEventsLess() - originalSegment.getEventsLess());
    full.setEventsEqual(full.getEventsEqual() - originalSegment.getEventsEqual());
    full.setEventsGreater(full.getEventsGreater() - originalSegment.getEventsGreater());
  }

  private SimLoopReport simLoop(reducedLINKAGE RL) throws IOException {
    setGeneDropper(RL.iface, RL.model);
    SimLoopReport slr = new SimLoopReport();
    // The Business Loop: geneDrop; harvest events; assess; repeat
    boolean expendible = false;  //.: Is interest in this run exhausted?
    float confZ = getZvalue();
    long previousSimulations = getTotalSims(originalSegment);
    long targetSimulations = previousSimulations + getSimulationCount();
    // Now really a while loop
    for (slr.currentSim = previousSimulations + 1;; ) {
      long loopStartNano = System.nanoTime();
      long elapsedSeconds = (( System.currentTimeMillis() / 1000) - startWallclock);
      //simulate
      getGeneDropper().geneDrop();
      if (SGSDebug.isDebug(SGSDebugEnum.TIMEDROP)) {
        logger.info("gendrop {} took {} nano", slr.currentSim, System.nanoTime() - loopStartNano);
      }
      //then assess
      long postDropMillis = System.currentTimeMillis();
      dropEvaluation.assessSimulation(RL.iface, getMinimumSegmentSize());
      long thresholdEvents = dropEvaluation.getSharingRunList().get(0).minEvents1Tail();
      if (SGSDebug.isDebug(SGSDebugEnum.WRITEGDROP)) {
        long i = slr.currentSim - previousSimulations;
        try (PrintStream ps = new PrintStream(new BufferedOutputStream(new FileOutputStream(new File(getJsonDir(), getTag()+"-drop"+i+".ped"))));
             PrintStream ls = new PrintStream(new BufferedOutputStream(new FileOutputStream(new File(getJsonDir(), getTag()+"-drop"+i+"+.ld"))));
             PrintStream gs = new PrintStream(new BufferedOutputStream(new FileOutputStream(new File(getJsonDir(), getTag()+"-drop"+i+"+.leg")))))
        {
          RL.iface().raw().getPedigreeData().writeTo(ps);
          //ps.close();
          RL.iface().raw().getParameterData().writeTo(ls);
          RL.model().writeTo(ls);
          //ls.close();
          String leg = String.format("L: %8d  E: %8d G: %8d\n",
            dropEvaluation.getSharingRunList().get(0).getEventsLess() - originalSegment.getEventsLess(),
            dropEvaluation.getSharingRunList().get(0).getEventsEqual() - originalSegment.getEventsEqual(),
            dropEvaluation.getSharingRunList().get(0).getEventsGreater() - originalSegment.getEventsGreater());
          gs.print(leg);
        }
        catch(IOException ioe) {
          logger.error("trouble writing drop {}", slr.currentSim, ioe);
          System.exit(2);
        }
        if (i == 10 ) {
          System.exit(0);
        }
      }

      // Enough simulations?
      if (thresholdEvents > 0 && getThreshold() != null && atDropThreshold(slr.currentSim - previousSimulations)) {
	logger.debug("{}: now at droppable point: {} {}", getTag(), slr.currentSim, dropEvaluation.getSharingRunList().get(0).toString());
	
	slr.thresholdCrossed = thresholdAccomplished(confZ);
	if (slr.thresholdCrossed != null) {
	  break; // End simulation loop
	}
      }
      // Add a (rather slow) heartbeat
      if ( slr.currentSim % 100000 == 0 ) {
	SharingRun chased = dropEvaluation.getSharingRunList().get(0);
	logger.info("{}: now at {} total simulations: started with {} events, now have {}",
		    getTag(),
		    slr.currentSim,
		    originalSegment.getEventsGreater() + originalSegment.getEventsEqual(),
		    chased.minEvents1Tail());
      }
      // Last in the loop to keep sim count accurate
      // We set maxWall on commandline to be slightly sho
      if (getMaxWallclock() > 0 && elapsedSeconds >= getMaxWallclock()) {
	logger.error("we hit the wall clock at {}! Man the life boats", getMaxWallclock());
	break;
      }
      if (slr.currentSim < targetSimulations && SGS_NOKILL) {
	slr.currentSim++;
      }
      else {
	System.err.println("Chases see NOKILL");
	break;
      }
    }
    return slr;
  }

  @Override
  public SegmentBlock buildSegmentBlock() {
    // wired to a singleton/chase
    SegmentBlock segblock = new SegmentBlock();
    String segsetName = dropEvaluation.getLinkageIdSet().asCSV();
    SharingRun srun = null;
    int originalLength = originalSegment.getLastmarker() - originalSegment.getFirstmarker() + 1;
    for (SharingRun sr :  dropEvaluation.getSharingRunList()) { 
      // This is, or should be, a list of ONE because we're dealing with a single segment.
      if (sr.runLength() == originalLength) {
	srun = sr;
	srun.setId(originalSegment.getId());
	srun.setProbandsetId(originalSegment.getProbandsetId());
	break;
      }
      else {
	String msg = String.format("Different lengths found=%d v. chased=%d", sr.runLength(), originalLength);
	logger.error(msg);
	throw new RuntimeException(msg);
      }
    }
    if (srun == null) {
      RuntimeException rte = new RuntimeException("could not find original segment in set of " 
						  + dropEvaluation.getSharingRunList().size());
      logger.error("could not find original segment  in set of {}",  dropEvaluation.getSharingRunList().size(), rte);
      throw rte;
    }

    segblock.addSegmentset(segsetName, segsetName);
    logger.debug("updating segment id = {}, start index = {}, end index {}", 
		 srun.getId(),
		 srun.getFirstmarker(), 
		 srun.getLastmarker());
    segblock.addSegment(srun.getId(),
			segsetName,
			0,
			srun.getStartbase(),
			srun.getEndbase(),
			srun.getFirstmarker(),
			srun.getLastmarker(),
			srun.getEventsLess(),
			srun.getEventsEqual(),
			srun.getEventsGreater(),
			0);
    return segblock;
  }
 
  protected List<SharingRun> processSegFile(File segsFile, boolean isdb) throws IOException {
    ArrayList<SharingRun> seglist = new ArrayList<>();
    try (BufferedReader segReader = new BufferedReader( new FileReader(segsFile))) {
      String line = segReader.readLine().trim();
      while ( line != null ) {
	String[] markers = line.split("\\s+");
	SharingRun segdef = new SharingRun();
	segdef.setFirstmarker(Integer.parseInt(markers[1]));
	segdef.setLastmarker(Integer.parseInt(markers[2]));
	segdef.setStartbase(Integer.parseInt(markers[4]));
	segdef.setEndbase(Integer.parseInt(markers[5]));
	segdef.setEventsLess(Long.parseLong(markers[6]));
	segdef.setEventsEqual(Long.parseLong(markers[7]));
	segdef.setEventsGreater(Long.parseLong(markers[8]));
	if (isdb) {
	  setSegmentId(UUID.fromString(markers[0]));
	  logger.error("should be updating {}", segmentId); 
	  segdef.setId(segmentId);
	}
	seglist.add(segdef);
	line = segReader.readLine();
      }
    }
    catch(IOException ioe) {
      throw new RuntimeException("trouble reading segment file", ioe);
    }
    if (seglist.size() != 1) {
      throw new UnsupportedOperationException(String.format("We currently accept only a single segment definition files. %s is showing %d\n",
							    segsFile.getCanonicalPath(), seglist.size()));
    }
    return seglist;
  }

  public void buildLinkageDataSet() throws IOException, InterruptedException {
    try {
      // here we build a data set with "maximal" expansion around segment
      LinkageParameterData parData = requestParameterData();
      //parData.writeTo(new PrintStream(new BufferedOutputStream(new FileOutputStream("/home/rob/utah/server/tc/pedpar/request.par"))));
      LinkagePedigreeData pedData = requestPedigreeData(parData);
      //pedData.writeTo(new PrintStream(new BufferedOutputStream(new FileOutputStream("/home/rob/utah/server/tc/pedpar/request.ped"))));
      buildLinkageDataSet(parData, pedData);
    }
    catch (UnsupportedOperationException uoe) {
      shrinkLinkageDataSet(uoe);
    }
  }

   private boolean atDropThreshold(long sims) {
    int isims = 0;
    if (sims > Integer.MAX_VALUE) {
      return sims % 10000000 == 0;
    }
    else {
      isims = (int)sims;
      switch (isims) {
      case 10000   :
      case 30000   :
      case 100000  :
      case 300000  :
      case 1000000 :
      case 3000000 :
      case 10000000 :
	return true;
      default:
	return sims > 10000000 && sims % 1000000 == 0;
      }
    }
  }

  private long getTotalSims(Segment s) {
    return s.getEventsLess() + s.getEventsEqual() + s.getEventsGreater();
  }

  private ObjectMapper getObjectMapper() {
    if (objectMapper == null) {
      objectMapper = new ObjectMapper();
    }
    return objectMapper;
  }

  private void shrinkLinkageDataSet(Exception cause) {
    logger.error("Proving Myke's suspicion: segment {}", getSegmentId());
    throw new RuntimeException("knotty from the get-go: " + getSegmentId(), cause);
  }

  // If writing files goes away so does this, as the simLoop could then simply return the simulation count
  private class SimLoopReport {
    public long currentSim = 0;
    public Boolean thresholdCrossed  = null;
  }

  // need to get at headers
  private LinkageParameterData requestParameterData() throws IOException {
    HttpRequest request = HttpRequest.newBuilder()
      .header(/*AbstractSGSServlet.SGSServlet_HEADER_DBROLE*/"dbrole", getProjectName())
      .header(/*AbstractSGSServlet.SGSServlet_HEADER_DBNAME*/"dbname", getDbName())
      .uri(URI.create(String.format("https://%s:%d/sgs/parfile?%s=%s",
				    getAccumulationHost(), getAccumulationPort(),
				    "segment", originalSegment.getId())))
      .build();

    try {
      HttpResponse<InputStream> response = getHttpClient().send(request, HttpResponse.BodyHandlers.ofInputStream());
      if (response.statusCode() != 200) {
        logger.error(response.toString());
        throw new RuntimeException("get pardata failed: response status = "+ response.statusCode());
      }

      Optional<String>rebaseValue = response.headers().firstValue("rebaseSegment");
      if (rebaseValue.isPresent()) {
        logger.debug("rebaseSegment set to " + rebaseValue.get());
        reindexFirstMarker = Integer.parseInt(rebaseValue.get());
      }
      else {
        logger.error("We DO NOT have the first-marker offset correction");
      }
      SGSFormatter formatter = new SGSFormatter(response.body(), getMarkersetId());
      LinkageParameterData lparam = new LinkageParameterData(formatter, false);
      setLdModel(new LDModel(formatter));
      return lparam;
    }
    catch (InterruptedException ie) {
      logger.error("{}", ie);
      throw new RuntimeException(ie.getMessage(), ie);
    }
  }
}
