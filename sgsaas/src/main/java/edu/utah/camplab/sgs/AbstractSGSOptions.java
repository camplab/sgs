package edu.utah.camplab.sgs;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

import edu.utah.camplab.jx.SegmentBlock;
import edu.utah.camplab.tools.*;
import joptsimple.OptionSet;
import joptsimple.OptionSpec;
import jpsgcs.linkage.LinkageDataSet;
import jpsgcs.linkage.LinkageInterface;
import jpsgcs.pedmcmc.LDModel;

public abstract class AbstractSGSOptions extends AbstractSGSRun {
  private List<String> environOptions = Arrays.asList(new String[]{"AWS", "CHPC", "SGSDEV"});
  protected OptionSpec<String> trimSpec;
  protected OptionSpec<Integer> clockSpec;
  protected OptionSpec<String> debugSpec;
  protected OptionSpec<String> environSpec;
  protected OptionSpec<String> irbSpec;

    
  protected AbstractSGSOptions() {
    super();
    trimSpec  = op.accepts(SGS_OPT_TRIM, "List of probands by which to trim pedigree, comma separated").withRequiredArg().ofType(String.class);
    clockSpec = op.accepts(SGS_OPT_MAXTIME, "Maximum running time in seconds (default is 3 days less 20 minutes)").withOptionalArg().ofType(Integer.class).defaultsTo(258000);
    debugSpec = op.accepts(SGS_OPT_DEBUG, "CSV of tags for debug processing. See SGSDebugEnum").withRequiredArg().ofType(String.class);
    environSpec = op.accepts(SGS_OPT_ENV, "where are we running? AWS or CHPC").withRequiredArg().ofType(String.class).required();
    irbSpec = op.accepts("IRB-id", "CHPC assigned irb id").withRequiredArg().ofType(String.class);
  }
    
  @Override
  protected void init(OptionSet opSet) throws Exception {
    super.init(opSet);

    //TODO: change to CSV ONLY for trimming. SGSDB-190
    // Get disposable individual IDs if trim option is present.
    Set<String> tprobands = new HashSet<String>();
        
    if (opSet.has(trimSpec)) {
      String trimStr = trimSpec.value(opSet);
      String[] parts = trimStr.split(",");
      if (parts.length > 1) {
	for (String s : parts) {
	  tprobands.add(s);
	}
      }
    }
    trimmingProbands = tprobands;

    if (opSet.has(clockSpec)) {
      setMaxWallclock(clockSpec.value(opSet));
    }
    
    if (opSet.has(debugSpec)) {
      SGSDebug.addFlags(debugSpec.value(opSet));
    }
    if (! environOptions.contains(environSpec.value(opSet))) {
      throw new RuntimeException("Invalid choice of environment option");
    }
    String whereAt = environSpec.value(opSet);
    String operationName = operationSpec.value(opSet) == 1 ? "chase" : "interim";
    if (whereAt.equals("AWS")) {
      //I would make setInterimManager abstract but that forces updating the dead operations
      setInterimManager(new S3InterimFileManager(AbstractSGSRun.S3SGS_ROOT + operationName));
    }
    else {
      String irbId = irbSpec.value(opSet);
      setInterimManager(new CHPCInterimFileManager(irbId, operationName));
    }
  }
    
    
  protected int[] setToArray(TreeSet<Integer> inset) {
    int[] retval =  new int[inset.size()];
    int index  = 0;
    for (Integer m : inset) {
      retval[index++] = m;
    }
    logger.debug("{}: now trying {} markers", getTag(), retval.length);
    return retval;
  }
    
  @Override
  public abstract void run() throws Exception;
}
