package edu.utah.camplab.sgs;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

import edu.utah.camplab.tools.LinkageIdSet;
import edu.utah.camplab.tools.SGSDebug;
import edu.utah.camplab.tools.SGSDebugEnum;
import jpsgcs.linkage.LinkageInterface;
import jpsgcs.linkage.LinkageId;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DropEvaluation implements Serializable {
  private static final long serialVersionUID = 4404977591131208726L;
  final static Logger logger = LoggerFactory.getLogger(DropEvaluation.class);
    
  // Accumulated events
  private int               simulationCount       = 0;
  private LinkageIdSet      linkageIdSet          = null;
  private ArrayList<String> extraneousHeaderLines = new ArrayList<>();
  private File              inputFile             = null;
  List<SharingRun>          sharingRunList        = new ArrayList<>(); // aka a list of segments for a given set of affecteds

  public DropEvaluation(LinkageInterface linkageInterface, int minSegment) {
    linkageIdSet = new LinkageIdSet(linkageInterface.raw().getPedigreeData());
    loadSharingRunList(linkageInterface, minSegment);
  }

  private void readLoop(BufferedReader reader, String countTag) throws IOException {
    String filename = inputFile == null ? "inside accumulator" : inputFile.getAbsolutePath();
    String line = reader.readLine();
    int waited = 0;
    // TODO: pretty sure this while/wait loop is bogus, but might be useful for CHPC work?
    while (line == null && waited < SGSRunInterface.SGS_MAX_FILE_WAIT) {
      waited += patience("empty file!: " + filename);
      line = reader.readLine();
    }
    if (waited > SGSRunInterface.SGS_MAX_FILE_WAIT / 2) {
      logger.warn("sgs run file {} was empty {} ms", filename, waited);
    }
    while ( line != null && line.charAt(0) == '#') {
      String[] parts = line.split("\\s+");
      if (parts.length > 1 && parts[1].startsWith(SGSRunInterface.SGSARG_TAG)) {
	String kvPair[] = parts[1].substring(SGSRunInterface.SGSARG_TAG.length()).replaceAll("[\\[\\]]","").split("=");
	if (kvPair[0].contains(countTag)) {
	  setSimulationCount(Integer.parseInt(kvPair[1]));
	}
	// other arg checks here
      }
      else if (line.contains("Probands")) {                               
	linkageIdSet = new LinkageIdSet(parts[2]);
	extraneousHeaderLines.add(line);
      }
      else if (line.toLowerCase().contains("chrom")) {
	; //We replace this one
      }
      else {
	extraneousHeaderLines.add(line);
      }
      line = reader.readLine();
    }
    boolean first = true;
    while (line != null) {
      parseFileData(line);
      if (first) {
	int last = getSharingRunList().size() - 1;
	SharingRun testRun = getSharingRunList().get(last);
	first = testSimCount(testRun);
      }
      line = reader.readLine();
    }
    reader.close();
    if (sharingRunList.size() == 0 ) {
      logger.error("empty pval file?");
      throw new RuntimeException("Failed to read any file data");
    }
  }
    
  public LinkageIdSet getLinkageIdSet() {
    return linkageIdSet;
  }

  public void assessSimulation(LinkageInterface lif, int sizeCutoff) {
    simulationCountIncr();
    List<Integer>probandIndices = getProbandIndices(lif);
    long assessNano = System.nanoTime();
    int maxLocIdx = lif.nLoci() - 1;
    for (SharingRun srun : getSharingRunList()) {
      if (srun.runLength() < sizeCutoff) {
	continue;
      }
      boolean allShare = true;
      int locx = srun.getFirstmarker(); // just so I can see the breaker on exit of loop
      for (; locx <= srun.getLastmarker() && allShare; locx++) {
	allShare = isLocusShared(locx, probandIndices, lif);
      }
      if (allShare) {
	if ((srun.getFirstmarker() > 0 && isLocusShared(srun.getFirstmarker() - 1, probandIndices, lif)) ||
	    (srun.getLastmarker() < maxLocIdx && isLocusShared(srun.getLastmarker() + 1, probandIndices, lif)) ) {
	  srun.setEventsGreater(srun.getEventsGreater() + 1);
	}
	else {
	  srun.setEventsEqual(srun.getEventsEqual() + 1);
	}
      }
      else {
	srun.setEventsLess(srun.getEventsLess() + 1);
      }
      if (SGSDebug.isDebug(SGSDebugEnum.TIMEEVAL)) {
        logger.info("eval ({} sharing) took {} nano", allShare ? "is": (locx-srun.getFirstmarker()) + " not ", System.nanoTime() - assessNano);
      }
    }
  }

  public int getSimulationCount() {
    return simulationCount;
  }

  public void setSimulationCount(int count) {
    simulationCount = count;
  }

  public List<SharingRun> getSharingRunList() {
    return sharingRunList;
  }

  private boolean isLocusShared(int locus, List<Integer> probandx, LinkageInterface lif) {
    int hom = -1;
    for (Integer idx : probandx) {
      int alleleA = lif.getAllele(locus, idx, 0);
      int alleleB = lif.getAllele(locus, idx, 1);
      if (alleleA == alleleB && alleleA != -1) {
	if (hom == -1) {
	  hom = alleleA;
	}
	else {
	  if ( hom != alleleA ){
	    return false;
	  }
	}
      }
    }
    return true;
  }

  private void simulationCountIncr() {
    setSimulationCount(getSimulationCount()+1);
  }

  private boolean testSimCount(SharingRun srun) {
    if (srun.totalEvents() == 0) {
      return true;
    }
    if ( srun.totalEvents() != getSimulationCount() ) {
      logger.error( "MISMATCH ON SIMULATION COUNT: {} v {}", srun.totalEvents(), getSimulationCount());
      throw new RuntimeException("MISMATCH ON SIMULATION COUNT");
    }
    return false;
  }

  private void parseFileData(String dataline) {

    String[] dataparts = dataline.split("\\s");
    SharingRun srun = new SharingRun();
    srun.setFirstmarker(Integer.parseInt(dataparts[1]));
    srun.setLastmarker(Integer.parseInt(dataparts[2]));
    // do we want this when reading from file?
    // if (srun.runLength() < minseg) {
    //     return;
    // }
    srun.setStartbase(Integer.parseInt(dataparts[4]));
    srun.setEndbase(Integer.parseInt(dataparts[5]));
    srun.setEventsLess(Long.parseLong(dataparts[6]));
    srun.setEventsEqual(Long.parseLong(dataparts[7]));
    srun.setEventsGreater(Long.parseLong(dataparts[8]));

    sharingRunList.add(srun);
  }

  private long patience(String errmsg) {
    long waitTime = (long)(Math.random() * 200) + 1;
    try {
      Thread.sleep(waitTime);
    }
    catch (InterruptedException ie) {
      logger.error("Holy Sleepy Creepy Batman, sleepus interuptus", ie);
      throw new RuntimeException(ie.getMessage(), ie);
    }
    logger.warn("Waited {} ms: {}", waitTime, errmsg);
    return waitTime;
  }

  private void loadSharingRunList(LinkageInterface linkageInterface, int minseg) {
    ArrayList<SharingRun> tempRunList = new ArrayList<>();
    AlleleSharing.hetSharing(linkageInterface, tempRunList);
    if (sharingRunList == null) {
      sharingRunList = new ArrayList<SharingRun>();
    }
    if (minseg == 0) {
      sharingRunList.addAll(tempRunList);
    } else {
      for (SharingRun trun : tempRunList) {
        if (trun.runLength() >= minseg) {
          sharingRunList.add(trun);
        }
      }
    }
  }

  private List<Integer> getProbandIndices(LinkageInterface lif) {
    List<Integer> idxlist = new ArrayList<>();
    for (LinkageId<?> lid : lif.getProbandIdSet()) {
      idxlist.add(lif.raw().getPedigreeData().getIndividualIndex(lid));
    }
    return idxlist;
  }
}
