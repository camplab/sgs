package edu.utah.camplab.sgs;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveTask;

import edu.utah.camplab.tools.LinkageIdSet;
import jpsgcs.linkage.LinkageInterface;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MultiAssessExecutor extends RecursiveTask<Set<LinkageIdSet>> {
    private static final long serialVersionUID = 6853821412058945874L;
    
    final static Logger logger = LoggerFactory.getLogger(MultiAssessExecutor.class);
    
    final List<LinkageIdSet> probandSets;
    final HashMap<LinkageIdSet, DropEvaluation> maTupleMap;
    final int startIndex;
    final int endIndex;
    final int fit;
    final int shortSize;

    public MultiAssessExecutor(List<LinkageIdSet> pbIds,
                               HashMap<LinkageIdSet, DropEvaluation> tmap,
                               int start,
                               int end,
                               int chunk,
                               int shorts) {
        probandSets = pbIds;
        maTupleMap = tmap;
        startIndex = start;
        endIndex = end;
        fit = chunk;
        shortSize = shorts;
    }
    
    @Override
    public Set<LinkageIdSet> compute() {
        HashSet<LinkageIdSet> cullSet = new HashSet<>();
        if (endIndex - startIndex <= fit) {
            LITuple workingTuple = LITuple.getAvailableLI();
            for (LinkageIdSet lis : probandSets.subList(startIndex, endIndex)) {
                if (! maTupleMap.containsKey(lis) ) {
                    continue;
                }
                
                resetProbands(lis, workingTuple.getLinkageInterface());
                collectDropStatistics(workingTuple.getLinkageInterface(), maTupleMap.get(lis));
            }
            LITuple.setAvailableLI(workingTuple);
        }
        else {
            int half = (endIndex - startIndex)/2;
            MultiAssessExecutor nextExec = new MultiAssessExecutor(probandSets, maTupleMap, startIndex, startIndex+half, fit, shortSize);
            MultiAssessExecutor futureExec = new MultiAssessExecutor(probandSets, maTupleMap, startIndex+half, endIndex, fit, shortSize);
            nextExec.fork();
            Set<LinkageIdSet> futureCulls = futureExec.compute();
            Set<LinkageIdSet> nextCulls = nextExec.join();
            cullSet.addAll(futureCulls);
            cullSet.addAll(nextCulls);
        }
        return cullSet;
    }

    public Set<LinkageIdSet> assessAll() {
        Set<LinkageIdSet> result =  ForkJoinPool.commonPool().invoke(this);
        return result;
    }
    
    private void collectDropStatistics(LinkageInterface currentLI, DropEvaluation eval) {
        // done for each tuple, for each genedrop, updates the tuple
        eval.assessSimulation(currentLI, shortSize);
    }

    private void resetProbands(LinkageIdSet probandSet, LinkageInterface li) {
        li.raw().getPedigreeData().resetProbands(probandSet.asList());
        li.setProbandsCounted(probandSet.asList().size());
    }
}
