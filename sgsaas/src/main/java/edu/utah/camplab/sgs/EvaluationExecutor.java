package edu.utah.camplab.sgs;

import java.util.List;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveAction;

import jpsgcs.linkage.LinkageInterface;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EvaluationExecutor extends RecursiveAction {
	private static final long serialVersionUID = 1930028233105561745L;

	final static Logger logger = LoggerFactory.getLogger(EvaluationExecutor.class);
    
    List<DropEvaluation> nullList;
    final LinkageInterface LI;
    final int startIndex;
    final int endIndex;
    final int fit;
    final int minSegment;

    public EvaluationExecutor(List<DropEvaluation> nulls, LinkageInterface li, int start, int end, int chunk, int minseg) {
        nullList = nulls;
        LI = li;
        startIndex = start;
        endIndex = end;
        fit = chunk;
        minSegment = minseg;
    }
    
    @Override
    public void compute() {
        if (endIndex - startIndex <= fit) {
            logger.debug("Evaluating from {} to {}", startIndex, endIndex);
            for (int i = startIndex; i < endIndex; i++) {
                DropEvaluation eval = nullList.get(i);
                eval.assessSimulation(LI, minSegment);
            }
        }
        else {
            int half = (endIndex - startIndex)/2;
            EvaluationExecutor nextEval = new EvaluationExecutor(nullList, LI, startIndex, startIndex+half, fit, minSegment);
            EvaluationExecutor futureEval = new EvaluationExecutor(nullList, LI, startIndex+half, endIndex, fit, minSegment);
            nextEval.fork();
            futureEval.compute();
            nextEval.join();
        }
    }

    public void tally() {
        //int cores = Runtime.getRuntime().availableProcessors();
        //ForkJoinPool.commonPool().invoke(new EvaluationExecutor(nullList, li, 0, nullList.size(), nullList.size()/cores, minSegment));
        ForkJoinPool.commonPool().invoke(this);
    }
}
