package edu.utah.camplab.sgs;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jpsgcs.linkage.LinkageDataSet;
import jpsgcs.linkage.LinkageFormatter;
import jpsgcs.linkage.LinkageId;
import jpsgcs.linkage.LinkageIndividual;
import jpsgcs.linkage.LinkageInterface;
import jpsgcs.linkage.LinkageParameterData;
import jpsgcs.linkage.LinkagePedigreeData;
import jpsgcs.pedapps.GeneDropper;
import jpsgcs.pedmcmc.LDModel;

/* WARNING: this class is serving two masters and is internally
 * inconsistent, cannot serve both masters on one jvm
 */
public class LITuple {
    final static Logger logger = LoggerFactory.getLogger(LITuple.class);

    private static HashMap<LITuple,Boolean> LITMap = new HashMap<>();
    private static List<LinkageId<?>> probandIds;

    private LinkageInterface linkageInterface;
    private GeneDropper geneDropper;

    public LITuple(LinkageInterface li, GeneDropper gd) {
        linkageInterface = li;
        geneDropper = gd;
    }

    public GeneDropper getDropper() {
        return geneDropper;
    }

    public LinkageInterface getLinkageInterface() {
        return linkageInterface;
    }

    public LinkageInterface setLinkageInterface(LinkageInterface li) {
      return linkageInterface = new LinkageInterface(li.raw());
    }
    
    public static void resetLinkage(LinkageInterface starter) {
        //Master #2
        if ( LITMap.size() == 0 ) {
            for (int i = 0; i < Runtime.getRuntime().availableProcessors(); i++) {          	
                LinkageInterface probandsOnly = stripToProbands(starter);
                LITuple tup = new LITuple(probandsOnly, null);
                LITMap.put(tup, true);
                logger.debug("Built LI, reduced from {} to {} entries.", starter.nIndividuals(), probandsOnly.nIndividuals());
            }
        }
        else {
            for (Map.Entry<LITuple, Boolean> mapEntry : LITMap.entrySet()) {
                LITuple lit = mapEntry.getKey();
                if (! mapEntry.getValue()) {
                    logger.error("Should we be re-loading this LI {} ?", lit.hashCode());
                }
                reloadGenotypes(starter, lit.linkageInterface);
                mapEntry.setValue(true);
            }
        }
    }
    
    private static synchronized  LinkageInterface stripToProbands(LinkageInterface startLI) {
        LinkageDataSet lds = startLI.raw();
        
        ArrayList<LinkageIndividual> probandList = new ArrayList<>();
        for (LinkageId<?> lid : LITuple.getProbands(startLI)) {
            LinkageIndividual oneProband = new LinkageIndividual(lds.getPedigreeData().find(lid));
            if (oneProband.proband == 0) {
                logger.debug("PROBAND LOST: LI:{} LinkageIndividual {} (at {}) is not now a proband but is in the proband list",
                             startLI.hashCode(), oneProband.id, oneProband.hashCode());
                oneProband.proband = 1;
            }
            probandList.add(oneProband);
        }

        LinkagePedigreeData probandsOnly = new LinkagePedigreeData(probandList);
        LinkageDataSet probandLDS = new LinkageDataSet();
        probandLDS.set(startLI.raw().getParameterData(), probandsOnly);
        LinkageInterface strippedLI = new LinkageInterface(probandLDS);
        logger.debug("Reduced LI ({}) from {} to {} individuals, retain {} probands",
                     new Object[]{ startLI.hashCode(), startLI.nIndividuals(), probandsOnly.nIndividuals(), strippedLI.nProbands()});
        return strippedLI;
    }

    private static synchronized void reloadGenotypes(LinkageInterface srcLI, LinkageInterface dstLI) {
        // We only care about the genotypes for all original probands.
        for (LinkageId<?> lid : LITuple.getProbands(srcLI)) {
            LinkageIndividual srcProband = srcLI.raw().getPedigreeData().find(lid);
            LinkageIndividual dstProband = dstLI.raw().getPedigreeData().find(lid);
            if (srcProband.pheno != dstProband.pheno) {  //Yes, a pointer compare
                logger.debug("re-assigned genotypes for {}", srcProband.id.stringGetId());
                dstProband.setPhenotype(srcProband);
            }
        }
    }
    
    public static List<LinkageId<?>> getProbands(LinkageInterface li) {
        if (probandIds == null) {
            initProbandIndex(li);
        }
        return probandIds;
    }

    private static void initProbandIndex(LinkageInterface li) {
        LITuple.probandIds = new ArrayList<LinkageId<?>>();
        for (LinkageIndividual lid : li.raw().getPedigreeData().getIndividuals()) {
            if (lid.getProband() == 1) {
                LITuple.probandIds.add(lid.id);
                logger.info("Adding {} to proband set", lid.id);
            }
        }
    }

    public static void resetAll() {
        int counter = 0;
        for (Map.Entry<LITuple, Boolean> me : LITMap.entrySet()) {
            if (! me.getValue() ) {
                me.setValue(true);
                counter++;
            }
        }
        logger.debug("Reset {} of {} LI to AVAILABLE", counter, LITMap.size());
    }
    
    public static void clear() {
        LITMap = new HashMap<LITuple, Boolean>();
    }

    public static void setAvailableLI(LITuple lit) {
        LITuple.LITMap.put(lit, true);
    }
    
    public static synchronized LITuple getAvailableLI() {
        LITuple available = null;
        int i = 0;
        int sleep = 0;
        while (available == null && sleep < 10000) {
            for (Map.Entry<LITuple, Boolean> entry : LITMap.entrySet()) {
                if (entry.getValue()) {
                    entry.setValue(false);
                    available = entry.getKey();
                    if (sleep > 0) {
                        logger.debug("Found available LI at {} of max {} in {} ms", i, LITMap.size(), sleep);
                    }
                    break;
                }
                i++;
            }
            if (available == null) {
                try {
                    logger.error("No available LI after snoozing {} ms", sleep);
                    Thread.sleep(1000);
                }
                catch (Exception exception) {
                    logger.error("Swallowed sleep exception {}", exception);
                }
                sleep += 1000;  //Does this warrant randomization?
            }
        }
        if (available == null) {
            throw new RuntimeException("Could not attain available LI in 10 seconds");
        }
        return available;
    }
}
