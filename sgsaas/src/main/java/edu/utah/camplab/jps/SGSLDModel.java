package edu.utah.camplab.jps;

import edu.utah.camplab.db.generated.base.tables.records.LdCliqueRecord;

import jpsgcs.markov.Variable;
import jpsgcs.pedmcmc.LDModel;
import jpsgcs.util.InputFormatter;
import org.jooq.DSLContext;
import org.jooq.Record1;
import org.jooq.impl.DSL;

import static edu.utah.camplab.db.generated.base.Tables.LD_CLIQUE;
import static edu.utah.camplab.db.generated.base.Tables.MARKERSET;

import java.io.IOException;
import java.util.ArrayList;
import java.util.UUID;
/*
 * currently for reading file, writing to database
 */
public class SGSLDModel extends LDModel {

  private ArrayList<LdCliqueRecord> cliqueList = new ArrayList<>();
  private SGSFormatter formatter;
  
  public SGSLDModel(SGSFormatter sgsf, double ep) throws IOException {
    super(new Variable[0]); //
    formatter = sgsf;
    readStateSizes(sgsf);
    readFunctions(sgsf, ep);
    saveLd();  // should we hold off on this?
  }

  public boolean readStateSizes(InputFormatter fmt) throws IOException {
    return fmt.newLine();  // read, ignore the 2's
  }

  public void readFunctions(InputFormatter fmt, double epi) throws IOException {
    //ignore the fmt
    int cliqueOrd = 0;

    while (formatter.newLine()) {
      LdCliqueRecord ldq = formatter.getDbContext().newRecord(LD_CLIQUE);
      ArrayList<Integer> mpos = new ArrayList<>();
      while (formatter.newToken()) {
	mpos.add(formatter.getInt());
      }
      Integer[] ti = new Integer[mpos.size()];
      ti = mpos.toArray(ti);
      ldq.setLociOrdinals(ti);

      formatter.newLine();
      ArrayList<Double> rfs = new ArrayList<>();
      while (formatter.newToken()) {
	rfs.add(formatter.getDouble());
      }
      Double[] td = new Double[rfs.size()];
      td = rfs.toArray(td);
      ldq.setPotential(td);
      ldq.setOrdinal(cliqueOrd++);  // ZERO based??
      //ldq.setLdId(ldReq.getId());
      cliqueList.add(ldq);
    }
  }
    
  private void saveLd() {
    formatter.getDbContext().transaction( localconf -> {
	DSLContext lct = DSL.using(localconf);
	Record1<UUID> mid = lct.select(MARKERSET.ID).from(MARKERSET).where(MARKERSET.ID.equal(formatter.getContextId())).fetchOne();
	// block load these, maybe
	for (LdCliqueRecord lcRec : cliqueList) {
	  lcRec.setMarkersetId((UUID)(mid.getValue(0)));
	}
	lct.batchStore(cliqueList).execute();
      });
  }
}
