BEGIN {
  rref= "<resource-ref>\n" "<res-ref-name>jdbc/sgsdb/%s</res-ref-name>\n" "<res-type>javax.sql.DataSource</res-type>\n" "<res-auth>Container</res-auth>\n" "</resource-ref>\n"
}
/addResourceRef/ {
    if (PILIST ~ /:/) {
        print "Start with " PILIST
        split(PILIST, pis, ":")
        for (pi in pis) {
            print pi
            split(pi, dbdef, ",")
            print dbdef[1]
            printf rref, dbdef[1]
        }
    }
    else {
        split(PILIST, pi, ",")
        printf rref, pi[1]        
    }
    next
}
{ print }
