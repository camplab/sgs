##command line assigns server, port, user-list and valve
{
  if (NR == 1) {
    template = 0
    split(PVLIST, pis,":")
    for ( i in pis ) {
      split(pis[i], userpass,",")
      unm = userpass[1]
      j = 2
      while (j <= length(userpass)) {
          ups = userpass[j]
          ofiles[ups] = "rsrc" ups
          j++
      }
    }
    printf "\n"
  }
}
/jndi_template/ {
    orig = $0  
    for ( i in pis ) {
        split(pis[i], userpass,",")
        unm = userpass[1]
        j = 2
        while (j <= length(userpass)) {
            ups = userpass[j]
            jndi = sprintf("%s-%s", ups, unm)
            gsub("jndi_template", jndi, orig)
            printf "%s\n", orig > ofiles[ups]
            orig = $0
            j++
        }
    }
    template=1

}
/dbname_template/{
    orig = $0  
    for ( i in pis ) {
        split(pis[i], userpass,",")
        unm = userpass[1]
        j = 2
        while (j <= length(userpass)) {
            ups = userpass[j]
            gsub("dbname_template", unm, orig)
            if ( orig ~ /dbhost_template/ ) {
                gsub("dbhost_template", pgserver, orig)
            }
            if ( orig ~ /dbport_template/ ) {
                gsub("dbport_template", pgport, orig)
            }
            printf "%s\n", orig > ofiles[ups]
            j++
            orig = $0
        }
    }
    template=1
}
/role_template/ {
    orig = $0  
    for ( i in pis ) {
        split(pis[i], userpass,",")
        unm = userpass[1]
        j = 2
        while (j <= length(userpass)) {
            ups = userpass[j]
            gsub("role_template", ups, orig)
            printf "%s\n", orig > ofiles[ups]
            orig = $0
            j++
        }
    }
    template=1
}
/rolepwd_template/ {
    orig = $0  
    for ( i in pis ) {
        split(pis[i], userpass,",")
        unm = userpass[1]
        j = 2
        while (j <= length(userpass)) {
            ups = userpass[j]
            gsub("rolepwd_template", ups "_notnull", orig)
            printf "%s\n", orig > ofiles[ups]
            orig = $0
            j++
        }
    }
    template=1
}
{
    if (! template) {
        for ( i in pis ) {
            split(pis[i], userpass, ",")
            j = 2
            while (j <= length(userpass)) {
                printf "%s\n",$0 > ofiles[userpass[j]]
                j++
            }
        }
    }
    else {
        template = 0
    }
}
END {
    pcmd = sprintf("<Context reloadable=\"true\">\n")
    system("echo '" pcmd "'")  # > context.xml
    for ( i in pis ) {
        split(pis[i], userpass, ",")
        j = 2
        while (j <= length(userpass)) {
            system("cat " ofiles[userpass[j]] ) ## " >> context.xml"
            j++
        }
    }
    system( "cat " valve)  ##  >> context.xml
    pcmd = sprintf("</Context>\n")
    system( "echo '" pcmd "'")   ## >> context.xml
}
