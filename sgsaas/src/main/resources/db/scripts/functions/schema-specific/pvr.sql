-- The following function cannot live with the other pv routines since it uses a
-- table definition known only /after/ the project (e.g. sui) has been set.
create or replace function pvr(seg segment, plus float default 0.0)
returns float as $$
  select ((1.0*seg.events_equal)+seg.events_greater+plus)/((1.0*seg.events_less)+seg.events_equal+seg.events_greater+plus)::float as result;
$$
language sql;
   
