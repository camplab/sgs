create or replace function public.show_chaseable(thresholdname text)
returns table(tname text, sid uuid, chrom int, sig float, sug float, empirical_pv float, sgs_call text, lcb float, ucb float, events bigint, simstotal bigint)
language plpgsql
as
$$
declare
  tid uuid;
begin
return query
  select thresholdname as tname
         , s.id as sid
         , s.chrom
         , t.significant as sig
         , t.suggestive as sug
         , pvr(s,0) as empirical_pv
         , case when t.significant between pv_lower95(s.events_less,s.events_equal,s.events_greater,0) and pv_upper95(s.events_less,s.events_equal,s.events_greater,0)
                then 'sig chaseable'
                when t.suggestive between pv_lower95(s.events_less,s.events_equal,s.events_greater,0) and pv_upper95(s.events_less,s.events_equal,s.events_greater,0)
                then 'sug chaseable'
                when pv_upper95(s.events_less,s.events_equal,s.events_greater,0) < t.significant
                then 'significant'
                when pv_upper95(s.events_less,s.events_equal,s.events_greater,0) < t.suggestive and pv_lower95(s.events_less,s.events_equal,s.events_greater,0) > t.significant
                then 'suggestive'
                else 'boring'
                end
         , pv_lower95(s.events_less,s.events_equal,s.events_greater,0) as lcb
         , pv_upper95(s.events_less,s.events_equal,s.events_greater,0) as ucb
         , s.events_equal + s.events_greater
         , s.events_less + s.events_equal + s.events_greater
  from threshold t 
      join chaseable c on t.id = c.threshold_id 
      join segment s on c.segment_id = s.id
  where t.name = thresholdname;
end;
$$
;
