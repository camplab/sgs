package edu.utah.camplab.jooq;

import org.jooq.codegen.DefaultGeneratorStrategy;
import org.jooq.codegen.GeneratorStrategy;
import org.jooq.codegen.JavaGenerator;
import org.jooq.codegen.JavaWriter;
import org.jooq.meta.Definition;
import org.jooq.tools.StringUtils;

import java.util.ArrayList;
import java.util.List;
/**
 *  We need to let JXON know how to deal with our transport classes
 **/

public class JxonAnnotationGenerator extends JavaGenerator {

    @Override
    protected void printClassAnnotations(JavaWriter out, Definition schema, GeneratorStrategy.Mode mode) {
      super.printClassAnnotations(out, schema, mode);
        if (out.file().getAbsolutePath().contains("pojo")) {
            String pack = getStrategy().getJavaPackageName(schema, DefaultGeneratorStrategy.Mode.POJO);
            System.out.println("package: " + pack);
            String[] parts = out.file().getName().split("\\.");
            String nomDeClass = pack + ".tables.pojos." + parts[0];
            System.out.println("Add annotations to " + nomDeClass);
            out.println("@%s(use=%s.Id.MINIMAL_CLASS, include=%s.As.PROPERTY, property=\"class\")",
                        "JsonTypeInfo",
                        "JsonTypeInfo",
                        "JsonTypeInfo");
            out.println("@JsonTypeName(\"%s\")", nomDeClass);
            out.println("@JsonFormat(shape=JsonFormat.Shape.ARRAY)");
            out.println("@JsonPropertyOrder(alphabetic=true)");
        }
    }

  @Override
    protected void printPackage(JavaWriter out, Definition definition, DefaultGeneratorStrategy.Mode mode) {

       String header = getStrategy().getFileHeader(definition, mode);
//       if (!StringUtils.isBlank(header)) {
//            out.println("/*");
//            printJavadocParagraph(out, header, "  ");
//            out.println("*/");
//        }

       out.printPackageSpecification(getStrategy().getJavaPackageName(definition, mode) );

       out.println();
       if (mode == DefaultGeneratorStrategy.Mode.POJO) { 
           List<String> importeds = new ArrayList<>();
           importeds.add("com.fasterxml.jackson.annotation.JsonCreator");
           importeds.add("com.fasterxml.jackson.annotation.JsonTypeInfo");
           importeds.add("com.fasterxml.jackson.annotation.JsonTypeName");
           importeds.add("com.fasterxml.jackson.annotation.JsonFormat");
           importeds.add("com.fasterxml.jackson.annotation.JsonPropertyOrder");
           out.ref(importeds);
       }
       out.printImports();
       out.println();

    }
}
