package jpssgs.app;


import jpsgcs.linkage.LinkageDataSet;
import jpsgcs.pedmcmc.LDModel;
import jpsgcs.util.InputFormatter;
import java.io.BufferedReader;
import java.io.FileReader;
 
class McLinkOptions
{
    public int sampler = 3;
    public int lmlevel = 2;
 
    public boolean report = false;
    public double errorprob = 0.0D;
    public int n_samples = 1000;
    // public int[] randups = { 2, 3, 4 };
    public double[] thetas = { 0.0D, 0.01D, 0.1D, 0.2D, 0.3D, 0.4D, 0.5D, 0.6D, 0.7D, 0.8D, 0.9D, 0.99D, 1.0D };
    public int wherehalf = 6;
    public LDModel ldmod = null;
    public LinkageDataSet[] rawdata = null;
 
    public McLinkOptions(String[] paramArrayOfString) throws Exception {
	Object localObject = paramArrayOfString;
	String[] arrayOfString = null;
 
	if ((arrayOfString = strip((String[])localObject, "-e")) != localObject) {
	    this.errorprob = 0.01D;
	    localObject = arrayOfString;
	}
 
	if ((arrayOfString = strip((String[])localObject, "-v")) != localObject) {
	    this.report = true;
	    localObject = arrayOfString;
	}
 
	if ((arrayOfString = strip((String[])localObject, "-t")) != localObject) {
	    this.report = false;
	    localObject = arrayOfString;
	}
 
	if ((arrayOfString = strip((String[])localObject, "-ils")) != localObject) {
	    this.sampler = 0;
	    localObject = arrayOfString;
	}
 
	if ((arrayOfString = strip((String[])localObject, "-lls")) != localObject) {
	    this.sampler = 1;
	    localObject = arrayOfString;
	}
 
	if ((arrayOfString = strip((String[])localObject, "-lms")) != localObject) {
	    this.sampler = 2;
	    localObject = arrayOfString;
	}
 
	if ((arrayOfString = strip((String[])localObject, "-elms")) != localObject) {
	    this.sampler = 3;
	    localObject = arrayOfString;
	}
 
	if ((arrayOfString = strip((String[])localObject, "-ums")) != localObject) {
	    this.sampler = -1;
	    localObject = arrayOfString;
	}
 
	switch (((String[])localObject).length) {
	case 4:
	    this.ldmod = new LDModel(new InputFormatter(new BufferedReader(new FileReader(((String[])localObject)[3]))));
	    this.errorprob = 0.01D;
	case 3:
	    this.n_samples = new Integer(((String[])localObject)[2]).intValue();
	case 2:
	    LinkageDataSet localLinkageDataSet = new LinkageDataSet(((String[])localObject)[0], ((String[])localObject)[1]);
	    this.rawdata = localLinkageDataSet.splitByPedigree();
	    break;
	default:
	    System.err.println("Usage: java McLink input.par input.ped [n_samples] [ld_model] [-v/t] [-e] [-ils/lls/lms/elms]");
	    System.exit(1);
	}
    }
 
    public static String[] strip(String[] paramArrayOfString, String paramString) {
	if (paramArrayOfString != null) {
	    for (int i = 0; i < paramArrayOfString.length; i++) {
		if (paramArrayOfString[i].equals(paramString)) {
		    String[] arrayOfString = new String[paramArrayOfString.length - 1];
		    for (int j = 0; j < i; j++) {
			arrayOfString[j] = paramArrayOfString[j];
		    }
		    for (int k = i + 1; k < paramArrayOfString.length; k++) {
			arrayOfString[(k - 1)] = paramArrayOfString[k];
		    }
		    return arrayOfString;
		}
	    }
	}
	return paramArrayOfString;
    }
}

/* Location:           /export/home/rob/tools/SGSMod/SGSmod.jar
 * Qualified Name:     McLinkOptions
 * JD-Core Version:    0.6.2
 */
