To transform *.org file in to GUI format:
1. Start emacs from command line as follows: emacs -file target.org
2. Within emacs (with .org file open) type: Control-C followed by Control-E
3. Follow on-screen instruction/selections
4. To exit emacs after generating output type:  Control-X followed by Control-C

