package edu.utah.camplab.servlet;

import com.fasterxml.jackson.databind.ObjectMapper;

import edu.utah.camplab.db.generated.base.tables.records.*;
import edu.utah.camplab.db.crafted.SegmentExtended;
import edu.utah.camplab.db.generated.default_schema.tables.Segment;
import edu.utah.camplab.db.generated.default_schema.tables.records.SegmentRecord;
import edu.utah.camplab.db.generated.public_.tables.AscendTrim;
import edu.utah.camplab.jx.SGSFullContext;

import javax.sql.DataSource;
import org.jooq.*;
import org.jooq.impl.DSL;

import java.io.*;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import javax.annotation.Resource;
import javax.servlet.annotation.WebServlet;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static edu.utah.camplab.db.generated.base.Tables.*;
import static edu.utah.camplab.db.generated.default_schema.tables.Segment.SEGMENT;

@WebServlet(
  name = "Segment",
  urlPatterns = {"/segment"}
)

public class SegmentServlet extends AbstractSGSServlet {

  public void doGet(HttpServletRequest req, HttpServletResponse resp) {
    resp.setContentType("application/json");
    try {
      SegmentExtended segmentExt = readSegment(req, getParameterMap(req));
      ServletOutputStream out = resp.getOutputStream();
      getObjectMapper().writeValue(out, segmentExt);
      out.flush();
      out.close();
      resp.setStatus(200);
    }
    catch (IOException | SQLException ioe) {
      resp.setStatus(500);
      ioe.printStackTrace();
    }
  }

  private SegmentExtended readSegment(HttpServletRequest request, HashMap<String, ArrayList<String>> map) throws SQLException {
    try (Connection conn =  getDbConn(request, map)) {
      DSLContext dbc = SGSFullContext.DSLContextFactory(conn);
      UUID segmentId = UUID.fromString(map.get("segmentid").get(0));
      SegmentExtended sex = new SegmentExtended();
      sex.fill(dbc, segmentId);
      return sex;
    }
  }

  /*
   * We won't use this unless and until we give up on the process
   * wrapper.  i.e. over my dead body (well if not expired, certainly
   * retired).
   */
  public void doPut(HttpServletRequest req, HttpServletResponse resp) {
    try (Connection conn = getDbConn(req, getParameterMap(req))) {
      DSLContext dbc = SGSFullContext.DSLContextFactory(conn);
      saveSegment(req, dbc);
    } catch (SQLException sqle) {
      //TODO tell the client of trouble
      //lauger, anyone?
    }
  }

  private void saveSegment(HttpServletRequest request, DSLContext context) {
    HashMap<String, ArrayList<String>> pmap = getParameterMap(request);
    // Working on the version 3 notion of just supplying the event counts from a single run.
    UUID sid = UUID.fromString(pmap.get("segmentid").get(0));
    long eventsLess = Long.parseLong(pmap.get("nless").get(0));
    long eventsEqual = Long.parseLong(pmap.get("nequal").get(0));
    long eventsGreater = Long.parseLong(pmap.get("ngreater").get(0));
    context.update(SEGMENT)
      .set(SEGMENT.EVENTS_LESS, SEGMENT.EVENTS_LESS.add(eventsLess))
      .set(SEGMENT.EVENTS_EQUAL, SEGMENT.EVENTS_EQUAL.add(eventsEqual))
      .set(SEGMENT.EVENTS_GREATER, SEGMENT.EVENTS_GREATER.add(eventsGreater))
      .where(SEGMENT.ID.equal(sid))
      .execute();
  }
}
    

