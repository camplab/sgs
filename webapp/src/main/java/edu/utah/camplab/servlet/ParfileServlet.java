package edu.utah.camplab.servlet;

import edu.utah.camplab.db.generated.base.tables.records.*;
import edu.utah.camplab.db.generated.default_schema.tables.records.SegmentRecord;
import edu.utah.camplab.jx.SGSFullContext;

import org.jooq.*;
import org.jooq.impl.DSL;

import java.io.*;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static edu.utah.camplab.db.generated.base.Tables.MARKER;
import static edu.utah.camplab.db.generated.base.Tables.GENOME_MARKERSET;
import static edu.utah.camplab.db.generated.base.Tables.MARKERSET;
import static edu.utah.camplab.db.generated.base.Tables.MARKERSET_MEMBER;
import static edu.utah.camplab.db.generated.base.Tables.LD_CLIQUE;
import static edu.utah.camplab.db.generated.default_schema.tables.Segment.SEGMENT;
import static org.jooq.impl.DSL.*;

@WebServlet(
  name = "Parfile",
  urlPatterns = {"/parfile"}
)

public class ParfileServlet extends AbstractSGSServlet {
  public final LoggerDelegate logger = new LoggerDelegate(getClass().getName());

  //Hold the offset of the first explicit marker.  That marker at most
  //16 upstream of segment.firstMarker.  Alternatively the next
  //position after the last marker "discovered" in the spanning cliques.
  //  int rebase = -1;

  public void doGet(HttpServletRequest req, HttpServletResponse resp) {
    HashMap<String, ArrayList<String>> pmap = getParameterMap(req);
    MarkersetRecord msRec = null;
    try (Connection conn = getDbConn(req, pmap) ){
      DSLContext dsl = SGSFullContext.DSLContextFactory(conn);
      if (pmap.containsKey("markerset")) {
        UUID markersetId = UUID.fromString(pmap.get("markerset").get(0));
        msRec = dsl.selectFrom(MARKERSET).where(MARKERSET.ID.equal(markersetId)).fetchOne();
        if (msRec == null) {
          resp.setStatus(503);
          resp.getOutputStream().println(String.format("Error: is %s a valid markeset?", markersetId));
        }
	setGenomeBuild(req, dsl, msRec.getId());
      }
      else {
        UUID segmentId = UUID.fromString(pmap.get("segment").get(0));
        SegmentRecord segRec = dsl.selectFrom(SEGMENT).where(SEGMENT.ID.equal(segmentId)).fetchOne();
	setGenomeBuild(req, dsl, segRec.getMarkersetId());

	msRec = dsl.selectFrom(MARKERSET).where(MARKERSET.ID.equal(segRec.getMarkersetId())).fetchOne();
        // ParLd fullPair = readSegmentMarkerset(req, dsl, segRec);
        // sendParfile(resp, fullPair, getRebase(req));
      }
      ParLd fullPair = readFullMarkerset(dsl, msRec);
      sendParfile(resp, fullPair, getGenomeBuild(req));
    } 
    catch (IOException | SQLException ioe) {
      try {
        ioe.printStackTrace();
        resp.setStatus(503);
        resp.getOutputStream().write(ioe.getMessage().getBytes());
        resp.getOutputStream().flush();
        resp.getOutputStream().close();
      } catch (IOException ignored) { }
    }
  }

  private ParLd readFullMarkerset(DSLContext dsl, MarkersetRecord msRec) throws IOException {

    Result<?> markerdata = dsl
      .select(MARKERSET_MEMBER.asterisk(), MARKER.asterisk())
      .from(MARKERSET_MEMBER
        .join(MARKER).on(MARKERSET_MEMBER.MEMBER_ID.equal(MARKER.ID)))
      .where(MARKERSET_MEMBER.MARKERSET_ID.equal(msRec.getId()))
      .orderBy(MARKERSET_MEMBER.ORDINAL)
      .fetch();

    List<LdCliqueRecord> lddata = dsl
      .selectFrom(LD_CLIQUE)
      .where(LD_CLIQUE.MARKERSET_ID.equal(msRec.getId()))
      .orderBy(LD_CLIQUE.ORDINAL)
      .fetch(r -> r.into(LD_CLIQUE));
    ParLd fullLd = new ParLd(markerdata, lddata, msRec);
    return fullLd;
  }

  //TODO: SegmentPojo, not Record?
  private ParLd readSegmentMarkerset(HttpServletRequest req, DSLContext context, SegmentRecord sRec) throws IOException {
    // We expand the segment at both ends to handle "knot" retries, allow JPS to be comfortable
    SegAdj adjusted = adjustedLociIndex(sRec);

    List<LdCliqueRecord> lddata = readSegmentCliques(context, adjusted, sRec);
    //Collect all the marker ordinals seen in the cliques
    TreeSet<Integer> ordinalSet = lociOrdinalsFromCliques(lddata);

    Result<?> markerdata = context
      .select(MARKERSET_MEMBER.asterisk(), MARKER.asterisk())
      .from(MARKERSET_MEMBER.join(MARKER).on(MARKERSET_MEMBER.MEMBER_ID.eq(MARKER.ID)))
      .where(MARKERSET_MEMBER.MARKERSET_ID.eq(sRec.getMarkersetId())
             /*.and(MARKERSET_MEMBER.ORDINAL.in(ordinalSet))*/ )
      .orderBy(MARKERSET_MEMBER.ORDINAL)
      .fetch();

    Map<Integer, Integer> remappedLoci = remappingOrdinal(ordinalSet);
    remapCliques(req, lddata, remappedLoci, sRec);
    MarkersetRecord msRec = context.selectFrom(MARKERSET).where(MARKERSET.ID.equal(sRec.getMarkersetId())).fetchOne();
    
    ParLd fullLd = new ParLd(markerdata, lddata, msRec);
    return fullLd;
  }

  private void sendParfile(HttpServletResponse response, ParLd data, int buildNumber) {
    sendParfile(response, data, buildNumber, -1);
  }

  private void sendParfile(HttpServletResponse response, ParLd data, int buildNumber, int rebase ) {
    response.setHeader("rebaseSegment", "" + rebase);
    try (ServletOutputStream ps = response.getOutputStream();) {
      StringBuffer sbuf = new StringBuffer();
      ps.print(String.format("%d 0 0 5 \n%d %3.1f %3.1f %d \n", data.markers().size(), 0, 0.0, 0.0, 0));
      for (int m = 1; m <= data.markers().size(); m++) {
        ps.print(String.format("%d ", m));
      }
      ps.println();

      StringBuffer alleleBuffer = new StringBuffer("");

      double gapTheta = 0.0D;
      double averageTheta = data.markerset().getAvgTheta();
      // Need a good ol' for i loop as we'll be peeking ahead  org.jooq.Record mline :
      for (int i = 0; i < data.markers().size(); i++) { 
	org.jooq.Record mline = data.markers().get(i);
        int alen = mline.getValue(MARKER.ALLELES) == null ? 2 : mline.getValue(MARKER.ALLELES).length;
	double currentTheta = mline.getValue(MARKERSET_MEMBER.THETA);
	int currentOrdinal = mline.getValue(MARKERSET_MEMBER.ORDINAL);
	if (i + 2 < data.markers().size()) {
	  org.jooq.Record nextline = data.markers().get(i+1);
	  int markerGap = nextline.getValue(MARKERSET_MEMBER.ORDINAL) - currentOrdinal;
	  if ( markerGap > 1) {
	    currentTheta = interpolateGapTheta(markerGap, averageTheta, currentTheta);
	  }
	}
	// a LIE eles
        alleleBuffer.append(" " + alen);

	// jack extra info into marker "name" for par file, trickles through to AlleleSharing.  For shame.
	if (buildNumber == 37) {
	  ps.print(String.format("3 %d     %s|%d|%d\n", alen, mline.getValue(MARKER.NAME), mline.getValue(MARKER.BASEPOS37), mline.getValue(MARKER.CHROM)));
	}
	else {
	  ps.print(String.format("3 %d     %s|%d|%d\n", alen, mline.getValue(MARKER.NAME), mline.getValue(MARKER.BASEPOS38), mline.getValue(MARKER.CHROM)));
	}

        sbuf.append(String.format("%s ", currentTheta));
	double sumToOne = 0.0;
        for (int j = 1; j < alen; j++) {
	  sumToOne += 1.0 / alen;
          ps.print(String.format(" %8.6f ", (1.0 / alen)));
        }
	ps.print(String.format(" %8.6f ", 1.0 - sumToOne));
        ps.println();
      }
      ps.print(String.format("0 0 \n%s\n1 5.0 0.2     0.1\n", sbuf.toString()));
      // now the ld
      ps.println(alleleBuffer.toString());
      for (LdCliqueRecord crec : data.cliques()) {
        for (int ord : crec.getLociOrdinals()) {
          ps.print(String.format(" %d", ord));
        }
        ps.println();
        // the array of values contributing to the cliques potential
        for (double d : crec.getPotential()) {
          ps.print(String.format(" %s", d));
        }
        ps.println();
      }
      // now send it
      ps.flush();
      ps.close();
    } catch (IOException ioe) {
      ioe.printStackTrace();
    }

  }

  private Map<Integer, Integer> remappingOrdinal(TreeSet<Integer> usedOrdinals) {
    int newPosition = 0;
    Map<Integer,Integer> remap = new HashMap<>();
    for (int chromosomeIndex : usedOrdinals) {
      remap.put(chromosomeIndex, newPosition++);
    }
    return remap;
  }

  // Rebuild the cliques mapped to current reduced chromosome. Also set the offset for the start of the expanded-by-sixteen-segment
  // Map has originalOrder, newPosition
  private void remapCliques(HttpServletRequest req, 
			    List<LdCliqueRecord> cliques, 
			    Map<Integer,Integer> map, 
			    SegmentRecord srec) {
    int rebase = -1;
    logger.error(String.format("setting rebase from %d loci, looking for %s", map.size(), srec.getFirstmarker()));
    for (LdCliqueRecord qrec : cliques) {
      for (int i = 0; i < qrec.getLociOrdinals().length; i++) {
        int oldPostion = qrec.getLociOrdinals()[i];
        if (oldPostion == srec.getFirstmarker()) {
          if (rebase < 0) {
            rebase = map.get(oldPostion);
            logger.error("set rebase to " + rebase);
          }
	}  //Now to send this to client? Passed as header
        qrec.getLociOrdinals()[i] = map.get(oldPostion);
      }
    }
    if (rebase < 0) {
      throw new RuntimeException("Did not set first marker index");
    }
    setRebase(req, rebase);
  }

  // TODO: send in actual gap size as number of loci missing, not
  // next-current: 3-1 is a gap of one. Iterate from zero
  private double interpolateGapTheta(int gapsize, double avgtheta, double locustheta) {
    double aggregateCM = thetaTocMKosambi(locustheta);
    for (int i = 1; i < gapsize; i++) {
      aggregateCM += thetaTocMKosambi(avgtheta);
    }
    return cMToThetaKosambi(aggregateCM);
  }

  // Stolen from JPSGCS
  private double thetaTocMKosambi(double t)
  {
    if (t >= 0.5)
      return 1000000.0;  
    return 100 * 0.25 * ( Math.log(1+2*t) - Math.log(1-2*t) );
  }

  // Stolen from JPSGCS
  private double cMToThetaKosambi(double x)
  {
    // Won't likely see these extremes but if we do the abort is welcome.
    // if (Double.isInfinite(x) || Double.isNaN(x))
    //   return 0.5;
    return 0.5 * Math.tanh(2*x/100.0);
  }
  
  //Hold the offset of the first explicit marker in the chased
  //segment.  That marker at most 16th locus up-stream of
  //segment. Less at pter.  There are more markers found within the
  //spanning cliques but these are not at predictable positions
  private void setRebase(HttpServletRequest req, Integer value) {
    req.getSession().setAttribute("rebase", value);
  }

  private Integer getRebase(HttpServletRequest req) {
    return (Integer)(req.getSession().getAttribute("rebase"));
  }

  private void setGenomeBuild(HttpServletRequest req, DSLContext dsl, UUID msetId) {
    Field<UUID> mksId = DSL.val(msetId);
    GenomeMarkersetRecord gmRec = dsl.select(GENOME_MARKERSET.asterisk())
      .from(GENOME_MARKERSET)
      .join(MARKERSET).on(GENOME_MARKERSET.ID.eq(MARKERSET.GENOME_ID))
      .where(MARKERSET.ID.eq(mksId))
      .limit(1)
      .fetchOne(r->r.into(GENOME_MARKERSET));
    
    setSessionInteger(req, "genomeBuild", gmRec.getPrimaryBuild());
  }

  private Integer getGenomeBuild(HttpServletRequest req) {
    return getSessionInteger(req, "genomeBuild");
  }
}
