package edu.utah.camplab.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

@WebServlet(
  name = "WebMonitor",
  urlPatterns = {"/webmonitor"}
)
public class MonitorServlet extends AbstractSGSServlet {

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    ServletOutputStream out = resp.getOutputStream();
    if (getDbHost() == null) {
      out.write("SGS_FAIL_DB".getBytes());
      out.write(String.format("\n who is %s, anyway", getDbRole(req)).getBytes());
      //logger.error("No db info in jndi lookups");
    }
    else {
      try {
        out.write(String.format("SGS_OK_WEB using db %s:%s", getDbHost(), getDbPort()).getBytes());
        out.write(("\nTime now is " + java.time.LocalDateTime.now()).getBytes());
        out.write(("\n" + getClass().getName()).getBytes());
        for (String tuple : getAvailableProjects(sgsContext())) {
          out.write(("\n" + tuple).getBytes());
        }
      }
      catch (NamingException nex) {
        log(nex.getMessage());
      }
    }
    out.flush();
    out.close();
  }
}
