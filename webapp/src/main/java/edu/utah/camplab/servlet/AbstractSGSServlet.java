package edu.utah.camplab.servlet;

import edu.utah.camplab.db.generated.default_schema.tables.records.SegmentRecord;
import edu.utah.camplab.db.generated.base.tables.records.LdCliqueRecord;
import edu.utah.camplab.db.generated.base.tables.records.MarkersetRecord;
import edu.utah.camplab.jx.SGSFullContext;
import static edu.utah.camplab.db.generated.base.Tables.MARKER;
import static edu.utah.camplab.db.generated.base.Tables.MARKERSET;
import static edu.utah.camplab.db.generated.base.Tables.MARKERSET_MEMBER;
import static edu.utah.camplab.db.generated.base.Tables.LD_CLIQUE;
import static edu.utah.camplab.db.generated.default_schema.tables.Segment.SEGMENT;

import static org.jooq.impl.DSL.*;

import org.apache.tomcat.dbcp.dbcp2.BasicDataSource;
import org.jooq.*;
import org.jooq.impl.DSL;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;

import javax.naming.Binding;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.*;

public abstract class AbstractSGSServlet extends HttpServlet {

  public record ParLd(Result<?> markers, List<LdCliqueRecord> cliques, MarkersetRecord markerset) { }
  public record SegAdj(int pter, int qter) { }
  
  // expected request headers
  public static final String SGSServlet_HEADER_DBNAME = "dbname";  // PI
  public static final String SGSServlet_HEADER_DBROLE = "dbrole";  // study
  public static final String SGSServlet_HEADER_DBHOST = "dbhost";  // machine
  public static final String SGSServlet_HEADER_DBPORT = "dbport";  // connection port
  public static final String SGSServlet_HEADER_DBWHAT = "dbwhat";  // process

  // We believe these are set at server start-up
  private static String	dbhost;
  private static Integer dbport;
  private static String	extension;
  private static Integer expansionStep;

  // We are told this is a) heavyweight and b) thread safe
  private ObjectMapper objectMapper;

  public void init() {
    try {
      Class.forName("org.postgresql.Driver");
    }
    catch (ClassNotFoundException  ne) {
      ne.printStackTrace();
    }
    objectMapper = new ObjectMapper();
    objectMapper.configure(SerializationFeature.INDENT_OUTPUT, false);
    objectMapper.configure(JsonGenerator.Feature.AUTO_CLOSE_TARGET, false);
    objectMapper.setDefaultPropertyInclusion(JsonInclude.Value.construct(Include.NON_EMPTY, Include.ALWAYS));
    
    lookupHostAndPort();
  }

  public DataSource getDataSource(String jndi) {
     DataSource lookupDS = null;
     try {
	log("get db context for " + jndi);
	lookupDS = (org.apache.tomcat.jdbc.pool.DataSource) sgsContext().lookup("jdbc/sgsdb/"+jndi);
      }
     catch (NamingException ne) {
       lookupDS = null;
       log(ne.getMessage()); //TODO
     }
    return lookupDS;
  }

  public void lookupHostAndPort() {
    try {
      InitialContext ic = new InitialContext();
      dbhost = (String) ic.lookup("java:comp/env/databaseHost");
      dbport = (Integer) ic.lookup("java:comp/env/databasePort");
      extension = (String) ic.lookup("java:comp/env/roleExtension");
      expansionStep = (Integer) ic.lookup("java:comp/env/expansionStep");
      log("initial db connections strings: host=" + dbhost + " port=" + dbport);
    } 
    catch (NamingException ne) {
      ne.printStackTrace();
      throw new RuntimeException(ne);
    }
  }

  public ObjectMapper getObjectMapper() { return objectMapper; }

  public Connection getDbConn(HttpServletRequest req, HttpServletResponse resp) {
    HashMap<String, ArrayList<String>> pmap = getParameterMap(req);
    return getDbConn(req, pmap);
  }

  public Connection getDbConn(HttpServletRequest req, HashMap<String, ArrayList<String>> pmap) {
    String dbrole;
    String dbname;
    Connection conn = null;
    try {
      dbrole = getDbRole(req);
      if (dbrole == null && pmap.containsKey(AbstractSGSServlet.SGSServlet_HEADER_DBROLE)) {
        dbrole = getParameterMap(req).get(AbstractSGSServlet.SGSServlet_HEADER_DBROLE).get(0);
      }
      dbname = getDbName(req);
      if (dbname == null && pmap.containsKey(AbstractSGSServlet.SGSServlet_HEADER_DBNAME)) {
        dbname = getParameterMap(req).get(AbstractSGSServlet.SGSServlet_HEADER_DBNAME).get(0);
      }
      // If neither role or db are set, we'll hear about it
      //String turl = String.format("jdbc:postgresql://%s:%d/%s", dbhost, dbport, dbname);
      String jndiName = String.format("%s-%s", dbrole, dbname);
      conn = getDataSource(jndiName).getConnection(dbrole, dbrole + extension);
    }
    catch (SQLException ne) {
      ne.printStackTrace();
    }
    return conn;
  }

  public String getDbHost() {
    return dbhost;
  }
  public int getDbPort() {
    return dbport;
  }
  public String getExtension() {
    return extension;
  }
  public int getExpansionStep() {
    return expansionStep;
  }

  // The PI's surname, typically
  public String getDbName(HttpServletRequest req) {
    return req.getHeader(AbstractSGSServlet.SGSServlet_HEADER_DBNAME);
  }

  // A particular project/study of PI
    public String getDbRole(HttpServletRequest req) {
    return req.getHeader(SGSServlet_HEADER_DBROLE);
  }

  // public String getDbWhat(HttpServletRequest req) {
  //   return req.getHeader(SGSServlet_HEADER_DBWHAT);
  // }

  public HashMap<String, ArrayList<String>> getParameterMap(HttpServletRequest request) {
    HashMap<String, ArrayList<String>>pmap = new HashMap<>();
    String queryString = request.getQueryString();
    if (queryString != null && queryString.length() > 0) {
      try {
        queryString = URLDecoder.decode(queryString, StandardCharsets.UTF_8.toString());
        String[] queryParts = queryString.split("&");
        for (String part : queryParts) {
          String[] pv = part.split("=");
          String paramName = pv[0];
          ArrayList<String> valueList;
          if (! pmap.containsKey(paramName)) {
            valueList = new ArrayList<>();
            pmap.put(paramName, valueList);
          }
          if (pv.length == 1) {
            pmap.get(paramName).add("true");
          }
          else {
            for (int i = 1; i < pv.length; i++) {
              pmap.get(paramName).add(pv[i]);
            }
          }
        }
      }
      catch(UnsupportedEncodingException eue) {
        eue.printStackTrace();
      }
    }
    return pmap;
  }

  // gather all cliques which mention any locus in the adjusted (+/-16) segment definition
  public List<LdCliqueRecord> readSegmentCliques(DSLContext context, SegAdj adjuster, SegmentRecord segRec) {
    List<LdCliqueRecord> lddata = context
      .selectDistinct(LD_CLIQUE.asterisk())
      .from(LD_CLIQUE)
      .join(MARKERSET_MEMBER)
      .on(MARKERSET_MEMBER.ORDINAL.eq(DSL.any(LD_CLIQUE.LOCI_ORDINALS))
          .and(LD_CLIQUE.MARKERSET_ID.equal(MARKERSET_MEMBER.MARKERSET_ID)))
      .where(LD_CLIQUE.MARKERSET_ID.equal(segRec.getMarkersetId())
	     .and(MARKERSET_MEMBER.ORDINAL.between(adjuster.pter()).and(adjuster.qter())))
      .orderBy(LD_CLIQUE.ORDINAL)
      .fetch(r -> r.into(LD_CLIQUE));
    return lddata;
  }

  public TreeSet<Integer> lociOrdinalsFromCliques(List<LdCliqueRecord> cliques) {
    TreeSet<Integer> ordinalSet = new TreeSet<>();
    for(LdCliqueRecord clique : cliques) {
      for (Integer ord : clique.getLociOrdinals()) {
        ordinalSet.add(ord);
      }
    }
    return ordinalSet;
  }

  public SegAdj adjustedLociIndex(SegmentRecord segment) {
    int expander = 3 * getExpansionStep() + 1;  //Three steps of 5, plus the boundning marker
    int pterIndex = expander > segment.getFirstmarker() ? 0 : segment.getFirstmarker() - expander;
    int qterIndex = segment.getLastmarker() + expander; // We could lookup max ordinal but the /between/ will safely stop
    return new SegAdj(pterIndex, qterIndex);
  }

  public void setSessionString(HttpServletRequest req, String varname, String value ) {
    req.getSession().setAttribute(varname, value);
  }

  public Integer getSessionInteger(HttpServletRequest req, String varname ) {
    return (Integer) req.getSession().getAttribute(varname);
  }

  public void setSessionInteger(HttpServletRequest req, String varname, Integer value ) {
    req.getSession().setAttribute(varname, value);
  }

  public String getSessionString(HttpServletRequest req, String varname ) {
    return (String) req.getSession().getAttribute(varname);
  }

  public void setSessionUUID(HttpServletRequest req, String varname, UUID value ) {
    req.getSession().setAttribute(varname, value);
  }

  public UUID getSessionUUID(HttpServletRequest req, String varname ) {
    return (UUID) req.getSession().getAttribute(varname);
  }

  public List<String> getAvailableProjects(javax.naming.Context context) {
    ArrayList<String> plist = new ArrayList<>();
    try {
      NamingEnumeration<Binding> items = context.listBindings("");
      while(items.hasMore()) {
        Binding binder = items.next();
        Object item = binder.getObject();
        if (item instanceof javax.naming.Context) {
          plist.addAll(getAvailableProjects((javax.naming.Context)item));
        }
        String entry;
        switch(item.getClass().getSimpleName()) {
          case "BasicDataSource":
            entry = " ==> database";
            break;
          case "DataSource":
            String prop = ((org.apache.tomcat.jdbc.pool.DataSource)item).getPoolProperties().getUsername();
            entry = " ==> project " + prop;
            prop = ((org.apache.tomcat.jdbc.pool.DataSource)item).getPoolProperties().getUrl();
            entry += ", url = " + prop;
            break;
          case "NamingContext":
            entry = " ==> db context";
            break;
          case "String":
          case "Integer":
            entry = "==> " + item.toString();
            break;
          default:
            entry = "";
        }
        plist.add(binder.getName() + entry);
      }
     }
    catch (Throwable nex) {
      log("sgsContext lookup failed using java:/comp/env\n" + nex.getMessage());
      throw (new RuntimeException("initial context trouble", nex));
    }
    return plist;
  }

  public Context sgsContext() throws NamingException {
    Context dbctx = null;
    InitialContext ic = new InitialContext();
    dbctx  = (Context) ic.lookup("java:/comp/env");
    return dbctx;
  }
}
