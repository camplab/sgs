package edu.utah.camplab.servlet;

import edu.utah.camplab.db.generated.base.tables.Person;
import edu.utah.camplab.db.generated.base.tables.records.LdCliqueRecord;
import edu.utah.camplab.db.generated.default_schema.tables.Segment;
import edu.utah.camplab.db.generated.default_schema.tables.records.ProbandsetRecord;
import edu.utah.camplab.db.generated.default_schema.tables.records.SegmentRecord;
import edu.utah.camplab.db.generated.public_.tables.AscendTrim;
import edu.utah.camplab.db.generated.public_.routines.Byteasub;

import edu.utah.camplab.jx.SGSFullContext;

import org.jooq.*;
import org.jooq.impl.DSL;

import java.io.*;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.*;
import javax.annotation.Resource;
import javax.servlet.annotation.WebServlet;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import static edu.utah.camplab.db.generated.base.Tables.*;
import static edu.utah.camplab.db.generated.default_schema.Tables.*;
import static edu.utah.camplab.db.generated.public_.Routines.ascendTrim;
import static edu.utah.camplab.db.generated.public_.tables.AscendTrim.*;
import static edu.utah.camplab.db.generated.public_.routines.Byteasub.*;
import static org.jooq.impl.DSL.*;

@WebServlet(
  name = "Pedfile",
  urlPatterns = {"/pedfile"}
)

public class PedfileServlet extends AbstractSGSServlet {
  private static  final String SESSION_VAR_MARKERS = "markersetId";
  private static  final String SESSION_VAR_PEOPLE = "peopleId";
  private static  final String SESSION_VAR_SEGMENT = "segmentId";

  public void doGet(HttpServletRequest req, HttpServletResponse resp) {
    HashMap<String, ArrayList<String>> pmap = getParameterMap(req);
    try (Connection conn = getDbConn(req, pmap);) {
      DSLContext dsl = SGSFullContext.DSLContextFactory(conn);
      if (pmap.containsKey("segment")) {
        SegmentRecord segRec =
          dsl.selectFrom(SEGMENT).where(SEGMENT.ID.equal(UUID.fromString(pmap.get("segment").get(0)))).fetchOne();
        setSessionUUID(req, SESSION_VAR_SEGMENT, segRec.getId());
        setSessionUUID(req, SESSION_VAR_MARKERS, segRec.getMarkersetId());
        setSessionUUID(req, SESSION_VAR_PEOPLE, readPeopleId(dsl, segRec));
      } 
      if (pmap.containsKey("markerset")) {
        setSessionUUID(req, SESSION_VAR_MARKERS, UUID.fromString(pmap.get("markerset").get(0)));
        setSessionUUID(req, SESSION_VAR_PEOPLE, UUID.fromString(pmap.get("people").get(0)));
      }
      Result<?> peddata = readPedData(req, dsl);
      ServletOutputStream ps = resp.getOutputStream();
      byte lomask = 0x0000000F;
      for (org.jooq.Record gline : peddata) {
        byte[] calldata = (byte[]) (gline.getValue("gtdata"));
        int hasCall = calldata == null ? 0 : 1;
        ps.print(String.format("%s %s %s %s 0 0 0 %d %d",
			       gline.getValue("pedname"),
			       gline.getValue(PERSON.NAME),
			       gline.getValue("paname"),
			       gline.getValue("maname"),
			       gline.getValue("regender"),
			       hasCall));
        if (hasCall == 1) {
          for (byte bt : calldata) {
            int lo = (0 + bt) & lomask;
            int hi = (0 + bt) >> 4;
            ps.print(String.format(" %d %d", hi, lo));
          }
        }
        ps.println();
        ps.flush();
      }
    } catch (IOException | SQLException ee) {
      try {
        resp.setStatus(501);
        resp.getOutputStream().write(ee.getMessage().getBytes());
        resp.getOutputStream().flush();
        resp.getOutputStream().close();
      } catch (IOException ignore) {
        ;
      }
    }
  }

  private UUID readPeopleId(DSLContext ctx, SegmentRecord sRec) {
    ResultQuery<Record1<UUID>> query = ctx
      .select(PROBANDSET.PEOPLE_ID)
      .from(PROBANDSET)
      .where(PROBANDSET.ID.equal(sRec.getProbandsetId()));
    return ctx.fetchValue(query);
  }
    
  private Result<?> readPedData(HttpServletRequest req, DSLContext dsl) throws IOException {
    Person ego = PERSON.as("ego");
    Person ma = PERSON.as("ma");
    Person pa = PERSON.as("pa");

    Result<?> peddata = dsl
      .select(PEOPLE.ID.as(SESSION_VAR_PEOPLE),   // or just req.param...getSessionUUID(req, "peopleid"))
	      PEOPLE.NAME.as("pedname"),
	      ego.NAME,
              (coalesce(pa.NAME, "0")).as("paname"),
              (coalesce(ma.NAME, "0")).as("maname"),
	      choose(ego.GENDER)
	      .when("m", 1)
	      .when("f", 2)
	      .otherwise(0).as("regender"),
	      GENOTYPE.CALLS.as("gtdata"))
      .from(PEOPLE)
      .join(PEOPLE_MEMBER).on(PEOPLE.ID.equal(PEOPLE_MEMBER.PEOPLE_ID))
      .join(ego).on(PEOPLE_MEMBER.PERSON_ID.equal(ego.ID))
      .leftJoin(pa).on(ego.PA.equal(pa.ID))
      .leftJoin(ma).on(ego.MA.equal(ma.ID))
      .leftJoin(GENOTYPE).on(ego.ID.equal(GENOTYPE.PERSON_ID)
			     .and(GENOTYPE.MARKERSET_ID.equal(getSessionUUID(req, SESSION_VAR_MARKERS)))
			     .and(PEOPLE.ID.equal(GENOTYPE.PEOPLE_ID)))
      .where(peopleCondition(req, dsl))
      .fetch();
    return peddata;
  }

  private Condition peopleCondition(HttpServletRequest req, DSLContext ctx) {
    Condition clause = trueCondition();
    clause = clause.and(PEOPLE.ID.equal(getSessionUUID(req, SESSION_VAR_PEOPLE)));
    if (getParameterMap(req).containsKey("segment")) {
      clause = clause.and(GENOTYPE.PERSON_ID
    			  .in(getSegmentPersons(getSessionUUID(req, SESSION_VAR_SEGMENT), ctx))
			  .and(GENOTYPE.PEOPLE_ID.eq(getSessionUUID(req, SESSION_VAR_PEOPLE))));
    }
    return clause;
  }

  private List<UUID> getSegmentPersons(UUID segId, DSLContext context) {
    ProbandsetRecord pbsRec =
      context.select(PROBANDSET.asterisk()).from(PROBANDSET)
      .join(SEGMENT).on(PROBANDSET.ID.eq(SEGMENT.PROBANDSET_ID))
      .where(SEGMENT.ID.eq(segId))
      .fetchOne(r->r.into(PROBANDSET));
    List<UUID> segIdList = Arrays.asList(pbsRec.getProbands());
    return segIdList;
  }

  private Result<?> readTrimmedPedData(HttpServletRequest req, DSLContext dsl, SegmentRecord sr) {

    SegAdj adjusted = adjustedLociIndex(sr);

    List<LdCliqueRecord> cliques = readSegmentCliques(dsl, adjusted, sr);
    //Collect all the marker ordinals seen in the cliques.
    //This is not guaranteed to be consecutive outside the segement+stops+padding region
    TreeSet<Integer> ordinalSet = lociOrdinalsFromCliques(cliques);
    // find where the "contiguous loci" start in the list ref'd by cliques
    int pterOffset = -1;
    for (Integer ordinal : ordinalSet) {
      pterOffset++;
      if (ordinal.equals(sr.getFirstmarker()))
	break;
    }
    if (pterOffset < 0) {
      throw new RuntimeException("Trouble with expanded loci set: size = " + ordinalSet.size());
    }
    // So we walk back from pter
    int paddingStart = sr.getFirstmarker() - pterOffset;
    Field<Integer> fieldStart = DSL.val(paddingStart);
    Field<Integer> fieldLength = DSL.val(ordinalSet.size());
    Person ego = PERSON.as("ego");
    Person ma = PERSON.as("ma");
    Person pa = PERSON.as("pa");    
    Byteasub bsub = new Byteasub();

    Result<?> peddata = dsl
      .selectDistinct(PEOPLE.NAME.as("pedname"),
		      ego.NAME,
		      (coalesce(pa.NAME, "0")).as("paname"),
		      (coalesce(ma.NAME,"0")).as("maname"),
		      choose(ego.GENDER)
		      .when("m", 1)
		      .when("f", 2)
		      .otherwise(0).as("regender"),
		      bsub.setBits(GENOTYPE.CALLS)
		      .setFbyte(fieldStart)
		      .setLbyte(fieldLength).asField("gtdata"))
      .from(PEOPLE)
      .join(PROBANDSET).on(PEOPLE.ID.equal(PROBANDSET.PEOPLE_ID))
      .join(SEGMENT).on(PROBANDSET.ID.equal(SEGMENT.PROBANDSET_ID))
      .join(PEOPLE_MEMBER).on(PEOPLE.ID.equal(PEOPLE_MEMBER.PEOPLE_ID))
      .join(ego).on(PEOPLE_MEMBER.PERSON_ID.equal(ego.ID))
      .join(ascendTrim(PROBANDSET.PROBANDS)).on(ego.ID.equal(ASCEND_TRIM.ID))
      .leftJoin(pa).on(ego.PA.equal(pa.ID))
      .leftJoin(ma).on(ego.MA.equal(ma.ID))
      .leftJoin(GENOTYPE).on(ego.ID.equal(GENOTYPE.PERSON_ID).and(GENOTYPE.MARKERSET_ID.equal(SEGMENT.MARKERSET_ID)))
      .where(SEGMENT.ID.equal(getSessionUUID(req, SESSION_VAR_SEGMENT)))
      .fetch();
    return peddata;
  }
}
