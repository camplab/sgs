#+OPTIONS: toc:nil
#+OPTIONS: author:nil
#+OPTIONS: ^:nil
#+LaTeX_HEADER: \usepackage[T1]{fontenc}
#+LaTeX_HEADER: \usepackage{libertine}
#+LaTeX_HEADER: \usepackage{lmodern}
#+LATEX_HEADER: \usepackage[margin=0.65in]{geometry}
#+LATEX_HEADER: \usepackage{setspace}
#+LATEX_HEADER: \usepackage{enumitem}
#+LaTeX_HEADER: \usepackage{endnotes}
#+LaTeX_HEADER: \renewcommand*\oldstylenums[1]{{\fontfamily{fxlj}\selectfont #1}}
#+LATEX_HEADER: \onehalfspacing
#+LATEX_HEADER: \FlushLeft
#+LATEX_HEADER: \newenvironment{JUSTIFYLEFT}{\begin{FlushLeft}}{\end{FlushLeft}}
#+ODT_STYLES_FILE: "/home/rob/utah/orgs/odtstyles.xml"
#+ATTR_LATEX: :options [noitemsep]
#+TITLE: SGS Handbook
#+SUBTITLE: Preparation and Execution

Shared Genomic Segments[fn:1] (SGS) analysis is a compute intensive endeavour and best
done on today's time-share platforms such as Amazon Web Services(AWS) or University of
Utah Center for High Performance Computing (CHPC).  Described herein are the hardware and
software facilities which come into play when performing an SGS analysis.

#+TITLE:     handbook.org
#+AUTHOR:    Rob Sargent
#+EMAIL:     u0138544@imap.umail.utah.edu
#+DATE:      2023-11-08 Wed
#+DESCRIPTION:
#+KEYWORDS:
#+LANGUAGE:  en
#+OPTIONS:   H:3 num:t toc:t \n:nil @:t ::t |:t ^:nil -:t f:t *:t <:t
#+OPTIONS:   TeX:t LaTeX:t skip:nil d:nil todo:t pri:nil tags:not-in-toc
#+INFOJS_OPT: view:nil toc:nil ltoc:t mouse:underline buttons:0 path:http://orgmode.org/org-info.js
#+EXPORT_SELECT_TAGS: export
#+EXPORT_EXCLUDE_TAGS: noexport
#+LINK_UP:   
#+LINK_HOME: 
#+XSLT:
* System preparation
** Obtaining the software
Though all the code for SGS analysis is publicly available [[https://gitlab.com/camplab/sgs]], it's
not trivial to build.  The suite id built using the gradle tool and does build cleanly from a
clone.[fn:2] The necessary binaries are also available.  On the other hand, it is practical to
get the file preparation software from https://gitlab.com/camplab/prep.  For each of the
supported environments we supply all necessary ingredients via shared resources at each target.
Not addressed here is the software involved in determining the correct strand
orientation for data generated via micro-array technology.  Suffice it is to say that this
correction has been performed for all micro-array platforms thus far encountered[fn:3] and the
lists of loci needing to be converted are available.  This correction is not strictly necessary
for SGS analysis /per se/ but will make post-SGS analysis involving sequence data much more
straight forward ([[TrueForward][TrueForward]]).

** Obtaining the hardware
Before any work is done on genetic data, it is necessary to establish a working
environment in which these data may be properly restructured for the analysis.  SGS uses
an RDBMS to store the genetic data and the SGS results.  One portion of the file
preparation protocol relies on this service being available.  However detail on installing
a database server is out of scope for this document.  The necessary database tables and
such are created by running =createInvestigator.sh=, which can be found in the SGS
distribution tar.  The only RDBMS tried to date is PostgreSQL.  A central server
(middleware) must be identified and accessible to all machines which perform the analyses.
The middleware hands all communication between analysis clients and the database.
*** At AWS
- *Bastion*: strictly a landing spot for accessing AWS run-time operations.  This is the
  only AWS device that is exposed to the internet.  It runs Linux.  From the bastion, one
  connects to the middleware server.  AWS keys are required to connect the bastion, and it
  accepts connections only from allow-listed IP addresses.
- *Middleware server*: this Linux machine runs the embedded Tomcat server and is the only
  device which can access the database server.  An agent[fn:4] must log-on to this device
  to install the current SGS software, start the service, access the database.  The "prep"
  project contains the script which deploys this component, =sgsStartServer.sh=.  Access
  to the Rutgers Genetic Maps must be made available in the shared directory of each
  environment.
- *Database server*: currently at as (=sgsdbv4.cvh22rpnwift.us-west-2.rds.amazonaws.com=),
  this is an RDS PostgreSQL (Version 12) instance,
- *Web app*: a browser-based application for running all analyses at AWS.  Accessed at
  https://ppr-sgs.hci.utah.edu.  This application allows an Agent to send jobs to
  on-demand EC2 instances which communicate with the middleware server to get data input
  and send results.  There are several database scripts which need only be run at AWS.
  These are part of the [[https://gitlab.com/camplab/web-app repo]]
  
- *SGS Shared resources*: 
  | S3 Bucket                        | Description                             |
  | S3://691459864434-sgs-source     | SGS release software (distribution tar) |
  | S3://691459864434-sgs-input      | Investigator source data                |
  | S3://691459864434-sgs-interim    | MultiAssess mid-run save files          |
  | S3://691459864434-sgs-chase      | currently under utilized                |
  | S3://691459864434-sgs-processing | file prep storage area                  |
  | S3://691459864434-sgs-logs       | log files generated on analysis runs    |

- *Run-time Topology* 
     [[./SGS-AWS-V6.diagram.png]]
*** At CHPC
- *Interactive node*: general access to CHPC facilities.  A user would connect here
  (=redwood.chpc.utah.edu=) to initiate SGS analysis jobs.  This is accomplished using the
  SLURM tool set to submit (=sbatch=) and manage (=squeue=, =scancel=) SGS analysis jobs.
- *Middleware server* (=dohertyint.chpc.utah.edu=): this device runs an embedded Tomcat
  server.  All analysis jobs (per =sbatch=) get their input from and send their results to
  this service, which in turn writes to the database.  *camplab* personnel maintain this
  service and as such have sudo permission to edit the
  =/etc/systemd/system/campselect.service= file.  The main point here is to name the
  current SGS release tar file.  Note that each restart of this service rebuilds the
  installation.
- *Database server* (=csgsdb.chpc.utah.edu=): A postgres installation maintaining all SGS
  databases for *camplab* and collaborators.  This service is accessible from an
  interactive node at CHPC (not from outside CHPC).
- *SGS Shared resources*: =/uufs/chcp.utah.edu/sys/installdir/sgspub= is made generally
  available by CHPC. Here we deposit new releases of SGS in the form of a tar file.
  Importantly the =bin= directory therein houses the middleware start script named in the
  systemd service file referenced above.
  
- *Run-time Topology* 
     [[./chpcTopology.png]]

* Input file preparation
There maybe several data sources involved in an SGS analysis.  Typically at Utah, from the
investigator (PI) come the genetic data, from PPR (UPDB) come the pedigree definitions and
from UGP (or 1KG) [fn:5] we get a form of control data.  Genetic mapping information is supplied
by Rutgers University.

** Genotype input
   The raw genetic data may have been ascertained from either micro-array chip genotyping or
   whole genome sequencing[fn:6].  Accepted data formats for SGS are either a =plink= file set (.map
   and .ped) or as a VCF.  An incoming VCF is immediately converted to a =plink= file pair and
   the processing picks up there, using only the loci associated with an approved chip[fn:7].
   Three scripts accomplish this portion of the workflow.  Each does a fair jobs of describing
   its usage.

1. =genoPrepQC.sh= takes the PI's genetic data (either a vcf or plink file) and combines
   it with a suitable control data set in order to present a single data file for the
   following quality control step.  *If the incoming data contains sufficient control
   data[fn:8] this step is not necessary.* However, if it is not run then =qc_by_plink.sh=
   MUST stll be run and that requires the data in plink format.
1. =qc_by_plink.sh= assesses the full data set (case plus control) removing under
   performing markers and poorly genotyped individuals.  The final plink file pair from this script
   must be split to per chromosome data sets.
#+BEGIN_SRC language=bash
  parallel --joblog split.ll --jobs 22 --verbose plink --bfile ./<final> --chr {1} --out <PI_STUDY>-chr{1} --recode ::: {1..22}
#+END_SRC
1. [@3] =downcode.sh= re-codes the genotype calls from A,C,G,T to 1,2 and transforms the data
   to the LINKAGE[fn:9] file format (.ped and .par file pairs per chromosome).  =downcode.sh=
   establishes the file names used hereafter. [fn:10]

#+BEGIN_SRC language=bash
  # downcode with parallel
  rutmapDir=/uufs/chpc.utah.edu/common/HIPAA/proj_updb/RutgerMapV3
  parallel --joblog log.ll --verbose --jobs N \ 
    downcode.sh --chrom {} --stem <PI_STUDY>-chr --ma $rutmapDir --mi 100 -9 --split ::: {1..22}
#+END_SRC
   
*** Files
+ Input 
  + WGS VCF or Genome-wide plink file pair (.ped + .map).
+ Output
  + LINKAGE .ped and .par files per-chromosome containing all /case/ genotypes
  + LINKAGE .ped and .par files per-chromosome containing /control/ genotypes

Though all steps listed above could be done in a single script, the cautious investigator will
appreciate the access to the results of each phase.  They all take but a minute or two to
complete.  The scripts allow the user to specify working directories, in which the processed
data is written along with any log files generated.

** Pedigree input (requires database and SGS executables[fn:11])
Given a complete population file (kindred, ego, father, mother, sex, affection-status)
describing all cases in each kinship presumably presenting elevated disease risk, this
portion of the SGS file prep workflow will reduce the input to non-redundant pedigree
definitions trimmed of irrelevant members. PDFs of those pedigrees are generated
(providing that R is available).  This is accomplished by a single control script
  - =pedPrepQC.sh= which calls
    1) =pedPrep.py= 
    1) =pedPrintR.sh=
    1) =meiosesCount.sh=
The control script allows the user to supply the name of the directory for the files generated.
During the execution of this controlling script an sql script is created (by =pedPrep.py=) and
later run (by =pedPrintR.sh=).  The purpose of this sql is to derive the minimal pedigrees
based on finding the nearest common ancestor for a give set of cases.  The pedigrees so
constructed are then passed to a drawing package (pedPrintR.sh).  These drawings should be
reviewed by all concerned before continuing.  For each of these pedigrees a total
meioses count is reported along with kinship coefficients[fn:12] (=meiosesCount.sh=)

*** Files
+ Input
  + Entire study population with pedigree structure information.  This may be degenerate
    if any persons are shared across multiple pedigrees.  In such a case the shared case
    uses the same ego id and separate kinship ids.
+ Output
  + CSV file per pedigree trimmed to minimal participation
  + .meiosis file per pedigree with total meioses counts and kinship coefficients for all
    subsets of 2 or more cases

*** At AWS
- these are placed in a project specific AWS S3 bucket.  
*** At CHPC 
- the agent controls the file locations, but must send to an IRB-restricted mount point specific to the Investigator (and/or project)
    
** Conjunction
The project genotype data (in LINKAGE .ped files) needs to be expanded to include the
encompassing pedigree members (as to this point only the genotyped cases are present in the
LINKAGE files).  For this we run =finalizeQCped.py= supplying the case genotype data from the
files generated in [[ Genotype input ][Genotype Input]].  An id mapping file may be require to coordinate study
sample ids with person ids (e.g. ids from PPR).  This does have the unfortunate consequence of
inflating the .ped files with copious zero genotype calls.  This is one of the driving forces
for the next revision of SGS software.
*** Files
+ Input
  + LINKAGE file pairs for case data from [[ Genotype input ]]
+ Output
  + LINKAGE file pairs per study pedigree

The entire product of [[Input file preparation][file prep]] amounts to
 - Per chromosome LINKAGE parameter files (=*.par=)
 - Per chromosome LINKAGE pedigree files for the /control genotypes/ (e.g. =CUE.chrN.ped=)
 - Per chromosome LINKAGE pedigree files /for each pedigree/ (e.g. =kinId.chrN.ped=)
 - Per chromosome allele mapping files produced by =downcode.sh= (e.g. =Downcode-chr9.a12=) which contain
   the actual alleles (ACGT) presented for each marker

*** At AWS
- these are placed in a project specific AWS S3 bucket.  
*** At CHPC
- the agent controls the file locations, but most appropriately sent to an IRB-restricted mount point.
    
** Linkage disequilibrium (FitGMLD)
The final step in file preparation is the generation of the Linkage Disequilibrium files.
*These are not quickly made.* This executable, =edu.utah.camplab.app.FitGMLD=, is
grossly copied from Dr Thomas's original of same name.  Current hardware employed locally
requires 5 days for longest chromosomes.  This programme is not multi-threaded and of modest
memory requirements.  As such, multiple instances of FitGMLD may be run simultaneously on
one machine.

 - _At AWS_: We have a script, *run locally*[fn:13], =fitGMLDJson.sh=, which will invoke FitGMLD, for
   each chromosome using an on-demand in-expensive EC2 instance.  
 - _At CHPC_: Each per chromosome pairing of "par" file and control "ped" file is used to
   generate that chromosome's linkage disequilibrium map using the control data.  A slurm job
   can be used to run each chromosome.  This will likely require using =owner-node= machines as
   the calculation will exceed the 3-day maximum wallclock for at least the largest half-dozen
   chromosomes. [fn:14]

#+CAPTION[Critical point]: NOTE: it is *quite important* to redirect stderr as shown on the last line of the example command.
#+BEGIN_SRC language=bash
  export CLASSPATH=<jardir>/*
  # Take care to capture stderr.
  java edu.utah.camplab.app.FitGMLD -l chromN.ld >2 fitgmld-chrN.dots
  # Days may go by
#+END_SRC
Going forward in the workflow, the resulting ".ld" files effectively replace the ".par"
files generated by =downcode.sh=.  The ".ld" files are valid LINKAGE ".par" files with an
appended linkage disequilibrium model.

* Database preparation
A single PostgreSQL database server handles all projects simultaneously on the assumption
that rarely will more than one project be active at any given time.  It is a moderately
equipped machine ([fn:15]) but can handle pedigrees with up to 16 probands, one pedigree at
a time.

The initial data import is handled by a java programme, =PedParLoader=.  This application
writes directly to the database, loading in people, pedigree structure, the loci and all
the genotype data.  This application uses a "file-of-files" parameter in which are all the
pairing of ped and par(ld) files generated by file preparation.  The structure of the file
is a list of lines of comma separated values and is easily generated (per pedigree, per
chromosome).  Each line of the "file-of-files" has, in this order
 - (absolute) path to .ped file
 - the pedigree name
 - (absolute) path to .ld file
 - (absolute) path to output .a12 file (allele map)
 - chromosome
Unlike the file preparations scripts, this program may be safely re-run on the same input
file as necessary.  Any line starting with a hash mark ('#') will not be processed.  Any
line which has been successfully processed previously will also be ignored.  The complete
java command line would be as follows:

#+Caption: Example command line for data import.
#+BEGIN_SRC language=bash
java edu.utah.camplab.app.PedParLoader \
  --pedparfof <file-of-files described above e.g. study.fof> \
  --genome <chip name e.g. GSA24v2> \
  --dbaddr <db server host name and port e.g. csgsdb:5432> \
  --investigator <PI surname> \
  --build <genome hg build number, 37 or 38> 
#+END_SRC

* Running SGS 

An SGS analysis for one pedigree is done in three steps.  The first and third steps are
variations on single main executable depending on the "operation" parameter.  The
necessary database identifiers are generated during the data import step above.  SQL
access is required to access these values.

1) *MultiAssess analysis*: detects in the observed data for each chromosome, all possible
   segments for all possible non-degenerate subsets of the given pedigree.  This is
   followed by simulation/assessment loops wherein each simulated data set is tested for
   the presence of each segment.  A segment may be replicated exactly or found within a
   simulated segment or not found at all.  Each such event is counted and the final tally
   of each of the three possibilities are recorded in the database.  This is typically
   done to 1,000,000 simulations (though for smaller pedigree a total of 300,000
   simulations might suffice.  MultiAssess may also be run more than once.  Doing so is
   *not advised* after any p-value refinement (Chasing) has been done.
#+Caption: Assumes CLASSPATH contains SGS and dependent jars
#+BEGIN_SRC language=bash
     # Command line for "MultiAssess" (MA)
     java edu.utah.camplab.app.SGSPValue \
       --operation 3 \
       --pedigree <pedigree's UUID from db> \
       --markerset <markerset's UUID from db> \
       --accumulator <host:port eg csgsdb:6432> \
       --dbname <PI surname> \
       --json-dir <local dirname of interim files>
       --effortName <specific study> \
       --environment <AWS or CHPC> \
       --IRB-id <CHPC assigned value> required on CHPC\
       --simscount 1000000 \
       --maxtime <maximum runtime, in seconds if specified. default = 258000> 

       # Help command line
     java edu.utah.camplab.app.SGSPValue \
       --operation 3 \
        --help
#+END_SRC
     
1) [@2] *Threshold analysis*: from the genome-wide distribution of p-values produced by
   MultiAssess we obtain suggestive and significant p-values.  This analysis is performed
   within the database by execution of a /database procedure/.

#+Caption:
#+BEGIN_SRC :language sql
select * from genome_threshold_mono('<probandset-name>', '<genome-name>');
given: 
-- probandset-name is most likely the name of the probandset which includes all probands for
  the pedigree of interest.
-- genome-name represent the genome wide collection of markers
#+END_SRC

1) [@3] *p-value refinement (Chase)*: for any segment whose p-value confidence interval spans
   either derived threshold (suggestive or significant) we simulate and re-assess the p-value
   until the segment can be resolved as distinct from the threshold.

   It is safe to supply Chase operation with an arbitrarily high number of simulations as
   the current nominal p-value is tested against the threshold value every one million
   simulations.  If nominal p-value is significantly different from threshold, the analysis
   terminates.  (By default, 95% confidence limits are applied.  This can be changed using
   =--confidence=2= (or 3) for 99% (or 99.9%) limits.

#+Caption: Example command line for p-value refinement operation
#+BEGIN_SRC language=bash
    java edu.utah.camplab.app.SGSPValue \
      --segment <UUID from db> \
      --accumulator <host:port> \
      --json-dir <dirname of output files>
      --dbname <PI surname> \
      --effortName <specific study> \
      --environment <AWS or CHPC> \
      --IRB-id <CHPC assigned value> \
      --simscount 50000000 \
      --threshold <genome wide significant or suggestive cut-off> \
      --maxtime \
      --operation 1
#+END_SRC

Users and accounts differ according to environment.
- _At AWS_: An agent will be required to use *AWS Cognito* to create a user and password
  by which the agent will interact with the *WebApp*.  This browser-based application can
  be located at https://ppr-sgs.hci.utah.edu using the credentials generated at Cognito.
  (This web application is the only component for the SGS suite which is housed at the
  University of Utah.  It has been thoroughly vetted in terms of HIPAA relate concerns.)
  From this web application one may initiate =MultiAssess=, =Threshold=, =Chase= actions
  for the chosen investigator. There is no "dashboard" but the agent can see all currently
  running analyses via AWS Console.  The agent has direct access to the database only from
  the middleware server (an EC2 Linux instance).

- _At CHPC_: Investigators will be required to attain appropriate accounts, disc space and
  CPU allocation in the Protected Environment (CHPC-PE).  Running analyses on the CHPC
  compute hardware is managed via the *SLURM* job management system.  Investigator will
  generate "slurm" files, which are essentially bash scripts with specifically formatted
  comments[fn:16].  These file are submitted to the general queue for processing.  There
  will be two distinct types of slurm files: one type for =MultiAssess= jobs and one for
  =Chase= operations.  The former will generally make efficient use of the assigned
  machine, while the latter will not.  For the =Chase= operation, one may either
  self-manage a queue of segments or send a single segment and have it use all available
  cores.  T requesting a single core of the machine is also theoretically
  possible.[fn:17] The Web application referenced in the AWS sections *is not* used when
  running SGS within CHCP/PE.

  - *Adding a project*:
    - edit /uufs/chcp.utah.edu/sys/installdir/sgspub/SGS_TC_ENV; extent ACTIVE_PROJ line
    - add PI db to pgbouncer
    - add project role to pgbouncer/userlist.txt

  - *Starting/stop middleware*: 
    - systemctl restart pgbouncer (after adding new db/study)
    - systemctl restart campselect

* Ancillaries
** TrueForward
A /camplab/ exclusive, this small suite calculates which loci of a given micro-array chip
technology need to be "flipped" to the other strand in order to agree with sequencing data
over the same genomic region.  The analysis requires genotype data from *both* sequencing
and the array at hand for a common set of individuals.  As few as 88 individuals have been
used but the recommendation of to have at least 100.  Markers are rejected when there is
less than 95% concordance (or discordance as the case may be).  Obviously with too few
samples, a single genotyping error can cause a marker to be rejected.  Typical lossage
rates are on the order of 1% per chip.  Recall that SGS uses only loci which have "RS" ids
and that the SGS database understands marker definitions for builds 37 and 38.  Among the
known technologies are:
 - Illumina GSA version 2 and 3
 - Human610-Quadv1_H
 - HumanOmniExpress-24v1-[01]_A
 - OmniExpressExome-8v1-1_B
 - OncoArray500Kv1
 - PSY-24v1 version 0 and 1
 - HumanOmniExpressExome-8v1-2
 - Human1M-Duov3_H
The genotyping data from TrueForward has been found to do very, very well at /topmed/[fn:18]

** MonitorServlet
This servlet within the SGS middleware will show an assortment of current runtime
configuration values.  Accessed via a browser at https://sgshost:port/sgs/webmonitor a
simple text presents as follows
#+Caption: Example web page
#+begin_src language=bash
SGS_OK_WEB using db localhost
Time now is 2022-11-30T14:48:46.161603648
edu.utah.camplab.servlet.MonitorServlet
databaseHost==> localhost
ver4024 ==> database
v24-ver4024 ==> project v24,   url = jdbc:postgresql://localhost:5432/ver4024?prepareThreshold=0
chpc-ver4024 ==> project chpc, url = jdbc:postgresql://localhost:5432/ver4024?prepareThreshold=0
sgsdb ==> jndi context
jdbc ==> jndi context
roleExtension==> _notnull
expansionStep==> 5
databasePort==> 5432
#+end_src

* Footnotes

[fn:1] Shared Genomic Segment Analysis. Mapping Disease Predisposition Genes in Extended
Pedigrees Using SNP Genotype Assays. Annals of Human Genetics. A. Thomas et
al. https://onlinelibrary.wiley.com/doi/full/10.1111/j.1469-1809.2007.00406.x

[fn:2]  Building SGS from git clone depends having the keys to access two components, jpsgcs
and longpowerset, each of which can be cloned from their repositories and built using gradle.

[fn:3] Here-to-fore un-seen chips may be analyzed (*TrueForward*) under separate cover.

[fn:4] We use "agent" for any actor responsible for any portion of SGS execution.  At
AWS, a PPR affiliate.  At CHPC it may be any of collaborating PI, PI designate, *camplab*
member.

[fn:5] Utah Genome Project (https://uofuhealth.utah.edu/center-genomic-medicine/research/utah-genome-project), Thousand Genome Project (https://www.internationalgenome.org)

[fn:6] Whole Exome Sequencing (WES) does not perform well.

[fn:7] We're currently recommending the GSAv3 from Illumina

[fn:8]Suitable control genotype data should have at least 100 samples which are
sufficiently unrelated. It will be necessary for the software to be able to distinguish
between the case and control data.

[fn:9] https://www.jurgott.org/linkage/LinkageUser.pdf. See especially Chapter 2.  In a
nutshell, the ".ped" file has the pedigree structure and the genotypes while the .par file
has the definition of each loci and the corresponding order of the loci in the .ped file.
SGS uses a special .par file called an ".ld" file.  This is a complete .par file extended
with data describing the linkage disequilibrium across the chromosome.

[fn:10] The convention for the project specific name is the principal investigator's
surname, underscore, projectCode.  It is strongly recommended that the project code be short and
arbitrary but it must begin with a letter.  The project code will also be a role (account)
in the db server.

[fn:11] 
_At AWS_: all the files/scripts/jars are available on s3://691459864434-sgs-source/<release>.tar
_At CHPC_: /uufs/chcp.utah.edu/sys/installdir/sgspub

[fn:12] A minimum and maximum kinship coefficients are produced for each subset of cases
with more than two members.  (For 2-member probandset the minimum is equal to the maximum
and is exact.)

[fn:13] This requires the AWS command line tool be installed on the local (non-aws) machine

[fn:14] The hard 3-day time limit on general cluster machines make them nearly useless for
this process.  FitGMLD is single threaded which makes running all 22 chromosomes on one
machine perfectly acceptable.  It might be possible to use the anticipated middleware
machine, with two caveats: not while any SGS analysis is in progress; permission per
collaborator might be required.  The owner-nodes (e.g. camp-rw and doherty-rw) are
appropriate for this work only because no known FitGMLD run has to date taken more the 14
days.  This limit would likely be exceeded (on current hardware) by either having in
excess of 100,000 loci or more than 200 controls.  Control data here is a set of at least
100 unrelated samples using the exact set of loci as the study.  These LINKAGE files are
created as part of the file preparation work.

[fn:15] _At AWS_: db.m5.xlarge.  _At CHPC_ (csgsdb.chpc.utah.edu): 196G ram, 8 cores

[fn:16] https://slurm.schedmd.com/documentation.html

[fn:17] A discussion with CHPC on this point is likely in order

[fn:18]TOPMed Imputation Server https://imputation.biodatacatalyst.nhlbi.nih.gov/

[fn:19] This location may change

[fn:20] Likely a broad-spectrum account would be generated for each agent.  This may more
appropriately addressed in the discussion of the AWS infrastructure.
